<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */

namespace backend\components\controllers;

class BackController extends \yii\web\Controller {

    public $enableCsrfValidation = false;
    public $layout;
//	public $breadcrumbs = array();
    public $description;
    public $keywords;
    public $menu = array();
    public $main_menu = array();
    public $user_menu = array();
    public $data = [];

    public function behaviors() {
        return [
            'access' => [
                'class' => \yii2mod\rbac\components\AccessControl::className(),
                'rules' => [
                    [
//                        'allow' => true,
///                        'roles' => ['@'], // залогинен
//                        'roles' => ['?'], // все
//                        'roles' => ['admin-panel-access'],
                    // 'matchCallback' => function ($rule, $action) {return in_array(\Yii::$app->user->identity->username, ['chuzzlik', 'admin']);}
                    ],
//					[
//						'actions' => ['create'],
//						'roles' => ['?'],
//						'allow' => true,
//					],
                ]
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

#	public $layout = '//layouts/column2';

    public function __construct($id, $module, $config = array()) {
        // .. начальная инициализация перед назначением настроек
        parent::__construct($id, $module, $config);
    }

    public function init() {
        parent::init();

        $this->main_menu = array(
            ['label' => '<i class="shortcut-icon fa fa-power-off"></i> Dashboard', 'url' => ['/']],
            ['label' => '', 'options' => array('class' => 'divider')],
            ['label' => '<i class="shortcut-icon fa fa-folder-open"></i> Категории', 'url' => array('/category')],
            ['label' => '<i class="shortcut-icon fa fa-globe"></i> Поселки', 'url' => array('/village')],
            ['label' => '<i class="shortcut-icon fa fa-globe"></i> Поселки - типовые проекты', 'url' => array('/village-townhouse')],
            ['label' => '', 'options' => ['class' => 'divider']],
            ['label' => '<i class="shortcut-icon fa fa-globe"></i> Частные объявления', 'url' => array('/realt')],
            ['label' => '', 'options' => ['class' => 'divider']],
            ['label' => '<i class="shortcut-icon fa fa-file-text"></i> Информация', 'url' => array('/information')],
            ['label' => '<i class="shortcut-icon fa fa-picture-o"></i> Баннеры', 'url' => array('/banner')],
            ['label' => '<i class="shortcut-icon fa fa-envelope"></i> Шаблоны писем', 'url' => ['/email-template']],
            ['label' => '', 'options' => ['class' => 'divider']],
            ['label' => '<i class="shortcut-icon fa fa-envelope"></i> Теги', 'url' => ['/field-tag']],
            ['label' => '', 'options' => ['class' => 'divider']],
            ['label' => '<i class="shortcut-icon fa fa-wrench"></i> Seo+', 'url' => ['seo/index']],
            ['label' => '<i class="shortcut-icon fa fa-wrench"></i> Настройки', 'url' => ['/settings']],
            ['label' => '', 'options' => ['class' => 'divider']],
            ['label' => '<i class="shortcut-icon fa fa-user"></i> Обратная связь', 'url' => ['/feedback']],
            [
                'label' => '<i class="shortcut-icon fa fa-unlock"></i> Заявки на просмотр',
                'linkOptions' => [
                ],
                'items' => [
                    ['label' => '<i class="shortcut-icon fa fa-file-text"></i> Поселки', 'url' => ['/village-view-request']],
                    ['label' => '<i class="shortcut-icon fa fa-file-text"></i> Частные объявления', 'url' => ['/realt-view-request']],
                ],
            ],
            ['label' => '', 'options' => ['class' => 'divider']],
            ['label' => '<i class="shortcut-icon fa fa-user"></i> Пользователи', 'url' => ['/user']],
            [
                'label' => '<i class="shortcut-icon fa fa-unlock"></i> Права',
                'url' => ['/rbac'],
                'linkOptions' => [
                ],
                'items' => [
                    ['label' => '<i class="shortcut-icon fa fa-file-text"></i> Пользователи', 'url' => ['/rbac/assignment']],
                    ['label' => '<i class="shortcut-icon fa fa-file-text"></i> Роли', 'url' => ['/rbac/role']],
                    ['label' => '<i class="shortcut-icon fa fa-file-text"></i> Разрешения', 'url' => ['/rbac/permission']],
                    ['label' => '<i class="shortcut-icon fa fa-file-text"></i> Route', 'url' => ['/rbac/route']],
                    ['label' => '<i class="shortcut-icon fa fa-file-text"></i> Правила', 'url' => ['/rbac/rule']],
                ],
            ],
        );


        $this->user_menu = array();
        if (!\Yii::$app->user->isGuest) {
            $this->user_menu = array(
                ['url' => '/', 'label' => 'Перейти к сайту', 'linkOptions' => array('target' => '_blank')],
                [
                    'label' => "Выход",
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
            );
        }
    }

    public function afterAction($action, $result) {
        \Yii::$app->getUser()->setReturnUrl(\Yii::$app->request->url);
        return parent::afterAction($action, $result);
    }

    /**
     * переопределяем, чтобы переменные страницы были доступны в макете
     */
    public function render($view, $params = []) {
        $params = array_merge($this->data, $params);

        $output = $this->getView()->render($view, $params, $this);
        $layoutFile = $this->findLayoutFile($this->getView());
        if ($layoutFile !== false) {
            $params['content'] = $output;
            return $this->getView()->renderFile($layoutFile, $params, $this);
        } else {
            return $output;
        }
    }

}
