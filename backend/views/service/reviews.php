<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use dosamigos\ckeditor\CKEditorInline;
use kartik\widgets\Select2;
use yii\web\JsExpression;

$this->title = "Reviews";
$this->params['breadcrumbs'][] = ['label' => 'Service', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<form method="post">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            from: &nbsp;&nbsp;&nbsp;&nbsp;
            <?php if (!empty($companies_from)) { ?>
                <b>
                    <?= implode(", ", ArrayHelper::map($companies_from, "id", "title")) ?></b>
            <?php } ?>
            <?=
            Select2::widget([
                'name' => 'ReviewsMove[companies_id_from]',
                'value' => ArrayHelper::getValue($ReviewsMove, 'companies_id_from'),
                'pluginOptions' => [
                    'multiple' => true,
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['/company/autocomplete']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(term,page) { return {q:term}; }'),
                        'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                    ],
                ],
            ]);
            ?>
            <br>
            to &nbsp;&nbsp;&nbsp;&nbsp; <b><?= !empty($company_to) ? $company_to->title : "" ?></b>
            <?=
            Select2::widget([
                'name' => 'ReviewsMove[company_id_to]',
                'value' => ArrayHelper::getValue($ReviewsMove, 'company_id_to'),
                'pluginOptions' => [
                    'multiple' => false,
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['/company/autocomplete']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(term,page) { return {q:term}; }'),
                        'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                    ],
                ],
            ]);
            ?>
            <br>
            <?= Html::submitButton("Переместить", ['name' => 'action', 'class' => 'btn btn-default', 'value' => 'reviews-move']) ?>
        </div>
    </div>
</form>