<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use dosamigos\ckeditor\CKEditorInline;
use kartik\widgets\Select2;

$this->title = "Cache";
$this->params['breadcrumbs'][] = ['label' => 'Service', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<form method="post">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <table class="table table-bordered">
                <?php
                $total = 0;
                foreach ($categories as $category) {
                    $total += $category['size'];
                    ?>
                    <tr>
                        <td><?= $category['path'] ?></td>
                        <td><?= $category['size'] ?>Mb</td>
                    </tr>
                <?php } ?>
                <tr>
                    <td></td>
                    <td><?= $total ?>Mb</td>
                </tr>
            </table>
            <?= Html::submitButton("Посчитать", ['name' => 'action', 'class' => 'btn btn-default', 'value' => 'calc-cache-space'])?>
            <?= Html::submitButton("Очистить", ['name' => 'action', 'class' => 'btn btn-default', 'value' => 'clear-cache'])?>
        </div>
    </div>
</form>