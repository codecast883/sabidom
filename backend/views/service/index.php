<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use dosamigos\ckeditor\CKEditorInline;
use kartik\widgets\Select2;
use yii\web\JsExpression;

$this->title = "Service";
$this->params['breadcrumbs'][] = $this->title;
?>

<form method="post">
    <div class="row">
        <div class="col-xs-12">
            <?= Html::a("Перемещение отзывов", ['service/reviews']) ?><br>
            <?= Html::a("cache", ['service/cache']) ?><br>
            <?= Html::a("Восстановить отсутствующие правила Seo+", ['seo/fix-all']) ?>
        </div>
    </div>
</form>