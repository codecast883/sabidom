<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;
use kartik\widgets\ActiveForm;

;

$this->title = 'Обратная связь';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="feedback-index">
<?php // echo $this->render('_search', ['model' => $searchModel]);   ?>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'checkboxColumn' => TRUE,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['class' => 'col-id'],
            ],
            'created_at',
            'name',
            'email:email',
            'phone',
            ['class' => 'common\owerride\yii\grid\ActionColumn',],
        ],
    ]);
    ?>

    <div class='btn-group pull-left'>

        <?=
        Html::submitButton('Удалить', [
            'class' => 'btn btn-danger btn-xs',
            'onclick' => "
                        if(confirm('Действительно удалить?')){
                            $('#list-form').prop('action', '" . yii\helpers\Url::toRoute(['delete', 'id' => 0, 'get_params' => Yii::$app->request->get()]) . "');
                            $('#list-form').submit();
                }
                "])
        ?>
    </div>    

</div>
