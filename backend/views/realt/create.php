<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Realt */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Realts', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="realt-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
