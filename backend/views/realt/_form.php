<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;
use common\models\Realt;
use common\models\Field;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Realt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="realt-form">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
        <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
    </div>
    <div class="clearfix"></div><br>
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#common" role="tab" data-toggle="tab">Общая информация</a></li>

            <li><a href="#params" role="tab" data-toggle="tab">Опции</a></li>

        <?php if(Yii::$app->controller->route == 'realt/update'):?>
            <li><a href="#images" role="tab" data-toggle="tab">Изображения</a></li>
        <?php endif;?>


        <li><a href="#seo" role="tab" data-toggle="tab">SEO</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="common">
            <?= $form->field($model, 'title')->textInput() ?>
            <?= $form->field($model, 'type_id')->dropDownList(Realt::itemAlias('type_id')) ?>

            <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'village_id')->textInput() ?>
            <?= $form->field($model, 'price_service')->textInput() ?>

            <?= $form->field($model, 'stead_area')->textInput() ?>
            <?= $form->field($model, 'house_area')->textInput() ?>

            <?= $form->field($model, 'price')->textInput() ?>
            <?= $form->field($model, 'agent_name')->textInput(['maxlength' => 512]) ?>
            <?= $form->field($model, 'phone')->textInput() ?>

            <hr>

            <?= $form->field($model, 'coord_lng')->textInput() ?>
            <?= $form->field($model, 'coord_lat')->textInput() ?>
            <hr>

            <?= $form->field($model, 'description')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>
            <?= $form->field($model, 'description_bottom')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>
        </div>
        <div class="tab-pane fade" id="params">

            <?php foreach (common\models\FieldGroup::find()->where(['id' => [1, 2]])->all() as $field_group) { ?>

                <h3><?= $field_group->title ?></h3>

                <?php foreach ($field_group->getFields()->orderBy(['title' => SORT_ASC])->all() as $field) { ?>
                    <div class="form-group">
                        <label class="col-xs-2 control-label"><?= $field->title ?></label>
                        <div class="col-xs-10">
                            <?php if ($field->type_id == Field::TYPE_TEXT) { ?>
                                <?= Html::textInput('Realt[field_values][' . $field->id . '][]', implode(", ", ArrayHelper::getColumn($model->fieldValues($field->id), 'value')), ['class' => 'form-control']) ?>
                            <?php } elseif ($field->type_id == Field::TYPE_LIST) { ?>
                                <?=
                                Select2::widget([
                                    'name' => 'Realt[field_values][' . $field->id . '][]',
                                    'data' => ArrayHelper::map($field->fieldTags, 'id', 'value'),
                                    'size' => Select2::SMALL,
                                    'value' => ArrayHelper::getColumn($model->fieldValues($field->id), 'field_tag_id'),
                                    'options' => ['placeholder' => 'Выбрать...', 'multiple' => true],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                                ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
<!--            --><?php //foreach ($model->getFields(NULL, 1) as $field) { ?>
<!--                --><?//= $field['info']->title ?><!--:  --><?//= $field['values_formated'] ?><!--<br>-->
<!--            --><?php //} ?>
<!--            --><?php //foreach ($model->getFields(NULL, 2) as $field) { ?>
<!--                --><?//= $field['info']->title ?><!--:  --><?//= $field['values_formated'] ?><!--<br>-->
<!--            --><?php //} ?>
        </div>
        <div class="tab-pane fade" id="seo">
            <?= $form->field($model, 'seo_title')->textInput(['maxlength' => 1024]) ?>
            <?= $form->field($model, 'seo_h1')->textInput(['maxlength' => 1024]) ?>
            <?= $form->field($model, 'seo_description')->textInput(['maxlength' => 1024]) ?>
            <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 1024]) ?>
        </div>


        <div class="tab-pane fade" id="images">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#image-upload">
                Загрузить
            </button><br><br>

            <?php \yii\widgets\Pjax::begin(['id' => 'sortable-images', 'enablePushState' => false, "linkSelector" => "none", "formSelector" => "none"]); ?>
            <?php
            foreach ($model->images as $image) {
                ?>
                <div class="sortable-images-item" style="">
                    <?= Html::img($image->getUploadedFileUrl('image')) ?>
                    <?= Html::hiddenInput("images_list[]", $image->id); ?>
                    <i class="fa fa-trash sortable-images-item-remove"></i>
                </div>
            <?php } ?>
            <script>
                $(function () {
                    $("#sortable-images").sortable();
                    $("#sortable-images").disableSelection();


                    $("#sortable-images .sortable-images-item-remove").on("click", function () {
                        $(this).parent().remove();
                    });

                });
            </script>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>


    </div>

    <?php ActiveForm::end(); ?>

</div>


<!-- Modal -->
<div class="modal fade" id="image-upload" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php \yii\widgets\Pjax::begin(['enablePushState' => false, "linkSelector" => "none", 'formSelector' => "#image-upload-form"]); ?>
            <?php
            $form = ActiveForm::begin([
                'id' => 'image-upload-form',
                'enableClientValidation' => false,
                'options' => ['enctype' => 'multipart/form-data'],
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY,
                ]]);
            ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Загрузить изображение</h4>
            </div>
            <div class="modal-body">
                <div class="city-form">
                    <?= Html::hiddenInput("current_action", "upload_image"); ?>
                    <?= Html::hiddenInput("RealtImage[realt_id]", $model->id); ?>
                    <?= Html::hiddenInput("RealtImage[sort_order]", 1000); ?>
<!--                    --><?php //if (!empty($apm)) { ?>
                        <div class="bg-info" style="padding: 5px; margin-bottom: 10px;">
<!--                            --><?//= $apm ?>
<!--                            --><?// echo '123124124124'; ?>

                        </div>
                        <script>
                            $(function () {
                                $.pjax.reload('#sortable-images');
                            });
                        </script>
<!--                    --><?php //} ?>
                    <div class="form-group">
                        <label class="control-label col-xs-2">url</label>
                        <div class="col-xs-10">
                            <?= Html::textInput("RealtImage[image_load_url]", NULL, ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-2">file</label>
                        <div class="col-xs-10">
                            <?= Html::fileInput("RealtImage[image]") ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Загрузить</button>
            </div>
            <?php ActiveForm::end(); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
