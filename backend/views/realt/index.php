<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;
use kartik\widgets\ActiveForm;

;

$this->title = 'Частные объявления';
$this->params['breadcrumbs'][] = $this->title;


$this->params['menu'] = [
    Html::a('Создать', ['create'], ['class' => 'btn btn-success'])
];
?>
<div class="realt-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'checkboxColumn' => TRUE,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['class' => 'col-id'],
            ],
            'title:ntext',
            [
                'format' => 'raw',
                'attribute' => 'type_id',
                'filter' => "",
                'value' => function ($model, $index, $widget) {
                    return \common\models\Realt::itemAlias('type_id', $model->type_id);
                },
                'options' => ['class' => 'col-xs-1'],
            ],
            // 'address:ntext',
            // 'price',
            // 'date',
            // 'type_id',
            // 'description:ntext',
            // 'phone:ntext',
            // 'subtitle:ntext',
            // 'local_id',
            // 'seller:ntext',
            // 'src_url:url',
            // 'src_html:ntext',
            // 'coord_lat',
            // 'coord_lng',
            // 'views',
            // 'premium',
            // 'stead_area',
            // 'house_area',
            // 'agent_name',
            // 'village_id',
            // 'description_bottom:ntext',
            // 'seo_title',
            // 'seo_h1',
            // 'seo_description',
            // 'seo_keywords',
            ['class' => 'common\owerride\yii\grid\ActionColumn',],
        ],
    ]);
    ?>

    <div class='btn-group pull-left'>

        <?=
        Html::submitButton('Удалить', [
            'class' => 'btn btn-danger btn-xs',
            'onclick' => "
                        if(confirm('Действительно удалить?')){
                            $('#list-form').prop('action', '" . yii\helpers\Url::toRoute(['delete', 'id' => 0, 'get_params' => Yii::$app->request->get()]) . "');
                            $('#list-form').submit();
                }
                "])
        ?>
    </div>    

</div>
