<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Realt */

$this->title = 'Изменить: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Realts', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="realt-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
