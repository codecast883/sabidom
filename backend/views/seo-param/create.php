<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SeoParam */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Seo Params', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="seo-param-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
