<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SeoParam */

$this->title = 'Изменить: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Seo Params', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="seo-param-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
