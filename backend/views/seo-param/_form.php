<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\SeoParam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seo-param-form">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
        <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
    </div>
    <div class="clearfix"></div><br>

    <?= $form->field($model, 'field')->textInput(['maxlength' => 128]) ?>
    <?= $form->field($model, 'alias')->textInput(['maxlength' => 128]) ?>
    <?= $form->field($model, 'template')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'field_type')->textInput(['maxlength' => 128]) ?>
    <?= $form->field($model, 'sort_order')->textInput() ?>
    <?= $form->field($model, 'seo_id')->textInput() ?>

    <?php ActiveForm::end(); ?>

</div>
