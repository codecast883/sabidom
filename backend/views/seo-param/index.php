<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;
use kartik\widgets\ActiveForm;;


$this->title = 'Seo Params';
$this->params['breadcrumbs'][] = $this->title;
    

$this->params['menu'] = [
        Html::a('Создать', ['create'], ['class' => 'btn btn-success'])
];



?>
<div class="seo-param-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'checkboxColumn' => TRUE,
        'columns' => [
            [
              'attribute' => 'id',
              'options' => ['class' => 'col-id'],
            ],

            'id',
            'field',
            'template:ntext',
            'field_type',
            'sort_order',
            // 'seo_id',
            // 'created_at',
            // 'updated_at',

            ['class' => 'common\owerride\yii\grid\ActionColumn',],
        ],
    ]); ?>
    
    <div class='btn-group pull-left'>

        <?= Html::submitButton('Удалить', [
            'class' => 'btn btn-danger btn-xs',
            'onclick' => "
                        if(confirm('Действительно удалить?')){
                            $('#list-form').prop('action', '" . yii\helpers\Url::toRoute(['delete', 'id' => 0, 'get_params' => Yii::$app->request->get()]) . "');
                            $('#list-form').submit();
                }
                "])
        ?>
    </div>    

</div>
