<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VillageTownhouse */

$this->title = 'Изменить: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Типовой проект', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="village-townhouse-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
