<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VillageTownhouse */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Village Townhouses', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="village-townhouse-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
