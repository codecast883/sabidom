<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\VillageTownhouse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="village-townhouse-form">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
        <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
    </div>
    <div class="clearfix"></div><br>

    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#common" role="tab" data-toggle="tab">Общая информация</a></li>
        <?php if (!$model->isNewRecord) { ?>
            <li><a href="#images" role="tab" data-toggle="tab">Интерьер</a></li>
            <li><a href="#params" role="tab" data-toggle="tab">Опции</a></li>
        <?php } ?>
        <li><a href="#seo" role="tab" data-toggle="tab">SEO</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="common">
            <?= $form->field($model, 'title')->textInput() ?>
            <?= $form->field($model, 'village_id')->textInput() ?>

            <?= $form->field($model, 'description')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>
            <?= $form->field($model, 'slug')->textInput(['maxlength' => 256]) ?>
            <?= $form->field($model, 'price_from')->textInput() ?>
            <?= $form->field($model, 'house_area')->textInput() ?>
            <?= $form->field($model, 'stead_area')->textInput() ?>
            <?= $form->field($model, 'number_of_floors')->textInput() ?>
            <?= $form->field($model, 'type')->textInput() ?>
            <?= $form->field($model, 'phone')->textInput() ?>
            <?= $form->field($model, 'price_for_hundred')->textInput() ?>

            <?=
            $form->field($model, 'image')->widget(\mihaildev\fileupload\ImageUploadWidget::className(), [
                'imageOptions' => ['width' => '200'],
                'imageUrl' => $model->getUploadedFileUrl('image')
                    ]
            )
            ?>
            <?=
            $form->field($model, 'layout')->widget(\mihaildev\fileupload\ImageUploadWidget::className(), [
                'imageOptions' => ['width' => '200'],
                'imageUrl' => $model->getUploadedFileUrl('layout')
                    ]
            )
            ?>
            <?=
            $form->field($model, '3d')->widget(\mihaildev\fileupload\FileUploadWidget::className(), [
                'fileUrl' => $model->getUploadedFileUrl('3d')
                    ]
            )
            ?>
        </div>
        <div class="tab-pane fade" id="params">
            <?php foreach ($model->getFields(NULL, 3) as $field) { ?>
                <?= $field['info']->title ?>:  <?= $field['values_formated'] ?><br>
            <?php } ?>
            <?php foreach ($model->getFields(NULL, 4) as $field) { ?>
                <?= $field['info']->title ?>:  <?= $field['values_formated'] ?><br>
            <?php } ?>
            <?php foreach ($model->getFields(NULL, 5) as $field) { ?>
                <?= $field['info']->title ?>:  <?= $field['values_formated'] ?><br>
            <?php } ?>
            <?php foreach ($model->getFields(NULL, 6) as $field) { ?>
                <?= $field['info']->title ?>:  <?= $field['values_formated'] ?><br>
            <?php } ?>
        </div>
        <div class="tab-pane fade" id="seo">
            <?= $form->field($model, 'seo_title')->textInput(['maxlength' => 1024]) ?>
            <?= $form->field($model, 'seo_h1')->textInput(['maxlength' => 1024]) ?>
            <?= $form->field($model, 'seo_description')->textInput(['maxlength' => 1024]) ?>
            <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 1024]) ?>
            <?= $form->field($model, 'seo_breadcrumb')->textInput(['maxlength' => 1024]) ?>
        </div>
        <div class="tab-pane fade" id="images">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#image-upload">
                Загрузить
            </button><br><br>

            <?php \yii\widgets\Pjax::begin(['id' => 'sortable-images', 'enablePushState' => false, "linkSelector" => "none", "formSelector" => "none"]); ?>
            <?php

            foreach ($model->images as $image) {
                ?>
                <div class="sortable-images-item" style="">
                    <?= Html::img($image->getUploadedFileUrl('image')) ?>
                    <?= Html::hiddenInput("images_list[]", $image->id); ?>
                    <i class="fa fa-trash sortable-images-item-remove"></i>
                </div>
            <?php } ?>
            <script>
                $(function () {
                    $("#sortable-images").sortable();
                    $("#sortable-images").disableSelection();


                    $("#sortable-images .sortable-images-item-remove").on("click", function () {
                        $(this).parent().remove();
                    });

                });
            </script>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<!-- Modal -->
<div class="modal fade" id="image-upload" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php \yii\widgets\Pjax::begin(['enablePushState' => false, "linkSelector" => "none", 'formSelector' => "#image-upload-form"]); ?>
            <?php
            $form = ActiveForm::begin([
                        'id' => 'image-upload-form',
                        'enableClientValidation' => false,
                        'options' => ['enctype' => 'multipart/form-data'],
                        'type' => ActiveForm::TYPE_HORIZONTAL,
                        'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY,
            ]]);
            ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Загрузить изображение</h4>
            </div>
            <div class="modal-body">
                <div class="city-form">
                    <?= Html::hiddenInput("current_action", "upload_image"); ?>
                    <?= Html::hiddenInput("VillageTownhouseImage[village_townhouse_id]", $model->id); ?>
                    <?= Html::hiddenInput("VillageTownhouseImage[sort_order]", 1000); ?>

                    <script>
                        $(function () {
                            $.pjax.reload('#sortable-images');
                        });
                    </script>

                    <?php if (!empty($upload_image_message)) { ?>
                        <div class="bg-info" style="padding: 5px; margin-bottom: 10px;">
                            <?= $upload_image_message ?>
                        </div>

                    <?php } ?>
                    <div class="form-group">
                        <label class="control-label col-xs-2">url</label>
                        <div class="col-xs-10">
                            <?= Html::textInput("VillageTownhouseImage[image_load_url]", NULL, ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-2">file</label>
                        <div class="col-xs-10">
                            <?= Html::fileInput("VillageTownhouseImage[image]") ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Загрузить</button>
            </div>
            <?php ActiveForm::end(); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>