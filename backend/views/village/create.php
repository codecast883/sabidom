<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Village */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Villages', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="village-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
