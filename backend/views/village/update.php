<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Village */

$this->title = 'Изменить: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Villages', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="village-update">
    <?= $this->render('_form', [
        'model' => $model,
        'upload_image_message' => $upload_image_message,
        'upload_progress_image_message' => $upload_progress_image_message,
        'upload_document_message' => $upload_document_message,
    ]) ?>

</div>
