<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
use common\models\Field;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Village */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="village-form">
	<?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
		<?php if (!$model->isNewRecord) { ?>
			<?= Html::a('Посмотреть', $model->getUrl(), ['target' => '_blank', 'class' => 'btn btn-default']) ?>
		<?php } ?>
		<?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
    </div>
    <div class="clearfix"></div><br>
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#common" role="tab" data-toggle="tab">Общая информация</a></li>
		<?php if (!$model->isNewRecord) { ?>
			<li><a href="#images" role="tab" data-toggle="tab">Изображения</a></li>
			<li><a href="#route" role="tab" data-toggle="tab">Как добраться</a></li>
			<li><a href="#documents" role="tab" data-toggle="tab">Документы</a></li>
			<li><a href="#images-progress" role="tab" data-toggle="tab">Ход строительства</a></li>
			<li><a href="#params" role="tab" data-toggle="tab">Опции</a></li>
			<li><a href="#owner" role="tab" data-toggle="tab">Владелец</a></li>
		<?php } ?>
        <li><a href="#seo" role="tab" data-toggle="tab">SEO</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane fade in" id="owner">
			<?= $form->field($model, 'phone')->textInput() ?>
			<?= $form->field($model, 'email_for_notifications')->textInput()->hint("Email для отправки сообщений. Можно указать несколько через запятую"); ?>
			<?= $form->field($model, 'email_for_notifications_notify')->widget(SwitchInput::classname(), []); ?>
        </div>
        <div class="tab-pane fade in" id="route">
			<?= $form->field($model, 'address')->textarea() ?>
			<?= $form->field($model, 'route_explain_auto')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>
			<?= $form->field($model, 'route_explain_public')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>
        </div>
        <div class="tab-pane fade in active" id="common">
			<?= $form->field($model, 'title')->textInput() ?>
			<?= $form->field($model, 'subtitle')->textInput() ?>


			<?= $form->field($model, 'premium')->widget(SwitchInput::classname(), []); ?>
			<?= $form->field($model, 'on_top')->widget(SwitchInput::classname(), []); ?>
			<?= $form->field($model, 'on_home')->widget(SwitchInput::classname(), []); ?>
			<?= $form->field($model, 'sort_order')->textInput(['maxlength' => 10]) ?>



			<?= $form->field($model, 'description')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>
			<?= $form->field($model, 'description_price')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>
			<?= $form->field($model, 'description_photo')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>
			<?= $form->field($model, 'coord_lat')->textInput(['maxlength' => 10]) ?>
			<?= $form->field($model, 'coord_lng')->textInput(['maxlength' => 10]) ?>
			<?=
			$form->field($model, 'master_plan')->widget(\mihaildev\fileupload\ImageUploadWidget::className(), [
				'imageOptions' => ['width' => '200'],
				'imageUrl' => $model->getUploadedFileUrl('master_plan')
					]
			)
			?>
        </div>
        <div class="tab-pane fade" id="params">
            <?= $form->field($model, 'price_from')->textInput() ?>
            <?= $form->field($model, 'price_to')->textInput() ?>
            <?= $form->field($model, 'price_for_hundred')->textInput() ?>
            <?= $form->field($model, 'house_area_from')->textInput() ?>
            <?= $form->field($model, 'house_area_to')->textInput() ?>
            <?= $form->field($model, 'village_square')->textInput() ?>
            <?= $form->field($model, 'stead_area_from')->textInput() ?>
            <?= $form->field($model, 'stead_area_to')->textInput() ?>


			<?php foreach (common\models\FieldGroup::find()->where(['id' => [1, 2]])->all() as $field_group) { ?>
				<h3><?= $field_group->title ?></h3>
				<?php foreach ($field_group->getFields()->orderBy(['title' => SORT_ASC])->all() as $field) { ?>
					<div class="form-group">
						<label class="col-xs-2 control-label"><?= $field->title ?></label>
						<div class="col-xs-10">
							<?php if ($field->type_id == Field::TYPE_TEXT) { ?>
								<?= Html::textInput('Village[field_values][' . $field->id . '][]', implode(", ", ArrayHelper::getColumn($model->fieldValues($field->id), 'value')), ['class' => 'form-control']) ?>
							<?php } elseif ($field->type_id == Field::TYPE_LIST) { ?>						
								<?=
								Select2::widget([
									'name' => 'Village[field_values][' . $field->id . '][]',
									'data' => ArrayHelper::map($field->fieldTags, 'id', 'value'),
									'size' => Select2::SMALL,
									'value' => ArrayHelper::getColumn($model->fieldValues($field->id), 'field_tag_id'),
									'options' => ['placeholder' => 'Выбрать...', 'multiple' => true],
									'pluginOptions' => [
										'allowClear' => true
									],
								]);
								?>
							<?php } ?>
						</div>
					</div>
				<?php } ?>
			<?php } ?>
        </div>
        <div class="tab-pane fade" id="seo">
			<?= $form->field($model, 'slug')->textInput() ?>
			<?= $form->field($model, 'seo_title')->textInput(['maxlength' => 1024]) ?>
			<?= $form->field($model, 'seo_h1')->textInput(['maxlength' => 1024]) ?>
			<?= $form->field($model, 'seo_description')->textInput(['maxlength' => 1024]) ?>
			<?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 1024]) ?>
        </div>

        <div class="tab-pane fade" id="images">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#image-upload">
                Загрузить
            </button><br><br>   

			<?php \yii\widgets\Pjax::begin(['id' => 'sortable-images', 'enablePushState' => false, "linkSelector" => "none", "formSelector" => "none"]); ?>
			<?php
			foreach ($model->images as $image) {
				?>
				<div class="sortable-images-item" style="">
					<?= Html::img($image->getUploadedFileUrl('image')) ?> 
					<?= Html::hiddenInput("images_list[]", $image->id); ?>
					<i class="fa fa-trash sortable-images-item-remove"></i>
				</div>
			<?php } ?>
            <script>
				$(function () {
					$("#sortable-images").sortable();
					$("#sortable-images").disableSelection();


					$("#sortable-images .sortable-images-item-remove").on("click", function () {
						$(this).parent().remove();
					});

				});
            </script>
			<?php \yii\widgets\Pjax::end(); ?>
        </div>
        <div class="tab-pane fade" id="images-progress">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#image-progress-upload">
                Загрузить
            </button><br><br>

			<?php \yii\widgets\Pjax::begin(['id' => 'sortable-progress-images', 'enablePushState' => false, "linkSelector" => "none", "formSelector" => "none"]); ?>
			<?php
			foreach ($model->progress_images as $image) {
				?>
				<div class="sortable-images-item" style="">
					<?= Html::img($image->getUploadedFileUrl('image')) ?> 
					<?= Html::hiddenInput("progress_images_list[]", $image->id); ?>
					<i class="fa fa-trash sortable-images-item-remove"></i>
				</div>
			<?php } ?>
            <script>
				$(function () {
					$("#sortable-progress-images").sortable();
					$("#sortable-progress-images").disableSelection();


					$("#sortable-progress-images .sortable-images-item-remove").on("click", function () {
						$(this).parent().remove();
					});

				});
            </script>
			<?php \yii\widgets\Pjax::end(); ?>
        </div>

        <div class="tab-pane fade" id="documents">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#document-upload">
                Загрузить
            </button><br><br>

            <div class="sortable-documents" style="">
				<?php \yii\widgets\Pjax::begin(['id' => 'sortable-documents', 'enablePushState' => false, "linkSelector" => "none", "formSelector" => "none"]); ?>
				<?php foreach ($model->documents as $document) { ?>
					<div class="sortable-documents-item row">
						<div class="col-xs-2">
							<p class="form-control-static"><?= Html::a($document->file, $document->getUploadedFileUrl('file')) ?></p>
						</div>
						<div class="col-xs-3">
							<?= Html::hiddenInput("documents_list[]", $document->id); ?>
							<?= Html::textInput("documents_list_title[$document->id]", $document->title, ['class' => 'form-control']); ?>
						</div>
						<div class="col-xs-1">
							<span class="btn btn-xs btn-danger sortable-documents-item-remove"><i class="fa fa-trash"></i></span>
						</div>
					</div>
				<?php } ?>
                <script>
					$(function () {
						$("#sortable-documents").sortable();
						$("#sortable-documents .sortable-documents-item-remove").on("click", function () {
							$(this).parent().parent().remove();
						});

					});
                </script>
				<?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>

    </div>
	<?php ActiveForm::end(); ?>

</div>

<!-- Modal -->
<div class="modal fade" id="image-upload" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<?php \yii\widgets\Pjax::begin(['enablePushState' => false, "linkSelector" => "none", 'formSelector' => "#image-upload-form"]); ?>
			<?php
			$form = ActiveForm::begin([
						'id' => 'image-upload-form',
						'enableClientValidation' => false,
						'options' => ['enctype' => 'multipart/form-data'],
						'type' => ActiveForm::TYPE_HORIZONTAL,
						'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY,
			]]);
			?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Загрузить изображение</h4>
            </div>
            <div class="modal-body">
                <div class="city-form">
					<?= Html::hiddenInput("current_action", "upload_image"); ?>
					<?= Html::hiddenInput("VillageImage[village_id]", $model->id); ?>
					<?= Html::hiddenInput("VillageImage[sort_order]", 1000); ?>
					<?php if (!empty($upload_image_message)) { ?>
						<div class="bg-info" style="padding: 5px; margin-bottom: 10px;">
							<?= $upload_image_message ?>
						</div>
						<script>
							$(function () {
								$.pjax.reload('#sortable-images');
							});
						</script>
					<?php } ?>
					<div class="form-group">
						<label class="control-label col-xs-2">url</label>
						<div class="col-xs-10">
							<?= Html::textInput("VillageImage[image_load_url]", NULL, ['class' => 'form-control']); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-2">file</label>
						<div class="col-xs-10">
							<?= Html::fileInput("VillageImage[image]") ?>
						</div>
					</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Загрузить</button>
            </div>
			<?php ActiveForm::end(); ?>
			<?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
<div class="modal fade" id="image-progress-upload" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<?php \yii\widgets\Pjax::begin(['enablePushState' => false, "linkSelector" => "none", 'formSelector' => "#image-progress-upload-form"]); ?>
			<?php
			$form = ActiveForm::begin([
						'id' => 'image-progress-upload-form',
						'enableClientValidation' => false,
						'options' => ['enctype' => 'multipart/form-data'],
						'type' => ActiveForm::TYPE_HORIZONTAL,
						'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY,
			]]);
			?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Загрузить изображение</h4>
            </div>
            <div class="modal-body">
                <div class="city-form">
					<?= Html::hiddenInput("current_action", "upload_progress_image"); ?>
					<?= Html::hiddenInput("VillageProgress[village_id]", $model->id); ?>
					<?= Html::hiddenInput("VillageProgress[sort_order]", 0); ?>
						<?php if (!empty($upload_progress_image_message)) { ?>
						<div class="bg-info" style="padding: 5px; margin-bottom: 10px;">
							<?= $upload_progress_image_message ?>
						</div>
						<script>
							$(function () {
								$.pjax.reload('#sortable-progress-images');
							});
						</script>
					<?php } ?>


					<div class="form-group">
						<label class="control-label col-xs-2">url</label>
						<div class="col-xs-10">
							<?= Html::textInput("VillageProgress[image_load_url]", NULL, ['class' => 'form-control']); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-2">file</label>
						<div class="col-xs-10">
							<?= Html::fileInput("VillageProgress[image]") ?>
						</div>
					</div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Загрузить</button>
            </div>
			<?php ActiveForm::end(); ?>
			<?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>

<div class="modal fade" id="document-upload" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<?php \yii\widgets\Pjax::begin(['enablePushState' => false, "linkSelector" => "none", 'formSelector' => "#document-upload-form"]); ?>
			<?php
			$form = ActiveForm::begin([
						'id' => 'document-upload-form',
						'enableClientValidation' => false,
						'options' => ['enctype' => 'multipart/form-data'],
						'type' => ActiveForm::TYPE_HORIZONTAL,
						'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY,
			]]);
			?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Загрузить документ</h4>
            </div>
            <div class="modal-body">
                <div class="city-form">
					<?= Html::hiddenInput("current_action", "upload_document"); ?>
					<?= Html::hiddenInput("VillageDocument[village_id]", $model->id); ?>
					<?= Html::hiddenInput("VillageDocument[sort_order]", 0); ?>
					<?php if (!empty($upload_document_message)) { ?>
						<div class="bg-info" style="padding: 5px; margin-bottom: 10px;">
							<?= $upload_document_message ?>
						</div>
						<script>
							$(function () {
								$.pjax.reload('#sortable-documents');
							});
						</script>
					<?php } ?>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Название</label>
                        </div>
                        <div class="col-xs-10">
							<?= Html::textInput("VillageDocument[title]", "", ['class' => 'form-control']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Файл</label>
                        </div>
                        <div class="col-xs-10">
							<?php //= Html::textInput("VillageDocument[file]", "", ['class' => 'form-control']) ?>
							<?= Html::fileInput("VillageDocument[file]") ?>
                        </div>
                    </div>


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-primary">Загрузить</button>
            </div>
			<?php ActiveForm::end(); ?>
			<?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
