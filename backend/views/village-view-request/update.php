<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;

$this->title = 'Изменить: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Заявки на просмотр поселков', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>

<div class="village-view-request-form">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
        <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
    </div>
    <div class="clearfix"></div><br>

    <?= $form->field($model, 'created_at')->textInput(['readonly' => 'readonly']) ?>
    <?php if ($model->village) { ?> 
        <div class="form-group">
            <label class="col-xs-2 control-label">Поселок</label>
            <div class="col-xs-10"><p class="form-control-static"><?= Html::a($model->village->title, ['village/update', 'id' => $model->village_id]) ?></p></div>
        </div>
    <?php } ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => 256]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => 256]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => 256]) ?>
    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
    <?php ActiveForm::end(); ?>

</div>
