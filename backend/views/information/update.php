<?php

use yii\helpers\Html;


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Информация', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="information-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
