<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Information */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Информация', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'] = [
	Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])
];
?>
<div class="information-view">
	<?=
	DetailView::widget([
		'model' => $model,
		'attributes' => [
			'title',
			'slug',
			'status',
			'seo_h1',
			'seo_title',
			'seo_description',
			'seo_keywords',
			'text:html',
		],
	])
	?>

</div>
