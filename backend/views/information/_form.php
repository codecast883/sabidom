<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Information */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="information-form">
	<?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>

	<div class="btn-group pull-right">
		<?php if(!$model->isNewRecord){ ?>
			<?= Html::a('Посмотреть', "/information/".$model->slug, ['target' => '_blank', 'class' => 'btn btn-default']) ?>
		<?php } ?>
		<?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
	</div>
	<div class="clearfix"></div>
	<ul class="nav nav-tabs" role="tablist">
		<li class="active"><a href="#common" role="tab" data-toggle="tab">Общая информация</a></li>
		<li><a href="#seo" role="tab" data-toggle="tab">SEO</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade in active" id="common">
			<?= $form->field($model, 'status')->widget(SwitchInput::classname(), []); ?>
			<?= $form->field($model, 'title')->textInput(['maxlength' => 1024]) ?>
			<?= $form->field($model, 'slug')->textInput(['maxlength' => 1024]) ?>
			<?= $form->field($model, 'text')->widget(CKEditor::className(), ['options' => ['rows' => 2], 'preset' => 'standart']); ?>
		</div>
		<div class="tab-pane fade" id="seo">
			<?= $form->field($model, 'seo_h1')->textInput(['maxlength' => 1024]) ?>
			<?= $form->field($model, 'seo_title')->textInput(['maxlength' => 1024]) ?>
			<?= $form->field($model, 'seo_description')->textInput(['maxlength' => 1024]) ?>
			<?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 1024]) ?>
		</div>
	</div>
	<?php ActiveForm::end(); ?>
</div>