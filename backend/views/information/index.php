<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;


$this->title = 'Информация';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'] = [
	Html::a('Создать', ['create'], ['class' => 'btn btn-success'])
];
?>
<div class="information-index">
	<?=
	GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			[
				'attribute' => 'id',
				'options' => ['class' => 'col-id'],
			],

			'title',
			'status' => [
				'attribute' => 'status',
				'value' => function ($model, $index, $widget) {
					return common\models\Information::itemAlias("status", $model->status);
				},
				'options' => ['class' => 'col-xs-1'],
			],
            ['class' => 'common\owerride\yii\grid\ActionColumn',],
		],
	]);
	?>

</div>
