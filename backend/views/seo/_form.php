<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Seo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seo-form">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
        <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
    </div>
    <div class="clearfix"></div><br>
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#common" role="tab" data-toggle="tab">Правила</a></li>
        <li><a href="#config" role="tab" data-toggle="tab">Config</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="common">
            <div class="form-group"><div class="col-xs-offset-2 col-xs-10"><div class="help-block"><?= $model->help ?></div></div></div>
            <?php foreach ($model->params as $param) { ?>
                <div class="form-group field-companycategory-seo_plus_h1">
                    <label class="col-xs-2 control-label"><?= $param->alias ?></label>
                    <div class="col-xs-10">
                        <?php if ($param->field_type == "textInput") { ?>
                            <?= Html::textInput("SeoParam[" . $param->id . "]", $param->template, ["class" => "form-control"]) ?>
                        <?php } elseif ($param->field_type == "textarea") { ?>
                            <?= Html::textarea("SeoParam[" . $param->id . "]", $param->template, ["class" => "form-control"]) ?>
                        <?php } elseif ($param->field_type == "CKEditor") { ?>
                            <?= CKEditor::widget(['name' => "SeoParam[" . $param->id . "]", 'value' => $param->template]); ?>
                        <?php } ?>

                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="tab-pane fade" id="config">
            <?= $form->field($model, 'route')->textInput(['maxlength' => 128]) ?>
            <?= $form->field($model, 'title')->textInput() ?>
            <?= $form->field($model, 'help')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'sort_order')->textInput() ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>