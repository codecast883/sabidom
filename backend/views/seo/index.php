<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;
use kartik\widgets\ActiveForm;

;

$this->title = 'Seo+';
$this->params['breadcrumbs'][] = $this->title;


$this->params['menu'] = [
    Html::a('Создать', ['create'], ['class' => 'btn btn-success']),
];
?>
<div class="seo-index">
    <?=
    GridView::widget([
        'dataProvider' => $data_provider,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['class' => 'col-id'],
            ],
            'title:ntext',
            [
                'attribute' => 'route',
                'options' => ['class' => 'col-xs-2'],
            ],
            [
                'attribute' => 'sort_order',
                'options' => ['class' => 'col-xs-1'],
            ],
            ['class' => 'common\owerride\yii\grid\ActionColumn',],
        ],
    ]);
    ?>
</div>
