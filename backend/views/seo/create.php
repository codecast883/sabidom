<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Seo */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Seo+', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="seo-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
