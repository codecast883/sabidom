<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Seo */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Seo+', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="seo-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
