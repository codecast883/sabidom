<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FieldGroup */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Field Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="field-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
