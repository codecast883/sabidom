<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FieldGroup */

$this->title = 'Изменить: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Field Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="field-group-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
