<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;
use kartik\widgets\ActiveForm;

$this->title = 'Заявки на просмотр частных объявлений';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realt-view-request-index">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'checkboxColumn' => TRUE,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['class' => 'col-id'],
            ],
            [
                'attribute' => 'created_at',
                'options' => ['class' => 'col-xs-2'],
            ],
            'name',
            'phone',
            [
                'label' => 'Объявлений',
                'format' => 'raw',
                'attribute' => 'realte_id',
                'filter' => "",
                'value' => function ($model, $index, $widget) {
                    return $model->realt ? Html::a("Объявление №".$model->realt_id, ['realt/update', 'id' => $model->realt_id]) : "";
                },
                        'options' => ['class' => 'col-xs-3'],
                    ],
                    ['class' => 'common\owerride\yii\grid\ActionColumn',],
                ],
            ]);
            ?>

            <div class='btn-group pull-left'>

                <?=
                Html::submitButton('Удалить', [
                    'class' => 'btn btn-danger btn-xs',
                    'onclick' => "
                        if(confirm('Действительно удалить?')){
                            $('#list-form').prop('action', '" . yii\helpers\Url::toRoute(['delete', 'id' => 0, 'get_params' => Yii::$app->request->get()]) . "');
                            $('#list-form').submit();
                }
                "])
                ?>
    </div>    

</div>
