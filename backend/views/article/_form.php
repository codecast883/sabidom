<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
		<?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
	</div>
    <div class="clearfix"></div><br>

    <?= $form->field($model, 'type_id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'seo_h1')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'seo_description')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'seo_breadcrumb')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description_short')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'on_top')->textInput() ?>

    <?= $form->field($model, 'on_home')->textInput() ?>

    <?= $form->field($model, 'image')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?php ActiveForm::end(); ?>

</div>
