<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;
use kartik\widgets\ActiveForm;;


$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
    

$this->params['menu'] = [
        Html::a('Создать', ['create'], ['class' => 'btn btn-success'])
];



?>
<div class="article-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'checkboxColumn' => TRUE,
        'filterModel' => $searchModel,
        'columns' => [
            [
              'attribute' => 'id',
              'options' => ['class' => 'col-id'],
            ],

            'id',
            'type_id',
            'title',
            'seo_h1',
            'seo_description',
            // 'seo_keywords',
            // 'seo_title',
            // 'seo_breadcrumb:ntext',
            // 'description:ntext',
            // 'description_short:ntext',
            // 'created_at',
            // 'updated_at',
            // 'on_top',
            // 'on_home',
            // 'image:ntext',
            // 'status',

            ['class' => 'common\owerride\yii\grid\ActionColumn',],
        ],
    ]); ?>
    
    <div class='btn-group pull-left'>

        <?= Html::submitButton('Удалить', [
            'class' => 'btn btn-danger btn-xs',
            'onclick' => "
                        if(confirm('Действительно удалить?')){
                            $('#list-form').prop('action', '" . yii\helpers\Url::toRoute(['delete', 'id' => 0, 'get_params' => Yii::$app->request->get()]) . "');
                            $('#list-form').submit();
                }
                "])
        ?>
    </div>    

</div>
