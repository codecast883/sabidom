<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use common\models\Banner;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use yii\web\JsExpression;
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
        <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
    </div>
    <div class="clearfix"></div>
    <br>

    <?= $form->field($model, 'position_id')->dropDownList(Banner::itemAlias("position_id")) ?>
    <?= $form->field($model, 'status')->widget(SwitchInput::classname(), []); ?>
    <?= $form->field($model, 'sort_order')->textInput() ?>

    
    <?= $form->field($model, 'code')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'html')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>

    <?php ActiveForm::end(); ?>

</div>
