<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;

$this->title = 'Баннеры';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'] = [
    Html::a('Создать', ['create'], ['class' => 'btn btn-success'])
];
?>
<div class="banner-index">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'position_id',
                'filter' => \common\models\Banner::itemAlias("position_id"),
                'value' => function ($model, $index, $widget) {
                    return \common\models\Banner::itemAlias("position_id", $model->position_id);
                },
            ],
            [
                'attribute' => 'status',
                'filter' => \common\models\Banner::itemAlias("status"),
                'value' => function ($model, $index, $widget) {
                    return \common\models\Banner::itemAlias("status", $model->status);
                },
                'options' => ['class' => 'col-xs-1'],
            ],
            [
                'attribute' => 'sort_order',
                'options' => ['class' => 'col-min'],
            ],
            ['class' => 'common\owerride\yii\grid\ActionColumn',],
        ],
    ]);
    ?>

</div>
