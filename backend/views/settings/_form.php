<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;
?>

<div class="contacts-form">
    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
        <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
    </div>
    <div class="clearfix"></div><br>
    <?= $form->field($model, 'alias')->textInput(['maxlength' => 128, 'readonly' => 'readonly']) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => 1024, 'readonly' => 'readonly']) ?>

    <?php if ($model->type == 1) { ?>
        <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>
    <?php } elseif ($model->type == 2) { ?>
        <?= $form->field($model, 'value')->widget(CKEditor::className(), ['options' => ['rows' => 2], 'preset' => 'standart']); ?>
    <?php } else { ?>
        <?= $form->field($model, 'value')->input('text') ?>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
