<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-index">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'alias',
            'description',
            [
                'class' => 'common\owerride\yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]);
    ?>

</div>
