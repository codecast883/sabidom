<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contacts */

$this->title = 'Изменить: ' . ' ' . $model->alias;
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->alias;
?>
<div class="contacts-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
