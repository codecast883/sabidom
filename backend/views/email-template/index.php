<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Шаблоны писем';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-template-index">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'title',
            [
                'template' => '{update}',
                'class' => 'common\owerride\yii\grid\ActionColumn',
            ],
        ],
    ]);
    ?>

</div>
