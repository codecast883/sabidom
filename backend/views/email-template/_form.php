<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;
?>

<div class="email-template-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>

    <div class="btn-group pull-right">
        <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
    </div>
    <div class="clearfix"></div>
    <br>
    <div class="form-group field-emailtemplate-alias">
        <label class="col-xs-2 control-label">Подстановки</label>
        <div class="col-xs-10"><div class="help-block"><?= $model->replaces ?></div></div>
    </div>
    <?= $form->field($model, 'send_from')->textInput(['maxlength' => 1024]) ?>
    <?= $form->field($model, 'theme')->textInput(['maxlength' => 1024]) ?>
    <?= $form->field($model, 'body')->widget(CKEditor::className(), ['options' => ['rows' => 6], 'preset' => 'standart']); ?>

    <?php ActiveForm::end(); ?>

</div>
