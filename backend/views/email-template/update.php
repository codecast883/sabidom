<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\EmailTemplate */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Компании', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="email-template-update">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
