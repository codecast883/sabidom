<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>


<div class='container contacts-page'>
	<div class='row'>
		<div class='col-xs-12 main-column'>
			<div class="panel panel-danger" style="margin-top: 200px">
                <div class="panel-heading">
                    <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
                </div>
				<div class="panel-body">
					<?= nl2br(Html::encode($message)) ?>
				</div>
			</div>

			
		</div>
	</div>
</div>