<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
?>




<div class="login-form">
	<div class="panel panel-primary">
		<?php
		$form = ActiveForm::begin([
					'id' => 'login-form-horizontal',
					'type' => ActiveForm::TYPE_HORIZONTAL,
					'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
		]);
		?>
		<div class="panel-body">
			<h4 class="text-center" style="margin-bottom: 25px;">Log in to get started</h4>
			<?= $form->field($model, 'login') ?>
			<?= $form->field($model, 'password')->passwordInput() ?>
			<?= $form->field($model, 'rememberMe')->checkbox() ?>
        </div>
		<div class="panel-footer">
			<div class="pull-right">
				<?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
			</div>
			<div class="clearfix"></div>
		</div>

		<?php ActiveForm::end(); ?>
    </div>
</div>



