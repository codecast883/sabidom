<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Field */

$this->title = 'Изменить: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="field-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
