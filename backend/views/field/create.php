<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Field */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="field-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
