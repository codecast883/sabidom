<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\widgets\AlertBlock;

/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="skin-blue">
        <?php $this->beginBody() ?>
        <?=
        AlertBlock::widget([
            'type' => AlertBlock::TYPE_ALERT,
            'useSessionFlash' => true,
            'delay' => 3000
        ]);
        ?>
        <div id="page-container">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a href="/admin" class="logo navbar-brand"><?= \Yii::$app->params['site_name'] ?></a>
                    </div>
                    <div class="" id="navbar">
                        <ul class="nav navbar-nav navbar-right">
                            <?php if (count($new_reviews)) { ?>
                                <li class="notifications-menu">
                                    <a href="/admin/company-review/moderate" title="Новые отзывы">
                                        <i class="fa fa-comment"></i>
                                        <span class="label label-success"><?= count($new_reviews) ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if (count($new_reviews_answer)) { ?>
                                <li class="notifications-menu">
                                    <a href="/admin/company-review-answer/moderate" title="Новые ответы на отзывы">
                                        <i class="fa fa-comment"></i>
                                        <span class="label label-success"><?= count($new_reviews_answer) ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if (count($companies_for_moderate)) { ?>
                                <li class="notifications-menu">
                                    <a href="/admin/company/index?CompanySearch[status]=2" title="Компании, ожидающие модерации">
                                        <i class="fa fa-warning"></i>
                                        <span class="label label-danger"><?= count($companies_for_moderate) ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                            <li>
                                <?php
                                echo Nav::widget([
                                    'options' => ['class' => ''],
                                    'items' => $this->context->user_menu,
                                ]);
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container-fluid">
                <div class="row">
                    <!-- BEGIN SIDEBAR -->
                    <aside class="left-side sidebar-offcanvas col-xs-2">
                        <!-- BEGIN SIDEBAR MENU -->
                        <?php
                        echo Nav::widget([
                            'encodeLabels' => false,
                            'activateParents' => true,
                            'route' => (Yii::$app->controller->module->getUniqueId() ? Yii::$app->controller->module->getUniqueId() . "/" : "") . \Yii::$app->controller->id,
                            'options' => ['id' => 'sidebar', 'class' => 'nav nav-list'],
                            'items' => $this->context->main_menu
                        ])
                        ?>
                    </aside>
                    <aside class="right-side col-xs-10">
                        <section class="content-header">
                            <h1><?= Html::encode($this->title) ?></h1>

                            <?php
                            echo Breadcrumbs::widget([
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ])
                            ?>
                        </section>
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12 ">
                                    <div class="options-buttons pull-right">
                                        <?php
                                        if (isset($this->params['menu'])) {
                                            echo \yii\bootstrap\ButtonGroup::widget(['buttons' => $this->params['menu']]);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <?php echo $content; ?>
                                </div>
                            </div>
                        </section>
                    </aside>
                </div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>