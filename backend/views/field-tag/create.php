<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FieldTag */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Field Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="field-tag-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
