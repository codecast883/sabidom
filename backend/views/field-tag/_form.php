<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FieldTag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="field-tag-form">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
    <div class="btn-group pull-right">
		<?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
	</div>
    <div class="clearfix"></div><br>

    <?= $form->field($model, 'field_id')->dropDownList(\common\models\Field::getDropdown(2, $empty)) ?>
    
    <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>

    <?php ActiveForm::end(); ?>

</div>
