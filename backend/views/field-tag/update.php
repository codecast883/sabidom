<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FieldTag */

$this->title = 'Изменить: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Field Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="field-tag-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
