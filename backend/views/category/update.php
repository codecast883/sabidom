<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use iutbay\yii2kcfinder\KCFinderInputWidget;
use common\models\CategoryFilter;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->titlePath();
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="company-category-update">
    <div class="company-category-form">

        <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
        <div class="row">
            <div class="col-xs-12 options-buttons">
                <div class="btn-group pull-right">
                    <?= Html::a('Посмотреть', $model->getFrontUrl(), ['target' => '_blank', 'class' => 'btn btn-default']) ?>
                    <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type', 'value' => 'index']) ?>
                </div>
                <?php if ($model->parent) { ?>
                    <?= Html::a('<i class="fa fa-arrow-up" aria-hidden="true"></i>', ['update', 'id' => $model->parent_id], ['title' => 'Родительская категория', 'class' => 'btn btn-default pull-right', 'style' => 'margin-right: 10px']) ?>
                <?php } ?>
            </div>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="#common" role="tab" data-toggle="tab">Общая информация</a></li>
            <li class="active"><a href="#filter" role="tab" data-toggle="tab">Фильтр</a></li>
            <li><a href="#seo" role="tab" data-toggle="tab">SEO</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane" id="common">
                <div class="col-xs-12">

                    <?php //= $form->field($model, 'parent_id')->dropDownList(\common\models\Category::categoriesDropdown());  ?>

                    <?=
                    $form->field($model, 'parent_id')->widget(
                            Select2::classname(), [
                        'data' => \common\models\Category::categoriesDropdown(),
                        'size' => Select2::SMALL,
                        'options' => ['placeholder' => 'Select a state ...', 'multiple' => false],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => 1024]) ?>
                    <?= $form->field($model, 'slug')->textInput(['maxlength' => 1024]) ?>
                    <?= $form->field($model, 'product_name_mask')->textInput(['maxlength' => 4096]) ?>
                    <?= $form->field($model, 'group_title')->textInput(['maxlength' => 1024]) ?>
                    <?= $form->field($model, 'sort_order')->textInput(['maxlength' => 1024]) ?>
                    <?= $form->field($model, 'on_header')->textInput(['maxlength' => 1024]) ?>
                    <?= $form->field($model, 'description_top')->widget(CKEditor::className(), ['options' => ['rows' => 2], 'preset' => 'standart']); ?>
                    <?= $form->field($model, 'description_bottom')->widget(CKEditor::className(), ['options' => ['rows' => 2], 'preset' => 'standart']); ?>
                </div>
            </div>
            <div class="tab-pane fade in active" id="filter">
                <table class="table table-bordered table-condensed table-hover">
                    <colgroup>
                        <col class="col-xs-2">
                        <col class="col-xs-10">
                    </colgroup>
                    <tr>
                        <th>Поле</th>
                        <th>Значение</th>
                    </tr>
                    <?php foreach ($full_filter as $row) { ?>
                        <tr>
                            <td><?= $row['title'] ?></td>
                            <td><?= $row['value'] ?></td>
                        </tr>
                    <?php } ?>
                </table>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapse-filter" aria-expanded="false" aria-controls="collapse-filter">
                    Редактировать фильтр
                </button>
                <br><br>
                <div class="collapse in" id="collapse-filter">
                    <?= $form->field($model, 'content_type_id')->dropDownList(\common\models\Category::itemAlias("content_type_id")); ?>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Цена</label>
                        <div class="col-xs-10">
                            <div class="row">
                                <div class="col-xs-2">
                                    <?= Html::textInput("filter[custom][price][from]", !empty($filter[CategoryFilter::TYPE_CUSTOM]['price']) ? $filter[CategoryFilter::TYPE_CUSTOM]['price']['from'] : "", ['class' => 'form-control', 'placeholder' => "От"]) ?>
                                </div>
                                <div class="col-xs-2">
                                    <?= Html::textInput("filter[custom][price][to]", !empty($filter[CategoryFilter::TYPE_CUSTOM]['price']) ? $filter[CategoryFilter::TYPE_CUSTOM]['price']['to'] : "", ['class' => 'form-control', 'placeholder' => "До"]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Площадь дома</label>
                        <div class="col-xs-10">
                            <div class="row">
                                <div class="col-xs-2">
                                    <?= Html::textInput("filter[custom][house_area][from]", !empty($filter[CategoryFilter::TYPE_CUSTOM]['house_area']) ? $filter[CategoryFilter::TYPE_CUSTOM]['house_area']['from'] : "", ['class' => 'form-control', 'placeholder' => "От"]) ?>
                                </div>
                                <div class="col-xs-2">
                                    <?= Html::textInput("filter[custom][house_area][to]", !empty($filter[CategoryFilter::TYPE_CUSTOM]['house_area']) ? $filter[CategoryFilter::TYPE_CUSTOM]['house_area']['to'] : "", ['class' => 'form-control', 'placeholder' => "До"]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Площадь участка</label>
                        <div class="col-xs-10">
                            <div class="row">
                                <div class="col-xs-2">
                                    <?= Html::textInput("filter[custom][stead_area][from]", !empty($filter[CategoryFilter::TYPE_CUSTOM]['stead_area']) ? $filter[CategoryFilter::TYPE_CUSTOM]['stead_area']['from'] : "", ['class' => 'form-control', 'placeholder' => "От"]) ?>
                                </div>
                                <div class="col-xs-2">
                                    <?= Html::textInput("filter[custom][stead_area][to]", !empty($filter[CategoryFilter::TYPE_CUSTOM]['stead_area']) ? $filter[CategoryFilter::TYPE_CUSTOM]['stead_area']['to'] : "", ['class' => 'form-control', 'placeholder' => "До"]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php foreach ($field_groups as $group) { ?>
                        <h3><?= $group['group_info']->title ?></h3>
                        <?php foreach ($group['fields'] as $field) { ?>
                            <div class="form-group">
                                <label class="col-xs-2 control-label"><?= $field['field_info']->title ?></label>
                                <div class="col-xs-10">
                                    <?php if ($field['field_info']->type_id == 1) { ?>
                                        <?= Html::textInput("filter[field][" . $field['field_info']->id . "]", $field['values'], ['class' => 'form-control', 'placeholder' => ""]) ?>
                                    <?php } ?>
                                    <?php if ($field['field_info']->type_id == 2) { ?>
                                        <?=
                                        Select2::widget([
                                            'name' => "filter[field][" . $field['field_info']->id . "]",
                                            'data' => $field['available_values'],
                                            'value' => $field['values'],
                                            'options' => [
                                                'multiple' => true
                                            ],
                                        ]);
                                        ?>
                                    <?php } ?>
                                    <?php if ($field['field_info']->type_id == 3) { ?>
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <?= Html::textInput("filter[field][" . $field['field_info']->id . "][from]", $field['from'], ['class' => 'form-control', 'placeholder' => "От"]) ?>
                                            </div>
                                            <div class="col-xs-2">
                                                <?= Html::textInput("filter[field][" . $field['field_info']->id . "][to]", $field['to'], ['class' => 'form-control', 'placeholder' => "До"]) ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    <?php } ?>
                </div>
            </div>
            <div class="tab-pane fade" id="seo">
                <?= $form->field($model, 'seo_title')->textInput(['maxlength' => 1024]) ?>
                <?= $form->field($model, 'seo_breadcrumb')->textInput(['maxlength' => 1024]) ?>
                <?= $form->field($model, 'seo_h1')->textInput(['maxlength' => 1024]) ?>
                <?= $form->field($model, 'seo_description')->textarea(['maxlength' => 1024]) ?>
                <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 1024]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
