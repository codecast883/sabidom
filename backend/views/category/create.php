<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
use yii\web\JsExpression;
use iutbay\yii2kcfinder\KCFinderInputWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-category-create">
    <?php
    /* @var $this yii\web\View */
    /* @var $model common\models\Category */
    /* @var $form yii\widgets\ActiveForm */
    ?>

    <div class="company-category-form">
        <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
        <div class="row">
            <div class="col-xs-12 options-buttons">
                <div class="btn-group pull-right">
                    <?= Html::submitButton('Создать', ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#common" role="tab" data-toggle="tab">Общая информация</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade in active" id="common">
                <div class="col-xs-12">

                    <?php //= $form->field($model, 'parent_id')->dropDownList(\common\models\Category::categoriesDropdown());  ?>

                    <?=
                    $form->field($model, 'parent_id')->widget(
                            Select2::classname(), [
                        'data' => \common\models\Category::categoriesDropdown(),
                        'size' => Select2::SMALL,
                        'options' => ['placeholder' => 'Select a state ...', 'multiple' => false],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => 1024]) ?>
                    <?= $form->field($model, 'slug')->textInput(['maxlength' => 1024]) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>