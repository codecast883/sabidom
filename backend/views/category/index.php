
<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 options-buttons">
        <div class="btn-group pull-right">
            <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::button("<i class='fa fa-cog' aria-hidden='true'></i> <span class='caret'></span>", ['class' => 'btn btn-default dropdown-toggle', 'data-toggle' => "dropdown", 'aria-haspopup' => "true", 'aria-expanded' => "false"]) ?>
            <ul class="dropdown-menu">
                <li><?=
                    Html::a("Перестроить выпадающие списки", ['service', 'return' => array_merge([Yii::$app->controller->action->id], Yii::$app->request->get())], [
                        'data-method' => 'post',
                        'data-params' => ['action' => 'clear-dropdown-cache']
                    ])
                    ?></li>
            </ul>
        </div>
    </div>
</div>
<div class="company-category-index">

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['class' => 'col-id'],
            ],
            [
                'label' => 'Родительская категория',
                'attribute' => 'parent_id',
                'filter' => \common\models\Category::categoriesDropdown(),
                'value' => function ($model, $index, $widget) {
                    return $model->parent ? $model->parent->title : "";
                },
                'options' => ['class' => 'col-xs-3'],
            ],
            'title',
            [
                'attribute' => 'filters_count',
                'options' => ['class' => 'col-xs-1'],
            ],
            ['class' => 'common\owerride\yii\grid\ActionColumn',],
        ],
    ]);
    ?>

</div>
