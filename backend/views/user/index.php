<?php

use yii\helpers\Html;
use common\owerride\yii\grid\GridView;

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menu'] = [
    Html::a('Создать', ['create'], ['class' => 'btn btn-success'])
];
?>

<?php

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n{pager}",
    'columns' => [
        'username',
        'email:email',
        [
            'attribute' => 'registration_ip',
            'value' => function ($model, $key, $index, $widget) {
                return $model->registration_ip == null ? '<span class="not-set">' . '(not set)' . '</span>' : long2ip($model->registration_ip);
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'created_at',
            'value' => function ($model, $key, $index, $widget) {
                return date("Y-m-d", $model->created_at);
            }
        ],

        [
            'class' => 'common\owerride\yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ],
]);
?>
