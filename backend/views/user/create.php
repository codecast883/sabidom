<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
<div class="btn-group pull-right">
    <?= Html::a('Список', ['index'], ['class' => 'btn btn-default']); ?>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default']) ?>
</div>
<div class="clearfix"></div><br>
<?= $form->field($model, 'username')->textInput(['maxlength' => 25, 'autofocus' => true]) ?>
<?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'password')->passwordInput() ?>

<?php ActiveForm::end(); ?>