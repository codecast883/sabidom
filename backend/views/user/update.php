<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;

$this->title = 'Update user account';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;




$this->title = 'Изменить: ' . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username];
?>


<?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
<div class="btn-group pull-right">
    <?= Html::a('Список', ['index'], ['class' => 'btn btn-default']); ?>
    <?= Html::submitButton('Применить', ['class' => 'btn btn-default']) ?>
</div>
<h1><i class="glyphicon glyphicon-user"></i> <?= Html::encode($model->username) ?>
    <?php /* if (!$model->getIsConfirmed()): ?>
      <?= Html::a('Confirm', ['confirm', 'id' => $model->id], ['class' => 'btn btn-success btn-xs', 'data-method' => 'post']) ?>
      <?php endif; */ ?>

</h1>
<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#common" role="tab" data-toggle="tab">Общая информация</a></li>
    <li><a href="#roles" role="tab" data-toggle="tab">Роли</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade in active" id="common">

        <?= $form->field($model, 'username')->textInput(['maxlength' => 25]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
    </div>


    <div class="tab-pane fade" id="roles">
        <div class="form-group field-user-username required">
            <label class="control-label">Роли</label>
            <div class="row">
                <div class="col-xs-5">
                    <table class="table table-condensed table-striped">
                        <colgroup>
                            <col class="col-xs-4">
                            <col class="col-xs-8">
                        </colgroup>
                        <tr>
                            <th>Название</th>
                            <th>Описание</th>
                        </tr>
                        <?php foreach ($model->roles as $role) { ?>
                            <tr>
                                <td><?= $role->name ?></td>
                                <td><?= $role->description ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            <br>
            <?= Html::a("Управление ролями", ["rbac/assignment/view", "id" => $model->id], ['class' => "btn btn-default"]) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>