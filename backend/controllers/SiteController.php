<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends \backend\components\controllers\BackController {

    var $layout = "simple";

    public function behaviors() {
        $behaviors = [
            'access' => [
                'class' => \yii2mod\rbac\components\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'logut', 'accessDenied'],
                        'roles' => ['?'],
                        'allow' => true,
                    ],
                ]
            ],
        ];

        return $behaviors;
        //return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $this->layout = "main";
        return $this->render('index');
    }

    public function actionAccessDenied() {
        return $this->render('access-denied');
    }

    public function actionLogin() {
        $model = new \common\models\LoginForm();

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goHome();
        }

        return $this->render('login', [
                    'model' => $model
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
