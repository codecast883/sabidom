<?php

namespace backend\controllers;

use Yii;
use common\models\VillageViewRequest;
use common\models\VillageViewRequestSearch;
use backend\components\controllers\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ViewRequestController implements the CRUD actions for ViewRequest model.
 */
class ViewRequestController extends BackController
{   

    public function actionIndex()
    {
        $searchModel = new ViewRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new ViewRequest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Сохранили');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Сохранили');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $selected = [];
        if ($id) {
            $selected[] = $id;
        } else {
            $selected = Yii::$app->request->post("selection");
        }

        if ($selected) {
            foreach (ViewRequest::find()->where(["id" => $selected])->all() as $model){
                $model->delete();
            }
        }

        return $this->redirect(Yii::$app->request->get('return', ['index']));
    }

    protected function findModel($id)
    {
        if (($model = ViewRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
