<?php

namespace backend\controllers;

use Yii;
use common\models\Realt;
use common\models\RealtSearch;
use backend\components\controllers\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\RealtImage;

/**
 * RealtController implements the CRUD actions for Realt model.
 */
class RealtController extends BackController {

	public function actionW1() {
		foreach (Realt::find()->each() as $model) {
			if(preg_replace("|\D|isu", "", $model->phone) == "74953171770"){
				$model->delete();
			}
		}
	}

	public function actionIndex() {
		$searchModel = new RealtSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate() {



		$model = new Realt();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {


			Yii::$app->getSession()->setFlash('success', 'Сохранили');
			return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
		} else {
			return $this->render('create', [
						'model' => $model,
			]);
		}
	}

	public function actionUpdate($id) {
        $this->data['model'] = $this->findModel($id);
        $this->data['upload_image_message'] = "";
        $uploadm= '';
        if (Yii::$app->request->post("current_action") == "upload_image") {
//            echo "<script type='text/javascript'>console.log('sdf');</script>";
            $model_image = new RealtImage;
            if ($model_image->load(Yii::$app->request->post()) && $model_image->save()) {
                $uploadm = "Изображение загружено";
            } else {
                $uploadm = print_r($model_image->getErrors(), true);
            }
        }




        $model = $this->findModel($id);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {



           $old_images = [];
            foreach ($this->data['model']->images as $old_image) {
                $old_images[$old_image->id] = $old_image;
            }

            foreach (Yii::$app->request->post("images_list") as $key => $image_id) {
                $old_images[$image_id]->sort_order = $key + 1;
                $old_images[$image_id]->save();
                unset($old_images[$image_id]);
            }

            foreach ($old_images as $old_image) {
                $old_image->delete();
            }




			Yii::$app->getSession()->setFlash('success', 'Сохранили');

            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $this->data['model']->id)));
        }

        return $this->render('update',[
            'apm' => $uploadm,
        ] );
	}

	public function actionDelete($id) {
		$selected = [];
		if ($id) {
			$selected[] = $id;
		} else {
			$selected = Yii::$app->request->post("selection");
		}

		if ($selected) {
			foreach (Realt::find()->where(["id" => $selected])->all() as $model) {
				$model->delete();
			}
		}

		return $this->redirect(Yii::$app->request->get('return', ['index']));
	}

	protected function findModel($id) {
		if (($model = Realt::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
