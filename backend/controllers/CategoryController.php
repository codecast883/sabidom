<?php

namespace backend\controllers;

use Yii;
use common\models\CategorySearch;
use common\models\CategoryFilter;
use common\models\FieldGroup;
use common\models\Field;
use yii\helpers\ArrayHelper;
use common\models\FieldTag;
use common\models\Category;
use yii\data\ActiveDataProvider;
use backend\components\controllers\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends BackController {

    public function actionExport() {
        $data = [];
        $categories = Category::find()->where(['parent_id' => 0])->orderBy("title")->asArray()->all();
        foreach ($categories as $l1) {
            $data[] = [
                'id' => $l1['id'],
                'path' => $l1['title'],
                'title' => $l1['title'],
                'seo_title' => $l1['seo_title'],
                'seo_h1' => $l1['seo_h1'],
                'path' => $l1['path_cache'],
            ];

            $categories = Category::find()->where(['parent_id' => $l1['id']])->orderBy("title")->asArray()->all();
            foreach ($categories as $l2) {
                $data[] = [
                    'id' => $l2['id'],
                    'path' => $l1['title'] . " - " . $l2['title'],
                    'title' => $l2['title'],
                    'seo_title' => $l2['seo_title'],
                    'seo_h1' => $l2['seo_h1'],
                    'path' => $l2['path_cache'],
                ];

                $categories = Category::find()->where(['parent_id' => $l2['id']])->orderBy("title")->asArray()->all();
                foreach ($categories as $l3) {
                    $data[] = [
                        'id' => $l3['id'],
                        'path' => $l1['title'] . " - " . $l2['title'] . " - " . $l3['title'],
                        'title' => $l3['title'],
                        'seo_title' => $l3['seo_title'],
                        'seo_h1' => $l3['seo_h1'],
                        'path' => $l3['path_cache'],
                    ];

                    $categories = Category::find()->where(['parent_id' => $l3['id']])->orderBy("title")->asArray()->all();
                    foreach ($categories as $l4) {
                        $data[] = [
                            'id' => $l4['id'],
                            'path' => $l1['title'] . " - " . $l2['title'] . " - " . $l3['title'] . " - " . $l4['title'],
                            'title' => $l4['title'],
                            'seo_title' => $l4['seo_title'],
                            'seo_h1' => $l4['seo_h1'],
                            'path' => $l4['path_cache'],
                        ];

                        $categories = Category::find()->where(['parent_id' => $l4['id']])->orderBy("title")->asArray()->all();
                        foreach ($categories as $l5) {
                            $data[] = [
                                'id' => $l5['id'],
                                'path' => $l1['title'] . " - " . $l2['title'] . " - " . $l3['title'] . " - " . $l4['title'] . " - " . $l5['title'],
                                'title' => $l5['title'],
                                'seo_title' => $l5['seo_title'],
                                'seo_h1' => $l5['seo_h1'],
                                'path' => $l5['path_cache'],
                            ];
                        }
                    }
                }
            }
        }

        $file = fopen($_SERVER['DOCUMENT_ROOT'] . "/frontend/web/vendor/export.csv", "w");

        foreach ($data as $row) {
            fputcsv($file, $row, ";");
        }
    }

    public function actionImport() {
        $f = fopen($_SERVER['DOCUMENT_ROOT'] . "/objects/categories.csv", "r+");
        $categories = [];

        $model_category = new Category;
        while ($row = fgetcsv($f, 0, ";")) {
            $category['title'] = explode(" - ", $row[0]);
            $category['seo_h1'] = $row[2];
            $category['seo_title'] = $row[3];

            foreach ($category['path'] as &$item) {
                $item = $model_category->generateSlug($item);
            }

            if (count($category['title']) > 0) {
                $categories[] = $category;
            }
        }

        foreach ($categories as $src_category) {
            $level = 5;
            $parent = NULL;
            if (count($src_category['title']) == $level + 1) {
                if ($level > 0) {
                    for ($i = 0; $i < $level; $i++) {
                        $old_parent = isset($parent) ? $parent : NULL;

                        if (!$parent = Category::find()->where(['title' => $src_category['title'][$i], 'level' => $i, 'parent_id' => ($old_parent ? $old_parent->id : 0)])->one()) {
                            $new_parent = new Category;
                            $new_parent->parent_id = $old_parent ? $old_parent->id : 0;
                            $new_parent->title = $src_category['title'][$i];
                            $new_parent->level = $i;
                            if (!$new_parent->save()) {
                                print_r($new_parent->getErrors());
                                exit;
                            }

                            $parent = $new_parent;

                            print_r("parent not found ");
                            print "create parent_id:" . $new_parent->parent_id . " title:" . $new_parent->title . " level: " . $i . "<br>";
//                            print_r($src_category);
                            #exit;
                        }
                    }
                }



                if ($level > 0) {
                    if (!$category = Category::find()->where(['title' => $src_category['title'][$level], 'seo_h1' => $src_category['seo_h1'], 'parent_id' => $parent->id])->one()) {
                        $category = new Category;
                    }
                } else {
                    if (!$category = Category::find()->where(['title' => $src_category['title'], 'seo_h1' => $src_category['seo_h1']])->one()) {
                        $category = new Category;
                    }
                }


                $category->parent_id = $parent ? $parent->id : 0;

                $category->level = $level;
                $category->title = trim($src_category['title'][$level]);
                $category->seo_h1 = $src_category['seo_h1'];
                $category->seo_title = $src_category['seo_title'];

                print $category->title . "<br>";

                if (!$category->save()) {
                    print_r("error saving");
                    print_r($category->getErrors());
                    exit;
                }
            }
        }
    }
    public function actionIndex() {
//        foreach (Category::find()->each() as $cat) {
//            $cat->slug = "";
//            $cat->save();
//        }

        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionService() {
        $action = Yii::$app->request->post('action');
        if ($action == "clear-dropdown-cache") {
            Category::clearDropdownCache();
            Yii::$app->getSession()->setFlash('success', 'Кэш очищен');
        }

        return $this->redirect(Yii::$app->request->get('return', ['index']));
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Сохранили');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $this->data['model'] = $this->findModel($id);


        if ($this->data['model']->load(Yii::$app->request->post()) && $this->data['model']->save()) {
            CategoryFilter::deleteAll(['category_id' => $id]);
            $filter_in = Yii::$app->request->post("filter", []);

            foreach (ArrayHelper::getValue($filter_in, "custom", []) as $label => $values) {
                $filter_row = new CategoryFilter;
                $filter_row->type_id = CategoryFilter::TYPE_CUSTOM;
                $filter_row->category_id = $id;

                if ($label == "price") {
                    if (!ArrayHelper::getValue($values, "from") AND ! ArrayHelper::getValue($values, "to"))
                        continue;
                    $filter_row->field = "price";
                    $filter_row->value = ((float) ArrayHelper::getValue($values, "from") ? (float) ArrayHelper::getValue($values, "from") : "") . "-" . (ArrayHelper::getValue($values, "to") ? (float) ArrayHelper::getValue($values, "to") : "");
                }

                if ($label == "house_area") {
                    if (!ArrayHelper::getValue($values, "from") AND ! ArrayHelper::getValue($values, "to"))
                        continue;
                    $filter_row->field = "house_area";
                    $filter_row->value = ((float) ArrayHelper::getValue($values, "from") ? (float) ArrayHelper::getValue($values, "from") : "") . "-" . (ArrayHelper::getValue($values, "to") ? (float) ArrayHelper::getValue($values, "to") : "");
                }

                if ($label == "stead_area") {
                    if (!ArrayHelper::getValue($values, "from") AND ! ArrayHelper::getValue($values, "to"))
                        continue;
                    $filter_row->field = "stead_area";
                    $filter_row->value = ((float) ArrayHelper::getValue($values, "from") ? (float) ArrayHelper::getValue($values, "from") : "") . "-" . (ArrayHelper::getValue($values, "to") ? (float) ArrayHelper::getValue($values, "to") : "");
                }

                if ($filter_row->field) {
                    if (!$filter_row->save()) {
                        print_r($filter_row->getErrors());
                        exit;
                    }
                }
            }

            foreach (ArrayHelper::getValue($filter_in, "field", []) as $field_id => $values) {
                $field = Field::findOne($field_id);

                if ($field->type_id == 1) {
                    if (!$values)
                        continue;
                    $filter_row = new CategoryFilter;
                    $filter_row->category_id = $id;
                    $filter_row->type_id = CategoryFilter::TYPE_FIELD_TEXT;
                    $filter_row->field = (string) $field_id;
                    $filter_row->operand = "LIKE";
                    $filter_row->value = $values;
                    if (!$filter_row->save()) {
                        print_r($filter_row->getErrors());
                        exit;
                    }
                } elseif ($field->type_id == 2) {
                    foreach ($values as $value) {
                        $filter_row = new CategoryFilter;
                        $filter_row->type_id = CategoryFilter::TYPE_FIELD_TAG;
                        $filter_row->category_id = $id;
                        $filter_row->field = (string) $field_id;
                        $filter_row->value = $value;
                        if (!$filter_row->save()) {
                            print_r($filter_row->getErrors());
                            exit;
                        }
                    }
                } elseif ($field->type_id == 3) {
                    if (!ArrayHelper::getValue($values, "from") AND ! ArrayHelper::getValue($values, "to"))
                        continue;

                    $filter_row = new CategoryFilter;
                    $filter_row->type_id = CategoryFilter::TYPE_FIELD_FROM_TO;
                    $filter_row->category_id = $id;
                    $filter_row->field = (string) $field_id;
                    $filter_row->value = ((float) ArrayHelper::getValue($values, "from") ? (float) ArrayHelper::getValue($values, "from") : "") . "-" . (ArrayHelper::getValue($values, "to") ? (float) ArrayHelper::getValue($values, "to") : "");
                    if (!$filter_row->save()) {
                        print_r($filter_row->getErrors());
                        exit;
                    }
                }
            }

            $this->data['model']->updateAttributes(['filters_count' => CategoryFilter::find()->where(['category_id' => $this->data['model']->id])->count()]);

            Yii::$app->getSession()->setFlash('success', 'Сохранили');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $this->data['model']->id)));
        }


        $filter = $this->data['model']->getFilterArray();

        $field_groups = [1, 2];
        $this->data['field_groups'] = [];
        foreach ($field_groups as $group_id) {
            $this->data['field_groups'][$group_id]['group_info'] = FieldGroup::findOne($group_id);
            $this->data['field_groups'][$group_id]['fields'] = [];
            foreach (Field::find()->where(['field_group_id' => $group_id, 'type_id' => [1, 2, 3]])->orderBy(['title' => SORT_ASC])->all() as $field) {
                if ($field->type_id == 1) {
                    $this->data['field_groups'][$group_id]['fields'][] = [
                        'field_info' => $field,
                        'values' => ArrayHelper::getValue($filter[CategoryFilter::TYPE_FIELD_TEXT], $field->id),
                            //ArrayHelper::getValue(CategoryFilter::find()->where(['category_id' => $id, 'type_id' => CategoryFilter::TYPE_FIELD_TEXT, 'field' => $field->id])->one(), "value")
                    ];
                } elseif ($field->type_id == 2) {
                    $this->data['field_groups'][$group_id]['fields'][] = [
                        'field_info' => $field,
                        'available_values' => ArrayHelper::map(FieldTag::find()->where(['field_id' => $field->id])->orderBy(['value' => SORT_ASC])->all(), "id", "value"),
                        'values' => ArrayHelper::getValue($filter[CategoryFilter::TYPE_FIELD_TAG], $field->id),
                            //'values' => explode(",", ArrayHelper::getValue(CategoryFilter::find()->where(['category_id' => $id, 'type_id' => CategoryFilter::TYPE_FIELD_TAG, 'field' => $field->id])->one(), "value"))
                    ];
                } elseif ($field->type_id == 3) {
                    //$value = explode("-", ArrayHelper::getValue(CategoryFilter::find()->where(['category_id' => $id, 'type_id' => CategoryFilter::TYPE_FIELD_FROM_TO, 'field' => $field->id])->one(), "value"));
                    $value = ArrayHelper::getValue($filter[CategoryFilter::TYPE_FIELD_FROM_TO], $field->id, []);
                    $this->data['field_groups'][$group_id]['fields'][] = [
                        'field_info' => $field,
                        'from' => ArrayHelper::getValue($value, 'from'),
                        'to' => ArrayHelper::getValue($value, 'to'),
                    ];
                }
            }
        }



        $this->data['filter'] = $filter;

        $this->data['full_filter'] = [];



        $this->data['full_filter'][] = [
            'title' => "Тип объявлений",
            'value' => $this->data['model']->itemAlias('content_type_id', $this->data['model']->getContentTypeId())
        ];



        foreach ($this->data['model']->getFullFilterArray() as $type_id => $field) {

            foreach ($field as $field_id => $value) {
                if ($type_id == CategoryFilter::TYPE_CUSTOM) {
                    if ($field_id == 'price') {
                        $this->data['full_filter'][] = [
                            'title' => "цена",
                            'value' => $value['from'] . " - " . $value['to'],
                        ];
                    } elseif ($field_id == 'stead_area') {
                        $this->data['full_filter'][] = [
                            'title' => "Площадь участка",
                            'value' => $value['from'] . " - " . $value['to'],
                        ];
                    } elseif ($field_id == 'house_area') {
                        $this->data['full_filter'][] = [
                            'title' => "Площадь дома",
                            'value' => $value['from'] . " - " . $value['to'],
                        ];
                    } else {
                        $this->data['full_filter'][] = [
                            'title' => $field_id,
                            'value' => $value,
                        ];
                    }
                } elseif ($type_id == CategoryFilter::TYPE_FIELD_TEXT) {
                    $field = Field::findOne($field_id);
                    $this->data['full_filter'][] = [
                        'title' => $field->title,
                        'value' => $value,
                    ];
                } elseif ($type_id == CategoryFilter::TYPE_FIELD_TAG) {
                    $field = Field::findOne($field_id);
                    $tags = FieldTag::find()->where(['id' => $value])->all();

                    $this->data['full_filter'][] = [
                        'title' => $field->title,
                        'value' => implode(", ", ArrayHelper::map($tags, 'id', 'value')),
                    ];
                } elseif ($type_id == CategoryFilter::TYPE_FIELD_FROM_TO) {
                    $field = Field::findOne($field_id);

                    $this->data['full_filter'][] = [
                        'title' => $field->title,
                        'value' => $value['from'] . " - " . $value['to'],
                    ];
                }
            }
        }

        return $this->render('update');
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $selected = [];
        if ($id) {
            $selected[] = $id;
        } else {
            $selected = Yii::$app->request->post("selection");
        }

        if ($selected) {
            foreach (Category::find()->where(["id" => $selected])->all() as $model) {
                $model->delete();
            }
        }

        return $this->redirect(Yii::$app->request->get('return', ['index']));
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAutocomplete($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $query->select('id, title AS text')
                    ->from('company_category')
                    ->where('title LIKE "%' . $q . '%"')
                    ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Category::find($id)->title];
        }
        return $out;
    }

}
