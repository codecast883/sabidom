<?php

namespace backend\controllers;

use Yii;
use common\models\Village;
use common\models\VillageSearch;
use backend\components\controllers\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\FieldTag;
use common\models\VillageImage;
use common\models\VillageDocument;
use common\models\VillageProgress;

/**
 * VillageController implements the CRUD actions for Village model.
 */
class VillageController extends BackController {

	public function actionIndex() {
		$searchModel = new VillageSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate() {
		$model = new Village();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->getSession()->setFlash('success', 'Сохранили');
			return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
		} else {
			return $this->render('create', [
						'model' => $model,
			]);
		}
	}

	public function actionWork() {
		$html = "<p>Фотографии НАЗВАНИЕ {сделаны|созданы|были сделаны|были созданы} в коттеджном поселке, {являются реальными|являются настоящими|настоящие|реальные}. Фото {не могут|не смогут|не способны} {передать|передавать|описать|отобразить|показать} {неповторимую|непревзойденную|невероятную} {атмосферу|природу|обстановку} НАЗВАНИЕ. {Записывайтесь|Оформляйтесь|Оставляйте заявку} {на просмотр|на посещение} {КП|поселка|коттеджного поселка}.</p>";
//
//		foreach (Village::find()->each() as $village) {
//			$substitutions = [
//				'НАЗВАНИЕ' => $village->title,
//			];
//
//			$village->updateAttributes(['description_photo' => \common\helpers\YText::textGenerator($html, $substitutions)]);
//		}
	}

	public function actionUpdate($id) {
		$this->data['model'] = $this->findModel($id);

		$this->data['upload_image_message'] = "";
		if (Yii::$app->request->post("current_action") == "upload_image") {
			$model_image = new VillageImage;
			if ($model_image->load(Yii::$app->request->post()) && $model_image->save()) {
				$this->data['upload_image_message'] = "Изображение загружено";
			} else {
				$this->data['upload_image_message'] = print_r($model_image->getErrors(), true);
			}
		}

		$this->data['upload_progress_image_message'] = "";
		if (Yii::$app->request->post("current_action") == "upload_progress_image") {
			$model_image = new VillageProgress();
			if ($model_image->load(Yii::$app->request->post()) && $model_image->save()) {
				$this->data['upload_progress_image_message'] = "Изображение загружено";
			} else {
				$this->data['upload_progress_image_message'] = print_r($model_image->getErrors(), true);
			}
		}


		$this->data['upload_document_message'] = "";
		if (Yii::$app->request->post("current_action") == "upload_document") {
			$model_document = new VillageDocument();
			if ($model_document->load(Yii::$app->request->post()) && $model_document->save()) {
				$this->data['upload_document_message'] = "Документ загружен";
			} else {
				$this->data['upload_document_message'] = print_r($model_document->getErrors(), true);
			}
		}

		if ($this->data['model']->load(Yii::$app->request->post()) && $this->data['model']->save()) {
			$old_images = [];
			foreach ($this->data['model']->images as $old_image) {
				$old_images[$old_image->id] = $old_image;
			}

			foreach (Yii::$app->request->post("images_list") as $key => $image_id) {
				$old_images[$image_id]->sort_order = $key + 1;
				$old_images[$image_id]->save();
				unset($old_images[$image_id]);
			}

			foreach ($old_images as $old_image) {
				$old_image->delete();
			}

			#######################
			$old_images = [];
			foreach ($this->data['model']->progress_images as $old_image) {
				$old_images[$old_image->id] = $old_image;
			}

			foreach (Yii::$app->request->post("progress_images_list") as $key => $image_id) {
				$old_images[$image_id]->sort_order = $key + 1;
				$old_images[$image_id]->save();
				unset($old_images[$image_id]);
			}

			foreach ($old_images as $old_image) {
				$old_image->delete();
			}

			#######################
			$old_documents = [];
			foreach ($this->data['model']->documents as $old_document) {
				$old_documents[$old_document->id] = $old_document;
			}

			foreach (Yii::$app->request->post("documents_list") as $key => $document_id) {
				$old_documents[$document_id]->sort_order = $key + 1;
				$old_documents[$document_id]->title = \yii\helpers\ArrayHelper::getValue(Yii::$app->request->post("documents_list_title"), $document_id);
				$old_documents[$document_id]->save();
				unset($old_documents[$document_id]);
			}

			foreach ($old_documents as $old_document) {
				$old_document->delete();
			}

			Yii::$app->getSession()->setFlash('success', 'Сохранили');
			return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $this->data['model']->id)));
		}

		return $this->render('update');
	}

	public function actionDelete($id) {
		$selected = [];
		if ($id) {
			$selected[] = $id;
		} else {
			$selected = Yii::$app->request->post("selection");
		}

		if ($selected) {
			foreach (Village::find()->where(["id" => $selected])->all() as $model) {
				$model->delete();
			}
		}

		return $this->redirect(Yii::$app->request->get('return', ['index']));
	}

	protected function findModel($id) {
		if (($model = Village::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
