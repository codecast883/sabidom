<?php

namespace backend\controllers;

use Yii;
use common\models\VillageTownhouse;
use common\models\VillageTownhouseImage;
use common\models\VillageTownhouseSearch;
use backend\components\controllers\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VillageTownhouseController implements the CRUD actions for VillageTownhouse model.
 */
class VillageTownhouseController extends BackController {

    public function actionIndex() {
        $searchModel = new VillageTownhouseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new VillageTownhouse();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Сохранили');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $this->data['model'] = $this->findModel($id);



        $upload_image_message = "";
        if (Yii::$app->request->post()['VillageTownhouse']['image'] == 'DELETE_FILE'){

            $connection = Yii::$app->db;

            $connection->createCommand("Update village_townhouse Set image = '' Where id=$id")->execute();

        }

        if (Yii::$app->request->post()['VillageTownhouse']['layout'] == 'DELETE_FILE'){

            $connection = Yii::$app->db;

            $connection->createCommand("Update village_townhouse Set layout = '' Where id=$id")->execute();

        }

        if (Yii::$app->request->post()['VillageTownhouse']['3d'] == 'DELETE_FILE'){

            $connection = Yii::$app->db;

            $connection->createCommand("Update village_townhouse Set 3d = '' Where id=$id")->execute();

        }

        if (Yii::$app->request->post("current_action") == "upload_image") {
            $model_image = new VillageTownhouseImage;
            if ($model_image->load(Yii::$app->request->post()) && $model_image->save()) {
                $upload_image_message = "Изображение загружено";
            } else {
                $upload_image_message = print_r($model_image->getErrors(), true);
            }
        }
        $this->data['upload_image_message'] = $upload_image_message;

        if ($this->data['model']->load(Yii::$app->request->post()) && $this->data['model']->save()) {
            $old_images = [];
            foreach ($this->data['model']->images as $old_image) {
                $old_images[$old_image->id] = $old_image;
            }

            foreach (Yii::$app->request->post("images_list") as $key => $image_id) {
                $old_images[$image_id]->sort_order = $key + 1;
                $old_images[$image_id]->save();
                unset($old_images[$image_id]);
            }

            foreach ($old_images as $old_image) {
                $old_image->delete();
            }
            Yii::$app->getSession()->setFlash('success', 'Сохранили');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $this->data['model']->id)));
        } else {
            return $this->render('update');
        }
    }

    public function actionDelete($id) {
        $selected = [];
        if ($id) {
            $selected[] = $id;
        } else {
            $selected = Yii::$app->request->post("selection");
        }

        if ($selected) {
            foreach (VillageTownhouse::find()->where(["id" => $selected])->all() as $model) {
                $model->delete();
            }
        }

        return $this->redirect(Yii::$app->request->get('return', ['index']));
    }

    protected function findModel($id) {
        if (($model = VillageTownhouse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
