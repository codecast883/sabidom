<?php

namespace backend\controllers;

use Yii;
use common\models\Settings;
use common\models\Company;
use common\models\CompanyReview;
use common\models\CompanyReviewAnswer;
use yii\data\ActiveDataProvider;
use backend\components\controllers\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\helpers\FileHelper;
use yii\helpers\ArrayHelper;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class ServiceController extends BackController {

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionReviews() {
        $action = Yii::$app->request->post('action');
        $data = [];

        $data['ReviewsMove'] = Yii::$app->request->post('ReviewsMove');


        if ($action == "reviews-move") {
            $config = $data['ReviewsMove'];


            $companies_id_from = explode(",", ArrayHelper::getValue($config, 'companies_id_from'));
            
            $companies_from = [];
            foreach ($companies_id_from as $company_id_from) {
                $companies_from[] = Company::findOne($company_id_from);
            }
                        
            $company_to = Company::findOne(ArrayHelper::getValue($config, 'company_id_to'));

            if (!$companies_from) {
                Yii::$app->getSession()->setFlash('error', 'Не указана компания источник');
            }

            if (!$company_to) {
                Yii::$app->getSession()->setFlash('error', 'Не указана компания получатель');
            }

            if ($company_to AND $companies_from) {
                $message = [];
                $message[] = "Переместили";
                foreach ($companies_from as $company_from) {
                    $reviews_count = CompanyReview::find()->where(['company_id' => $company_from->id])->count();
                    $message[] =  "Отзывов (".$company_from->title."): " . $reviews_count;

                    CompanyReview::updateAll(['company_id' => $company_to->id], ['company_id' => $company_from->id]);
                    $company_from->trigger(Company::TRIGGER_REVIEWS_CHANGED);
                    $company_to->trigger(Company::TRIGGER_REVIEWS_CHANGED);

                }
                
                $this->data['companies_from'] = $companies_from;
                $this->data['company_to'] = $company_to;
                Yii::$app->getSession()->setFlash('success', implode("<br>", $message));
            }

            $data['ReviewsMove'] = $config;
        }

        return $this->render('reviews', $data);
    }

    public function actionCache() {
        $categories = [
            'backend/web/assets',
            'backend/runtime/cache',
            'backend/runtime/debug',
            'backend/runtime/logs',
            'frontend/web/assets',
            'frontend/runtime/cache',
            'frontend/runtime/debug',
            'frontend/runtime/logs',
            'frontend/web/img/cache',
        ];

        $action = Yii::$app->request->post('action');

        if ($action == "clear-cache") {
            foreach ($categories as $category) {
                $dir = $_SERVER['DOCUMENT_ROOT'] . "/" . $category;
                if (!($handle = opendir($dir))) {
                    continue;
                }

                while (($file = readdir($handle)) !== false) {
                    if ($file === '.' || $file === '..') {
                        continue;
                    }

                    $path = $dir . DIRECTORY_SEPARATOR . $file;
                    if (is_dir($path)) {
                        if (!FileHelper::removeDirectory($path)) {
                            //print "1: ".$path."\n<br>";
                        }
                    } else {
                        if (!unlink($path)) {
                            //print "2: ".$path."\n<br>";
                        }
                    }
                }

                closedir($handle);
            }
        }


        $data['categories'] = [];
        foreach ($categories as $category) {
            $data['categories'][] = [
                'path' => $category,
                'size' => sprintf("%.2f", ($action == "calc-cache-space" ? FileHelper::dirSize($_SERVER['DOCUMENT_ROOT'] . "/" . $category) / 1024 / 1024 : 0))
            ];
        }


        return $this->render('cache', $data);
    }

}
