<?php

namespace backend\controllers;

use Yii;
use common\models\PersonCategory;
use common\models\Village;
use common\models\VillageImage;
use common\models\Townhouse;
use common\models\TownhouseImage;
use common\models\Cottage;
use common\models\CottageImage;
use common\models\Stead;
use common\models\SteadImage;
use common\models\PersonVideo;
use common\models\PersonToTag;
use common\models\Realt;
use common\models\Party;
use common\models\PartyImage;
use common\models\RealtFieldValue;
use common\models\PartyVideo;
use common\models\Field;
use common\models\FieldTag;
use common\models\FieldValue;
use common\models\ParsePages;
use yii\data\ActiveDataProvider;
use backend\components\controllers\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\parser;
use common\helpers\YText;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class ParserController extends BackController {

    private $msg_errors = [];
    private $msg_info = [];

    public function behaviors() {
        $behaviors = [
            'verbs' => [
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    /**
     * Lists all Settings models.
     * @return mixed
     */
    public function actionIndex() {
        exit;
//        $this->parseRealtFields();
        set_time_limit("6000000");
        //$this->parseRealtFields2();
        //$this->parseImage();
//        $this->parseCompanies();
        //$this->fillCoord();
//        $this->fillUsername();
//        $this->fillPhone();
    }

    private function parseFields() {
        foreach (Village::find()->where(['mark' => 0])->limit(NULL)->each() as $village) {
            if ($dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($village->src_html)) {
                if ($obj = $dom->find('.catalog-item-properties__list', 0)) {
                    $text = trim($obj->plaintext);

                    if (preg_match("/Готовность:(.*?)\n|	/isu", $text, $matches)) {
                        $field = Field::autoGet("Готовность", Field::TYPE_LIST);
                        if ($value = trim($matches[1])) {
//                            print_r("Готовность: " . $value . "\n");

                            VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);
                            $field_value = new VillageFieldValue;
                            $field_tag = FieldTag::autoGet($field->id, $value);
                            $field_value->field_id = $field->id;
                            $field_value->field_tag_id = $field_tag->id;
                            $field_value->village_id = $village->id;
                            if (!$field_value->save()) {
                                print_r($field_value->getErrors());
                                exit;
                            }
                        }
                    }

                    if (preg_match("/Лес:([^\n	]*)/isu", $text, $matches)) {
                        $field = Field::autoGet("Лес", Field::TYPE_LIST);
                        $value = trim($matches[1]);
                        //                      print_r("Лес: " . $value . "\n");

                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);
                        $field_value = new VillageFieldValue;
                        $field_tag = FieldTag::autoGet($field->id, $value);
                        $field_value->field_id = $field->id;
                        $field_value->field_tag_id = $field_tag->id;
                        $field_value->village_id = $village->id;
                        if (!$field_value->save()) {
                            print_r($field_value->getErrors());
                            exit;
                        }
                    }

                    if (preg_match("/Водоём:([^\n	]*)/isu", $text, $matches)) {
                        $field = Field::autoGet("Водоём", Field::TYPE_LIST);
                        $values = trim($matches[1]);
//                        print_r("Водоём: " . $values . "\n");

                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);
                        foreach (explode(",", $values) as $value) {
                            $field_tag = FieldTag::autoGet($field->id, $value);
                            $field_value = new VillageFieldValue;
                            $field_value->field_id = $field->id;
                            $field_value->field_tag_id = $field_tag->id;
                            $field_value->village_id = $village->id;
                            if (!$field_value->save()) {
                                print_r($field_value->getErrors());
                                exit;
                            }
                        }
                    }

                    if (preg_match("/Стоимость проживания:([^\n	]*)/isu", $text, $matches)) {
                        $field = Field::autoGet("Стоимость проживания", Field::TYPE_TEXT);
                        $value = trim($matches[1]);
//                        print_r("Стоимость проживания: " . $value . "\n");

                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);
                        $field_value = new VillageFieldValue;
                        $field_value->field_id = $field->id;
                        $field_value->village_id = $village->id;
                        $field_value->value = $value;
                        if (!$field_value->save()) {
                            print_r($field_value->getErrors());
                            exit;
                        }
                    }

                    if (preg_match("/Дома([^\n	]*)/isu", $text, $matches)) {
                        $values = trim($matches[1]);
//                            print $values . "\n";
                        $field = Field::autoGet("Дома", Field::TYPE_FROM_TO);

                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);

                        $field_value = new VillageFieldValue;
                        $field_value->field_id = $field->id;
                        $field_value->village_id = $village->id;
                        if (preg_match('/от\s+(\d+?)\s+до\s+(\d+)/simx', $values, $regs)) {
                            $field_value->value_from = $regs[1];
                            $field_value->value_to = $regs[2];
                        } elseif (preg_match('/от\s+(\d+)/simx', $values, $regs)) {
                            $field_value->value_from = $regs[1];
                        } elseif (preg_match('/до\s+(\d+)/simx', $values, $regs)) {
                            $field_value->value_to = $regs[1];
                        }

                        if (!$field_value->save()) {
                            print_r($field_value->getErrors());
                            exit;
                        }
                    }
                    if (preg_match("/Участки([^\n	]*)/isu", $text, $matches)) {
                        $values = trim($matches[1]);
                        //                      print $values . "\n";
                        $field = Field::autoGet("Участки", Field::TYPE_FROM_TO);

                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);

                        $field_value = new VillageFieldValue;
                        $field_value->field_id = $field->id;
                        $field_value->village_id = $village->id;
                        if (preg_match('/от\s+(\d+?)\s+до\s+(\d+)/simx', $values, $regs)) {
                            $field_value->value_from = $regs[1];
                            $field_value->value_to = $regs[2];
                        } elseif (preg_match('/от\s+(\d+)/simx', $values, $regs)) {
                            $field_value->value_from = $regs[1];
                        } elseif (preg_match('/до\s+(\d+)/simx', $values, $regs)) {
                            $field_value->value_to = $regs[1];
                        }

//                        print $field_value->value_from . " + " . $field_value->value_to . "\n";

                        if (!$field_value->save()) {
                            print_r($field_value->getErrors());
                            exit;
                        }
                    }
                    if (preg_match("/Домов\/участков:\s+(\d+)/isu", $text, $matches)) {
                        $field = Field::autoGet("Домов/участков");
                        $value = trim($matches[1]);
//                       print_r("Домов/участков: " . $value . "\n");

                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);
                        $field_value = new VillageFieldValue;
                        $field_value->field_id = $field->id;
                        $field_value->village_id = $village->id;
                        $field_value->value = $value;
                        if (!$field_value->save()) {
                            print_r($field_value->getErrors());
                            exit;
                        }
                    }


                    if (preg_match("/Назначение земли:  (.+)/isu", $text, $matches)) {
                        $field = Field::autoGet("Назначение земли", Field::TYPE_LIST);
                        $value = trim($matches[1]);
//                        print_r("Назначение земли: " . $value . "\n");

                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);
                        $field_value = new VillageFieldValue;
                        $field_tag = FieldTag::autoGet($field->id, $value);
                        $field_value = new VillageFieldValue;
                        $field_value->field_id = $field->id;
                        $field_value->field_tag_id = $field_tag->id;
                        $field_value->village_id = $village->id;
                        if (!$field_value->save()) {
                            print_r($field_value->getErrors());
                            exit;
                        }
                    }
                    if (preg_match("/Район:  ([^ ]+) /isu", $text, $matches)) {
                        $value = trim($matches[1]);
//                        print_r("Район: " . $value . "\n");

                        $field = Field::autoGet("Район", Field::TYPE_LIST);
                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);
                        $field_value = new VillageFieldValue;
                        $field_tag = FieldTag::autoGet($field->id, $value);
                        $field_value = new VillageFieldValue;
                        $field_value->field_id = $field->id;
                        $field_value->field_tag_id = $field_tag->id;
                        $field_value->village_id = $village->id;
                        if (!$field_value->save()) {
                            print_r($field_value->getErrors());
                            exit;
                        }
                    }
                    if (preg_match("/Шоссе:  (.+) /isu", $text, $matches)) {
                        $value = trim($matches[1]);
                        $variants = ['Алтуфьевское', 'Калужское', 'Ильинское', 'Новорижское', 'Рублево-Успенское', 'Каширское', 'Горьковское', 'Щелковское', 'Ярославское', 'Егорьевское', 'Варшавское', 'Ленинградское', 'Симферопольское', 'Носовихинское', 'Дмитровское', 'Киевское', 'Рязанское', 'Пятницкое', 'Волоколамское', 'Минское', 'Можайское', 'Осташковское', 'Боровское', 'Сколковское', 'Фряновское', 'Рогачевское', 'Новосходненское', 'Новорязанское', 'Красноармейское', 'Куркинское', 'Лихачевское', 'Павелецкое',];
                        $field = Field::autoGet("Шоссе", Field::TYPE_LIST);
                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);
                        foreach ($variants as $variant) {
                            if (preg_match("|" . preg_quote($variant) . "|isu", $value)) {
                                $field_value = new VillageFieldValue;
                                $field_tag = FieldTag::autoGet($field->id, $variant);
                                $field_value = new VillageFieldValue;
                                $field_value->field_id = $field->id;
                                $field_value->field_tag_id = $field_tag->id;
                                $field_value->village_id = $village->id;
                                if (!$field_value->save()) {
                                    print_r($field_value->getErrors());
                                    exit;
                                }
                            }
                        }
                    }


                    if (preg_match("/Виды предложений:  (.+)/isu", $text, $matches)) {
                        $value = trim($matches[1]);
//                        print $value . "\n";

                        $field = Field::autoGet("Виды предложений", Field::TYPE_LIST);
                        VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);
                        $variants = [ "участки без подряда", "участки с подрядом",
                            "коттеджи  под отделку", "коттеджи с отделкой", "строящиеся коттеджи",
                            "квартиры под отделку", "квартиры с отделкой", "строящиеся квартиры",
                            "таунхаусы под отделку", "таунхаусы с отделкой", "строящиеся таунхаусы",
                            "дуплексы под отделку", "дуплексы с отделкой", "строящиеся дуплексы"];
                        foreach ($variants as $variant) {
                            if (preg_match("|" . preg_quote($variant) . "|isu", $value)) {
                                $field_value = new VillageFieldValue;
                                $field_tag = FieldTag::autoGet($field->id, $variant);
                                $field_value = new VillageFieldValue;
                                $field_value->field_id = $field->id;
                                $field_value->field_tag_id = $field_tag->id;
                                $field_value->village_id = $village->id;
                                if (!$field_value->save()) {
                                    print_r($field_value->getErrors());
                                    exit;
                                }
                            }
                        }
                    }
                }
            }
            $village->updateAttributes(['mark' => 1]);
        }
    }

    private function parseRealtFields() {
//      +  Лес Лесной участок
//      +  Населённый пункт: Уварово
//      +  Район: Одинцовский
//        Шоссе: Рублево-Успенское (10-20 км)
//        посёлок: ИРБИС
//        назначение земли

        foreach (Realt::find()->where(['mark' => 0])->limit(600)->each() as $realt) {
            if ($dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($realt->src_html)) {
//                print "<br><br>" . $realt->id . ": ";
                //print "<hr>" . $realt->src_html . "<br><br><br>";
                if ($objs = $dom->find('.catalog-item-properties__list')) {
                    foreach ($objs as $obj) {
                        $text = trim($obj->plaintext);
                        //print "<hr>" . $text . "<br><br><br>";

                        if (preg_match("/Населённый пункт:  ([^\n]+)/isu", $text, $matches)) {
                            $field = Field::autoGet("Населённый пункт", Field::TYPE_LIST);
                            if ($value = trim($matches[1])) {
//                                print_r("Населенный пункт: " . $value . "<br>\n");
                                RealtFieldValue::deleteAll(['realt_id' => $realt->id, 'field_id' => $field->id]);
                                $field_value = new RealtFieldValue;
                                $field_tag = FieldTag::autoGet($field->id, $value);
                                $field_value->field_id = $field->id;
                                $field_value->field_tag_id = $field_tag->id;
                                $field_value->realt_id = $realt->id;
                                if (!$field_value->save()) {
                                    print_r($field_value->getErrors());
                                    exit;
                                }
                            }
                        }

                        if (preg_match("/посёлок:  ([^\n]+)/isu", $text, $matches)) {
                            $field = Field::autoGet("Поселок", Field::TYPE_TEXT);
                            if ($value = trim($matches[1])) {
                                //print_r("посёлок: " . $value . "<br>\n");
                                if ($village = Village::find()->where(['title' => trim($value)])->one()) {
                                    $realt->updateAttributes(['village_id' => $value->id]);
                                }
                            }
                        }

                        if (preg_match("/Назначение земли:(.+)/isu", $text, $matches)) {
                            $field = Field::autoGet("Назначение земли", Field::TYPE_LIST);

                            if ($value = trim($matches[1])) {
                                //print_r("Назначение земли:");
                                $variants = [];
                                foreach ($field->getFieldTags()->asArray()->all() as $tag) {
                                    $variants[] = $tag['value'];
                                }

                                RealtFieldValue::deleteAll(['realt_id' => $realt->id, 'field_id' => $field->id]);
                                foreach ($variants as $variant) {
                                    if (preg_match("|" . preg_quote($variant) . "|isu", $value)) {
                                        //print $value . "--";
                                        $field_value = new RealtFieldValue;
                                        $field_tag = FieldTag::autoGet($field->id, $value);
                                        $field_value->field_id = $field->id;
                                        $field_value->field_tag_id = $field_tag->id;
                                        $field_value->realt_id = $realt->id;
                                        if (!$field_value->save()) {
                                            print_r($field_value->getErrors());
                                            exit;
                                        }
                                    }
                                }
                            }
                        }

                        if (preg_match("/Ближайшая ж\/д станция:  ([^\n]+)\n/isu", $text, $matches)) {
                            $field = Field::autoGet("Ближайшая ж/д станция", Field::TYPE_LIST);
                            if ($value1 = trim($matches[1])) {
                                if (preg_match("/(.*), (\d+\.\d+) км/isu", $value1, $matches)) {
                                    //print_r("Ближайшая ж/д станция: " . $matches[1] . " " . $matches[2] . "<br>\n");
                                    RealtFieldValue::deleteAll(['realt_id' => $realt->id, 'field_id' => $field->id]);
                                    $field_value = new RealtFieldValue;
                                    $field_tag = FieldTag::autoGet($field->id, $matches[1]);
                                    $field_value->field_id = $field->id;
                                    $field_value->field_tag_id = $field_tag->id;
                                    $field_value->realt_id = $realt->id;
                                    $field_value->distance = $matches[2];
                                    if (!$field_value->save()) {
                                        print_r($field_value->getErrors());
                                        exit;
                                    }
                                }
                            }
                        }

                        if (preg_match("/Лес\s([^\n	]*)/isu", $text, $matches)) {
                            $field = Field::autoGet("Лес", Field::TYPE_LIST);
                            if ($value = trim($matches[1])) {
                                //print_r("Лес: " . $value . "<br>\n");
                                RealtFieldValue::deleteAll(['realt_id' => $realt->id, 'field_id' => $field->id]);
                                $field_value = new RealtFieldValue;
                                $field_tag = FieldTag::autoGet($field->id, $value);
                                $field_value->field_id = $field->id;
                                $field_value->field_tag_id = $field_tag->id;
                                $field_value->realt_id = $realt->id;
                                if (!$field_value->save()) {
                                    print_r($field_value->getErrors());
                                    exit;
                                }
                            }
                        }

                        if (preg_match("/Водоём\s([^\n]*)/isu", $text, $matches)) {
                            $field = Field::autoGet("Водоём", Field::TYPE_LIST);
                            $values = trim($matches[1]);
//                            print_r("Водоём: " . $values . "\n");

                            RealtFieldValue::deleteAll(['realt_id' => $realt->id, 'field_id' => $field->id]);
                            foreach (explode(",", $values) as $value) {
                                $field_tag = FieldTag::autoGet($field->id, $value);
                                $field_value = new RealtFieldValue;
                                $field_value->field_id = $field->id;
                                $field_value->field_tag_id = $field_tag->id;
                                $field_value->realt_id = $realt->id;
                                if (!$field_value->save()) {
                                    print_r($field_value->getErrors());
                                    exit;
                                }
                            }
                        }

                        if (preg_match("/Район:  ([^ ]+) /isu", $text, $matches)) {
                            $field = Field::autoGet("Район", Field::TYPE_LIST);
                            if ($value = trim($matches[1])) {
//                                print_r("Район: " . $value . "<br>\n");
                                RealtFieldValue::deleteAll(['realt_id' => $realt->id, 'field_id' => $field->id]);
                                $field_value = new RealtFieldValue;
                                $field_tag = FieldTag::autoGet($field->id, $value);
                                $field_value->field_id = $field->id;
                                $field_value->field_tag_id = $field_tag->id;
                                $field_value->realt_id = $realt->id;
                                if (!$field_value->save()) {
                                    print_r($field_value->getErrors());
                                    exit;
                                }
                            }
                        }

                        if (preg_match("/Шоссе:(.+)/isu", $text, $matches)) {
                            $value = trim($matches[1]);
                            $variants = ['Алтуфьевское', 'Калужское', 'Ильинское', 'Новорижское', 'Рублево-Успенское', 'Каширское', 'Горьковское', 'Щелковское', 'Ярославское', 'Егорьевское', 'Варшавское', 'Ленинградское', 'Симферопольское', 'Носовихинское', 'Дмитровское', 'Киевское', 'Рязанское', 'Пятницкое', 'Волоколамское', 'Минское', 'Можайское', 'Осташковское', 'Боровское', 'Сколковское', 'Фряновское', 'Рогачевское', 'Новосходненское', 'Новорязанское', 'Красноармейское', 'Куркинское', 'Лихачевское', 'Павелецкое'];
                            $field = Field::autoGet("Шоссе", Field::TYPE_LIST);
                            RealtFieldValue::deleteAll(['realt_id' => $realt->id, 'field_id' => $field->id]);
                            foreach ($variants as $variant) {
                                if (preg_match("|" . preg_quote($variant) . "|isu", $value)) {
                                    //        print "---" . $variant . "---";
                                    $field_value = new RealtFieldValue;
                                    $field_tag = FieldTag::autoGet($field->id, $variant);
                                    $field_value->field_id = $field->id;
                                    $field_value->field_tag_id = $field_tag->id;
                                    $field_value->realt_id = $realt->id;
                                    if (!$field_value->save()) {
                                        print_r($field_value->getErrors());
                                        exit;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $realt->updateAttributes(['mark' => 1]);
        }

        print "OK";
    }

    private function parseFields2() {
        foreach (Village::find()->where(['mark' => 0])->limit(NULL)->each() as $village) {
            if ($dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($village->src_html)) {
                if ($objs = $dom->find('.communications-list .communications-list__item')) {
                    foreach ($objs as $obj) {
                        $label = "";
                        if ($obj_label = $obj->find(".communications-list__label", 0)) {
                            $label = trim($obj_label->plaintext);
                        }


                        $values = preg_replace("%" . preg_quote($label) . "%isu", "", $obj->plaintext);
                        $label = trim(trim($label, ":"));
                        if ($label AND $values) {
                            $field = Field::autoGet($label, Field::TYPE_LIST, 2);

                            VillageFieldValue::deleteAll(['village_id' => $village->id, 'field_id' => $field->id]);

                            foreach (explode(",", $values) as $value) {
                                $value = trim(trim($value, ","));

                                $field_tag = FieldTag::autoGet($field->id, $value);
                                $field_value = new VillageFieldValue;
                                $field_value->field_id = $field->id;
                                $field_value->field_tag_id = $field_tag->id;
                                $field_value->village_id = $village->id;
                                if (!$field_value->save()) {
                                    print_r($field_value->getErrors());
                                    exit;
                                }
                            }
                        }
                    }
                }
            }

            $village->updateAttributes(['mark' => 1]);
        }
    }

    private function parseRealtFields2() {
        foreach (Realt::find()->where(['mark' => 0])->limit(500)->each() as $realt) {
            print $realt->id . " ";
            if ($dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($realt->src_html)) {
                if ($objs = $dom->find('.communications-list .communications-list__item')) {
                    foreach ($objs as $obj) {
                        $label = "";
                        if ($obj_label = $obj->find(".communications-list__label", 0)) {
                            $label = trim($obj_label->plaintext);
                        }

                        $values = preg_replace("%" . preg_quote($label) . "%isu", "", $obj->plaintext);
                        $label = trim(trim($label, ":"));
                        if ($label AND $values) {
                            print $label . ": " . $values . "<br>";

                            $field = Field::autoGet($label, Field::TYPE_LIST, 2);
                            RealtFieldValue::deleteAll(['realt_id' => $realt->id, 'field_id' => $field->id]);
                            foreach (explode(",", $values) as $value) {
                                $value = trim(trim($value, ","));

                                $field_tag = FieldTag::autoGet($field->id, $value);

                                $field_value = new RealtFieldValue;
                                $field_value->field_id = $field->id;
                                $field_value->field_tag_id = $field_tag->id;
                                $field_value->realt_id = $realt->id;
                                if (!$field_value->save()) {
                                    print_r($field_value->getErrors());
                                    exit;
                                }
                            }
                        }
                    }
                }
            }

            $realt->updateAttributes(['mark' => 1]);
        }

        print "OK";
    }

    private function fillCoord() {
        foreach (Realt::find()->where(['type_id' => 4])->limit(NULL)->each() as $model) {
            if (preg_match('/data-ymap-config=\'\{"zoom":\d+,\s"center":\[(\d+\.\d+),(\d+\.\d+)\]\}\'/simx', $model->src_html, $regs)) {
                $model->coord_lat = $regs[1];
                $model->coord_lng = $regs[2];
                if (!$model->save()) {
                    print_r($model->getErrors());
                    exit;
                }
            }
        }
    }

    private function fillUsername() {
        foreach (Realt::find()->where(['mark' => 0])->limit(NULL)->each() as $model) {
            if ($dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($model->src_html)) {
                if ($obj = $dom->find('.agent-item-small', 0)) {
                    $erase = "";
                    if ($obj2 = $obj->find(".agent-item-small__phone", 0)) {
                        $erase = $obj2->plaintext;
                    }


                    $text = preg_replace("%" . preg_quote($erase) . "%isu", "", $obj->plaintext);
                    $model->agent_name = trim($text);
                    $model->save();
                }
            }
        }
    }

    private function fillPhone() {
        foreach (Realt::find()->where(['type_id' => 4])->limit(NULL)->each() as $model) {
            if ($dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($model->src_html)) {
                if ($obj = $dom->find('.showPhone', 0)) {
                    $id = trim($obj->attr['data-id']);
                    $json = json_decode(file_get_contents("http://www.domzamkad.ru/adv/phone/" . $id));

                    $model->mark = 1;
                    $model->phone = $json->phone;
                    $model->updateAttributes(['phone' => $model->phone]);
                }
            }
        }
    }

    private function fillPrices() {
        foreach (Realt::find()->where(['mark' => 0])->limit(NULL)->each() as $model) {
            if ($dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($model->src_html)) {
                if ($obj = $dom->find('.catalog-item-properties__price', 0)) {
                    $price_line = trim($obj->plaintext);
                    print $price_line . ": ";


                    if (preg_match('/^((\d+\s)+)/simx', $price_line, $regs)) {
                        $model->price = preg_replace("|\s|isu", "", $regs[1]);
                        print $model->price;
                    }

                    print " - ";

                    if (preg_match('/Дом\s(\d+(,\d+){0,10})\sм2/simx', $price_line, $regs)) {
                        $model->house_area = preg_replace("|\s|isu", "", $regs[1]);
                        print $model->house_area;
                    }

                    print " - ";

                    if (preg_match('/Участок\s(\d+(,\d+){0,10})\sсот/simx', $price_line, $regs)) {
                        $model->stead_area = preg_replace("|\s|isu", "", $regs[1]);
                        print $model->stead_area;
                    }

                    $model->mark = 1;

                    if (!$model->save()) {
                        print_r($model->getErrors());
                    }
                }
            }

            print "<br>";
        }
    }

    private function parseImage() {
//        foreach (\common\models\RealtImage::find()->where(['image' => ''])->all() as $image) {
//            $realt = Realt::findOne($image->realt_id);
//            $realt->mark = 0;
//            $realt->save();
//
//            $image->delete();
//        }

        foreach (Realt::find()->where(['mark' => 0])->each() as $realt) {
            foreach ($realt->images as $image) {
                $image->delete();
            }

            if ($dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($realt->src_html)) {
                $i = 0;
                foreach ($dom->find("#settlementSlider a.popup") as $obj) {
                    if (!empty($obj->attr['href'])) {
                        $href = $obj->attr['href'];
                        $href = preg_replace("|/cache|isu", "", $href);
                        $href = preg_replace("|/w800h600_wm.jpg|isu", "", $href);
                        $href = preg_replace("|/w800h600.jpg|isu", "", $href);
                        $href = preg_replace("|/w800h600_wm.PNG|isu", "", $href);
                        $href = preg_replace("|/w800h600.jpeg|isu", "", $href);
                        $href = preg_replace("|/w800h600_wm.jpeg|isu", "", $href);
                        $href = preg_replace("|/w800h600.png|isu", "", $href);
                        $href = preg_replace("|/w800h600.gif|isu", "", $href);

                        $image = new \common\models\RealtImage;
                        $image->realt_id = $realt->id;
                        $image->image = $href;
                        $image->sort_order = $i++;
                        $image->save();
                        if (!$image->image) {
                            print_r($realt->src_url . "<br>");
                            print_r($href);

//                            exit;
                        }
                    }
                }
            }

            $realt->updateAttributes(['mark' => 1]);
        }
    }

    private function fixDescriptions() {
//        foreach (Realt::find()->select(['id', 'description'])->where(['mark' => 0])->asArray()->each(10) as $realt) {
//            if ($realt['description']) {
//                $dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($realt['description']);
//
//                # рубим ссылочки
//                foreach ($dom->find("a") as $obj) {
//                    $obj->outertext = $obj->innertext;
//                }
//
//                foreach ($dom->find("script") as $obj) {
//                    $obj->outertext = "";
//                }
//                foreach ($dom->find("*") as $obj) {
//                    if (!empty($obj->attr['style'])) {
//                        if (preg_match("|display: none|isu", $obj->attr['style'])) {
//                            $obj->outertext = "";
//                        }
//                    }
//                }
//                $realt['description'] = $dom->outertext;
//
//                $realt['description'] = preg_replace("|<!-- BEGIN CLICKTEX CODE {literal} -->    <!-- {/literal} END CLICKTEX CODE -->|isu", "", $realt['description']);
//                $realt['description'] = preg_replace("|<!--noindex-->|isu", "", $realt['description']);
//                $realt['description'] = preg_replace("|<!--/noindex-->|isu", "", $realt['description']);
//                $realt['description'] = trim($realt['description']);
//            }
//            
//            Realt::updateAll(['description' => $realt['description'], 'mark' => 1], ['id' => $realt['id']]);
//        }
    }

    private function fillParsePages() {
        $page = new ParsePages;
        $page->mark = 0;
        $page->type_id = 7;
        $page->url = "http://www.domzamkad.ru/townhouses/";
        $page->save();

        for ($i = 2; $i <= 25; $i++) {
            $page = new ParsePages;
            $page->mark = 0;
            $page->type_id = 7;
            $page->url = "http://www.domzamkad.ru/townhouses/" . $i . ".html";
            $page->save();
        }
    }

    private function parseCompanies() {
        foreach (ParsePages::find()->where(['mark' => 0, 'type_id' => 8])->limit(10000000)->all() as $page) {
            $this->parseCompany($page->url);
            $page->updateAttributes(['mark' => 1]);
        }
    }

    private function parseCompany($page) {
        $html = file_get_contents($page);
        $dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);

        if (!$realt = Realt::find()->where(['src_url' => $page])->one()) {
            $realt = new Realt();
            $realt->src_url = $page;
            $realt->type_id = 4;

            $realt->src_html = $dom->find(".page-content", 0)->innertext;

            if ($obj = $dom->find(".page-header h1", 0)) {
                $realt->title = trim($obj->plaintext);
            }

            if ($obj = $dom->find(".page-header .page-title__sub", 0)) {
                $realt->subtitle = trim($obj->plaintext);
            }

            if ($obj = $dom->find(".catalog-item-description", 0)) {
                $realt->description = trim($obj->innertext);
            }

//            print "<table class='table'>";
//            print "<tr><td>Имя</td><td>". $realt->title."</td></tr>";
//            print "<tr><td>Картинка</td><td>". $realt->subtitle."</td></tr>";
//            print "<tr><td>Место рождения</td><td>". $realt->description."</td></tr>";
//            print "<tr><td>День рождения</td><td>". $realt->src_html."</td></tr>";
//            print "<tr><td>Пол</td><td>". $person->sex."</td></tr>";
//            print "<tr><td>Описание краткое</td><td>".  $person->description_short."</td></tr>";
//            print "<tr><td>Описание полное</td><td>". $person->description."</td></tr>";
//            print "</table>";

            if (!$realt->save()) {
                print_r($realt->getErrors());
                print_r($realt);
                return;
            }
        }


//        foreach (PersonImage::find()->where(["person_id" => $realt->id])->all() as $image) {
//            $image->delete();
//        }
//
//        if ($obj = $dom->find(".person-content-wrapper1 .related-to li", 0)) {
//            if ($photos_page_obj = $obj->find("a", 0)) {
//                foreach ($this->_parsePhotos("http://perebezhchik.ru" . $photos_page_obj->attr['href']) as $photo) {
//                    $image = new PersonImage;
//                    $image->image = "http://perebezhchik.ru" . $photo;
//                    $image->person_id = $realt->id;
//                    if (!$image->save()) {
//                        print_r($image->getErrors());
//                        print_r($image);
//                        exit;
//                    }
//                }
//            }
//        }
//
//        foreach (PersonVideo::find()->where(["person_id" => $realt->id])->all() as $video) {
//            $video->delete();
//        }
//
//        if ($obj = $dom->find(".person-content-wrapper1 .related-to li", 1)) {
//            if ($videos_page_obj = $obj->find("a", 0)) {
//                foreach ($this->_parseVideos("http://perebezhchik.ru" . $videos_page_obj->attr['href']) as $src_video) {
//                    $video = new PersonVideo();
//                    $video->image = "http://perebezhchik.ru" . $src_video['poster'];
//                    $video->video = "http://perebezhchik.ru" . $src_video['video'];
//                    $video->person_id = $realt->id;
//                    if (!$video->save()) {
//                        print_r($video->getErrors());
//                        print_r($video);
//                        exit;
//                    }
//                }
//            }
//        }
    }

    private function _parsePhotos($page) {
        $html = file_get_contents($page);
        $dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);

        $photos = [];
        if ($elements = $dom->find(".gallery-list .image a")) {
            foreach ($elements as $element) {
                $photos[] = $element->attr["href"];
            }
        }

        return $photos;
    }

    private function _parseVideos($page) {
        $html = file_get_contents($page);
        $dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);

        $videos = [];
        if ($elements = $dom->find(".gallery-list li video")) {
            foreach ($elements as $element) {
                $video['poster'] = $element->attr['poster'];
                $video['video'] = "";

                if ($video_obj = $element->find("source", 0)) {
                    $video['video'] = $video_obj->attr['src'];
                }

                $videos[] = $video;
            }
        }

        return $videos;
    }

    /**
     * разбираем листинги категорий и достаем ссылки на страницы людей
     */
    private function parseCompanyLinks() {
        foreach (ParsePages::find()->where(['mark' => 0, 'type_id' => 7])->all() as $page) {
            foreach ($this->parseLinks($page->url) as $link) {
                if (!ParsePages::find()->where(['url' => $link])->count()) {
                    $new_page = new ParsePages;
                    $new_page->type_id = 8;
                    $new_page->mark = 0;
                    $new_page->url = $link;
                    if (!$new_page->save()) {
                        print_r($new_page->getErrors());
                        exit;
                    }
                }
            }

            $page->updateAttributes(['mark' => 1]);
        }
    }

    private function parseLinks($url) {
        $links = [];

        $dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html(file_get_contents($url));

        if ($items = $dom->find(".catalog-list__item__annotation-cell .catalog-list__item__title a")) {
            foreach ($items as $item) {
                $links[] = "http://www.domzamkad.ru" . $item->attr['href'];
            }
        }

        return $links;
    }

    private function _parseDate($date) {
        $ru_month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $en_month = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");

        if (preg_match('/(\d+)\s+(.*)\s+(\d+)/sim', $date, $regs)) {
            return trim($regs[3]) . "-" . trim(str_replace($ru_month, $en_month, $regs[2])) . "-" . trim($regs[1]);
        }

        return "";
    }

}
