<?php

namespace backend\controllers;

use Yii;
use common\models\Article;
use common\models\ArticleSearch;
use backend\components\controllers\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends BackController {

    public function actionIndex() {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Сохранили');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Сохранили');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $selected = [];
        if ($id) {
            $selected[] = $id;
        } else {
            $selected = Yii::$app->request->post("selection");
        }

        if ($selected) {
            foreach (Article::find()->where(["id" => $selected])->all() as $model) {
                $model->delete();
            }
        }

        return $this->redirect(Yii::$app->request->get('return', ['index']));
    }

    protected function findModel($id) {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
