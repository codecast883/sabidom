<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace backend\controllers;

use app\components\UserManager;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AdminController allows you to administrate users.
 *
 * @property \dektrium\user\Module $module
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */
class UserController extends \backend\components\controllers\BackController {

	/** @inheritdoc */
	public function behaviors() {
		$behaviors = [
			'verbs' => [
				'actions' => [
					'delete' => ['post'],
					'confirm' => ['post'],
					'block' => ['post']
				],
			],
		];

		return array_merge_recursive(parent::behaviors(), $behaviors);
	}

	/**
	 * Lists all User models.
	 * @return mixed
	 */
	public function actionIndex() {
		$dataProvider = new ActiveDataProvider([
			'query' => User::find(),
		]);

		return $this->render('index', [
					'dataProvider' => $dataProvider
		]);
	}

	public function actionCreate() {
		$model = new \common\models\User(['scenario' => 'create']);

		if ($model->load(\Yii::$app->request->post()) && $model->create()) {
			\Yii::$app->getSession()->setFlash('success', 'User has been created');
			return $this->redirect(['index']);
		}

		return $this->render('create', [
					'model' => $model
		]);
	}

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'index' page.
	 * @param  integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		$model->scenario = 'update';

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->getSession()->setFlash('success', 'User has been updated');
			return $this->refresh();
		}

		return $this->render('update', [
					'model' => $model
		]);
	}

	/**
	 * Confirms the User.
	 * @param $id
	 * @return \yii\web\Response
	 */
	public function actionConfirm($id) {
		$this->findModel($id)->confirm();
		\Yii::$app->getSession()->setFlash('success', 'User has been confirmed');

		return $this->redirect(['update', 'id' => $id]);
	}

	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param  integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		\Yii::$app->getSession()->setFlash('user.success', 'User has been deleted');

		return $this->redirect(['index']);
	}

	/**
	 * Blocks the user.
	 *
	 * @param $id
	 * @return \yii\web\Response
	 */
	public function actionBlock($id) {
		$user = $this->findModel($id);
		if ($user->getIsBlocked()) {
			$user->unblock();
			\Yii::$app->getSession()->setFlash('user.success', 'User has been unblocked');
		} else {
			$user->block();
			\Yii::$app->getSession()->setFlash('user.success', 'User has been blocked');
		}

		return $this->redirect(['index']);
	}

	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param  integer                    $id
	 * @return \dektrium\user\models\User the loaded model
	 * @throws NotFoundHttpException      if the model cannot be found
	 */
	protected function findModel($id) {
		$user = User::findOne($id);

		if ($user === null) {
			throw new NotFoundHttpException('The requested page does not exist');
		}

		return $user;
	}

	/**
	 * Logs the user out and then redirects to the homepage.
	 *
	 * @return \yii\web\Response
	 */
	public function actionLogout() {
		\Yii::$app->getUser()->logout();
		return $this->goHome();
	}

}
