<?php

namespace backend\controllers;

use Yii;
use common\models\Seo;
use common\models\SeoParam;
use yii\data\ActiveDataProvider;
use backend\components\controllers\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SeoController implements the CRUD actions for Seo model.
 */
class SeoController extends BackController {

    static $default_fields_config = [
        'title' => [
            'template' => '',
            'field_type' => 'textInput',
            'sort_order' => 10,
            'alias' => 'Title',
        ],
        'h1' => [
            'template' => '',
            'field_type' => 'textInput',
            'sort_order' => 20,
            'alias' => 'H1',
        ],
        'h2' => [
            'template' => '',
            'field_type' => 'textInput',
            'sort_order' => 20,
            'alias' => 'H2',
        ],
        'breadcrumb' => [
            'template' => '',
            'field_type' => 'textInput',
            'sort_order' => 30,
            'alias' => 'Хлебная крошка',
        ],
        'description' => [
            'template' => '',
            'field_type' => 'textarea',
            'sort_order' => 40,
            'alias' => 'Description',
        ],
        'keywords' => [
            'template' => '',
            'field_type' => 'textInput',
            'sort_order' => 50,
            'alias' => 'Keywords',
        ],
        'description_top' => [
            'template' => '',
            'field_type' => 'CKEditor',
            'sort_order' => 60,
            'alias' => 'Описание (верх)',
        ],
        'description_bottom' => [
            'template' => '',
            'field_type' => 'CKEditor',
            'sort_order' => 70,
            'alias' => 'Описание (низ)',
        ],
    ];

    public function actionIndex() {
        //$this->data['data_provider'] = new ActiveDataProvider(['query' => Seo::find()->where(['type_id' => 2])]);
        $this->data['data_provider'] = new ActiveDataProvider(['query' => Seo::find()]);

        return $this->render('index');
    }

    public function actionSections() {
        $this->data['data_provider'] = new ActiveDataProvider(['query' => Seo::find()->where(['type_id' => 1])]);

        return $this->render('index');
    }

    public function actionCreate() {
        $model = new Seo();
        $model->route = Yii::$app->request->get('route');
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->fixParams($model->id);

            Yii::$app->getSession()->setFlash('success', 'Сохранили');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionFixAll() {
        foreach (Seo::find()->all() as $seo) {
            $this->fixParams($seo->id);
        }

        Yii::$app->getSession()->setFlash('success', 'Схемы восстановлены');
        return $this->redirect(['index']);
    }

    private function fixParams($seo_id) {
        foreach (SeoParam::find()->where(['seo_id' => $seo_id])->all() as $old_param) {
            if (empty(self::$default_fields_config[$old_param->field])) {
                $old_param->delete();
            }
        }

        foreach (self::$default_fields_config as $field => $values) {
            if (!$check_param = SeoParam::find()->where(['seo_id' => $seo_id, 'field' => $field])->one()) {
                $check_param = new SeoParam;
                $check_param->seo_id = $seo_id;
                $check_param->field = $field;
                $check_param->template = $values['template'];
            }

            $check_param->field_type = $values['field_type'];
            $check_param->sort_order = $values['sort_order'];
            $check_param->alias = $values['alias'];
            if (!$check_param->save()) {
                print_r($check_param->getErrors());
            }
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $action = Yii::$app->request->post("action");
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            foreach (Yii::$app->request->post("SeoParam", []) as $param_id => $value) {
                if ($param_model = SeoParam::findOne($param_id)) {
                    $param_model->template = $value;
                    $param_model->save();
                }
            }

            Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
            return $this->redirect((array) Yii::$app->request->post('submit-type', array('update', 'id' => $model->id)));
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $selected = [];
        if ($id) {
            $selected[] = $id;
        } else {
            $selected = Yii::$app->request->post("selection");
        }

        if ($selected) {
            foreach (Seo::find()->where(["id" => $selected])->all() as $model) {
                $model->delete();
            }
        }

        return $this->redirect(Yii::$app->request->get('return', ['index']));
    }

    public function actionAutoFind() {
        $route = Yii::$app->request->get('route');
        if ($model = Seo::find()->where(['route' => $route])->one()) {
            return $this->redirect(['seo/update', 'id' => $model->id]);
        } else {
            return $this->redirect(['seo/create', 'route' => $route]);
        }
    }

    protected function findModel($id) {
        if (($model = Seo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
