<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
//    'request' => [
//        'enableCsrfValidation' => false,
//    ],
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'rbac' => [
            'class' => 'yii2mod\rbac\Module',
        ],
    ],
    'components' => [
        'request' => [
            'class' => 'common\components\Request',
            'web' => '/backend/web',
            'adminUrl' => '/admin'
        ],
        'urlManager' => [
            'baseUrl' => '/admin',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
//		'urlManager' => [
//			'enablePrettyUrl' => true,
//			'showScriptName' => false,
//			'rules' => [
//				['pattern' => 'sitemap', 'route' => 'sitemap/default/index', 'suffix' => '.xml'],
//				'add' => 'company/insert',
//				'search' => 'site/search',
//				'company/<_a:[\w\-]+>' => 'company/<_a>',
//				'company/<_a:[\w\-]+>/<id:\d+>' => 'company/<_a>',
//				'information/<slug:[\w\-]+>' => 'information/view',
//				'<path:.+>' => 'site/section',
//				'<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
//				'<_c:[\w\-]+>' => '<_c>/index',
//				'<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_c>/<_a>',
//			],
//		],
//		
//		'authManager' => [
//#			'class' => 'app\components\PhpManager', // THIS IS YOUR AUTH MANAGER
//			'class' => '\yii\rbac\PhpManager', // THIS IS YOUR AUTH MANAGER
////			'defaultRoles' => ['guest'],
//		],
    ],
    'params' => $params,
];
