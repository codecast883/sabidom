<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "village_townhouse".
 *
 * @property integer $id
 * @property string $title
 * @property string $created_at
 * @property string $updated_at
 * @property string $description
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_breadcrumb
 * @property string $slug
 * @property integer $price_from
 * @property double $house_area
 * @property double $stead_area
 * @property integer $number_of_floors
 */
class VillageTownhouse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'village_townhouse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['price_from', 'number_of_floors'], 'integer'],
            [['house_area', 'stead_area'], 'number'],
            [['seo_title', 'seo_h1', 'seo_description', 'seo_keywords', 'seo_breadcrumb'], 'string', 'max' => 1024],
            [['slug'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'description' => 'Description',
            'seo_title' => 'Seo Title',
            'seo_h1' => 'Seo H1',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'seo_breadcrumb' => 'Seo Breadcrumb',
            'slug' => 'Slug',
            'price_from' => 'Price From',
            'house_area' => 'House Area',
            'stead_area' => 'Stead Area',
            'number_of_floors' => 'Number Of Floors',
        ];
    }
}
