$(document).ready(function () {
    $("#getAddressCoordinates").on("click", getAddressCoordinates);

    function getAddressCoordinates() {
        // { name: "John", time: "2pm" } 
        params = {
            geocode: $("#company-address").val(),
            format: 'json',
            results: 1
        };
        
        url = 'http://geocode-maps.yandex.ru/1.x/';
        
        $.get(url, params).done(function (data) {
            if (pos = data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos) {
                match = pos.match(/(.*) (.*)/);
                if (match != null) {
                    $("#company-coordinates").val(match[2] + "," + match[1]);
                } else {
                    alert("Не найдено");
                }


            }
            else {
                alert("Не найдено");
            }
        });

        return false;
    }

});