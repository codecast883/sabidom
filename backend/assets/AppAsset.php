<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
//		'//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',
		'//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
//		'vendor/fontawesome/css/font-awesome.css',
        '//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css',
        '../vendor/jquery-ui/1.11.4/jquery-ui.min.css',
        '../vendor/jquery-ui/1.11.4/jquery-ui.theme.min.css',
//		'css/animate.css',
		'css/AdminLTE.css',
		'css/site.css',
        'vendor/datetimepicker/jquery.datetimepicker.css'
	];
	public $js = [
        '../vendor/jquery-ui/1.11.4/jquery-ui.min.js',
		'js/main.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
	];
	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
