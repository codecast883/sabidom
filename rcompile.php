<?
// CSS

function minimize_css($input)
{
  
    // Remove comments
    $output = preg_replace('#/\*.*?\*/#s', '', $input);
    // Remove whitespace
    $output = preg_replace('/\s*([{}|:;,])\s+/', '$1', $output);
    // Remove trailing whitespace at the start
    $output = preg_replace('/\s\s+(.*)/', '$1', $output);
    // Remove unnecesairy ;'s
    $output = str_replace(';}', '}', $output);
    return $output;
}


$css_list = array(
'/frontend/web/vendor/slick.css',
'/frontend/web/vendor/jquery.rateyo.min.css',
'/frontend/web/vendor/raty/lib/jquery.raty.css',
'/frontend/web/vendor/fancybox/jquery.fancybox.css',
'/frontend/web/vendor/jquery-ui/1.11.4/jquery-ui.min.css',
'/frontend/web/vendor/jquery-ui/1.11.4/jquery-ui.theme.min.css',
'/frontend/web/css/styles.css'
);
$base = $_SERVER['DOCUMENT_ROOT'];
$result = '';
foreach ($css_list as $css) {
	$result .= minimize_css(preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!','',preg_replace('/ +/',' ',str_replace(array("\r","\n","\t"),'',file_get_contents($base.$css)))));
}
file_put_contents($base.'/frontend/web/css/style.min.css', $result);




// JS

$js_list = array(
'/frontend/web/vendor/slick.min.js',
'/frontend/web/vendor/jquery.rateyo.min.js',
'/frontend/web/vendor/raty/lib/jquery.raty.js',
'/frontend/web/vendor/jquery.total-storage.min.js',
'/frontend/web/vendor/jquery.maskedinput.min.js',
'/frontend/web/vendor/modernizr.custom.js',
'/frontend/web/vendor/scroll_to.js',
'/frontend/web/vendor/jquery-ui/1.11.4/jquery-ui.min.js',
'/frontend/web/vendor/slides/js/slides.min.jquery.js',
'/frontend/web/vendor/fancybox/jquery.fancybox.js',
'/frontend/web/js/php_functions.js',
'/frontend/web/js/main.js'
);
$base = $_SERVER['DOCUMENT_ROOT'];
$result = '';
foreach ($js_list as $js) {
  $result .=  preg_replace('#^\s*//.+$#m','',file_get_contents($base.$js));
}
file_put_contents($base.'/frontend/web/js/scripts.min.js', $result);

echo 'ok';

?>
