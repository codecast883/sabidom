<?php

$params = array_merge(
		require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
	'id' => 'app-frontend',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'modules' => [
		'sitemap' => [
			'class' => 'himiklab\sitemap\Sitemap',
			'models' => [
				'common\models\Category',
				'common\models\Village',
				'common\models\Realt',
				'common\models\Article',
				'common\models\Action',
				'common\models\Information',
				'common\models\VillageTownhouse',
			],
			'urls' => [
				[
					'loc' => '/',
					'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_DAILY,
					'priority' => 0.8,
				],
			],
			'enableGzip' => true, // default is false
			'cacheExpire' => 1, // 1 second. Default is 24 hours
		],
	],
	'controllerNamespace' => 'frontend\controllers',
	'components' => [
		'user' => [
			'identityClass' => 'common\models\User',
			'enableAutoLogin' => true,
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'request' => [
			'class' => 'common\components\Request',
			'web' => '/frontend/web'
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				['pattern' => 'sitemap', 'route' => 'sitemap/default/index', 'suffix' => '.xml'],
				['class' => 'frontend\components\url_rule\CommonRule'],
				['class' => 'frontend\components\url_rule\InformationRule'],
				['class' => 'frontend\components\url_rule\ArticleRule'],
				['class' => 'frontend\components\url_rule\ActionRule'],
				['class' => 'frontend\components\url_rule\CategoryRule'],
				['class' => 'frontend\components\url_rule\RealtRule'],
				['class' => 'frontend\components\url_rule\VillageRule'],
				'<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
				'<_c:[\w\-]+>' => '<_c>/index',
				'<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_c>/<_a>',
			],
		],
		'mail' => [
			'viewPath' => '@frontend/mail',
		],
	],
	'params' => $params,
];
