function number_format(num, decimal, decimalSep, milharSep) {
	/*
	 @author: Rafael Alves - contato@rafaelsouza.net
	 @date: 2014-13-03
	 @description: funcao similar a number_format do php. Retorna um numero formatado de acordo com os parametros informados
	 @params: num: o numero a ser formatado,
	 decimal: quantidade de casas apos a virgula,
	 decimalSep: separador da casa decimal - default: ".",
	 milharSep: separador da casa de milhar - default: ","
	 */
	decimalSep = decimalSep ? decimalSep : '.';
	milharSep = milharSep ? milharSep : ' ';

	var intval = parseInt(num);
	if (String(num).indexOf('.') !== -1) {
		var decval = String(num).split('.');
		decval = decval[1];
	}

	intval = String(intval).split('');
	intval = intval.reverse();
	var c = 1;
	var aux = '';
	for (var i = 0; i < intval.length; i++) {
		aux += intval[i];
		if (c == 3 && i != intval.length - 1) {
			aux += milharSep;
			c = 1;
		} else
			c++;
	}
	aux = aux.split("").reverse().join("");
	intval = aux;
	if (decval) {
		decval = String(decval).substring(0, decimal);
	} else
		decval = '';
	while (decval.length < decimal) {
		decval += '0';
	}
	num = String(intval) + (decval? decimalSep + decval : "");
	return num;
}

function empty(mixed_var) {	// Determine whether a variable is empty
	// 
	// +   original by: Philippe Baumann

	return (mixed_var === "" || mixed_var === 0 || mixed_var === "0" || mixed_var === null || mixed_var === false || (is_array(mixed_var) && mixed_var.length === 0) || mixed_var == "NaN");
}

function is_array(mixed_var) {	// Finds whether a variable is an array
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Legaev Andrey
	// +   bugfixed by: Cord

	return (mixed_var instanceof Array);
}


function explode( delimiter, string ) {	// Split a string by string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: kenneth
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

	var emptyArray = { 0: '' };

	if ( arguments.length != 2
		|| typeof arguments[0] == 'undefined'
		|| typeof arguments[1] == 'undefined' )
	{
		return null;
	}

	if ( delimiter === ''
		|| delimiter === false
		|| delimiter === null )
	{
		return false;
	}

	if ( typeof delimiter == 'function'
		|| typeof delimiter == 'object'
		|| typeof string == 'function'
		|| typeof string == 'object' )
	{
		return emptyArray;
	}

	if ( delimiter === true ) {
		delimiter = '1';
	}

	return string.toString().split ( delimiter.toString() );
}
