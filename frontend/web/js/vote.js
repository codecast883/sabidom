$(document).ready(function () {
    $(".company-vote").on("click", function () {
        _this = this;

        $.ajax({
            url: "/company/vote",
            type: "POST",
            elem: _this,
            data: {
                company_id: $(_this).data("company_id"),
                rating: $(_this).data("rating")
            },
            beforeSend: function (xhr) {
                $(_this).addClass("loading");
            },
            complete: function (jqXHR, textStatus) {
                $(_this).removeClass("loading");
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.success) {
                    $(this.elem).data("popover", data.error_text);
                }
                else {
                    $(this.elem).data("popover", data.success_text);
                    $(this.elem).find(".company-vote-count.pos").text(data.rating.pos);
                    $(this.elem).find(".company-vote-count.con").text(data.rating.con);
                }

                $(this.elem).popover("show");
                setTimeout(hidePopup, 3000, this.elem);
            },
            dataType: "json"
        });

    });

    function hidePopup(elem) {
        $(elem).popover('hide');
    }


    $('.company-vote').popover({
        trigger: "manual",
        container: "body",
        placement: "bottom",
        content: function () {
            return $(this).data("popover");
        }
    });
});