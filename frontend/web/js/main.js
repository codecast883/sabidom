var isMobile = {
    Android: function () {
	return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
	return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
	return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
	return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
	return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
	return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function initInputRating() {
    $(".review-rating-stars").each(function () {
	$(this).raty({
	    targetScore: $(this).data('target'),
	    number: 5,
	    halfShow: false,
	    score: $($(this).data('target')).val(),
	    dependenceOnScoreFolder: true,
	    path: '/img/raty'
	});
    });
}


$(function () {
    $(".rating-autostars").each(function () {
	$(this).raty({
	    halfShow: false,
	    readOnly: true,
	    number: 5,
	    dependenceOnScoreFolder: true,
	    score: $(this).data('rating'),
	    path: '/img/raty'
	});
    });
});



$(function () {
  $('.slick.items-3').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
	    {
	      breakpoint: 1000,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 800,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 500,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
  });

    $('.slick.items-1').slick({
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1
    });

    $('.slick.items-6').slick({
	infinite: true,
	slidesToShow: 6,
	slidesToScroll: 1
    });
});


function seoFixHeight() {
    $('.fix-height-container').css({position: 'relative'})
    $(".fix-height").each(function () {
	dst_block = $(this).data("fix-container");
	src_block = $(this);

	$(src_block).css({position: 'absolute'})
	$(dst_block).height($(src_block).outerHeight(true));
	$(src_block).offset($(dst_block).offset());
    });
}

$(document).ready(function () {
    $('.ajax-fog').css("position", "relative");
    $('.ajax-fog').append("<div class='fog' style='display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: url(\"/img/loading.gif\") center center no-repeat rgba(0,0,0,0.05); background-size: 100px 100px;'></div>");
    $('.ajax-fog')
            .on('pjax:start', function () {
                $(this).find(".fog").show();
            })
            .on('pjax:end', function () {
                $(this).find(".fog").hide();
            });
	    
    seoFixHeight();

    $(".fancybox").fancybox({
	helpers: {
	    overlay: {
		locked: false
	    }
	}
    });

    $(window).resize(function () {
	var footerHeight = $('footer').outerHeight();
	var stickFooterPush = $('.footer-buffer').height(footerHeight);

	$('#page').css({'marginBottom': '-' + footerHeight + 'px'});
    });

    $(window).resize();

    if (!isMobile.any()) {
	$(".masked-phone").mask("+7(ddd)ddd-dd-dd");
    }

    $(".subcategories-list-container .subcategories-list-control").on("click", function () {
	elem = $(this).parent(".subcategories-list-container").children(".subcategories-list");

	if (elem.hasClass("expanded")) {
	    elem.removeClass("expanded");
	    $(this).text($(this).data('text-off'));
	} else {
	    elem.addClass("expanded");
	    $(this).text($(this).data('text-on'));
	}
    });

    // $('.show-hidden-data').on('click', function () {
    // $(this).html($(this).data('hidden'));
    // });


    $('.mark').on('click', function () {
	mark_name = $(this).data('mark-name');
	$.ajax({
	    type: "POST",
	    url: '/service/mark',
	    data: {mark_name: mark_name},
	});

    });
});
