<?php

namespace frontend\controllers;

use Yii;
use common\models\Information;
use yii\data\ActiveDataProvider;
use frontend\components\controllers\FrontController;
use yii\web\NotFoundHttpException;

/**
 * InformationController implements the CRUD actions for Information model.
 */
class InformationController extends FrontController {

    public function behaviors() {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actionView($id) {

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $this->data['success'] = 1;

            // Yii::$app->mailer->compose()
            // ->setFrom('from@domain.com')
            // ->setTo('thlw200t@gmail.com')
            // ->setSubject('dsfdfsdf')
            // ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
            // ->send();

            $headers = 'From: Birthday Reminder <info@sabidom.ru>' . "\r\n" .
                'Reply-To: info@sabidom.ru' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            $to = ' info@sabidom.ru';
            $subject = 'request form sabidom.ru';
            $message = "Имя:" . $_POST['Information']['name'] ."\r\n" . "Телефон:" . $_POST['Information']['phone'] ."\r\n" . "Текст:" .$_POST['Information']['text'];

            mail($to, $subject, $message,$headers);
				
				
        }


        $this->data['message'] = 'Отправлено';

        if (!$model = Information::find()->where(["id" => $id, "status" => Information::STATUS_ENABLED])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Информация (id: " . $model->id . ")",
            'url' => ["/admin/information/update", "id" => $model->id]];


        $this->seo->setReplaces([
            'название' => $model->title
        ]);
        $this->seo->attachModel($model);

        $this->seo->addBreadcrumb($model->title);

        $this->data['model'] = $model;
        return $this->render('view');
    }

}
