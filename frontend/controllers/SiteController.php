<?php

namespace frontend\controllers;

use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Main;
use common\models\Company;
use common\models\Category;
use common\models\Rating;
use common\models\CompanySearch;
use common\models\CompanyCategory;
use common\models\CompanyToCategory;
use common\models\CompanyCategoryPath;
use \frontend\models\CompanyReview;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\components\CategoryFunctions;

/**
 * Site controller
 */
class SiteController extends \frontend\components\controllers\FrontController {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		$behaviors = [
			'access' => [
				'rules' => [
					[
						'allow' => true,
						'roles' => ['?', '@'], // все
					],
				]
			],
		];

		return array_merge_recursive(parent::behaviors(), $behaviors);
	}

	public function actionIndex() {
		$this->data['categories_for_blocks'] = Category::find()->where(['parent_id' => 0, 'on_header' => [1, 2]])->all();
		return $this->render('index');
	}

	public function actionRobots() {
		header("Content-Type: text/plain");
		$robots = "";
		$url_info = parse_url(Yii::$app->urlManager->hostInfo);
		if ($domain = preg_replace("|\.?sabidom.ru|isu", "", $url_info['host'])) {
			if ($village = \common\models\Village::find()->where(['slug' => $domain])->one()) {
				$robots = \common\models\Settings::getByAlias('village_robots');
				$substitutions = [
					'{host}' => parse_url($village->url, PHP_URL_HOST)
				];
				$robots = \common\helpers\YText::textGenerator($robots, $substitutions, FALSE);
			}
		} else {
			$robots = file_get_contents(\Yii::getAlias("@frontend/web/robots.txt"));
		}

		print $robots;
		exit;
	}

	public function actionObjects() {
		$this->admin_menu[] = [
			'label' => "Настройки раздела",
			'url' => ["/admin/section/update?alias=main"]
		];
		return $this->render('index');
	}

	public function actionSection($path) {
		$view = NULL;
		$path = trim($path, "/");

		$category = CompanyCategory::findByPath($path);
		if ($category = CompanyCategory::findByPath($path)) {
			return $this->viewCategory($category);
		} elseif ($company = Company::find()->where(['slug' => $path, 'status' => 1])->one()) {
			$a = new CompanyController($this->id, $this->module);
			return $a->actionView($path);
		} else {
			/**
			 * WARNING: временно редиректим все непонятное на главную
			 */
			if (time() < strtotime("2015-11-01")) {
				return $this->redirect("/", 301);
			}
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}

	private function viewCategory($category) {
		$this->admin_menu[] = [
			'label' => "Категория (id: " . $category->id . ")",
			'url' => ["/admin/company-category/update", "id" => $category->id]];

		$this->setPageTitle($category->generateSeoTitle());
		$this->setPageDescription($category->generateSeoDescription());
		$this->setPageKeywords($category->generateSeoKeywords());

		$this->data['template'] = $category->template;

		$query = Company::find();
		$query->where(['status' => 1]);
		$query->andWhere(['like', 'categories_cache', "_" . $category->id . "_"]);

		$category_parent_id = $category->parent_id ? $category->parent_id : $category->id;
		$category_for_menu = CompanyCategory::find()->where(['parent_id' => $category_parent_id])->orderBy("title ASC")->all();

		$this->data['categories_menu'] = [];
		foreach ($category_for_menu as $item) {
			$this->data['categories_menu'][] = [
				'label' => $item->title,
				'url' => $item->url,
				'active' => $item->id == $category->id ? TRUE : FALSE
			];
		}

		$countQuery = clone $query;
		$pagination = new Pagination(['totalCount' => $countQuery->count()]);
		$pagination->defaultPageSize = $this->data['template']['pageSize'];

		$this->data['paginaton'] = $pagination;

		$this->data['companies'] = $query
				->orderBy("on_top DESC, title")
				->offset($pagination->offset)
				->limit($pagination->limit)
				->all();

		$this->data['ratings_id'] = $category->ratings_id; //ArrayHelper::map(Rating::find()->where(["like", "categories", "_" . $category->id . "_"])->all(), "id", "id");

		$this->data['category'] = $category;
		//return $this->render("/category/index");
		return $this->render($this->data['template']['template_category']);
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'common\owerride\yii\web\ErrorAction',
			//'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
			],
		];
	}

	public function actionAccessDenied() {
		return $this->render('access-denied');
	}

	public function actionSitemap() {
		$this->seo->addBreadcrumb($this->seo->breadcrumb);

		$this->data['sitemap'] = [];
		$this->data['sitemap'][] = ['label' => 'Главная', 'url' => '/'];

		$subitems = [];
		foreach (\common\models\Article::find()->where(['type_id' => \common\models\Article::TYPE_NEWS, 'status' => 1])->orderBy(['created_at' => SORT_DESC])->all() as $model) {
			$subitems[] = ['label' => $model->title, 'url' => $model->url];
		}
		$this->data['sitemap'][] = ['label' => 'Новости', 'url' => ['article/news'], 'items' => $subitems];

		$subitems = [];
		foreach (\common\models\Article::find()->where(['type_id' => \common\models\Article::TYPE_NEWS, 'status' => 1])->orderBy(['created_at' => SORT_DESC])->all() as $model) {
			$subitems[] = ['label' => $model->title, 'url' => $model->url];
		}
		$this->data['sitemap'][] = ['label' => 'Статьи', 'url' => ['article/articles'], 'items' => $subitems];

		$subitems = [];
		foreach (\common\models\Action::find()->where(['status' => 1])->orderBy(['created_at' => SORT_DESC])->all() as $model) {
			$subitems[] = ['label' => $model->title, 'url' => $model->url];
		}
		$this->data['sitemap'][] = ['label' => 'Акции', 'url' => ['acttion/index'], 'items' => $subitems];

		foreach (\common\models\Information::find()->where(['status' => 1])->orderBy('title')->all() as $model) {
			$this->data['sitemap'][] = ['label' => $model->title, 'url' => $model->url];
		}

		$rows = \Yii::$app->cache->get("sitemap_categories");
		if ($rows === false) {
			$subitems = [];
			foreach (Category::find()->where(['status' => 1, 'parent_id' => 0])->orderBy(['title' => SORT_ASC])->all() as $model1) {
				$sub2 = [];
				foreach (Category::find()->where(['status' => 1, 'parent_id' => $model1->id])->orderBy(['title' => SORT_ASC])->all() as $model2) {
					$sub3 = [];
					foreach (Category::find()->where(['status' => 1, 'parent_id' => $model2->id])->orderBy(['title' => SORT_ASC])->all() as $model3) {
						$sub4 = [];
						foreach (Category::find()->where(['status' => 1, 'parent_id' => $model3->id])->orderBy(['title' => SORT_ASC])->all() as $model4) {
							$sub4[] = ['label' => $model4->title, 'url' => $model4->url];
						}
						$sub3[] = ['label' => $model3->title, 'url' => $model2->url, 'items' => $sub4];
					}
					$sub2[] = ['label' => $model2->title, 'url' => $model2->url, 'items' => $sub3];
				}

				$subitems[] = ['label' => $model1->title, 'url' => $model1->url, 'items' => $sub2];
			}
			$rows = ['label' => 'Категории', 'items' => $subitems];

			$dependency = new \yii\caching\DbDependency();
			$dependency->sql = 'SELECT MAX(updated_at) FROM category';
			\Yii::$app->cache->set("sitemap_categories", $rows, 0, $dependency);
		}

		$this->data['sitemap'][] = $rows;



		return $this->render('sitemap');
	}

	public function actionLogin() {
		$model = new \common\models\LoginForm();

		if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {
			return $this->goHome();
		}

		return $this->render('login', [
					'model' => $model
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();

		return $this->goHome();
	}

	public function actionGetUrl() {
		print \yii\helpers\Url::to(Yii::$app->request->get("url"), TRUE);
	}

}
