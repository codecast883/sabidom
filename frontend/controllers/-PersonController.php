<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Person;
use common\models\Review;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;
use yii\base\Exception;

class PersonController extends \frontend\components\controllers\FrontController {

    public function behaviors() {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actionIndex() {
        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=person"]
        ];

        $seo_settings = \common\models\Section::getSettings('person');
        $this->setPageTitle($seo_settings->seo_title);
        $this->setPageDescription($seo_settings->seo_description);
        $this->setPageKeywords($seo_settings->seo_keywords);

        $this->data['seo_h1'] = $seo_settings->seo_h1;
        $this->data['description_top'] = $seo_settings->description_top;
        $this->data['description_bottom'] = $seo_settings->description_bottom;

        $this->breadcrumbs[] = ['label' => $seo_settings->breadcrumb];

        $query = Person::find();
        $query->where(['status' => Person::STATUS_ENABLED]);

        $countQuery = clone $query;
        $pagination = new Pagination(['totalCount' => $countQuery->count()]);
        $pagination->defaultPageSize = 10;

        $this->data['paginaton'] = $pagination;

        $this->data['persons'] = $query
                ->orderBy("on_top DESC, title")
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        $this->data['letters_menu'] = Person::getCategories("full");
        return $this->render("/person/index");
    }

    public function actionCategory_filter($type, $letter) {
        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=person"]
        ];

        $seo_settings = \common\models\Section::getSettings('person');
        $this->setPageTitle($seo_settings->seo_title);
        $this->setPageDescription($seo_settings->seo_description);
        $this->setPageKeywords($seo_settings->seo_keywords);

        $this->data['seo_h1'] = $seo_settings->seo_h1;
        $this->data['description_top'] = $seo_settings->description_top;
        $this->data['description_bottom'] = $seo_settings->description_bottom;

        $this->breadcrumbs[] = ['label' => $seo_settings->breadcrumb, 'url' => ["person/index"]];
        $this->breadcrumbs[] = ['label' => $letter];

        $query = Person::find();
        $query->where(['status' => Person::STATUS_ENABLED]);

        $letters_diapason = ['А-Г' => ['а', 'б', 'в', 'г'], 'Д-З' => ['д', 'е', 'ё', 'ж', 'з'], 'И-М' => ['и', 'й', 'к', 'л', 'м'], 'Н-Р' => ['н', 'о', 'п', 'р'], 'С-У' => ['с', 'т', 'у'], 'Ф-Я' => ['ф', 'х', 'ц', 'ч', 'ш', 'щ', 'э', 'ю', 'я']];
        if ($type == "full") {
            $query->andWhere(['LIKE', 'title', $letter . "%", FALSE]);
        } elseif ($type == "short" AND ! empty($letters_diapason[$letter])) {
            $query->andWhere(["IN", "SUBSTRING(title, 1, 1)", $letters_diapason[$letter], FALSE]);
        }

        $countQuery = clone $query;
        $pagination = new Pagination(['totalCount' => $countQuery->count()]);
        $pagination->defaultPageSize = 10;

        $this->data['paginaton'] = $pagination;

        $this->data['persons'] = $query
                ->orderBy("on_top DESC, title")
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        $this->data['letters_menu'] = Person::getCategories("full", $letter);
        return $this->render("/person/index");
    }

    public function actionView($id) {
        if (!$person = Person::find()->where(['id' => $id, 'status' => 1])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=person"]
        ];

        $this->admin_menu[] = [
            'label' => "Person (id: " . $person->id . ")",
            'url' => ["/admin/person/update", "id" => $person->id]
        ];


        $person->incrementViews();

        $seo_settings = \common\models\Section::getSettings('person');
        $this->breadcrumbs[] = ['label' => $seo_settings->breadcrumb, 'url' => ["person/index"]];
        $this->breadcrumbs[] = ['label' => $person->generateSeoBreadcrumb()];

        $this->setPageTitle($person->generateSeoTitle());
        $this->setPageDescription($person->generateSeoDescription());
        $this->setPageKeywords($person->generateSeoKeywords());

        $this->data['person'] = $person;

        $reviews_form['model'] = new Review();
        $reviews_form['model']->item_id = $person->id;
        $reviews_form['model']->type_id = Person::REVIEWS_TYPE_ID;

        $reviews_form['message'] = "";
        $reviews_form['success'] = FALSE;
        $reviews_form['error'] = FALSE;
        $reviews_form['model']->scenario = "create_from_front";

        if ($reviews_form['model']->load(Yii::$app->request->post())) {
            if ($reviews_form['model']->save()) {
                $reviews_form['model'] = new Review;
                $reviews_form['model']->item_id = $person->id;
                $reviews_form['model']->type_id = Person::REVIEWS_TYPE_ID;

                $reviews_form['success'] = TRUE;
                $reviews_form['message'] = "Спасибо за Ваш отзыв";
            } else {
                $reviews_form['error'] = TRUE;
                $reviews_form['message'] = "Проверьте форму на ошибки";
            }
        }

        $reviews_form['model']->scenario = "default";
        $this->data['reviews_form'] = $reviews_form;

        $query = Review::find()->where(['item_id' => $person->id, 'type_id' => Person::REVIEWS_TYPE_ID]);

        if (\Yii::$app->user->can("front-company-manager")) {
            # нет фильтра по статусу
        } elseif (\Yii::$app->user->can("front-company-owner") AND $company->user_id == \Yii::$app->user->identity->getId()) {
            # нет фильтра по статусу
        } else {
            $query->andWhere(['status' => Review::STATUS_ENABLED]);
        }

        $pagination = new Pagination(['totalCount' => $query->count()]);
        $pagination->defaultPageSize = 7;

        $this->data['pagination'] = $pagination;

        $this->data['reviews'] = $query
                ->orderBy("date_post DESC")
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();



        $this->data['data'] = $this->data;
        $this->data['tab'] = "_tab_main";

        return $this->render("/person/view");
    }

    public function actionView_foto($id) {
        if (!$person = Person::find()->where(['id' => $id, 'status' => 1])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=person"]
        ];

        $this->admin_menu[] = [
            'label' => "Person (id: " . $person->id . ")",
            'url' => ["/admin/person/update", "id" => $person->id]
        ];


        $seo_settings = \common\models\Section::getSettings('person');
        $this->breadcrumbs[] = ['label' => $seo_settings->breadcrumb, 'url' => ["person/index"]];
        $this->breadcrumbs[] = ['label' => $person->generateSeoBreadcrumb(), 'url' => $person->url];
        $this->breadcrumbs[] = ['label' => $person->generateSeoBreadcrumb("photo")];



        $this->setPageTitle($person->generateSeoTitle("photo"));
        $this->setPageDescription($person->generateSeoDescription("photo"));
        $this->setPageKeywords($person->generateSeoKeywords("photo"));

        $this->data['person'] = $person;


        $this->data['data'] = $this->data;
        $this->data['tab'] = "_tab_images";

        return $this->render("/person/view");
    }

    public function actionView_news($id) {
        if (!$person = Person::find()->where(['id' => $id, 'status' => 1])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=person"]
        ];

        $this->admin_menu[] = [
            'label' => "Person (id: " . $person->id . ")",
            'url' => ["/admin/person/update", "id" => $person->id]
        ];


        $seo_settings = \common\models\Section::getSettings('person');
        $this->breadcrumbs[] = ['label' => $seo_settings->breadcrumb, 'url' => ["person/index"]];
        $this->breadcrumbs[] = ['label' => $person->generateSeoBreadcrumb(), 'url' => $person->url];
        $this->breadcrumbs[] = ['label' => $person->generateSeoBreadcrumb("news")];

        $this->setPageTitle($person->generateSeoTitle("news"));
        $this->setPageDescription($person->generateSeoDescription("news"));
        $this->setPageKeywords($person->generateSeoKeywords("news"));

        $this->data['person'] = $person;
        $this->data['news'] = $person->getNews(0);


        $this->data['data'] = $this->data;
        $this->data['tab'] = "_tab_news";

        return $this->render("/person/view");
    }

}
