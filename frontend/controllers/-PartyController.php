<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Party;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;
use yii\base\Exception;

class PartyController extends \frontend\components\controllers\FrontController {

    public function behaviors() {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actionIndex() {
        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=party"]
        ];

        $seo_settings = \common\models\Section::getSettings('party');
        $this->setPageTitle($seo_settings->seo_title);
        $this->setPageDescription($seo_settings->seo_description);
        $this->setPageKeywords($seo_settings->seo_keywords);

        $this->data['seo_h1'] = $seo_settings->seo_h1;
        $this->data['description_top'] = $seo_settings->description_top;
        $this->data['description_bottom'] = $seo_settings->description_bottom;

        $this->breadcrumbs[] = ['label' => $seo_settings->breadcrumb];

        $query = Party::find();
        $query->where(['status' => Party::STATUS_ENABLED]);
//        $query->andWhere(['like', 'categories_cache', "_" . $category->id . "_"]);
//        $category_for_menu = CompanyCategory::find()->where(['parent_id' => $category_parent_id])->orderBy("title ASC")->all();
//
//        $this->data['categories_menu'] = [];
//        foreach ($category_for_menu as $item) {
//            $this->data['categories_menu'][] = [
//                'label' => $item->title,
//                'url' => $item->url,
//                'active' => $item->id == $category->id ? TRUE : FALSE
//            ];
//        }

        $countQuery = clone $query;
        $pagination = new Pagination(['totalCount' => $countQuery->count()]);
        $pagination->defaultPageSize = 10;

        $this->data['paginaton'] = $pagination;

        $this->data['partys'] = $query
                ->orderBy("on_top DESC, title")
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        //$this->data['category'] = $category;
        return $this->render("/party/index");
    }

    public function actionView($id) {
        if (!$party = Party::find()->where(['id' => $id, 'status' => 1])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=party"]
        ];
        $this->admin_menu[] = [
            'label' => "Party (id: " . $party->id . ")",
            'url' => ["/admin/party/update", "id" => $party->id]
        ];


        $party->incrementViews();
        $seo_settings = \common\models\Section::getSettings('party');
        $this->breadcrumbs[] = ['label' => $seo_settings->breadcrumb, 'url' => ["party/index"]];
        $this->breadcrumbs[] = ['label' => $party->generateSeoBreadcrumb()];


        $this->setPageTitle($party->generateSeoTitle());
        $this->setPageDescription($party->generateSeoDescription());
        $this->setPageKeywords($party->generateSeoKeywords());

        $this->data['party'] = $party;



        return $this->render("/party/view");
    }

}
