<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Tag;
use common\models\TagGroup;
use common\models\Person;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class TagController extends \frontend\components\controllers\FrontController {

    public function behaviors() {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actionGroup($tag_group_id) {
        if (!$group = \common\models\TagGroup::findOne($tag_group_id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->setPageTitle($group->generateSeoTitle());
        $this->setPageDescription($group->generateSeoDescription());
        $this->setPageKeywords($group->generateSeoKeywords());

        $this->breadcrumbs[] = ['label' => $group->generateSeoBreadcrumb()];

        $this->data['group'] = $group;

        return $this->render("group");
    }

    public function actionView($tag_group_id, $tag_id) {
        if (!$group = \common\models\TagGroup::findOne($tag_group_id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (!$tag = \common\models\Tag::findOne($tag_id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->setPageTitle($tag->generateSeoTitle());
        $this->setPageDescription($tag->generateSeoDescription());
        $this->setPageKeywords($tag->generateSeoKeywords());

        $this->breadcrumbs[] = ['label' => $group->generateSeoBreadcrumb(), 'url' => $group->url];
        $this->breadcrumbs[] = ['label' => $tag->generateSeoBreadcrumb()];

        $this->data['group'] = $group;
        $this->data['tag'] = $tag;

        $query = Person::find()
                ->where(['status' => Person::STATUS_ENABLED])
                ->join("JOIN", 'person_to_tag', 'person_to_tag.person_id = person.id')
                ->where(['person_to_tag.tag_id' => $tag->id]);

        $this->data['dataProvider'] = new ActiveDataProvider([
            'query' => $query,
        ]);
//        
//        $countQuery = clone $query;
//        $pagination = new Pagination(['totalCount' => $countQuery->count()]);
//        $pagination->defaultPageSize = 18;
//
//        $this->data['paginaton'] = $pagination;
//        $this->data['persons'] = $query
//                ->orderBy("on_top DESC, title")
//                ->offset($pagination->offset)
//                ->limit($pagination->limit)
//                ->all();

        return $this->render("view");
    }

}
