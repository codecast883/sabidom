<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Realt;
use common\models\Category;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\models\RealtViewRequest;

class RealtController extends \frontend\components\controllers\FrontController {

    public function behaviors() {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actionView($id) {
        if (!$realt = Realt::findOne($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Объект (id: " . $realt->id . ")",
            'url' => ["/admin/realt/update", "id" => $realt->id]];


        $realt->incrementViews();
        $this->data['realt'] = $realt;
        $this->seo->attachModel($realt);

        $root_category = NULL;
        if ($realt->type_id == Category::TYPE_COTTAGE) {
            $root_category = Category::find()->where(['content_type_id' => Category::TYPE_COTTAGE, 'parent_id' => 0])->orderBy(['id' => SORT_ASC])->one();
        } elseif ($realt->type_id == Category::TYPE_STEAD) {
            $root_category = Category::find()->where(['content_type_id' => Category::TYPE_STEAD, 'parent_id' => 0])->orderBy(['id' => SORT_ASC])->one();
        } elseif ($realt->type_id == Category::TYPE_TOWNHOUSE) {
            $root_category = Category::find()->where(['content_type_id' => Category::TYPE_TOWNHOUSE, 'parent_id' => 0])->orderBy(['id' => SORT_ASC])->one();
        }
        if ($root_category) {
            $this->seo->addBreadcrumb(['label' => $root_category->title, 'url' => $root_category->url]);
        }

        $this->seo->addBreadcrumb(['label' => $realt->title]);

        $view_request_form['model'] = new RealtViewRequest;
        $view_request_form['model']->realt_id = $realt->id;

        $view_request_form['message'] = "";
        $view_request_form['success'] = FALSE;
        $view_request_form['error'] = FALSE;
        //$reviews_form['model']->scenario = "create_from_front";

        if ($view_request_form['model']->load(Yii::$app->request->post())) {
            if ($view_request_form['model']->save()) {
                $view_request_form['model'] = new RealtViewRequest;
                $view_request_form['model']->realt_id = $realt->id;

                $view_request_form['success'] = TRUE;
                $view_request_form['message'] = "Заявка отправлена";
            } else {
                $view_request_form['error'] = TRUE;
                $view_request_form['message'] = "Проверьте форму на ошибки";
            }
        }

        $view_request_form['model']->scenario = "default";
        $this->data['view_request_form'] = $view_request_form;


        return $this->render('view');
    }
}
