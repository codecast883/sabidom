<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Seo;
use common\models\VillageViewRequest;
use common\models\Village;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\helpers\YText;

class PopupController extends \frontend\components\controllers\FrontController {

    public $layout = 'simple';

    public function actionRealtViewRequest() {
        // не доделано
        $this->data['model'] = new ViewRequest;
        $this->data['model']->item_type = Yii::$app->request->get('item_type');
        $this->data['model']->item_id = Yii::$app->request->get('item_id');

        $this->data['message'] = "";
        $this->data['success'] = FALSE;
        $this->data['error'] = FALSE;

        if ($this->data['model']->load(Yii::$app->request->post())) {
            if ($this->data['model']->save()) {
                $this->data['success'] = TRUE;
                $this->data['message'] = "Заявка отправлена";
            } else {
                $this->data['error'] = TRUE;
                $this->data['message'] = "Проверьте форму на ошибки";
            }
        }

        if (Yii::$app->request->get('template') == "simple") {
            return $this->render('realt_view_request_simple');
        }

        return $this->render('realt_view_request');
    }

}
