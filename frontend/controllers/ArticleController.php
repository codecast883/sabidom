<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Article;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\helpers\YText;

class ArticleController extends \frontend\components\controllers\FrontController {

    public function actionNews() {
        $this->admin_menu[] = [
            'label' => "Статьи",
            'url' => ["/admin/article"]
        ];

        $query = Article::find();

        $query->where(['type_id' => Article::TYPE_NEWS, 'status' => 1]);
        $query->limit(10);


        $this->seo->addBreadcrumb('Новости');

        $this->data['models'] = $query
                ->orderBy(["created_at" => SORT_DESC])
                ->all();

        $this->data['calendar_menu'] = $this->_getCalendar(Article::TYPE_NEWS);
        return $this->render("index");
    }

    public function actionFeed() {
        $query = Article::find();

        $query->where(['status' => 1]);
        $query->limit(30);

        $this->seo->addBreadcrumb('Лента');

        $this->data['models'] = $query
                ->orderBy(["created_at" => SORT_DESC])
                ->all();
        $this->data['calendar_menu'] = $this->_getCalendar(Article::TYPE_FEED, $year, $month);

        return $this->render("feed");
    }

    public function actionFeedDate($year, $month) {
        if (!$year OR ! $month) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Статьи",
            'url' => ["/admin/article"]
        ];

        $this->seo->setReplaces([
            'год' => $year,
            'месяц' => mb_convert_case(YText::getMonthName((int) $month), MB_CASE_LOWER, "UTF8")
        ]);

        $this->seo->addBreadcrumb(['label' => "Лента", 'url' => ['article/news']]);
        $this->seo->addBreadcrumb('Лента за ' . mb_convert_case(YText::getMonthName((int) $month), MB_CASE_LOWER, "UTF8") . " " . $year . " года");

        $query = Article::find();
        $query->where(['status' => 1]);
        $query->andWhere(['>', 'created_at', $year . "-" . $month . "-0"]);
        $query->andWhere(['<', 'created_at', $year . "-" . $month . "-31"]);

        $this->data['models'] = $query
                ->orderBy(["created_at" => SORT_DESC])
                ->all();

        $this->data['calendar_menu'] = $this->_getCalendar(Article::TYPE_FEED, $year, $month);


        return $this->render("feed");
    }

    public function actionNewsDate($year, $month) {
        if (!$year OR ! $month) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Статьи",
            'url' => ["/admin/article"]
        ];

        $this->seo->setReplaces([
            'год' => $year,
            'месяц' => mb_convert_case(YText::getMonthName((int) $month), MB_CASE_LOWER, "UTF8")
        ]);

        $this->seo->addBreadcrumb(['label' => "Новости", 'url' => ['article/news']]);
        $this->seo->addBreadcrumb('Новости за ' . mb_convert_case(YText::getMonthName((int) $month), MB_CASE_LOWER, "UTF8") . " " . $year . " года");

        $query = Article::find();

        $query->where(['type_id' => Article::TYPE_NEWS, 'status' => 1]);

        $query->andWhere(['>', 'created_at', $year . "-" . $month . "-0"]);
        $query->andWhere(['<', 'created_at', $year . "-" . $month . "-31"]);

        $this->data['models'] = $query
                ->orderBy(["created_at" => SORT_DESC])
                ->all();

        $this->data['calendar_menu'] = $this->_getCalendar(Article::TYPE_NEWS, $year, $month);

        return $this->render("index");
    }

    public function actionNewsItem($id, $filter_date) {
        if (!$article = Article::find()->where(['id' => $id, 'status' => 1])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($filter_date != date("Y-m", strtotime($article->created_at))) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->seo->attachModel($article);

        $this->seo->addBreadcrumb(['label' => 'Новости', 'url' => ['article/news']]);
        $this->seo->addBreadcrumb(['label' => $article->title]);

        $this->seo->setReplaces([
            'название новости' => $article->title
        ]);

        $this->admin_menu[] = [
            'label' => "Статьи",
            'url' => ["/admin/article"]
        ];
        $this->admin_menu[] = [
            'label' => "Новость (id: " . $article->id . ")",
            'url' => ["/admin/article/update", "id" => $article->id]
        ];


        $this->data['model'] = $article;

        return $this->render('view');
    }

    public function actionArticles() {
        $this->admin_menu[] = [
            'label' => "Статьи",
            'url' => ["/admin/article"]
        ];

        $query = Article::find();

        $query->where(['type_id' => Article::TYPE_ARTICLE, 'status' => 1]);
        $query->limit(10);


        $this->seo->addBreadcrumb('Статьи');

        $this->data['models'] = $query
                ->orderBy(["created_at" => SORT_DESC])
                ->all();

        $this->data['calendar_menu'] = $this->_getCalendar(Article::TYPE_ARTICLE);
        return $this->render("index");
    }

    public function actionArticlesDate($year, $month) {
        if (!$year OR ! $month) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Статьи",
            'url' => ["/admin/article"]
        ];

        $this->seo->setReplaces([
            'год' => $year,
            'месяц' => mb_convert_case(YText::getMonthName((int) $month), MB_CASE_LOWER, "UTF8")
        ]);

        $this->seo->addBreadcrumb(['label' => "Статьи", 'url' => ['article/articles']]);
        $this->seo->addBreadcrumb('Статьи за ' . mb_convert_case(YText::getMonthName((int) $month), MB_CASE_LOWER, "UTF8") . " " . $year . " года");

        $query = Article::find();

        $query->where(['type_id' => Article::TYPE_ARTICLE, 'status' => 1]);

        $query->andWhere(['>', 'created_at', $year . "-" . $month . "-0"]);
        $query->andWhere(['<', 'created_at', $year . "-" . $month . "-31"]);

        $this->data['models'] = $query
                ->orderBy(["created_at" => SORT_DESC])
                ->all();

        $this->data['calendar_menu'] = $this->_getCalendar(Article::TYPE_ARTICLE, $year, $month);

        return $this->render("index");
    }

    public function actionArticlesItem($id, $filter_date) {
        if (!$article = Article::find()->where(['id' => $id, 'status' => 1])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if ($filter_date != date("Y-m", strtotime($article->created_at))) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->seo->attachModel($article);

        $this->seo->addBreadcrumb(['label' => 'Статьи', 'url' => ['article/articles']]);
        $this->seo->addBreadcrumb(['label' => $article->title]);

        $this->seo->setReplaces([
            'название' => $article->title
        ]);

        $this->admin_menu[] = [
            'label' => "Статьи",
            'url' => ["/admin/news"]
        ];
        $this->admin_menu[] = [
            'label' => "Статья (id: " . $article->id . ")",
            'url' => ["/admin/news/article", "id" => $article->id]
        ];


        $this->data['model'] = $article;

        return $this->render('view');
    }

    private function _getCalendar($type_id, $current_year = NULL, $current_month = NULL) {
        $calendar = [];


        $menu_items = [];
        foreach (Article::find()->select(['created_at'])->filterWhere(['type_id' => $type_id])->asArray()->orderBy(['created_at' => SORT_DESC])->all() as $item) {
            $year = date("Y", strtotime($item['created_at']));
            $month = date("m", strtotime($item['created_at']));

            if (empty($menu_items[$year])) {
                $menu_items[$year] = [];
            }

            $menu_items[$year][$month] = $month;
        }

        $calendar = [];
        foreach ($menu_items as $year => $months) {
            $first_month = current($months);


            $route = "";

            if ($type_id == Article::TYPE_NEWS) {
                $route = 'article/news-date';
            } elseif ($type_id == Article::TYPE_ARTICLE) {
                $route = 'article/articles-date';
            } elseif ($type_id == Article::TYPE_FEED) {
                $route = 'article/feed-date';
            }

            $menu_item = [
                'label' => $year,
                'href' => [$route, 'year' => $year, 'month' => $first_month],
                'active' => ($year == $current_year ? TRUE : FALSE),
                'items' => []
            ];

            foreach ($months as $month) {
                $menu_item['items'][] = [
                    'label' => YText::getMonthName($month),
                    'href' => [$route, 'year' => $year, 'month' => $month],
                    'active' => ((int) $month == $current_month ? TRUE : FALSE),
                ];
            }

            $calendar[] = $menu_item;
        }

        return $calendar;
    }

}
