<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Action;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\helpers\YText;

class ActionController extends \frontend\components\controllers\FrontController {

    public function actionIndex() {
        $this->admin_menu[] = [
            'label' => "Акции",
            'url' => ["/admin/action"]
        ];

        $query = Action::find();
        $query->where(['status' => 1]);

        $this->seo->addBreadcrumb('Акции');

        $this->data['models'] = $query
                ->orderBy(["created_at" => SORT_DESC])
                ->all();

        return $this->render("index");
    }

    public function actionView($id, $year) {
        if (!$action = Action::find()->where(['id' => $id])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        if ($year != date("Y", strtotime($action->created_at))) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->seo->attachModel($action);

        $this->seo->addBreadcrumb(['label' => 'Акции', 'url' => ['action/index']]);
        $this->seo->addBreadcrumb(['label' => $action->title]);

        $this->seo->setReplaces([
            'название' => $action->title
        ]);

        $this->admin_menu[] = [
            'label' => "Акции",
            'url' => ["/admin/action"]
        ];
        $this->admin_menu[] = [
            'label' => "Акция (id: " . $action->id . ")",
            'url' => ["/admin/action/update", "id" => $action->id]
        ];


        $this->data['model'] = $action;

        return $this->render('view');
    }

}
