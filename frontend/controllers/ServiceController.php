<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Seo;
use common\models\Category;
use common\models\Village;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\helpers\YText;

class ServiceController extends \frontend\components\controllers\FrontController {

    public function actionIndex() {
        
    }

    public function actionMark() {
        $mark_name = Yii::$app->request->post('mark_name', Yii::$app->request->get('mark_name'));

        if ($mark_name == 'show-phone') {
            $mail = Yii::$app->mail->compose('clear', ['message' => 'mark: клик по номеру телефона']);
            $mail->setSubject('sabidom.ru: mark');
            $mail->setFrom(\Yii::$app->params['email_from']);
            $mail->setTo(explode(",", Settings::getByAlias('email_for_order')));
            $mail->send();

            //sabidom.zakaz@yandex.ru
        }
    }

}
