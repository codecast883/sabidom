<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Company;
use common\models\User;
use common\models\CompanyCategory;
use common\models\CompanyToCategory;
use common\models\CompanyCategoryPath;
use \frontend\models\CompanyReview;
use \frontend\models\CompanyWorkerReview;
use yii\helpers\ArrayHelper;

class UserController extends \frontend\components\controllers\FrontController {

	public function init() {
		parent::init();
		$this->data['menu'] = array(
			['label' => 'Мои компании', 'url' => ['/user/company-list']],
			['label' => 'Профиль', 'url' => ['/user/profile']],
		);


		$this->data['user'] = \Yii::$app->user;
	}

	public function behaviors() {
		$behaviors = [
			'access' => [
				'rules' => [
					[
						'allow' => true,
//						'roles' => ['@'], // залогинен
						'roles' => ['CompanyOwner', 'SuperAdministrator'],
//						'roles' => ['SuperAdministrator'],
//						'matchCallback' => function ($rule, $action) {return in_array(\Yii::$app->user->identity->username, ['chuzzlik', 'admin']);}
					],
				],
			],
		];
		return array_merge_recursive(parent::behaviors(), $behaviors);
	}

	public function actionIndex() {
		$this->data['user'] = \Yii::$app->user;


		print $this->render('index');
	}

	public function actionCompanyList() {
		$this->data['user_model'] = User::findOne(\Yii::$app->user->identity->getId());

		$this->data['companies'] = $this->data['user_model']->companies;

		print $this->render('company-list');
	}

	public function actionProfile() {
		$this->data['user'] = \Yii::$app->user;
		$this->data['user_model'] = User::findOne(\Yii::$app->user->identity->getId());
		$this->data['user_model']->scenario = 'user_update';

		if ($this->data['user_model']->load(\Yii::$app->request->post()) && $this->data['user_model']->save()) {
			\Yii::$app->getSession()->setFlash('success', 'User has been updated');
			return $this->refresh();
		}

		print $this->render('profile');
	}

}
