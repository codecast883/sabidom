<?php

namespace frontend\controllers;

use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Village;
use common\models\VillageSearch;
use common\models\Realt;
use common\models\RealtSearch;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\components\CategoryFunctions;

/**
 * Site controller
 */
class SearchController extends \frontend\components\controllers\FrontController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [];
    }

    public function actionIndex() {
        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=search"]
        ];

        $this->seo->addBreadcrumb(['label' => "Поиск"]);


        //preg_replace("|($q)|iu", "<span class='highlight'>$1</span>",  $company->title),

        $q = Yii::$app->request->get("q");

        $query = Realt::find();
        $query->andWhere(["OR",
            ['like', 'title', $q],
            ['like', 'description', $q],
                ]
        );
        $query->orderBy("title")->limit(15);
        $this->data['realts'] = $query->all();

        $query = Village::find();
        $query->andWhere(["OR",
            ['like', 'title', $q],
            ['like', 'description', $q],
                ]
        );
        $query->orderBy("title")->limit(15);
        $this->data['villages'] = $query->all();





        return $this->render('index');
    }

    public function actionAutocomplete() {
        $q = Yii::$app->request->get("q");

        $query = Company::find()->where(["status" => Company::STATUS_ENABLED]);
        $query->andWhere(['like', 'title', $q]);
        $query->orderBy("on_top DESC, title");
        $search_companies = $query->limit(10)->all();

        $json = [];
        $json["companies"] = [];
        foreach ($search_companies as $company) {
            $json["companies"][] = [
                "title_formated" => preg_replace("|($q)|iu", "<span class='highlight'>$1</span>", $company->title),
                "title" => $company->title,
                "href" => $company->url
            ];
        }

        return json_encode($json["companies"]);
    }

}
