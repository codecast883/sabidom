<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Field;
use common\models\Category;
use common\models\Village;
use common\models\VillageTownhouse;
use frontend\models\VillageSearch;
use frontend\models\VillageReview;
use common\models\VillageViewRequest;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\models\Section;

class VillageController extends \frontend\components\controllers\FrontController {

	public function actionView($id) {
		if (!$village = Village::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$village->incrementViews();

		$this->seo->attachModel($village);

		$root_category = Category::find()->where(['content_type_id' => Category::TYPE_VILLAGE, 'parent_id' => 0])->orderBy(['id' => SORT_ASC])->one();
		if ($root_category) {
			$this->seo->addBreadcrumb(['label' => $root_category->title, 'url' => $root_category->url]);
		}
		$this->seo->addBreadcrumb(['label' => $village->title]);

		$this->admin_menu[] = [
			'label' => "Поселок (id: " . $village->id . ")",
			'url' => ["/admin/village/update", "id" => $village->id]];


		$view_request_form['model'] = new VillageViewRequest;
		$view_request_form['model']->village_id = $village->id;

		$view_request_form['message'] = "";
		$view_request_form['success'] = FALSE;
		$view_request_form['error'] = FALSE;
		//$reviews_form['model']->scenario = "create_from_front";

		if ($view_request_form['model']->load(Yii::$app->request->post())) {
			if ($view_request_form['model']->save()) {
				$view_request_form['model'] = new VillageViewRequest;
				$view_request_form['model']->village_id = $village->id;

				$view_request_form['success'] = TRUE;
				$view_request_form['message'] = "Заявка отправлена";
			} else {
				$view_request_form['error'] = TRUE;
				$view_request_form['message'] = "Проверьте форму на ошибки";
			}
		}

		$view_request_form['model']->scenario = "default";
		$this->data['view_request_form'] = $view_request_form;

		$this->data['village'] = $village;
		return $this->render('view');
	}

	public function actionReviews($id) {
		if (!$village = Village::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$this->seo->fillReplacesFromModel($village);
		$this->admin_menu[] = [
			'label' => "Поселок (id: " . $village->id . ")",
			'url' => ["/admin/village/update", "id" => $village->id]];


		$this->seo->breadcrumbs = [
			['label' => 'Поселки', 'url' => '/objects/kottedzhnye-poselki'],
			['label' => $village->title, 'url' => $village->url],
			['label' => "Отзывы"],
		];

		$this->data['village'] = $village;

		$this->data['reviews_form'] = $this->getReviewForm($village);

		$this->data['reviews_info'] = $village->getReviewsInfo();
		$this->data['rating_info'] = $village->getRatingInfo();


		$query = VillageReview::find()->where(['village_id' => $village->id]);

		if (\Yii::$app->user->can("front-company-manager")) {
			# нет фильтра по статусу
		} else {
			$query->andWhere(['status' => VillageReview::STATUS_ENABLED]);
		}



		$pagination = new Pagination(['totalCount' => $query->count()]);
		$pagination->defaultPageSize = 16;

		$this->data['pagination'] = $pagination;

		$this->data['reviews'] = $query
				//->orderBy("created_at DESC")
				->orderBy("id ASC")
				->offset($pagination->offset)
				->limit($pagination->limit)
				->all();

		return $this->render('section_reviews');
	}

	private function getReviewForm($village) {
		$data['message'] = "";
		$data['success'] = FALSE;
		$data['error'] = FALSE;

		$data['model'] = new VillageReview();
		$data['model']->scenario = "create_from_front";
		if ($data['model']->load(Yii::$app->request->post())) {
			$data['model']->village_id = $village->id;
			$data['model']->status = VillageReview::STATUS_ENABLED;
			$data['model']->is_new = VillageReview::IS_NEW_YES;

			//if ($data['model']->save()) {
				$data['model'] = new VillageReview;
				$data['success'] = TRUE;
				$data['message'] = "Спасибо за Ваш отзыв";
//			} else {
//				$data['error'] = TRUE;
//				$data['message'] = "Проверьте форму на ошибки";
//				#$data['message'] = print_r($data['model']->getErrors(), 1);
//			}
		}

		$data['village'] = $village;
		return $data;
	}

	public function actionContacts($id) {
		if (!$village = Village::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$this->seo->fillReplacesFromModel($village);
		$this->admin_menu[] = [
			'label' => "Поселок (id: " . $village->id . ")",
			'url' => ["/admin/village/update", "id" => $village->id]];


		$this->seo->breadcrumbs = [
			['label' => 'Поселки', 'url' => '/objects/kottedzhnye-poselki'],
			['label' => $village->title, 'url' => $village->url],
			['label' => $this->seo->breadcrumb],
		];

		$this->data['village'] = $village;

		return $this->render('section_contacts');
	}

	public function actionDocumentation($id) {
		if (!$village = Village::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$this->seo->fillReplacesFromModel($village);
		$this->admin_menu[] = [
			'label' => "Поселок (id: " . $village->id . ")",
			'url' => ["/admin/village/update", "id" => $village->id]];


		$this->seo->breadcrumbs = [
			['label' => 'Поселки', 'url' => '/objects/kottedzhnye-poselki'],
			['label' => $village->title, 'url' => $village->url],
			['label' => $this->seo->breadcrumb],
		];

		$this->data['village'] = $village;

		return $this->render('section_documentation');
	}

	public function actionGeneralLayout($id) {
		if (!$village = Village::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$this->seo->fillReplacesFromModel($village);
		$this->admin_menu[] = [
			'label' => "Поселок (id: " . $village->id . ")",
			'url' => ["/admin/village/update", "id" => $village->id]];


		$this->seo->breadcrumbs = [
			['label' => 'Поселки', 'url' => '/objects/kottedzhnye-poselki'],
			['label' => $village->title, 'url' => $village->url],
			['label' => $this->seo->breadcrumb],
		];

		$this->data['village'] = $village;

		return $this->render('section_general-layout');
	}

	public function actionProgress($id) {
		if (!$village = Village::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$this->seo->fillReplacesFromModel($village);
		$this->admin_menu[] = [
			'label' => "Поселок (id: " . $village->id . ")",
			'url' => ["/admin/village/update", "id" => $village->id]];


		$this->seo->breadcrumbs = [
			['label' => 'Поселки', 'url' => '/objects/kottedzhnye-poselki'],
			['label' => $village->title, 'url' => $village->url],
			['label' => $this->seo->breadcrumb],
		];

		$this->data['village'] = $village;

		return $this->render('section_progress');
	}

	public function actionPhoto($id) {
		if (!$village = Village::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$this->seo->fillReplacesFromModel($village);

		$this->admin_menu[] = [
			'label' => "Поселок (id: " . $village->id . ")",
			'url' => ["/admin/village/update", "id" => $village->id]];


		$this->seo->breadcrumbs = [
			['label' => 'Поселки', 'url' => '/objects/kottedzhnye-poselki'],
			['label' => $village->title, 'url' => $village->url],
			['label' => $this->seo->breadcrumb],
		];

		$this->data['village'] = $village;

		return $this->render('section_photo');
	}

	public function actionTownhouse($id) {
		if (!$village = Village::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$this->seo->fillReplacesFromModel($village);
		$this->admin_menu[] = [
			'label' => "Поселок (id: " . $village->id . ")",
			'url' => ["/admin/village/update", "id" => $village->id]];


		$this->seo->breadcrumbs = [
			['label' => 'Поселки', 'url' => '/objects/kottedzhnye-poselki'],
			['label' => $village->title, 'url' => $village->url],
			['label' => $this->seo->breadcrumb],
		];

		$this->data['village'] = $village;

		$view_request_form['model'] = new VillageViewRequest;
		$view_request_form['model']->village_id = $village->id;

		$view_request_form['message'] = "";
		$view_request_form['success'] = FALSE;
		$view_request_form['error'] = FALSE;
		//$reviews_form['model']->scenario = "create_from_front";

		if ($view_request_form['model']->load(Yii::$app->request->post())) {
			if ($view_request_form['model']->save()) {
				$view_request_form['model'] = new VillageViewRequest;
				$view_request_form['model']->village_id = $village->id;

				$view_request_form['success'] = TRUE;
				$view_request_form['message'] = "Заявка отправлена";
			} else {
				$view_request_form['error'] = TRUE;
				$view_request_form['message'] = "Проверьте форму на ошибки";
			}
		}

		$this->data['projects'] = VillageTownhouse::find()->where(['village_id' => $id])->all();

		$view_request_form['model']->scenario = "default";
		$this->data['view_request_form'] = $view_request_form;

		return $this->render('section_townhouse');
	}

	public function actionTownhouseView($id) {
//        # услуги
//        $values = [
//            ['name' => 'Такси', 'template' => '<ins class="icon icon_s icon_s_taxi"></ins>Такси'],
//            ['name' => 'Школьный автобус', 'template' => '<ins class = "icon icon_s icon_s_school_bus"></ins> Школьный автобус'],
//            ['name' => 'Доставка домашней еды', 'template' => '<ins class = "icon icon_s icon_s_home_food"></ins> Доставка домашней еды'],
//            ['name' => 'Охрана', 'template' => '<ins class = "icon icon_s icon_s_security"></ins> Охрана'],
//            ['name' => 'Камера хранения', 'template' => '<ins class = "icon icon_s icon_s_cloakroom"></ins> Камера хранения'],
//            ['name' => 'Домашний персонал', 'template' => '<ins class = "icon icon_s icon_s_staff"></ins> Домашний персонал'],
//        ];
//        foreach ($values as $value) {
//            $model = Field::autoGet($value['name'], Field::TYPE_YES_NO, 5);
//            $model->template = $value['template'];
//            $model->save();
//
////            $val = new \common\models\VillageTownhouseFieldValue;
////            $val->model_id = 1;
////            $val->field_id = $model->id;
////            $val->value = "1";
////            if(!$val->save()){
////                print_r ($val->getErrors());
////            }
//        }
//
//        #инфраструктура
//        $values = [
//            ['name' => 'Бассейн', 'template' => '<ins class="icon icon_i icon_i_water"></ins> Бассейн'],
//            ['name' => 'Автобусная остановка', 'template' => '<ins class="icon icon_i icon_i_bus_stop"></ins> Автобусная остановка'],
//            ['name' => 'Детская площадка', 'template' => '<ins class="icon icon_i icon_i_playground"></ins> Детская площадка'],
//        ];
//        foreach ($values as $value) {
//            $model = Field::autoGet($value['name'], Field::TYPE_YES_NO, 6);
//            $model->template = $value['template'];
//            $model->save();
//
////            $val = new \common\models\VillageTownhouseFieldValue;
////            $val->model_id = 1;
////            $val->field_id = $model->id;
////            $val->value = "1";
////            if (!$val->save()) {
////                print_r($val->getErrors());
////            }
//        }
//
//        #коммуникации
//        $values = [
//            ['name' => 'Магистральный газ', 'template' => '<ins class="icon icon_c icon_c_gas"></ins> Магистральный газ'],
//            ['name' => 'Электричество', 'template' => '<ins class="icon icon_c icon_c_electricity"></ins> Электричество'],
//            ['name' => 'Водоснабжение', 'template' => '<ins class="icon icon_c icon_c_water_supply"></ins> Водоснабжение'],
//            ['name' => 'Канализация', 'template' => '<ins class="icon icon_c icon_c_sewerage"></ins> Канализация'],
//            ['name' => 'Вентиляция', 'template' => '<ins class="icon icon_c icon_c_ventilation"></ins> Вентиляция'],
//            ['name' => 'Камин', 'template' => '<ins class="icon icon_c icon_c_gas"></ins> Камин'],
//            ['name' => 'Гараж', 'template' => '<ins class="icon icon_c icon_c_garage"></ins> Гараж'],
//            ['name' => 'Эксплуатируемая кровля', 'template' => '<ins class="icon icon_c icon_c_operated_roof"></ins> Эксплуатируемая кровля'],
//        ];
//
//        foreach ($values as $value) {
//            $model = Field::autoGet($value['name'], Field::TYPE_YES_NO, 3);
//            $model->template = $value['template'];
//            $model->save();
//
////            $val = new \common\models\VillageTownhouseFieldValue;
////            $val->model_id = 1;
////            $val->field_id = $model->id;
////            $val->value = "1";
////            if (!$val->save()) {
////                print_r($val->getErrors());
////            }
//        }
//
//
//        #Технические параметры
//        $values = [
//            ['name' => "Стены", 'value' => "Газобетонный блок YTONG 300мм, D500"],
//            ['name' => "Утепление монолитных участков", 'value' => "Пенополистирол 100 мм"],
//            ['name' => "Облицовка фасада", 'value' => "клинкерный кирпич, баварская кладка"],
//            ['name' => "Перекрытия", 'value' => "Монолитные железобетонные"],
//            ['name' => "Дороги", 'value' => "Брусчатка"],
//            ['name' => "Фундамент", 'value' => "Монолитная лента, h = 1700 мм, w = 400 мм"],
//            ['name' => "Перегородки", 'value' => "Пеноблок, 100 мм"],
//            ['name' => "Дверь", 'value' => "Стандартные, установлены на двух входах"],
//            ['name' => "Окна", 'value' => "Двухкамерный стеклопакет KBE"],
//            ['name' => "Крыша", 'value' => "Эксплуатируемая"],
//            ['name' => "Внутри", 'value' => "Под отделку, вводы коммуникаций, разведена газовая магистраль, топочное и водогрейное оборудование установлено"],
//            ['name' => "Потолок", 'value' => "h = 3000 мм"],
//        ];
//
//        foreach ($values as $value) {
//            $model = Field::autoGet($value['name'], Field::TYPE_TEXT, 4);
////
////            $val = new \common\models\VillageTownhouseFieldValue;
////            $val->model_id = 1;
////            $val->field_id = $model->id;
////            $val->value = $value['value'];
////            if (!$val->save()) {
////                print_r($val->getErrors());
////            }
//        }

		if (!$village_townhouse = VillageTownhouse::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}


		$this->seo->attachModel($village_townhouse);
		$this->admin_menu[] = [
			'label' => "Поселок (id: " . $village_townhouse->village->id . ")",
			'url' => ["/admin/village/update", "id" => $village_townhouse->village->id]];

		$this->admin_menu[] = [
			'label' => "Таунхаус (id: " . $village_townhouse->id . ")",
			'url' => ["/admin/village-townhouse/update", "id" => $village_townhouse->id]];

		$this->seo->breadcrumbs = [
			['label' => 'Поселки', 'url' => '/objects/kottedzhnye-poselki'],
			['label' => $village_townhouse->village->title, 'url' => $village_townhouse->village->url],
			['label' => "Объявления", 'url' => $village_townhouse->village->getUrl('townhouse')],
			['label' => $village_townhouse->title],
		];

		$this->data['village'] = $village_townhouse->village;
		$this->data['village_townhouse'] = $village_townhouse;

		$this->data['other_projects'] = VillageTownhouse::find()->where(['village_id' => $village_townhouse->village_id])->all();

		return $this->render('section_townhouse_view');
	}

	public function actionAjaxAds($id) {
		$list_type = Yii::$app->request->get("list-type", "list");

		if ($list_type == "grid") {
			print $this->renderAjax('ads_grid', $data);
		} elseif ($list_type == "list") {
			print $this->renderAjax('ads_list', $data);
		}
	}

}
