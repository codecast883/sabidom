<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Company;
use common\models\Rating;
use common\models\CompanyCategory;
use common\models\CompanyRating;
use frontend\models\CompanyReview;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class RatingController extends \frontend\components\controllers\FrontController {

    public function behaviors() {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actionIndex() {
        $seo_settings = \common\models\Section::getSettings('rating');
        $this->setPageTitle($seo_settings->seo_title);
        $this->setPageDescription($seo_settings->seo_description);
        $this->setPageKeywords($seo_settings->seo_keywords);

        $this->data['seo_h1'] = $seo_settings->seo_h1;
        $this->data['description_top'] = $seo_settings->description_top;
        $this->data['description_bottom'] = $seo_settings->description_bottom;

        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=rating"]
        ];

        $this->data['blocks'] = [];
        foreach (Rating::find()->orderBy("sort_order ASC, title ASC")->all() as $rating_category) {
            $current_year = date("Y");
            $block = [];
            $block['category'] = $rating_category;
            $block['years_menu'] = [];

            $first_review_year = CompanyRating::find()->where(['>', 'year', '0'])->min("year");
            for ($i = 0; $i < 4 AND date("Y") - $i >= $first_review_year; $i++) {
                $block['years_menu'][] = [
                    'label' => date("Y") - $i,
                    'title' => $rating_category->title." ".(date("Y") - $i)." года",
                    'url' => \yii\helpers\Url::to(['/rating/category', 'category_slug' => $rating_category->slug, 'year' => date("Y") - $i]),
                    'active' => $current_year == date("Y") - $i ? TRUE : FALSE
                ];
            }

            $block = array_merge($block, CompanyRating::preparePage(1, 10, $rating_category->id, $current_year));
            $this->data['blocks'][] = $block;
        }


        return $this->render("index");
    }

    public function actionCategory() {
        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=rating"]
        ];

        $current_year = ArrayHelper::getValue($_GET, "year", date("Y"));
        $native_current_year = ArrayHelper::getValue($_GET, "year");

        $rating_category = Rating::find()->where(["slug" => \yii\helpers\ArrayHelper::getValue($_GET, "category_slug")])->one();

        $this->setPageTitle(($rating_category->seo_title ? $rating_category->seo_title : $rating_category->title) . ($native_current_year ? " " . $native_current_year . "" : ""));
        $this->setPageDescription($rating_category->seo_description);
        $this->setPageKeywords($rating_category->seo_keywords);

        $this->data['seo_h1'] = ($rating_category->seo_h1 ? $rating_category->seo_h1 : $rating_category->title) . ($native_current_year ? " " . $native_current_year . "" : "");
        $this->data['description_top'] = $rating_category->description;
//            $this->data['description_bottom'] = $rating_category->description_bottom;

        $this->data['description_top'] = $rating_category->description;

        $this->admin_menu[] = [
            'label' => "Рейтинг (id: " . $rating_category->id . ")",
            'url' => ["/admin/rating/update", "id" => $rating_category->id]
        ];

        $category_for_menu = Rating::find()->orderBy("title ASC")->all();


        $this->data['category'] = $rating_category;

        $this->data['years_menu'] = [];

        $first_review_year = CompanyRating::find()->where(['>', 'year', '0'])->min("year");
        for ($i = 0; $i < 4 AND date("Y") - $i >= $first_review_year; $i++) {
            $this->data['years_menu'][] = [
                'label' => date("Y") - $i,
                'title' => $rating_category->title." ".(date("Y") - $i)." года",
                'url' => \yii\helpers\Url::to(['/rating/category', 'category_slug' => $rating_category->slug, 'year' => date("Y") - $i]),
                'active' => $native_current_year == date("Y") - $i ? TRUE : FALSE
            ];
        }

        $this->data['categories_menu'] = [];
        foreach ($category_for_menu as $item) {
            $this->data['categories_menu'][] = [
                'label' => $item->title,
                'url' => \yii\helpers\Url::to(['/rating/category', 'category_slug' => $item->slug]),
                'active' => $item->id == $rating_category->id ? TRUE : FALSE
            ];
        }

        $this->data = array_merge($this->data, CompanyRating::preparePage(1, 10, $rating_category->id, $current_year));

        return $this->render("category");
    }

    public function actionPage() {
        $json = [];
        $data = CompanyRating::preparePage(
                        Yii::$app->request->post('page'), 20, Yii::$app->request->post('category_id'), Yii::$app->request->post('year', date("Y"))
        );

        $json['current_page'] = $data['pagination_info']['current_page'];
        $json['html'] = $this->renderAjax('_list', $data);
        return json_encode($json);
    }

}
