<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Company;
use common\models\CompanyCategory;
use common\models\CompanyToCategory;
use common\models\CompanyCategoryPath;
use \frontend\models\CompanyReview;
use \frontend\models\CompanyWorkerReview;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;

class CompanyWorkerReviewController extends \frontend\components\controllers\FrontController {

    public function behaviors() {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actionView($path) {
        $view = NULL;
        $path = trim($path, "/");
        if ($company = Company::find()->where(['slug' => $path, 'status' => 1])->one()) {
            return $this->viewCompany($company);
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function viewCompany($company) {
        $this->admin_menu[] = [
            'label' => "Компания (id: " . $company->id . ")",
            'url' => ["/admin/company/update", "id" => $company->id]
        ];

        $this->setPageTitle($company->generateSeoTitle());
        $this->setPageDescription($company->generateSeoDescription());
        $this->setPageKeywords($company->generateSeoKeywords());

        $this->data['company'] = $company;

        $this->data['top_companies'] = $company->getTopCompanies(5);
        $this->data['categories_crosslink'] = $company->getCategoriesCrosslinks();
        $this->data['ratings_id'] = $company->category->ratings_id;

        $query = \common\models\CompanyWorkerReview::find()->where(['company_id' => $company->id]);

        if (\Yii::$app->user->can("front-company-manager")) {
            # нет фильтра по статусу
        } elseif (\Yii::$app->user->can("front-company-owner") AND $company->user_id == \Yii::$app->user->identity->getId()) {
            # нет фильтра по статусу
        } else {
            $query->andWhere(['status' => CompanyReview::STATUS_ENABLED]);
        }

        $pagination = new Pagination(['totalCount' => $query->count()]);
        $pagination->defaultPageSize = 7;

        $this->data['paginaton'] = $pagination;

        $this->data['reviews'] = $query
                ->orderBy("date_post DESC")
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        $this->data['pagination'] = $pagination;
        $this->data['company_rating_positions'] = $company->getRatingPositions(date("Y"));
        return $this->render("view");
    }

    public function actionAddReview() {
        $this->data['model'] = new CompanyWorkerReview;

        $this->data['model']->scenario = "create_from_front";

        if ($this->data['model']->load(Yii::$app->request->post())) {
            $this->data['company'] = \common\models\Company::findOne($this->data['model']->company_id);
            if (!$this->data['company']) {
                print $this->renderAjax('_reviews_form_error', ['message' => "Фирма не найдена"]);
                return;
            }

            if ($this->data['model']->save()) {
                print $this->renderAjax('_reviews_form_success', $this->data);
                return;
            } else {
                print_r($this->data['model']->errors);
                exit;
            }
        } else {
            return print_r ($this->data['model']->getErrors(), 1);
        }


        print $this->renderAjax('_reviews_form', $this->data);
        return;
    }

    public function actionReviewToggleStatus() {
        $review_id = Yii::$app->request->post('review_id');

        if ($review = CompanyWorkerReview::findOne($review_id)) {
            if ($review->can("toggle-status")) {
                $review->scenario = "toggle_status";

                if ($review->status == CompanyWorkerReview::STATUS_ENABLED)
                    $review->status = CompanyWorkerReview::STATUS_DISABLED;
                else
                    $review->status = CompanyWorkerReview::STATUS_ENABLED;

                if (!$review->save()) {
                    throw new Exception('Error: ' . print_r($review->getErrors(), 1));
                } else {
                    
                }
            } else {
                throw new Exception('Access denied');
            }
        }
    }

}
