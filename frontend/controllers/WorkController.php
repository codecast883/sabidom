<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Seo;
use common\models\Category;
use common\models\Village;
use frontend\models\VillageSearch;
use frontend\models\RealtSearch;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\helpers\YText;
use common\models\Field;
use common\models\FieldTag;

class WorkController extends \frontend\components\controllers\FrontController {

	public function actionIndex() {
		exit;
		$fh = fopen($_SERVER['DOCUMENT_ROOT'] . "/1.csv", "r+");

		print "<table>";
		fgetcsv($fh, 0, ";");
		while ($row = fgetcsv($fh, 0, ";")) {
			print "<tr>";
			if ($row[0] OR $row[1]) {
				$category_id = "";
				//print "<td>" . $row[0] . "</td>";
				if ($category = Category::find()->where(['seo_title' => $row[1]])->one()) {
					print "<td>" . $category->url . "</td>";
					print "<td>" . $category->id . "</td>";
				} elseif ($category = Category::find()->where(['seo_h1' => $row[0]])->one()) {
					print "<td>" . $category->url . "</td>";
					print "<td>" . $category->id . "</td>";
				} else {
					print "<td>NOT FOUND</td>";
					print "<td></td>";
				}
			}

			print "</tr>";
		}
		print "</table>";
	}

	//     количество объявлений
	public function actionCountCategoryRealts() {
		exit;
		set_time_limit(10000);
		print "<table>";
		foreach (Category::find()->each() as $model) {
			print "<tr><td>" . $model->id . "</td>";
			print "<td>" . $model->title . "</td>";
			print "<td>" . $model->url . "</td>";
			switch ($model->getContentTypeId()) {
				case Category::TYPE_VILLAGE:
					$searchModel = new VillageSearch();
					if ($filter = $model->getFullFilterArray()) {
						$searchModel->setFilters($filter);
					}

					print "<td>" . $searchModel->search()->getTotalCount() . "</td>";
					break;
				case Category::TYPE_COTTAGE:
				case Category::TYPE_STEAD:
				case Category::TYPE_TOWNHOUSE:
					$searchModel = new RealtSearch();
					if ($filter = $model->getFullFilterArray()) {
						$searchModel->setFilters($filter);
					}

					print "<td>" . $searchModel->search()->getTotalCount() . "</td>";
					break;
			}
			print "</tr>";
		}
		print "</table>";
		exit;
	}

	public function actionUpdateCategories() {
		exit;
		$handle = fopen($_SERVER['DOCUMENT_ROOT'] . "/categories.csv", "r");

		$text = "{Предлагаем|Готовы предложить|Можем предложить|Актуальное предложение|Рекомендуем|Рекомендуем Вам} {КЛЮЧЕВОЕ СЛОВО 1} - {свежие|актуальные|новые|лучшие|все|избранные} {предложения|объявления|публикации|объекты} в базе {загородного жилья|загородной недвижимости|жилья за городом|недвижимости за городом|жилья в Подмосковье|жилья в Московской области|недвижимости Подмосковья|недвижимости Московской области} на СабиДом. {НАЗВАНИЕ РОДИТЕЛЬСКОГО РАЗДЕЛА ИЗ ГЛАВНОГО МЕНЮ} {в Подмосковье|в Московской области|в ближайшем и дальнем Подмосковье} из базы Сабидом {продаются|на продаже|можно купить|можно приобрести|можно продать} {без посредников|без участия посредников|без привлечения посредников|без любых посредников}. Цены на {объекты недвижимости|недвижимость|всю недвижимость|недвижимое имущество|жилье} {представлены|размещены|описаны} {собственниками|непосредственно собственниками|только собственниками|исключительно собственниками|обязательно только собственниками}.";

		$rows = [];
		while ($row = fgetcsv($handle, 0, ";")) {
//          $row[11],
//          $row[12],

			$rows[] = [
				'id' => $row[0],
				'title' => $row[7],
				'seo_h1' => $row[8],
				'seo_title' => $row[9],
				'kw1' => $row[10],
				'kw2' => $row[11],
				'group' => $row[12],
				'main_menu' => $row[13] ? 1 : 0,
			];
		}
		array_shift($rows);

		foreach ($rows as $row) {
			if ($category = Category::findOne($row['id'])) {
				$category->title = $row['title'];
				$category->seo_h1 = $row['seo_h1'];
				$category->seo_title = $row['seo_title'];
				$category->kw1 = $row['kw1'];
				$category->kw2 = $row['kw2'];

				$substitutions = [
					"{КЛЮЧЕВОЕ СЛОВО 1}" => $category->kw1,
					"{НАЗВАНИЕ РОДИТЕЛЬСКОГО РАЗДЕЛА ИЗ ГЛАВНОГО МЕНЮ}" => $category->topLevel->title
				];
				$category->description_bottom = YText::textGenerator($text, $substitutions);
				$category->slug = "";
				$category->group_title = $row['group'];
				$category->on_header = $row['main_menu'];
				if ($category->on_header AND $row['group'] == "Пункт меню ЕЩЕ") {
					$category->on_header = 2;
				}

				$category->save();
			}
		}
	}

	public function actionTagHelp() {
		$this->data['fields'] = Field::find()->orderBy(['title' => SORT_ASC])->where(['type_id' => [1, 2]])->all();
		return $this->render("tag_help");
	}

	public function actionExportSeoCategories() {
		print "<table>";
		foreach (Category::find()->each() as $category) {
			print "<tr>";
			print "<td>" . $category->id . "</td>";
			print "<td>" . $category->url . "</td>";
			print "<td>" . $category->title . "</td>";
			print "<td>" . $category->seo_h1 . "</td>";
			print "<td>" . $category->seo_title . "</td>";
			print "</tr>";
		}
		print "</table>";
	}
	
	public function actionExportVillages() {
		print "<table>";
		foreach (Village::find()->each() as $model) {
			print "<tr>";
			print "<td>" . $model->id . "</td>";
			print "<td>" . $model->url . "</td>";
			print "<td>" . $model->title . "</td>";
			print "<td>" . $model->seo_h1 . "</td>";
			print "<td>" . $model->seo_title . "</td>";
			print "</tr>";
		}
		print "</table>";
	}
	
	public function actionExportSeo() {
		print "<table>";
		foreach (Seo::find()->each() as $model) {
			$params = [];
			foreach ($model->params as $item){
				$params[$item->field] = $item->template;
			}
			
			print "<tr>";
			print "<td>" . $model->id . "</td>";
			print "<td>" . $model->route . "</td>";
			print "<td>" . $model->title . "</td>";
			print "<td>" . $params['h1'] . "</td>";
			print "<td>" . $params['title'] . "</td>";
			print "</tr>";
		}
		print "</table>";
	}

}
