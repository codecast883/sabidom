<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Seo;
use common\models\Category;
use common\models\Village;
use common\models\Realt;
use frontend\models\RealtSearch;
use frontend\models\VillageSearch;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use common\models\Field;
use yii\data\Pagination;
use common\models\RealtViewRequest;
use common\models\VillageViewRequest;

class CategoryController extends \frontend\components\controllers\FrontController {

	private $_current_category = NULL;

	public function behaviors() {
		$behaviors = [
			'access' => [
				'rules' => [
					[
						'allow' => true,
						'roles' => ['?', '@'], // все
					],
				]
			],
			[
				'class' => 'yii\filters\HttpCache',
//                //    'only' => ['index'],
				'lastModified' => function ($action, $params) {
//                    return 1460095573;
					$q = new \yii\db\Query();
					return strtotime($q->from('realt')->max('updated_at')) + strtotime($q->from('village')->max('updated_at'))+strtotime($q->from('category')->max('updated_at'));
				},
			],
		];

		return array_merge_recursive(parent::behaviors(), $behaviors);
	}

	public function actionView($id) {
		if (!$category = Category::findOne($id)) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		$this->_current_category = $category;
		$this->admin_menu[] = [
			'label' => "Категория (id: " . $category->id . ")",
			'url' => ["/admin/category/update", "id" => $category->id]];

		$this->seo->attachModel($category);

		$breadcrumbs = [];

		$p = $category;
		while ($p = $p->parent) {
			$breadcrumbs[] = ['label' => $p->title, 'url' => $p->url];
		}

		$this->seo->breadcrumbs = array_reverse($breadcrumbs);

		$this->data['category'] = $category;
		$this->data['current_page'] = \Yii::$app->request->get('page');

		$this->seo->canonical_url = $category->url . ($this->data['current_page'] ? "?page=" . $this->data['current_page'] : "");

		switch ($category->getContentTypeId()) {
			case Category::TYPE_VILLAGE:
				return $this->showVillages();
				break;
			case Category::TYPE_COTTAGE:
				return $this->showCottage();
				break;
			case Category::TYPE_STEAD:
				return $this->showStead();
			case Category::TYPE_TOWNHOUSE:
				return $this->showTownhouse();
				break;
		}
	}

	public function actionFilter($type_id) {
		switch ($type_id) {
			case Category::TYPE_VILLAGE:;
				return $this->showVillages();
				break;
			case Category::TYPE_COTTAGE:
				return $this->showCottage();
			case Category::TYPE_STEAD:
				return $this->showStead();
			case Category::TYPE_TOWNHOUSE:
				return $this->showTownhouse();
				break;
		}
	}

	private function showVillagesFilter() {
		$searchModel = new VillageSearch();
		if (!empty($this->data['category']) AND $filter = $this->data['category']->getFullFilterArray()) {
			$searchModel->setFilters($filter);
		}

		$searchModel->appendFilters(Yii::$app->request->get('filter', []));

		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		//$dataProvider->sort['defaultOrder'] = ['title' => SORT_ASC];
		$dataProvider->sort = ['defaultOrder' => ['premium' => SORT_DESC, 'sort_order'=>SORT_ASC, 'id' => SORT_ASC]];
		$dataProvider->pagination->defaultPageSize = 35;

		$this->data['models'] = $dataProvider->getModels();
		$this->data['pagination'] = $dataProvider->pagination;
		$this->data['search_model'] = $searchModel;

		// fix для превышения лимита страниц
		if ($dataProvider->pagination->page + 1 != Yii::$app->request->get($dataProvider->pagination->pageParam, 1)) {
			$valid_page = $dataProvider->pagination->page + 1;
			parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $get_params);
			unset($get_params[$dataProvider->pagination->pageParam]);
			return $this->redirect(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) . ($get_params ? "?" . http_build_query($get_params) : ""), 301);
		}

		$this->data['filter'] = Yii::$app->request->get('filter', []);
		$this->data['filter_fields'] = [];
		foreach ([3, 4, 7, 8, 9, 16, 19] as $field_id) {
			$field = Field::findOne($field_id);
			$this->data['filter_fields'][$field_id] = ['values' => ArrayHelper::map($field->getFieldTags()->orderBy('value')->all(), 'id', 'value')];
		}

		if ($this->data['pagination']->page > 0) {
			$this->seo->h1 = $this->seo->h1 . " " . ($this->data['pagination']->page + 1) . " страница каталога объектов недвижимости";

			if (!empty($this->data['category'])) {
				$this->seo->addBreadcrumb(['label' => $this->data['category']->title, 'url' => $this->data['category']->url]);
			} else {
				$this->seo->addBreadcrumb("Фильтр");
			}

			$this->seo->addBreadcrumb(['label' => "Страница " . ($this->data['pagination']->page + 1)]);
		} else {
			if (!empty($this->data['category'])) {
				$this->seo->addBreadcrumb($this->data['category']->title);
			} else {
				$this->seo->addBreadcrumb("Фильтр");
			}
		}

		$this->data['view_request_form'] = $this->viewRequestFormVillage();
	}

	private function showRealtFilter($type_id) {
		$searchModel = new RealtSearch();
		if (!empty($this->data['category']) AND $filter = $this->data['category']->getFullFilterArray()) {
			$searchModel->setFilters($filter);
		}

		$searchModel->appendFilters(Yii::$app->request->get('filter', []));
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$dataProvider->query->andWhere(['type_id' => $type_id]);
		$dataProvider->pagination->defaultPageSize = 35;


		$this->data['models'] = $dataProvider->getModels();
		$this->data['pagination'] = $dataProvider->pagination;
		$this->data['search_model'] = $searchModel;

		// fix для превышения лимита страниц
		if ($dataProvider->pagination->page + 1 != Yii::$app->request->get($dataProvider->pagination->pageParam, 1)) {
			$valid_page = $dataProvider->pagination->page + 1;
			parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $get_params);
			unset($get_params[$dataProvider->pagination->pageParam]);
			return $this->redirect(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) . ($get_params ? "?" . http_build_query($get_params) : ""), 301);
		}

		$this->data['filter'] = Yii::$app->request->get('filter', []);
		$this->data['filter_fields'] = [];
		foreach ([3, 4, 7, 8, 9, 16, 19] as $field_id) {
			$field = Field::findOne($field_id);
			$this->data['filter_fields'][$field_id] = ['values' => ArrayHelper::map($field->getFieldTags()->orderBy('value')->all(), 'id', 'value')];
		}

		if ($this->data['pagination']->page > 0) {
			$this->seo->h1 = $this->seo->h1 . " " . ($this->data['pagination']->page + 1) . " страница каталога объектов недвижимости";

			if (!empty($this->data['category'])) {
				$this->seo->addBreadcrumb(['label' => $this->data['category']->title, 'url' => $this->data['category']->url]);
			} else {
				$this->seo->addBreadcrumb("Фильтр");
			}

			$this->seo->addBreadcrumb(['label' => "Страница " . ($this->data['pagination']->page + 1)]);
		} else {
			if (!empty($this->data['category'])) {
				$this->seo->addBreadcrumb($this->data['category']->title);
			} else {
				$this->seo->addBreadcrumb("Фильтр");
			}
		}

		$this->data['view_request_form'] = $this->viewRequestFormRealt();
	}

	private function viewRequestFormRealt() {
		$data['model'] = new RealtViewRequest;
		$data['message'] = "";
		$data['success'] = FALSE;
		$data['error'] = FALSE;
		//$reviews_form['model']->scenario = "create_from_front";

		if ($data['model']->load(Yii::$app->request->post())) {
			if ($data['model']->save()) {
				$data['model'] = new RealtViewRequest;
				$data['success'] = TRUE;
				$data['message'] = "Заявка отправлена";
			} else {
				$data['error'] = TRUE;
				$data['message'] = "Проверьте форму на ошибки";
			}
		}

		$data['model']->scenario = "default";
		return $data;
	}

	private function viewRequestFormVillage() {
		$data['model'] = new VillageViewRequest;
		$data['message'] = "";
		$data['success'] = FALSE;
		$data['error'] = FALSE;
		//$reviews_form['model']->scenario = "create_from_front";

		if ($data['model']->load(Yii::$app->request->post())) {
			if ($data['model']->save()) {
				$data['model'] = new VillageViewRequest;
				$data['success'] = TRUE;
				$data['message'] = "Заявка отправлена";
			} else {
				$data['error'] = TRUE;
				$data['message'] = "Проверьте форму на ошибки";
			}
		}

		$data['model']->scenario = "default";
		return $data;
	}

	private function showVillages() {
		$this->showVillagesFilter();
		return $this->render('villages');
	}

//    !!!!!!!!!!!!
//    private function showRealt() {
//        $this->showRealtFilter($this->data['category']->getContentTypeId());
//        $this->data['search_view'] = '_realt_cottage_search';
//        return $this->render('realt');
//    }

	private function showCottage() {
		$this->showRealtFilter(Category::TYPE_COTTAGE);
		$this->data['search_view'] = '_realt_cottage_search';
		return $this->render('realt');
	}

	private function showStead() {
		$this->showRealtFilter(Category::TYPE_STEAD);
		$this->data['search_view'] = '_realt_stead_search';
		return $this->render('realt');
	}

	private function showTownhouse() {
		$this->showRealtFilter(Category::TYPE_TOWNHOUSE);
		$this->data['search_view'] = '_realt_townhouse_search';
		return $this->render('realt');
	}

	/*
	  public function actionExport() {
	  $rows = [];
	  foreach (Category::find()->where(['parent_id' => 0])->orderBy('title')->all() as $cat_1) {
	  $row = [
	  $cat_1->id,
	  $cat_1->title,
	  $cat_1->title,
	  "",
	  "",
	  "",
	  "",
	  $cat_1->title,
	  $cat_1->seo_h1,
	  $cat_1->seo_title,
	  $cat_1->kw1,
	  $cat_1->kw2,
	  $cat_1->group_title,
	  $cat_1->on_header,
	  ];
	  $rows [] = $row;


	  foreach ($cat_1->childrens as $cat_2) {
	  $row = [
	  $cat_2->id,
	  $cat_1->title . " / " . $cat_2->title,
	  $cat_1->title,
	  $cat_2->title,
	  "",
	  "",
	  "",
	  $cat_2->title,
	  $cat_2->seo_h1,
	  $cat_2->seo_title,
	  $cat_2->kw1,
	  $cat_2->kw2,
	  $cat_2->group_title,
	  $cat_2->on_header,
	  ];
	  $rows [] = $row;

	  foreach ($cat_2->childrens as $cat_3) {
	  $row = [
	  $cat_3->id,
	  $cat_1->title . " / " . $cat_2->title . " / " . $cat_3->title,
	  $cat_1->title,
	  $cat_2->title,
	  $cat_3->title,
	  "",
	  "",
	  $cat_3->title,
	  $cat_3->seo_h1,
	  $cat_3->seo_title,
	  $cat_3->kw1,
	  $cat_3->kw2,
	  $cat_3->group_title,
	  $cat_3->on_header,
	  ];
	  $rows [] = $row;

	  foreach ($cat_3->childrens as $cat_4) {
	  $row = [
	  $cat_4->id,
	  $cat_1->title . " / " . $cat_2->title . " / " . $cat_3->title . " / " . $cat_4->title,
	  $cat_1->title,
	  $cat_2->title,
	  $cat_3->title,
	  $cat_4->title,
	  "",
	  $cat_4->title,
	  $cat_4->seo_h1,
	  $cat_4->seo_title,
	  $cat_4->kw1,
	  $cat_4->kw2,
	  $cat_4->group_title,
	  $cat_4->on_header,
	  ];
	  $rows [] = $row;

	  foreach ($cat_4->childrens as $cat_5) {
	  $row = [
	  $cat_5->id,
	  $cat_1->title . " / " . $cat_2->title . " / " . $cat_3->title . " / " . $cat_4->title . " / " . $cat_5->title,
	  $cat_1->title,
	  $cat_2->title,
	  $cat_3->title,
	  $cat_4->title,
	  $cat_5->title,
	  $cat_5->title,
	  $cat_5->seo_h1,
	  $cat_5->seo_title,
	  $cat_5->kw1,
	  $cat_5->kw2,
	  $cat_5->group_title,
	  $cat_5->on_header,
	  ];
	  $rows [] = $row;
	  }
	  }
	  }
	  }
	  }

	  print "<table>";
	  foreach ($rows as $row) {
	  print "<tr>";
	  foreach ($row as $col) {
	  print "<td>" . $col . "</td>";
	  }
	  print "</tr>";
	  }
	  print "</table>";
	  }
	 */
}
