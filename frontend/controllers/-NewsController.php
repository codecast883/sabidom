<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use \common\models\News;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;

class NewsController extends \frontend\components\controllers\FrontController {

    public function behaviors() {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actionIndex() {
        $seo_settings = \common\models\Section::getSettings('news');
        $this->setPageTitle($seo_settings->seo_title);
        $this->setPageDescription($seo_settings->seo_description);
        $this->setPageKeywords($seo_settings->seo_keywords);

        $this->data['seo_h1'] = $seo_settings->seo_h1;
        $this->data['description_top'] = $seo_settings->description_top;
        $this->data['description_bottom'] = $seo_settings->description_bottom;

        $this->breadcrumbs[] = ['label' => $seo_settings->breadcrumb];

        $this->admin_menu[] = [
            'label' => "Настройки раздела",
            'url' => ["/admin/section/update?alias=news"]
        ];
        $this->admin_menu[] = [
            'label' => "Новости",
            'url' => ["/admin/news"]
        ];

        $query = News::find();

        $countQuery = clone $query;
        $pagination = new Pagination(['totalCount' => $countQuery->count()]);
        $pagination->defaultPageSize = 10;

        $this->data['paginaton'] = $pagination;

        $this->data['news'] = $query
                ->orderBy("on_top DESC, title")
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
        
        
        return $this->render("index");
    }

    public function actionView($slug) {
        if (!$model = News::find()->where(['slug' => $slug])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Новости",
            'url' => ["/admin/news"]
        ];
        $this->admin_menu[] = [
            'label' => "Новость (id: " . $model->id . ")",
            'url' => ["/admin/news/update", "id" => $model->id]
        ];


        $this->setPageTitle($model->generateSeoTitle());
        $this->setPageDescription($model->generateSeoDescription());
        $this->setPageKeywords($model->generateSeoKeywords());

        $seo_settings = \common\models\Section::getSettings('news');
        $this->breadcrumbs[] = ['label' => $seo_settings->breadcrumb, 'url' => ["news/index"]];
        $this->breadcrumbs[] = ['label' => $model->generateSeoBreadcrumb()];

        $this->data['model'] = $model;

        $this->data['block_catalog'] = \common\models\BlockCatalog::find()->orderBy("sort_order ASC")->all();

        return $this->render('view');
    }

}
