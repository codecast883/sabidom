<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Category;
use common\models\Townhouse;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\models\Section;

class TownhouseController extends \frontend\components\controllers\FrontController {

    public function actionIndex() {
        $query = Townhouse::find();
        $countQuery = clone $query;
        $pagination = new Pagination([
            'totalCount' => $countQuery->count(),
            'forcePageParam' => FALSE,
        ]);
        $pagination->defaultPageSize = 8;

        $this->data['pagination'] = $pagination;

        $this->data['models'] = $query
//                ->orderBy("on_top DESC, title")
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        $page = \Yii::$app->request->get('page');

        if ($page > 1) {
            $this->data['description_bottom'] = "";
        }

        return $this->render('index');
    }

    public function actionView($id) {
        if (!$townhouse = Townhouse::findOne($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Поселок (id: " . $townhouse->id . ")",
            'url' => ["/admin/townhouse/update", "id" => $townhouse->id]];

        $this->setSeo($townhouse->getSeo());


        $this->data['townhouse'] = $townhouse;
        return $this->render('view');
    }

}
