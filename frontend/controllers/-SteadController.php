<?php

namespace frontend\controllers;

use Yii;
use common\models\Settings;
use common\models\Category;
use common\models\Stead;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use common\models\Section;

class SteadController extends \frontend\components\controllers\FrontController {

    public function actionIndex() {
        $query = Stead::find();
        $countQuery = clone $query;
        $pagination = new Pagination([
            'totalCount' => $countQuery->count(),
            'forcePageParam' => FALSE,
        ]);
        $pagination->defaultPageSize = 8;

        $this->data['pagination'] = $pagination;

        $this->data['models'] = $query
//                ->orderBy("on_top DESC, title")
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        $page = \Yii::$app->request->get('page');

        if ($page > 1) {
            $this->data['description_bottom'] = "";
        }

        return $this->render('index');
    }

    public function actionView($id) {
        if (!$stead = Stead::findOne($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->admin_menu[] = [
            'label' => "Поселок (id: " . $stead->id . ")",
            'url' => ["/admin/stead/update", "id" => $stead->id]];

        $this->setSeo($stead->getSeo());


        $this->data['stead'] = $stead;
        return $this->render('view');
    }

}
