<?php

namespace frontend\controllers;

use Yii;
use common\models\Feedback;
use yii\data\ActiveDataProvider;
use frontend\components\controllers\FrontController;
use yii\web\NotFoundHttpException;

/**
 * InformationController implements the CRUD actions for Information model.
 */
class FeedbackController extends FrontController {

    public function behaviors() {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actionCommercial() {

        $this->seo->addBreadcrumb(['label' => 'Реклама на сайте']);

        $this->data['model'] = new Feedback();
        $this->data['model']->is_new = 1;

        $this->data['message'] = "";
        $this->data['success'] = FALSE;
        $this->data['error'] = FALSE;
        $this->data['model']->scenario = "create_from_front";


        if ($this->data['model']->load(Yii::$app->request->post())) {
            if ($this->data['model']->save()) {
                $this->data['model'] = new Feedback;
                $this->data['model']->is_new = 1;

                $this->data['success'] = TRUE;
                $this->data['message'] = "Сообщение отправлено";
            } else {
                $this->data['error'] = TRUE;
                $this->data['message'] = "Проверьте форму на ошибки";
            }
        }


        $this->data['model']->scenario = "default";

        return $this->render('commercial');
    }

}
