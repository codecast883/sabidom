<?php

namespace frontend\components\url_rule;

use Yii;
use yii\web\UrlRuleInterface;
use common\models\Category;

class CategoryRule extends BaseRule {

	private $realt_types = [
		1 => "villages",
		2 => "cottages",
		3 => "sites",
		4 => "townhouse",
	];

	public function parseRequest($manager, $request) {
		$path = trim($request->getPathInfo(), "/");

		$route_params = [];
		$params = explode('/', $path);
		$subdomain = $this->getSubdomain($manager);

		if (!count($params) OR $subdomain) {
			return false;
		}

		if (count($params) == 3 AND $params[0] == "objects" AND $params[1] == "filter" AND in_array($params[2], $this->realt_types)) {
			$route_params['type_id'] = \yii\helpers\ArrayHelper::getValue(array_flip($this->realt_types), $params[2]);
			$route = 'category/filter';
			return [$route, $route_params];
		}

		$category_path = "";

		if (count($params) > 1 AND $params[0] == "objects") {
			array_shift($params);
			$category_path = implode("/", $params);
		} else {
			return FALSE;
		}

		if ($category = Category::find()->select("id")->where(['path_cache' => mb_convert_case("/" . $category_path, MB_CASE_LOWER)])->asArray()->one()) {
			$route = 'category/view';
			$route_params['id'] = $category['id'];
			return [$route, $route_params];
		}

		return false;
	}

	/**
	 * Creates a URL according to the given route and parameters.
	 * @param \yii\web\UrlManager $manager the URL manager
	 * @param string $route the route. It should not have slashes at the beginning or the end.
	 * @param array $params the parameters
	 * @return string|boolean the created URL, or false if this rule cannot be used for creating this URL.
	 */
	public function createUrl($manager, $route, $params) {
		$cache_key = md5($route . serialize($params));
		if (!$data = self::cacheGet($cache_key)) {
			$data = [
				'url_parts' => [],
				'subdomain' => NULL,
				'params' => []
			];

			if ($route == 'category/view' AND ! empty($params["id"])) {
				if ($category = Category::find()->where(["id" => $params["id"]])->asArray()->one()) {
					$data['url_parts'][] = "objects";
					$data['url_parts'][] = trim($category['path_cache'], "/");
					unset($params["id"]);
				}
			}
			if ($route == 'category/filter' AND ! empty($params["type_id"]) AND ! empty($this->realt_types[$params["type_id"]])) {
				$data['url_parts'][] = "objects";
				$data['url_parts'][] = "filter";
				$data['url_parts'][] = $this->realt_types[$params["type_id"]];
				unset($params["type_id"]);
			}


			$data['params'] = $params;
		}

		if (count($data['url_parts']) > 0) {
			$result_url = $this->generateUrl($manager, $data);
			self::cacheSet($cache_key, $data);
			return $result_url;
		}

		return false;
	}

}
