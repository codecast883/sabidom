<?php

namespace frontend\components\url_rule;

use Yii;
use yii\web\UrlRuleInterface;
use common\models\Article;

class ArticleRule extends BaseRule {

	public function parseRequest($manager, $request) {
		$path = trim($request->getPathInfo(), "/");

		$route_params = [];
		$params = explode('/', $path);
		$params = array_filter($params);
		$subdomain = $this->getSubdomain($manager);

		if (!count($params) OR $subdomain) {
			return false;
		}

		$category_path = "";
		if ($params[0] == "news") {
			if (count($params) == 1) {
				$route = 'article/news';
			} elseif (count($params) == 2) {
				$calendar_data = explode("-", $params[1]);
				$route_params['year'] = \yii\helpers\ArrayHelper::getValue($calendar_data, 0);
				$route_params['month'] = \yii\helpers\ArrayHelper::getValue($calendar_data, 1);
				;
				$route = 'article/news-date';
			} elseif (count($params) == 3) {
				$route_params['filter_date'] = $params[1];
				$route_params['id'] = $params[2];
				$route = 'article/news-item';
			} else {
				return FALSE;
			}
			return [$route, $route_params];
		}
		if ($params[0] == "feed") {
			if (count($params) == 1) {
				$route = 'article/feed';
			} elseif (count($params) == 2) {
				$calendar_data = explode("-", $params[1]);
				$route_params['year'] = \yii\helpers\ArrayHelper::getValue($calendar_data, 0);
				$route_params['month'] = \yii\helpers\ArrayHelper::getValue($calendar_data, 1);
				;
				$route = 'article/feed-date';
			} else {
				return FALSE;
			}
			return [$route, $route_params];
		} elseif ($params[0] == "articles") {
			if (count($params) == 1) {
				$route = 'article/articles';
			} elseif (count($params) == 2) {
				$calendar_data = explode("-", $params[1]);
				$route_params['year'] = \yii\helpers\ArrayHelper::getValue($calendar_data, 0);
				$route_params['month'] = \yii\helpers\ArrayHelper::getValue($calendar_data, 1);
				;
				$route = 'article/articles-date';
			} elseif (count($params) == 3) {
				$route_params['filter_date'] = $params[1];
				$route_params['id'] = $params[2];
				$route = 'article/articles-item';
			} else {
				return FALSE;
			}
			return [$route, $route_params];
		}

		return false;
	}

	public function createUrl($manager, $route, $params) {
		$cache_key = md5($route . serialize($params));
		if (!$data = self::cacheGet($cache_key)) {
			$data = [
				'url_parts' => [],
				'subdomain' => NULL,
				'params' => []
			];

			if ($route == 'article/news') {
				$data['url_parts'][] = "news";
			} elseif ($route == 'article/feed') {
				$data['url_parts'][] = "feed";
			} elseif ($route == 'article/articles') {
				$data['url_parts'][] = "articles";
			} elseif ($route == 'article/view' AND ! empty($params["id"])) {
				if ($article = Article::find()->select(['id', 'type_id', 'created_at'])->where(["id" => $params["id"]])->asArray()->one()) {
					if ($article['type_id'] == Article::TYPE_NEWS) {
						$data['url_parts'][] = "news";
					} elseif ($article['type_id'] == Article::TYPE_ARTICLE) {
						$data['url_parts'][] = "articles";
					} else {
						return FALSE;
					}
					$data['url_parts'][] = date("Y-m", strtotime($article['created_at']));
					$data['url_parts'][] = $article['id'];
					unset($params["id"]);
				}
			} elseif ($route == 'article/news-date' AND ! empty($params["year"]) AND ! empty($params["month"])) {
				$data['url_parts'][] = "news";
				$data['url_parts'][] = $params["year"] . "-" . $params["month"];
				unset($params["year"]);
				unset($params["month"]);
			} elseif ($route == 'article/articles-date' AND ! empty($params["year"]) AND ! empty($params["month"])) {
				$data['url_parts'][] = "articles";
				$data['url_parts'][] = $params["year"] . "-" . $params["month"];
				unset($params["year"]);
				unset($params["month"]);
			} elseif ($route == 'article/feed-date' AND ! empty($params["year"]) AND ! empty($params["month"])) {
				$data['url_parts'][] = "feed";
				$data['url_parts'][] = $params["year"] . "-" . $params["month"];
				unset($params["year"]);
				unset($params["month"]);
			}

			$data['params'] = $params;
		}

		if (count($data['url_parts']) > 0) {
			$result_url = $this->generateUrl($manager, $data);
			self::cacheSet($cache_key, $data);
			return $result_url;
		}

		return false;
	}

}
