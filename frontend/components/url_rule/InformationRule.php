<?php

namespace frontend\components\url_rule;

use Yii;
use yii\web\UrlRuleInterface;
use common\models\Information;
use yii\helpers\ArrayHelper;

class InformationRule extends BaseRule {

	public function parseRequest($manager, $request) {
		$path = trim($request->getPathInfo(), "/");

		$route_params = [];
		$params = explode('/', $path);
		$params = array_filter($params);
		$subdomain = $this->getSubdomain($manager);

		if (!count($params) OR $subdomain) {
			return FALSE;
		}

		if (count($params) == 1) {

			if ($information_id = ArrayHelper::getValue(Information::find()->select(['id'])->where(['slug' => $params[0]])->asArray()->one(), 'id')) {
				$route_params['id'] = $information_id;
				$route = 'information/view';
				return [$route, $route_params];
			}
		}

		return false;
	}

	/**
	 * Creates a URL according to the given route and parameters.
	 * @param \yii\web\UrlManager $manager the URL manager
	 * @param string $route the route. It should not have slashes at the beginning or the end.
	 * @param array $params the parameters
	 * @return string|boolean the created URL, or false if this rule cannot be used for creating this URL.
	 */
	public function createUrl($manager, $route, $params) {
		$cache_key = md5($route . serialize($params));
		if (!$data = self::cacheGet($cache_key)) {
			$data = [
				'url_parts' => [],
				'subdomain' => NULL,
				'params' => []
			];

			if ($route == 'information/view' AND ! empty($params["id"])) {
				if ($information = Information::find()->select(['slug'])->where(["id" => $params["id"]])->asArray()->one()) {
					$data['url_parts'][] = $information['slug'];
					unset($params["id"]);
				}
			}

			$data['params'] = $params;
		}

		if (count($data['url_parts']) > 0) {
			$result_url = $this->generateUrl($manager, $data);
			self::cacheSet($cache_key, $data);
			return $result_url;
		}

		return false;
	}

}
