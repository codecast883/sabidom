<?php

namespace frontend\components\url_rule;

use Yii;
use common\models\Information;
use yii\helpers\ArrayHelper;

class CommonRule extends BaseRule {

	private $simple_path = [
		'get-url' => 'site/get-url',
		'search' => 'search/index',
		'captcha' => 'site/captcha',
		'sitemap' => 'site/sitemap',
		'news/page' => 'news/page',
		'search/autocomplete' => 'search/autocomplete'
	];

	public function parseRequest($manager, $request) {
		$path_info = $request->getPathInfo();
		$route_params = [];
		$params = array_filter(explode('/', trim($path_info, "/")));
		$subdomain = $this->getSubdomain($manager);
		
		if (empty($subdomain) AND array_key_exists(implode("/", $params), $this->simple_path)) {

			$route = $this->simple_path[implode("/", $params)];
			return [$route, $route_params];
		} elseif (count($params[0]) AND $params[0] == 'robots.txt') {
			$route = "site/robots";
			return [$route, $route_params];
		}

		return false;
	}

	public function createUrl($manager, $route, $params) {
		$cache_key = md5($route . serialize($params));
		if (!$data = self::cacheGet($cache_key)) {
			$data = [
				'url_parts' => [],
				'subdomain' => NULL,
				'params' => []
			];
			
			if (($slug = array_search($route, $this->simple_path)) !== FALSE) {
				$data['url_parts'][] = $slug;
			}

			$data['params'] = $params;
		}

		if (count($data['url_parts']) > 0) {
			$result_url = $this->generateUrl($manager, $data);
			self::cacheSet($cache_key, $data);
			return $result_url;
		}

		return false;
	}

}
