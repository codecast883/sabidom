<?php

namespace frontend\components\url_rule;

use Yii;
use yii\web\UrlRuleInterface;
use common\models\Action;

class ActionRule extends BaseRule {

	public function parseRequest($manager, $request) {
		$path_info = trim($request->getPathInfo(), "/");
		$route_params = [];
		$params = array_filter(explode('/', trim($path_info, "/")));
		$subdomain = $this->getSubdomain($manager);

		if (!count($params) OR $subdomain) {
			return false;
		}

		if ($params[0] == "action") {
			if (count($params) == 1) {
				$route = 'action/index';
			} elseif (count($params) == 3) {
				$route_params['year'] = $params[1];
				$route_params['id'] = $params[2];
				$route = 'action/view';
			} else {
				return FALSE;
			}

			return [$route, $route_params];
		}

		return false;
	}

	/**
	 * Creates a URL according to the given route and parameters.
	 * @param \yii\web\UrlManager $manager the URL manager
	 * @param string $route the route. It should not have slashes at the beginning or the end.
	 * @param array $params the parameters
	 * @return string|boolean the created URL, or false if this rule cannot be used for creating this URL.
	 */
	public function createUrl($manager, $route, $params) {
		$cache_key = md5($route . serialize($params));
		if (!$data = self::cacheGet($cache_key)) {
			$data = [
				'url_parts' => [],
				'subdomain' => NULL,
				'params' => []
			];

			if ($route == 'action/index') {
				$data['url_parts'][] = "action";
			} elseif ($route == 'action/view' AND ! empty($params["id"])) {
				$data['url_parts'][] = "action";
				if ($action = Action::find()->select(['id', 'created_at'])->where(["id" => $params["id"]])->asArray()->one()) {
					$data['url_parts'][] = date("Y", strtotime($action['created_at']));
					$data['url_parts'][] = $action['id'];
					unset($params["id"]);
				}
			}

			$data['params'] = $params;
		}

		if (count($data['url_parts']) > 0) {
			$result_url = $this->generateUrl($manager, $data);
			self::cacheSet($cache_key, $data);
			return $result_url;
		}

		return false;
	}

}
