<?php

namespace frontend\components\url_rule;

use Yii;
use yii\web\UrlRuleInterface;
use yii\helpers\ArrayHelper;
use common\models\Realt;
use common\models\Village;
use common\models\VillageTownhouse;

class RealtRule extends BaseRule {

	private $realt_types = [
		2 => "cottages",
		3 => "sites",
		4 => "townhouse",
	];

	public function parseRequest($manager, $request) {
		$path_info = trim($request->getPathInfo(), "/");
		$route_params = [];
		$params = array_filter(explode('/', trim($path_info, "/")));
		$subdomain = $this->getSubdomain($manager);

		if (!count($params) OR $subdomain) {
			return FALSE;
		}

		if (count($params) > 1 AND $params[0] != "objects") {
			return FALSE;
		}

		if (!(count($params) == 3 AND in_array($params[1], $this->realt_types))) {
			return false;
		}

		if (preg_match("|(.*)-(\d+)$|isu", $params[2], $matches)) {
			$route = 'realt/view';
			$route_params['id'] = $matches[2];
			return [$route, $route_params];
		}

		return false;
	}

	public function createUrl($manager, $route, $params) {
		$cache_key = md5($route . serialize($params));
		if (!$data = self::cacheGet($cache_key)) {
			$data = [
				'url_parts' => [],
				'subdomain' => NULL,
				'params' => []
			];

			if ($route == 'realt/view' AND ! empty($params["id"])) {
				if ($model = Realt::find()->select(['type_id'])->where(["id" => $params["id"]])->asArray()->one()) {
					$data['url_parts'][] = 'objects';
					$data['url_parts'][] = $this->realt_types[$model['type_id']];
					$data['url_parts'][] = "realt-" . $params["id"];
					unset($params["id"]);
				}
			}
		}

		if (count($data['url_parts']) > 0) {
			$result_url = $this->generateUrl($manager, $data);
			self::cacheSet($cache_key, $data);
			return $result_url;
		}

		return false;
	}

}
