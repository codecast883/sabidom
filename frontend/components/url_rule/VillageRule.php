<?php

namespace frontend\components\url_rule;

use Yii;
use yii\web\UrlRuleInterface;
use yii\helpers\ArrayHelper;
use common\models\Realt;
use common\models\Village;
use common\models\VillageTownhouse;

class VillageRule extends BaseRule {

	private $realt_types = [
		1 => "villages",
	];

	public function parseRequest($manager, $request) {
		$path_info = trim($request->getPathInfo(), "/");
		$route_params = [];
		$params = array_filter(explode('/', trim($path_info, "/")));
		$subdomain = $this->getSubdomain($manager);

		if ($subdomain) {
			if ($village_id = $this->getVillageId($subdomain)) {
				$route_params['id'] = $village_id;

				if (count($params) == 0) {
					$route = 'village/view';
					return [$route, $route_params];
				}
				if (count($params) == 1 AND $params[0] == 'reviews') {
					$route = 'village/reviews';
					return [$route, $route_params];
				}
				if (count($params) == 1 AND $params[0] == 'contacts') {
					$route = 'village/contacts';
					return [$route, $route_params];
				}
				if (count($params) == 1 AND $params[0] == 'documentation') {
					$route = 'village/documentation';
					return [$route, $route_params];
				}
				if (count($params) == 1 AND $params[0] == 'general-layout') {
					$route = 'village/general-layout';
					return [$route, $route_params];
				}
				if (count($params) == 1 AND $params[0] == 'progress') {
					$route = 'village/progress';
					return [$route, $route_params];
				}
				if (count($params) == 1 AND $params[0] == 'photo') {
					$route = 'village/photo';
					return [$route, $route_params];
				}
				if (count($params) == 1 AND $params[0] == 'price') {
					$route = 'village/townhouse';
					return [$route, $route_params];
				}
				if (count($params) == 2 AND $params[0] == 'price') {
					if ($village_townhouse_id = ArrayHelper::getValue(VillageTownhouse::find()->select(['id'])->where(['slug' => $params[1]])->asArray()->one(), 'id')) {
						$route_params['id'] = $village_townhouse_id;
						$route = 'village/townhouse-view';
						return [$route, $route_params];
					}
				}
			} else {
				$route = 'village/view';
				$route_params['id'] = -1; // специально вызываем ошибку для отсутствующих поддоменов
				return [$route, $route_params];
			}
		}


		return false;
	}

	public function createUrl($manager, $route, $params) {
		$cache_key = md5($route . serialize($params));
		if (!$data = self::cacheGet($cache_key)) {
			$data = [
				'url_parts' => [],
				'subdomain' => NULL,
				'params' => []
			];

			if ($route == 'village/view' AND ! empty($params["id"])) {
				$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = '';
				unset($params["id"]);
			} elseif ($route == 'village/reviews' AND ! empty($params["id"])) {
				$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'reviews';
				unset($params["id"]);
			} elseif ($route == 'village/townhouse' AND ! empty($params["id"])) {
				$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'price';
				unset($params["id"]);
			} elseif ($route == 'village/contacts' AND ! empty($params["id"])) {
				$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'contacts';
				unset($params["id"]);
			} elseif ($route == 'village/documentation' AND ! empty($params["id"])) {
				$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'documentation';
				unset($params["id"]);
			} elseif ($route == 'village/general-layout' AND ! empty($params["id"])) {
				$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'general-layout';
				unset($params["id"]);
			} elseif ($route == 'village/progress' AND ! empty($params["id"])) {
				$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'progress';
				unset($params["id"]);
			} elseif ($route == 'village/photo' AND ! empty($params["id"])) {
				$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'photo';
				unset($params["id"]);
			} elseif ($route == 'village/townhouse-view' AND ! empty($params["id"])) {
				if ($model_village_townhouse = VillageTownhouse::find()->select(["village_id", "slug"])->where(["id" => $params["id"]])->asArray()->one()) {
					$data['subdomain'] = $this->getVillageSlug($model_village_townhouse["village_id"]);
					$data['url_parts'][] = 'price';
					$data['url_parts'][] = $model_village_townhouse["slug"];
					unset($params["id"]);
				}
			}

			$data['params'] = $params;
		}

		if (count($data['url_parts']) > 0) {
			$result_url = $this->generateUrl($manager, $data);
			self::cacheSet($cache_key, $data);
			return $result_url;
		}

		return false;
	}

}
