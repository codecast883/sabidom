<?php

namespace frontend\components\url_rule;

use Yii;
use yii\web\UrlRuleInterface;
use yii\helpers\ArrayHelper;
use common\models\Realt;
use common\models\Village;
use common\models\VillageTownhouse;

class VillageRule extends BaseRule {

	private $realt_types = [
		1 => "villages",
	];

	public function parseRequest($manager, $request) {
		$path_info = trim($request->getPathInfo(), "/");
		$route_params = [];
		$params = explode('/', trim($path_info, "/"));

		if (count($params) > 1 AND $params[0] == "objects") {
			array_shift($params);
			$category_path = implode("/", $params);
		} else {
			return FALSE;
		}


		if (count($params) > 1) {
			if ($params[0] == "village" AND $village_id = ArrayHelper::getValue(Village::find()->select(['id'])->where(['slug' => $params[1]])->asArray()->one(), 'id')) {
				if (count($params) == 2) {
					$route_params['id'] = $village_id;
					$route = 'village/view';
					return [$route, $route_params];
				}
				if (count($params) == 3 AND $params[2] == 'reviews') {
					$route_params['id'] = $village_id;
					$route = 'village/reviews';
					return [$route, $route_params];
				}
				if (count($params) == 3 AND $params[2] == 'contacts') {
					$route_params['id'] = $village_id;
					$route = 'village/contacts';
					return [$route, $route_params];
				}
				if (count($params) == 3 AND $params[2] == 'documentation') {
					$route_params['id'] = $village_id;
					$route = 'village/documentation';
					return [$route, $route_params];
				}
				if (count($params) == 3 AND $params[2] == 'general-layout') {
					$route_params['id'] = $village_id;
					$route = 'village/general-layout';
					return [$route, $route_params];
				}
				if (count($params) == 3 AND $params[2] == 'progress') {
					$route_params['id'] = $village_id;
					$route = 'village/progress';
					return [$route, $route_params];
				}
				if (count($params) == 3 AND $params[2] == 'photo') {
					$route_params['id'] = $village_id;
					$route = 'village/photo';
					return [$route, $route_params];
				}
				if (count($params) == 3 AND $params[2] == 'price') {
					$route_params['id'] = $village_id;
					$route = 'village/townhouse';
					return [$route, $route_params];
				}
				if (count($params) == 4 AND $params[2] == 'price') {
					if ($village_townhouse_id = ArrayHelper::getValue(VillageTownhouse::find()->select(['id'])->where(['slug' => $params[3]])->asArray()->one(), 'id')) {
						$route_params['id'] = $village_townhouse_id;
						$route = 'village/townhouse-view';
						return [$route, $route_params];
					}
				}
			}
		}

		if (!count($params)) {
			return false;
		}
		return false;
	}

	public function createUrl($manager, $route, $params) {
		$cache_key = md5($route . serialize($params));
		if (!$data = self::cacheGet($cache_key)) {
			$data = [
				'url_parts' => [],
				'subdomain' => NULL,
				'params' => []
			];

			if ($route == 'village/view' AND ! empty($params["id"])) {
				#$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'objects';
				$data['url_parts'][] = 'village';
				$data['url_parts'][] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = '';
				unset($params["id"]);
			} elseif ($route == 'village/reviews' AND ! empty($params["id"])) {
				#$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'objects';
				$data['url_parts'][] = 'village';
				$data['url_parts'][] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'reviews';
				unset($params["id"]);
			} elseif ($route == 'village/townhouse' AND ! empty($params["id"])) {
				#$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'objects';
				$data['url_parts'][] = 'village';
				$data['url_parts'][] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'price';
				unset($params["id"]);
			} elseif ($route == 'village/contacts' AND ! empty($params["id"])) {
				#$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'objects';
				$data['url_parts'][] = 'village';
				$data['url_parts'][] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'contacts';
				unset($params["id"]);
			} elseif ($route == 'village/documentation' AND ! empty($params["id"])) {
				#$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'objects';
				$data['url_parts'][] = 'village';
				$data['url_parts'][] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'documentation';
				unset($params["id"]);
			} elseif ($route == 'village/general-layout' AND ! empty($params["id"])) {
				#$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'objects';
				$data['url_parts'][] = 'village';
				$data['url_parts'][] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'general-layout';
				unset($params["id"]);
			} elseif ($route == 'village/progress' AND ! empty($params["id"])) {
				#$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'objects';
				$data['url_parts'][] = 'village';
				$data['url_parts'][] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'progress';
				unset($params["id"]);
			} elseif ($route == 'village/photo' AND ! empty($params["id"])) {
				#$data['subdomain'] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'objects';
				$data['url_parts'][] = 'village';
				$data['url_parts'][] = $this->getVillageSlug($params["id"]);
				$data['url_parts'][] = 'photo';
				unset($params["id"]);
			} elseif ($route == 'village/townhouse-view' AND ! empty($params["id"])) {
				if ($model_village_townhouse = VillageTownhouse::find()->select(["village_id", "slug"])->where(["id" => $params["id"]])->asArray()->one()) {
					#$data['subdomain'] = $this->getVillageSlug($model_village_townhouse["village_id"]);
					$data['url_parts'][] = 'objects';
					$data['url_parts'][] = $this->getVillageSlug($params["id"]);
					$data['url_parts'][] = 'price';
					$data['url_parts'][] = $model_village_townhouse["slug"];
					unset($params["id"]);
				}
			}

			$data['params'] = $params;
		}

		if (count($data['url_parts']) > 0) {
			$result_url = $this->generateUrl($manager, $data);
			self::cacheSet($cache_key, $data);
			return $result_url;
		}

		return false;
	}

}
