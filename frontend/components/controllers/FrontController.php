<?php

/**
 * @property SeoPlus $seo
 */

namespace frontend\components\controllers;

use common\models\Section;
use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\components\SeoPlus;
use common\models\Information;
use common\models\Country;
use common\models\Category;
use common\models\Seo;
use common\models\Realt;

class FrontController extends \yii\web\Controller
{

    public $enableCsrfValidation = false;
    public $layout;
    public $main_menu = [];
    public $footer_menu = [];
    public $admin_menu = [];
    public $seo = NULL;
    protected $data;

    public function __construct($id, $module, $config = array())
    {
        if ($_GET['page'] == 1) {

            $host = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $url = strstr($host, "?", true);

            header('HTTP/1.1 301 Moved Permanently');
            header('Location: http://' . $url);
            exit();
        }
        parent::__construct($id, $module, $config);

    }

    public function setSeo()
    {
        $this->data['page_title'] = $this->seo->title;

        $page = \Yii::$app->request->get('page');
        if ($page > 1) {
            $this->data['page_title'] .= " - страница №" . $page;
        }

//        if (!empty($this->seo->canonical_url)) {
//            $this->getView()->registerLinkTag(['rel' => 'canonical', 'href' => $this->seo->canonical_url]);
//        }


        $fieldTable = [];

        $types = [
            1 => 'Поселок',
            2 => 'Коттедж',
            3 => 'Земля',
            4 => 'Таунхаус',
        ];

        $type_id = $this->data['realt']['type_id'];

        if ($type_id) {
            if ($fields = $this->data['realt']->getFields(NULL, 1)) {
                foreach ($fields as $field) {

                    switch ($field['info']->title) {
                        case 'Район':
                            $fieldTable['район'] = $field['values'][0]->value;
                            break;
                        case 'Шоссе':
                            $fieldTable['шоссе'] = $field['values'][0]->value;
                            break;
                        case 'Городской округ':
                            $fieldTable['городской округ'] = $field['values'][0]->value;
                            break;
                        default:
                            $fieldTable['default'] = 'в Подмосковье';
                    }

                }

                $titleH1 = $types[$type_id];
                $house_area = '';

                if($this->data['realt']['house_area'] != 0){
                    $house_area = ' ' . $this->data['realt']['house_area']. 'м2';
                }

                foreach ($fieldTable as $key => $value) {
                    if ($key == 'район') {
                        $titleH1 .= ' в районе ' . $value . ', ' . $this->data['realt']['title'] . $house_area;
                        break;
                    } elseif ($key == 'шоссе') {
                        $titleH1 .= ' на ' . $value . ' шоссе' . ', ' . $this->data['realt']['title'] . $house_area;
                        break;
                    } elseif ($key == 'городской округ') {
                        $titleH1 .= ', городской округ ' . $value . ', ' . $this->data['realt']['title'] . $house_area;
                        break;
                    } else {
                        $titleH1 .= ' в Подмосковье, ' . $this->data['realt']['title'] . $house_area;
                        break;
                    }
                }

            }

        }


        $title = '';
        if ($this->data['category']['seo_h1']) {
            $title = $this->data['category']['seo_h1'];
        } elseif ($this->data['realt']->title) {
            $title = $titleH1;
        }else{
            $title = $this->data['page_title'];
        }

        $descr = 'Агентство загородной недвижимости СабиДом – ' . $title . ' ☎: +7 (495) 765-63-69, +7 (926) 861-58-80. Продажа недвижимости в Подмосковье.';


        $this->getView()->registerMetaTag(['name' => 'description', 'content' => $descr], 'description');
        $this->getView()->registerMetaTag(['name' => 'keywords', 'content' => $this->seo->keywords], 'keywords');

        $this->data['h1'] = $this->seo->h1;
        $this->data['h2'] = $this->seo->h2;
        $this->data['seo_schemas'] = $this->seo->renderSchemas();

//        $this->data['description_top'] = $this->seo->description_top;
        $this->data['description_top'] = $this->seo->description_top;
        $this->data['description_bottom'] = $this->seo->description_bottom;
        $this->data['breadcrumbs'] = $this->seo->getBreadcrumbs();
    }

    public function init()
    {
        parent::init();
        Yii::$app->setHomeUrl(\Yii::$app->params['site_url']);
        $this->data = [];

        $this->admin_menu[] = [
            'label' => "Admin panel",
            'url' => \Yii::$app->getHomeUrl() . "admin"
        ];


        $this->seo = new SeoPlus();
        $this->seo->setBreadcrumbsHomelink(Yii::$app->getHomeUrl(), "Недвижимость в Московской области");

        if ($route = $this->seo->getRoute()) {
            $this->admin_menu[] = [
                'label' => "SeoPlus",
                'url' => ["/admin/seo/auto-find", "route" => $route]
            ];
        }

        $this->data['top_menu'] = false;
        $this->data['top_menu'] = \Yii::$app->cache->get("top_menu");

        if ($this->data['top_menu'] === false) {
            foreach (Category::find()->where(['on_header' => 1, 'status' => 1, 'parent_id' => 0])->orderBy(['sort_order' => SORT_ASC])->all() as $category) {
                $items = [];
                foreach (Category::find()->where(['parent_id' => $category->id, 'status' => 1, 'on_header' => 1])->orderBy(['sort_order' => SORT_ASC])->all() as $sub_category) {
                    $items[] = [
                        'label' => $sub_category->title,
                        'url' => $sub_category->url
                    ];
                }

                $this->data['top_menu'][] = [
                    'label' => $category->title,
                    'url' => $category->url,
                    'items' => $items,
                ];
            }


            $items = [];
            foreach (Category::find()->where(['on_header' => 2, 'status' => 1, 'parent_id' => 0])->orderBy(['sort_order' => SORT_ASC])->all() as $category) {
                $items[] = [
                    'label' => $category->title,
                    'url' => $category->url
                ];
            }

            $this->data['top_menu'][] = [
                'label' => "Еще",
                'container-class' => 'with-child',
                'items' => $items
            ];

            $dependency = new \yii\caching\DbDependency();
            $dependency->sql = 'SELECT MAX(updated_at) FROM category';
            \Yii::$app->cache->set("top_menu", $this->data['top_menu'], 0, $dependency);
        }

        $this->data['top_menu_highway'] = false;
        $this->data['top_menu_highway'] = \Yii::$app->cache->get("top_menu_highway");

        if ($this->data['top_menu_highway'] === false) {
            foreach (Category::find()->where(['parent_id' => 12, 'group_title' => 'По шоссе'])->orderBy(['title' => SORT_ASC])->all() as $category) {
                $this->data['top_menu_highway'][] = [
                    'label' => $category->title,
                    'url' => $category->url,
                ];
            }

            $dependency = new \yii\caching\DbDependency();
            $dependency->sql = 'SELECT MAX(updated_at) FROM category';
            \Yii::$app->cache->set("top_menu_highway", $this->data['top_menu_highway'], 0, $dependency);
        }

        $this->data['top_menu_area'] = false;
        $this->data['top_menu_area'] = \Yii::$app->cache->get("top_menu_area");
        if ($this->data['top_menu_area'] === false) {
            foreach (Category::find()->where(['parent_id' => 12, 'group_title' => 'По району'])->orderBy(['title' => SORT_ASC])->all() as $category) {
                $this->data['top_menu_area'][] = [
                    'label' => $category->title,
                    'url' => $category->url,
                ];
            }

            $dependency = new \yii\caching\DbDependency();
            $dependency->sql = 'SELECT MAX(updated_at) FROM category';
            \Yii::$app->cache->set("top_menu_area", $this->data['top_menu_area'], 0, $dependency);

        }

    }

    /**
     * переопределяем, чтобы переменные страницы были доступны в макете
     */
    public function render($view, $params = [])
    {
        $this->setSeo();

        if (\Yii::$app->user->can("admin-panel-access")) {
            $this->data['admin_menu'] = $this->admin_menu;
        }

        $params = array_merge($this->data, $params);

        $output = $this->getView()->render($view, $params, $this);
        $layoutFile = $this->findLayoutFile($this->getView());
        if ($layoutFile !== false) {
            $params['content'] = $output;
            return $this->getView()->renderFile($layoutFile, $params, $this);
        } else {
            return $output;
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\components\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'], // все
                    ],
                ]
//				'rules' => [
//					[
//						'allow' => true,
                //	'roles' => ['?'], // все
                // 'matchCallback' => function ($rule, $action) {return in_array(\Yii::$app->user->identity->username, ['chuzzlik', 'admin']);}
//					],
//				]
            ]
        ];
    }
}
