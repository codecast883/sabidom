<?php

namespace frontend\models;

use Yii;
use common\helpers\YText;

class CompanyWorkerReview extends \common\models\CompanyWorkerReview {

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->on(\yii\db\ActiveRecord::EVENT_BEFORE_VALIDATE, [$this, 'beforeValidateHandler']);
        $this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, [$this, 'sendOwnerNotification']);
    }

    public function scenarios() {
        return [
            'toggle_status' => ['status'],
            'create_from_front' => ['ip', 'user_agent', 'email', 'company_id', 'name', 'date_post', 'text', 'status', 'rating', 'user_id'],
            'default' => ['ip', 'user_agent', 'email', 'company_id', 'name', 'date_post', 'text', 'status', 'rating', 'user_id'],
        ];
    }

    
    public function beforeValidateHandler() {
        if($this->isNewRecord){
            if (!\Yii::$app->user->isGuest) {
                $this->user_id = \Yii::$app->user->id;
            } else {
                $this->user_id = NULL;
            }

            $this->date_post = date("Y-m-d H:i:s");

            $this->status = self::STATUS_ENABLED;
            $this->is_new = self::IS_NEW_YES;

            $this->name = trim(strip_tags($this->name));
            $this->email = trim(strip_tags($this->email));
            $this->text = trim(strip_tags($this->text));

            if (!in_array($this->rating, [1, 5])) {
                $this->rating = 5;
            }
        }
    }

    public function sendOwnerNotification() {
        $replaces = [
            "отзыв_дата" => $this->date_post,
            "компания_название" => $this->company->title,
            "компания_ссылка" => \yii\helpers\Html::a(Yii::$app->urlManager->createAbsoluteUrl([$this->company->url]), Yii::$app->urlManager->createAbsoluteUrl([$this->company->url]))
        ];

        if ($email_template = \common\models\EmailTemplate::find()->where(['alias' => 'company_review_for_owner'])->one()) {
            if ($this->company->email_notify AND $this->company->email AND filter_var($this->company->email, FILTER_VALIDATE_EMAIL)) {
                $mail = Yii::$app->mail->compose('company_review_for_owner', ['message' => YText::textGenerator($email_template->body, $replaces)]);
                $mail->setSubject(YText::textGenerator($email_template->theme, $replaces));
                $mail->setFrom($email_template->send_from);
                $mail->setTo($this->company->email);
                $mail->send();
            }
            if ($this->company->email_for_notifications_notify AND trim($this->company->email_for_notifications)) {
                $mail = Yii::$app->mail->compose('company_review_for_owner', ['message' => YText::textGenerator($email_template->body, $replaces)]);
                $mail->setSubject(YText::textGenerator($email_template->theme, $replaces));
                $mail->setFrom($email_template->send_from);
                $mail->setTo(explode(",", $this->company->email_for_notifications));
                $mail->send();
            }
        }
    }

}
