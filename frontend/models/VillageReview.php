<?php

namespace frontend\models;

use Yii;
use common\helpers\YText;

class VillageReview extends \common\models\VillageReview {
#    public $reCaptcha;

	public function __construct($config = array()) {
		parent::__construct($config);
	}

	public function rules() {
		return array_merge(parent::rules(), [
#			['reCaptcha', 'required', 'on' => ['create_from_front', 'create_answer_from_front'], 'message' => 'Подтвердите, что Вы не робот'],
#			[[], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LdH2CcTAAAAADLDWWvsQVco9TBqWQEO9DcXJTRj']
		]);
	}

	public function attributeLabels() {
		return array_merge(parent::attributeLabels(), [
#			'reCaptcha' => 'Проверочный код',
		]);
	}

	public function scenarios() {
		return [
			'toggle_status' => ['status'],
			'rate' => ['profit_yes', 'profit_no'],
			'create_from_front' => ['name', 'created_at', 'updated_at', 'rating_1', 'rating_2', 'rating_3', 'rating_4', 'rating_5', 'rating_avg', 'text', 'status', 'is_new', 'user_id', 'village_id', 'ip', 'user_agent'],
		];
	}

}
