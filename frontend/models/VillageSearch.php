<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Village;
use common\models\CategoryFilter;
use yii\helpers\ArrayHelper;

/**
 * ViewRequestSearch represents the model behind the search form about `common\models\ViewRequest`.
 */
class VillageSearch extends Village {

	public $field_3;
	public $field_7;
	public $field_8;
	public $field_9;
	public $field_16;
	public $field_19;
	public $filter;

	public function rules() {
		return [
			[['price_from', 'price_to'], 'integer'],
			[['title', 'filter'], 'safe'],
			[['field_3', 'field_7', 'field_8', 'field_9', 'field_16', 'field_19'], 'safe'],
			[['house_area_from', 'house_area_to', 'stead_area_from', 'stead_area_to',], 'safe'],
		];
	}

	public function scenarios() {
		return Model::scenarios();
	}

	public function search($params) {
		$query = self::find();

		$query->join("LEFT JOIN", 'village_field_value', 'village_id = village.id');
		$query->groupBy('village.id');

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		//	'sort' => ['defaultOrder' => ['premium' => SORT_DESC, 'id' => SORT_ASC]],
		]);

		foreach ($this->filter as $type_id => $rules) {
			if ($type_id == CategoryFilter::TYPE_CUSTOM) {
				foreach ($rules as $field => $value) {
					if ($field == "price") {
						if ($value['from']) {
							$query->andWhere(['or', 'price_to >= :price_from', 'price_to = 0']);
							$query->addParams([':price_from' => $value['from']]);
						}

						if ($value['to']) {
							$query->andWhere(['or', 'price_from < :price_to', 'price_from = 0']);
							$query->addParams([':price_to' => $value['to']]);
						}
					} elseif ($field == "stead_area") {
						if ($value['from']) {
							$query->andWhere(['or', 'stead_area_to >= :stead_area_from', 'stead_area_to = 0']);
							$query->addParams([':stead_area_from' => $value['from']]);
						}

						if ($value['to']) {
							$query->andWhere(['or', 'stead_area_from < :stead_area_to', 'stead_area_from = 0']);
							$query->addParams([':stead_area_to' => $value['to']]);
						}
					} elseif ($field == "house_area") {
						if ($value['from']) {
							$query->andWhere(['or', 'house_area_to >= :house_area_from', 'house_area_to = 0']);
							$query->addParams([':stead_area_from' => $value['from']]);
						}

						if ($value['to']) {
							$query->andWhere(['or', 'house_area_from < :house_area_to', 'house_area_from = 0']);

							$query->addParams([':house_area_to' => $value['to']]);
						}
					}
				}
			} elseif ($type_id == CategoryFilter::TYPE_FIELD_TEXT) {
				foreach ($rules as $field => $value) {
					$query->andWhere(['and', 'field_id = ' . $field, 'value LIKE :field_' . $field]);
					$query->addParams([':field_' . $field => "%" . $value . "%"]);
				}
			} elseif ($type_id == CategoryFilter::TYPE_FIELD_TAG) {
				foreach ($rules as $field => $values) {
					if ($values) {
						if (!is_array($values)) {
							$values = [$values];
						}
						$query->andWhere(['and', 'field_id = ' . $field, 'field_tag_id IN (' . implode(",", $values) . ')']);
					}
				}
			} elseif ($type_id == CategoryFilter::TYPE_FIELD_FROM_TO) {
				foreach ($rules as $field => $value) {
					if ($value['from']) {
						$query->andWhere(['and',
							'field_id = ' . $field,
							'value_to > :field_' . $field . '_from',
						]);
						$query->addParams([':field_' . $field . '_from' => (float) $value['from']]);
					}

					if ($value['to']) {
						$query->andWhere(['and',
							'field_id = ' . $field,
							['or', 'value_from < :field_' . $field . '_to', 'value_from = 0']
						]);
						$query->addParams([':field_' . $field . '_to' => (float) $value['to']]);
					}
				}
			}
		}

		return $dataProvider;
	}

	public function setFilters($filter) {
		$this->filter = $filter;
	}

	public function appendFilters($filter) {
		// тут нужно сделать нормальное слияние
		if ($filter) {
			$this->filter = $filter;
		}
	}

}
