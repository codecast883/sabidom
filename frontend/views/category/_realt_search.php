<?php

use common\helpers\Html;
use kartik\widgets\ActiveForm;
?>

<div class="filter clearfix">
    <?php
    $form = ActiveForm::begin([
                'method' => 'get',
                'id' => 'village_search_form',
                'type' => ActiveForm::TYPE_INLINE,
                'fieldConfig' => ['autoPlaceholder' => false]
    ]);
    ?>
    <div class="filter-row">
        <div class="filter-group">
            <div class="filter-group-title">
                Тип недвижимости:
            </div>
            <div class="filter-group-body">
                <div class="btn-group"><a class="btn btn-default" href="#">Дома</a><a class="btn btn-default" href="#">Участки</a><a class="btn btn-default" href="#">Таунхаусы</a></div>
            </div>
        </div>
        <div class="filter-group">
            <div class="filter-group-title">
                Цена, руб:
            </div>
            <div class="filter-group-body">
                <?= $form->field($model, 'price_from')->textInput(['class' => 'form-control size2', 'placeholder' => 'От']); ?>
                -
                <?= $form->field($model, 'price_to')->textInput(['class' => 'form-control size2', 'placeholder' => 'До']); ?>
            </div>
        </div>
        <div class="filter-group">
            <div class="filter-group-title">
                Дом, м<sup>2</sup>: 
            </div>
            <div class="filter-group-body">
                <?= $form->field($model, 'house_area_from')->textInput(['class' => 'form-control size1', 'placeholder' => 'От']); ?> - 
                <?= $form->field($model, 'house_area_to')->textInput(['class' => 'form-control size1', 'placeholder' => 'До']); ?>
            </div>
        </div>
        <div class="filter-group">
            <div class="filter-group-title">
                Участок, соток:
            </div>
            <div class="filter-group-body">
                <?= $form->field($model, 'stead_area_from')->textInput(['class' => 'form-control size1', 'placeholder' => 'От']); ?> - 
                <?= $form->field($model, 'stead_area_to')->textInput(['class' => 'form-control size1', 'placeholder' => 'До']); ?>
            </div>
        </div>
    </div>

    <div class="filter-row">
        <div class="filter-group pull-right">
            <div class="filter-group-title">&nbsp;</div>
            <div class="filter-group-body">              
                <input type="submit" class="hidden">
                <?=
                Html::a('Подобрать ' . mb_convert_case($category->topLevel->title, MB_CASE_LOWER, "utf8"), '', [
                    'onclick' => "village_search_form.submit(); return false;",
                    'class' => 'btn btn-sm btn-green'
                ])
                ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>