<?php

use common\helpers\Html;
use \yii\widgets\LinkPager;
use common\owerride\yii\widgets\Breadcrumbs;
use common\models\Category;

?>

    <div class="block block-white padding shadow">
        <?= $this->render($search_view, ['model' => $search_model, 'fields' => $filter_fields, 'filter' => $filter]); ?>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-10">
            <!--            --><? //= \common\owerride\yii\widgets\Breadcrumbs::widget($breadcrumbs); ?>
<!--                        --><?php //print_r($breadcrumbs) ?>
            <?php $category_level_list = Category::find()->where(['group_title' => $category['group_title'], 'parent_id' => $category['parent_id']])->orderBy(['sort_order' => SORT_ASC])->all();?>

<!--Begin custom breadcrumbs-->
            <ul class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    <a href="/" itemprop="name"><?= $breadcrumbs['homeLink']['label'] ?></a>
                </li>


                <?php if (count($breadcrumbs['links']) > 1): ?>
                    <?php if (count($breadcrumbs['links']) == 2 and !empty($_GET['page'])): ?>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                            class="active">
                            <span class="drpdwn_cat" itemprop="name"><?= $breadcrumbs['links'][0]['label'] ?></span>

                            <ul class="subcategory_cat">

                                <?php foreach ($category_level_list as $key => $value): ?>
                                    <li><a href="<?= 'http://' . $_SERVER['HTTP_HOST'] . '/objects'. $value['path_cache'] ?>"><?= $value['title'] ?></a></li>
                                <?php endforeach ?>

                            </ul>
                        </li>
                    <?php else: ?>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                            class="drpdwn">
                            <a href="<?= $breadcrumbs['links'][0]['url'] ?>" itemprop="item"><span
                                        itemprop="name"><?= $breadcrumbs['links'][0]['label'] ?></a>

                            <ul class="subcategory">

                                <?php foreach ($top_menu as $key => $value): ?>
                                    <?php if ($value['label'] == 'Еще') continue; ?>
                                    <li><a href="<?= $value['url'] ?>"><?= $value['label'] ?></a></li>
                                <?php endforeach ?>

                                <?php foreach ($top_menu[7]['items'] as $values): ?>
                                    <li><a href="<?= $values['url'] ?>"><?= $values['label'] ?></a></li>
                                <?php endforeach ?>

                            </ul>
                        </li>


                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                            class="active">
                        <!--                        <span itemprop="name">--><? //= $breadcrumbs['links'][1] ?><!--</span>-->
                        <!--                    </li>-->


                        <?php if ($_GET['page']): ?>
                            <span class="drpdwn_cat" itemprop="name"><?= $breadcrumbs['links'][1]['label'] ?></span>
                        <?php else: ?>
                            <span class="drpdwn_cat" itemprop="name"><?= $breadcrumbs['links'][1] ?></span>

                        <?php endif ?>
                            <ul class="subcategory_cat">

                                <?php foreach ($category_level_list as $key => $value): ?>
                                    <li><a href="<?= 'http://' . $_SERVER['HTTP_HOST'] . '/objects'. $value['path_cache'] ?>"><?= $value['title'] ?></a></li>
                                <?php endforeach ?>

                            </ul>
                        </li>
                    <?php endif ?>


                <?php else: ?>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="active">
                        <span class="drpdwn_cat" itemprop="name"><?= $breadcrumbs['links'][0] ?></span>

                        <ul class="subcategory_cat">

                            <?php foreach ($category_level_list as $key => $value): ?>
                                <li><a href="<?= 'http://' . $_SERVER['HTTP_HOST'] . '/objects'. $value['path_cache'] ?>"><?= $value['title'] ?></a></li>
                            <?php endforeach ?>

                        </ul>
                    </li>
                <?php endif ?>

            </ul>
<!--End custom breadcrumbs-->

        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="category-controls  pull-right">
                <!--div class="category-view-type-selector-block">
                    Параметры отображения: <a class="btn btn-default active category-view-type-selector category-view-type-selector-list"></a> <a class="btn btn-default category-view-type-selector category-view-type-selector-grid"></a>&nbsp;&nbsp;
                </div-->
                <?php if ($category) { ?>
                    <div class="category-order-selector">
                        <select class="form-control"
                                onchange="location.assign($(this).find('option:selected').data('location'))">
                            <option disabled <?= \Yii::$app->request->get('sort') == "" ? "selected" : "" ?>>
                                Сортировать
                            </option>
                            <option <?= \Yii::$app->request->get('sort') == "price_from" ? "selected" : "" ?>
                                    data-location="<?= $category->url . "?sort=price_from"; ?>">Сначала дешевле
                            </option>
                            <option <?= \Yii::$app->request->get('sort') == "-price_from" ? "selected" : "" ?>
                                    data-location="<?= $category->url . "?sort=-price_from"; ?>">Сначала дороже
                            </option>
                        </select>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="category-listing-container fix-height-container">
        <div class="category-title-row">
            <h1 class="like-h2"><?= $h1 ?></h1>
        </div>

        <?php if ($current_page == 0) { ?>
            <div class="category-info fix-height" data-fix-container="#category-description-bottom-fix"
                 id="category-description-bottom">
                <?= $description_bottom; ?>
                <div>
                    Найдено <?= $pagination->totalCount ?> <?= \common\helpers\YText::formatByCount($pagination->totalCount, "предложение", "предложения", "предложений") ?>
                    по Вашему запросу «<?= $category->kw2 ? $category->kw2 : $category->kw1 ?>».
                </div>
                <br>
                <br>
            </div>
        <?php } ?>

        <div class="offer-large 222">
            <?php $counter = 0; ?>
            <?php foreach ($models as $model) { ?>
                <?php $counter++; ?>
                <?php
                $map_position = "";
                if ($model->coord_lat AND $model->coord_lng) {
                    $map_position = $counter;
                }
                ?>
                <div class="offer-large-item block block-white padding shadow">
                    <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <div class="offer-large-item-image">
                                <?php if ($model->premium) { ?>
                                    <div class="badge badge-green">ПРЕМИУМ</div><?php } ?>
                                <div class="images-count"><i class="fa fa-picture-o"></i> <?= count($model->images) ?>
                                </div>
                                <div class="carousel-2">
                                    <div class="slick items-1">


                                        <?php
                                        $wordList = ['Дачи','Купить дачу','Купить недорогую дачу', 'Купить участок','Купить дуплекс', 'Купить дом','Купить коттедж','Коттеджные поселки', 'Продажа загородной недвижимости','Дачные участки','Дачи и участки','Таунхаусы с участком','поселки','Поселки','участки','Участки','Коттеджи','коттеджи','Таунхаусы','таунхаусы'];
                                        $wordListReplace = ['Дача','Дача','Недорогая дача', 'Участок', 'Дуплекс','Дом',  'Коттедж',  'Коттеджный поселок', 'Загородная недвижимость', 'Дачный участок','Дача и участок','Таунхаус с участком','поселок','Поселок','участок','Участок','Коттедж','коттедж','Таунхаус','таунхаус'];

                                        $title = '';
                                        $title = $category->seo_h1;
                                        $outTitle = str_replace($wordList,$wordListReplace,$title);


                                        ?>



                                        <?php foreach ($model->getImages()->limit(3)->all() as $image) { ?>
                                            <div class="item">
                                                <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'middle'), ["alt" =>$outTitle. ', ' . $model->title]); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-7 offer-large-col-right">
                            <div class="offer-large-item-title">
                                <?= Html::a(((!empty($category) AND $category->seo_h1) ? ($outTitle . ", ") : "") . $model->title, $model->url); ?>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 offer-large-sub-left">
                                    <?php if ($fields = $model->getFields([9])) { ?>
                                        <?php foreach ($fields as $field) { ?>
                                            <div class="options-row-simple">
                                                <span class="options-row-simple-value"><?= $field['info']->title ?>
                                                    : <?= $field['values_formated'] ?></span>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ($fields = $model->getFields([26])) { ?>
                                        <?php foreach ($fields as $field) { ?>
                                            <div class="options-row-simple">
											<span class="options-row-simple-value"><?= $field['values_formated'] ?> <i
                                                        class="fa fa-map-marker gray"></i>
                                                <?php if ($model->coord_lat AND $model->coord_lng) { ?>
                                                    <span class="dotted offer-show-on-map"
                                                          data-map-position="<?= $map_position ?>">показать на карте</span>
                                                <?php } ?>
											</span>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ($model->house_area) { ?>
                                        <div class="options-row-simple">
                                            <span class="options-row-simple-label">Площадь объекта недвижимости: </span>
                                            <span class="options-row-simple-value"><?= $model->house_area ?>
                                                м<sup>2</sup></span>
                                        </div>
                                    <?php } ?>

                                    <?php if ($model->stead_area) { ?>
                                        <div class="options-row-simple">
                                            <span class="options-row-simple-label">Площадь участка: </span>
                                            <span class="options-row-simple-value"><?= $model->stead_area ?> сот.</span>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6 col-xs-12 offer-large-sub-right">
                                    <div class="offer-large-item-price">
                                        <span class="offer-large-item-price-label">цена</span>
                                        <span class="offer-large-item-price-value"><?= $model->priceFormated() ?></span>
                                    </div>
                                    <?php if ($model->phone) { ?>
                                    <div class="realt-info-contacts-phone show-hidden-data mark"
                                         data-mark-name="show-phone" data-hidden="<?= $model->phone ?>">
                                        <span><?= $model->phone ?></span></div><?php } ?>
                                    <?php //!--a href ='#' class="realt-add-to-bookmark">Добавить в избранное <i class="fa fa-heart"></i></a--> ?>
                                    <div data-toggle="modal" data-realt-id="<?= $model->id ?>"
                                         data-target="#realt-view-request-modal" class="realt-info-signup-for-view">
                                        <a href ='<?=$model->url . '?order=show'?>'><span>Записаться на просмотр</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($counter == 1) { ?>
                    <div class="visible-lg-block">
                        <?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "category_realt_center_2"]); ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="text-center">
            <?php
            echo LinkPager::widget([
                'pagination' => $pagination,
            ]);
            ?>
        </div>

        <div class="modal fade" id="realt-view-request-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <?= $this->render("/realt/_view_request_form", $view_request_form) ?>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#realt-view-request-modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var realt_id = button.data('realt-id') // Extract info from data-* attributes
                var modal = $(this)
                modal.find('#realtviewrequest-realt_id').val(realt_id);
            })
        </script>

        <div class="block">
            <?php if ($h2) { ?>
                <h2><?= $h2 ?></h2>
            <?php } ?>
            <div id="map" style="height:360px; width: 100%;"></div>
            <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    ymaps.ready(init);
                });
                var myMap;

                function init() {
                    var myMap = new ymaps.Map('map', {
                        center: [55.7494733, 37.6723199],
                        zoom: 8,
                        controls: ["zoomControl", "fullscreenControl", "geolocationControl", "routeEditor", "typeSelector"]
                    });

                    window.clusterer = new ymaps.Clusterer({
                        preset: 'islands#redClusterIcons',
                        groupByCoordinates: false,
                        clusterDisableClickZoom: true,
                        clusterHideIconOnBalloonOpen: false,
                        geoObjectHideIconOnBalloonOpen: false,
                        clusterBalloonContentLayoutWidth: 500,
                        clusterBalloonLeftColumnWidth: 205
                    });
                    geoObjects = [];

                    <?php
                    $counter = 0;
                    foreach ($models as $model) {
                    if ($model->coord_lat AND $model->coord_lng) {
                    ?>

                    <?php
                    $body = "<div>";
                    $body .= "<b>" . $model->title . "</b>";
                    if ($model->image) {
                        $body .= "<div>" . Html::img($model->image->getUploadedFileUrl('image', 'small')) . "</div>";
                    }
                    $body .= "</div>";
                    $body .= Html::a("Подробней...", $model->url);
                    $body = addslashes($body);
                    ?>
                    geoObjects[<?= $counter ?>] = new ymaps.Placemark(
                        [<?= $model->coord_lat ?>, <?= $model->coord_lng ?>],
                        {
                            iconContent: '<?= $counter ?>',
                            balloonContentHeader: '<?= Html::encode($model->title) ?>',
                            balloonContentBody: "<?= preg_replace("|\n|isu", "", nl2br($body)) ?>"
                        },
                        {
                            preset: 'islands#redDotIcon',
                        }
                    );
                    <?php
                    $counter++;
                    }
                    }
                    ?>
                    window.clusterer.options.set({
                        gridSize: 1,
                        clusterDisableClickZoom: true,
                    });

                    window.clusterer.add(geoObjects);
                    myMap.geoObjects.add(window.clusterer);


                    myMap.setBounds(window.clusterer.getBounds(), {
                        checkZoomRange: true
                    });
                }


                $(".offer-show-on-map").on("click", function () {
                    $('html, body').animate({scrollTop: $("#map").position().top + 500}, 'slow');
                    item_num = $(this).data("map-position");

                    geoObjects = window.clusterer.getGeoObjects();

                    var objectState = window.clusterer.getObjectState(geoObjects[item_num]);
                    if (objectState.isClustered) {
                        // Если метка находится в кластере, выставим ее в качестве активного объекта.
                        // Тогда она будет "выбрана" в открытом балуне кластера.
                        objectState.cluster.state.set('activeObject', geoObjects[item_num]);
                        window.clusterer.balloon.open(objectState.cluster);
                    } else if (objectState.isShown) {
                        // Если метка не попала в кластер и видна на карте, откроем ее балун.
                        geoObjects[item_num].balloon.open();
                    }

                });

                $('.drpdwn_cat').on("mouseover",function () {
                    $('.subcategory_cat').css("display","block");

                    $('.subcategory_cat').on("mouseover",function () {
                        $('.subcategory_cat').css("display","block");
                    });
                    $('.subcategory_cat').on("mouseout",function () {
                        $('.subcategory_cat').css("display","none");
                    });
                });
                $('.drpdwn_cat').on("mouseout",function () {
                    $('.subcategory_cat').css("display","none");

                });


            </script>
        </div>

        <div id="category-description-bottom-fix"></div>
    </div>
    <div class="block visible-lg-block">
        <?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "footer"]); ?>
    </div>
<?php if ($category) { ?>
    <?= frontend\widgets\block_subcategories\BlockSubcategoriesWidget::widget(['category_id' => $category->id]); ?>
    <?php if ($category->id != $category->topLevel->id) { ?>
        <?= frontend\widgets\block_subcategories\BlockSubcategoriesWidget::widget(['category_id' => $category->topLevel->id]); ?>
    <?php } ?>
<?php } ?>