<?php

use common\helpers\Html;
use \yii\widgets\LinkPager;
use common\owerride\yii\widgets\Breadcrumbs;;
?>

<div class="block block-white padding shadow">
    <div class="filter">
        <form class="form-inline" id="form1">
            <div class="filter-row">
                <div class="filter-group">
                    <div class="filter-group-title">
                        Тип недвижимости:
                    </div>
                    <div class="filter-group-body">
                        <div class="btn-group"><a class="btn btn-default" href="#">Дома</a><a class="btn btn-default" href="#">Участки</a><a class="btn btn-default" href="#">Таунхаусы</a></div>
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        Цена, руб:
                    </div>
                    <div class="filter-group-body">
                        <input type="text" class="form-control size2" placeholder="От"> - <input type="text" class="form-control size2" placeholder="До">
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        От города:
                    </div>
                    <div class="filter-group-body">
                        <input type="text" class="form-control size1" placeholder="От"> - <input type="text" class="form-control size1" placeholder="До">
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        Жилая, м<sup>2</sup>: 
                    </div>
                    <div class="filter-group-body">
                        <input type="text" class="form-control size1" placeholder="От"> - <input type="text" class="form-control size1" placeholder="До">
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        Участок, м<sup>2</sup>: 
                    </div>
                    <div class="filter-group-body">
                        <input type="text" class="form-control size1" placeholder="От"> - <input type="text" class="form-control size1" placeholder="До">
                    </div>
                </div>
            </div>
            <div class="filter-row">
                <div class="filter-group">
                    <div class="filter-group-title">
                        Тип участка:
                    </div>
                    <div class="filter-group-body">
                        <label class="checkbox-inline"><input type="checkbox"> ИЖС</label>
                        <label class="checkbox-inline"><input type="checkbox"> Дачное строительство</label>
                    </div>
                </div>
            </div>
            <div class="filter-row">
                <div class="filter-group">
                    <div class="filter-group-title">
                        Газ:
                    </div>
                    <div class="filter-group-body">
                        <label class="checkbox-inline"><input type="checkbox"> Магистральный</label>
                        <label class="checkbox-inline"><input type="checkbox"> Баллон</label>
                        <label class="checkbox-inline"><input type="checkbox"> Газгольдер</label>
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        Спален:
                    </div>
                    <div class="filter-group-body">
                        <label class="checkbox-inline"><input type="checkbox"> 1</label>
                        <label class="checkbox-inline"><input type="checkbox"> 2</label>
                        <label class="checkbox-inline"><input type="checkbox"> 3</label>
                        <label class="checkbox-inline"><input type="checkbox"> 4+</label>
                    </div>
                </div>
                <div class="filter-group pull-right">
                    <div class="filter-group-title">&nbsp;</div>
                    <div class="filter-group-body">
                        <?=
                        Html::a('Подобрать ' . mb_convert_case($category->topLevel->title, MB_CASE_LOWER, "utf8"), '', [
                            'onclick' => "form1.submit(); return false;",
                            'class' => 'btn btn-sm btn-green'
                        ])
                        ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<h1 class="like-h2"><?= $h1 ?></h1>
<div class="offer-large grid">
    <?php foreach (Html::explodeToRows($models, 3) as $row) { ?>
        <div class="row">
            <?php foreach ($row as $model) { ?>
                <div class="col-xs-12 col-sm-4">
                    <div class="offer-large-item">
                        <div class="offer-large-item-image block block-white padding shadow">
                            <?php if ($model->premium) { ?><div class="badge badge-green">ПРЕМИУМ</div><?php } ?>
                            <div class="images-count"><i class="fa fa-picture-o"></i> <?= count($model->images) ?></div>
                            <div class="carousel-3">
                                <div class="slick items-1">
                                    <?php foreach ($model->images as $image) { ?>
                                        <div class="item">
                                            <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'middle'), ["alt" => $model->title]); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <a href ='#' class="realt-add-to-bookmark">Добавить в избранное <i class="fa fa-heart"></i></a>
                        <div class="offer-large-item-title">
                            <?= Html::a($model->title, $model->url); ?>
                        </div>

                        <div class="options-simple2">
                            <div class="options-simple2-row">
                                <span class="options-simple2-row-label">Площадь: </span>
                                <span class="options-simple2-row-value">133 м<sup>2</sup></span>
                            </div>
                            <div class="options-simple2-row">
                                <span class="options-simple2-row-label">Комнат: </span>
                                <span class="options-simple2-row-value">5</span>
                            </div>
                            <div class="options-simple2-row">
                                <span class="options-simple2-row-label">Этаж: </span>
                                <span class="options-simple2-row-value">-</span>
                            </div>
                            <div class="options-row-simple2">
                                <span class="options-simple2-row-label">Цена: </span>
                                <span class="options-simple2-row-value">9 000 000 руб.</span>
                            </div>
                        </div>
                        <a href="#" class="btn btn-lg btn-green">Записаться на просмотр</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
<div class="text-center">
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
    ]);
    ?>
</div>

<div class="block">
    <h2>Коттеджные поселки в Подмосковье</h2>
    <div id="map" style="height:360px; width: 100%;"></div>
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(init);
        function init() {
            var myMap = new ymaps.Map("map", {
                center: [45.436554, 12.338716],
                zoom: 18,
                controls: ["zoomControl", "fullscreenControl", "geolocationControl", "routeEditor", "typeSelector"]
            }
            );
            myGeoObject = new ymaps.GeoObject();
            myMap.geoObjects.add(new ymaps.Placemark([45.436554, 12.338716]));
        }
    </script>
</div>

<?php if (!empty($description_bottom)) { ?>
    <div class="block">
        <?= $description_bottom; ?>
    </div>
<?php } ?>
<div class="block">
    <?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "footer"]); ?>
</div>
<?php if ($childrens_menu) { ?>
    <div class="block">
        <?php foreach (Html::explodeToRows($childrens_menu, 3) as $row) { ?>
            <div class="row">
                <?php foreach ($row as $col) { ?>
                    <div class="col-xs-4">
                        <div class="list1-header"><?= $col['group']; ?></div>
                        <div class="list1">
                            <ul class="list1-items">
                                <?php foreach ($col['models'] as $model) { ?>
                                    <li><?= Html::a($model['label'], $model['url']) ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>