<?php

use common\helpers\Html;
use \yii\widgets\LinkPager;
use common\owerride\yii\widgets\Breadcrumbs;
use kartik\widgets\ActiveForm;
use common\models\Category;
?>

<div class="block block-white padding shadow">
	<?= $this->render("_villages_search", ['model' => $search_model, 'fields' => $filter_fields, 'filter' => $filter]); ?>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-10">
<!--		--><?//= \common\owerride\yii\widgets\Breadcrumbs::widget($breadcrumbs); ?>
<!--        --><?php //print_r($breadcrumbs)?>

        <?php $category_level_list = Category::find()->where(['group_title' => $category['group_title'], 'parent_id' => $category['parent_id']])->orderBy(['sort_order' => SORT_ASC])->all();?>

        <!--Begin custom breadcrumbs-->
        <ul class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a href="/" itemprop="name"><?= $breadcrumbs['homeLink']['label'] ?></a>
            </li>


            <?php if (count($breadcrumbs['links']) > 1): ?>
                <?php if (count($breadcrumbs['links']) == 2 and !empty($_GET['page'])): ?>
                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                        class="active">
                        <span class="drpdwn_cat"  itemprop="name"><?= $breadcrumbs['links'][0]['label'] ?></span>

                        <ul class="subcategory_cat">

                            <?php foreach ($category_level_list as $key => $value): ?>
                                <li><a href="<?= 'http://' . $_SERVER['HTTP_HOST'] . '/objects'. $value['path_cache'] ?>"><?= $value['title'] ?></a></li>
                            <?php endforeach ?>

                        </ul>
                    </li>
                <?php else: ?>

                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                        class="drpdwn">
                        <a href="<?= $breadcrumbs['links'][0]['url'] ?>" itemprop="item"><span
                                    itemprop="name"><?= $breadcrumbs['links'][0]['label'] ?></a>

                        <ul class="subcategory">

                            <?php foreach ($top_menu as $key => $value): ?>
                                <?php if ($value['label'] == 'Еще') continue; ?>
                                <li><a href="<?= $value['url'] ?>"><?= $value['label'] ?></a></li>
                            <?php endforeach ?>

                            <?php foreach ($top_menu[7]['items'] as $values): ?>
                                <li><a href="<?= $values['url'] ?>"><?= $values['label'] ?></a></li>
                            <?php endforeach ?>

                        </ul>
                    </li>


                    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                        class="active">
                    <!--                        <span itemprop="name">--><? //= $breadcrumbs['links'][1] ?><!--</span>-->
                    <!--                    </li>-->


                    <?php if ($_GET['page']): ?>
                        <span class="drpdwn_cat" itemprop="name"><?= $breadcrumbs['links'][1]['label'] ?></span>
                    <?php else: ?>
                        <span class="drpdwn_cat" itemprop="name"><?= $breadcrumbs['links'][1] ?></span>


                    <?php endif ?>
                    <ul class="subcategory_cat">

                        <?php foreach ($category_level_list as $key => $value): ?>
                            <li><a href="<?= 'http://' . $_SERVER['HTTP_HOST'] . '/objects'. $value['path_cache'] ?>"><?= $value['title'] ?></a></li>
                        <?php endforeach ?>

                    </ul>
                    </li>
                <?php endif ?>


            <?php else: ?>
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="active">
                    <span class="drpdwn_cat" itemprop="name"><?= $breadcrumbs['links'][0] ?></span>

                    <ul class="subcategory_cat">

                        <?php foreach ($category_level_list as $key => $value): ?>
                            <li><a href="<?= 'http://' . $_SERVER['HTTP_HOST'] . '/objects'. $value['path_cache'] ?>"><?= $value['title'] ?></a></li>
                        <?php endforeach ?>

                    </ul>
                </li>
            <?php endif ?>

        </ul>
        <!--End custom breadcrumbs-->




    </div>
    <div class="col-xs-12 col-sm-2">
        <div class="category-controls  pull-right">
            <!--div class="category-view-type-selector-block">
                Параметры отображения: <a class="btn btn-default active category-view-type-selector category-view-type-selector-list"></a> <a class="btn btn-default category-view-type-selector category-view-type-selector-grid"></a>&nbsp;&nbsp;
            </div-->
			<?php if ($category) { ?>
				<div class="category-order-selector">
					<select class="form-control" onchange="location.assign($(this).find('option:selected').data('location'))">
						<option disabled <?= \Yii::$app->request->get('sort') == "" ? "selected" : "" ?>>Сортировать</option>
						<option <?= \Yii::$app->request->get('sort') == "price_from" ? "selected" : "" ?> data-location="<?= $category->url . "?sort=price_from"; ?>">Сначала дешевле</option>
						<option <?= \Yii::$app->request->get('sort') == "-price_from" ? "selected" : "" ?> data-location="<?= $category->url . "?sort=-price_from"; ?>">Сначала дороже</option>
					</select>
				</div>
			<?php } ?>
        </div>
    </div>
</div>
<div class="category-listing-container">
    <div class="category-title-row">
        <h1 class="like-h2"><?= $h1 ?></h1>
    </div>
	<?php if ($category AND $current_page == 0) { ?>
		<div class="category-info fix-height" data-fix-container="#category-description-bottom-fix" id="category-description-bottom">
			<?= $description_bottom; ?>
			<div>Найдено <?= $pagination->totalCount ?> <?= \common\helpers\YText::formatByCount($pagination->totalCount, "предложение", "предложения", "предложений") ?> по Вашему запросу «<?= $category->kw2 ? $category->kw2 : $category->kw1 ?>».</div>
			<br><br>
		</div>
	<?php } ?>
    <div class="offer-large">
		<?php $counter = 0; ?>
		<?php foreach ($models as $model) { ?>
			<?php $counter++; ?>
			<div class="offer-large-item block block-white padding shadow">
				<div class="row">
						<div class="col-xs-12 col-md-5">
							<div class="offer-large-item-image">
								<?php if ($model->premium) { ?><div class="badge badge-green">ПРЕМИУМ</div><?php } ?>
								<div class="images-count"><i class="fa fa-picture-o"></i> <?= count($model->images) ?></div>
								<div class="carousel-2">
									<div class="slick items-1">
										<?php foreach ($model->getImages()->limit(3)->all() as $image) { ?>
											<div class="item">



                                                <?php
                                                $wordList = ['Дачные поселки','Купить коттедж','Продажа загородной недвижимости','Дачи','Коттеджные поселки','Дачные участки','Дачи и участки','Таунхаусы с участком','поселки','Поселки','участки','Участки','Коттеджи','коттеджи','Таунхаусы','таунхаусы'];
                                                $wordListReplace = ['Дачный поселок','Коттедж','Загородная недвижимость','Дача','Коттеджный поселок','Дачный участок','Дача и участок','Таунхаус с участком','поселок','Поселок','участок','Участок','Коттедж','коттедж','Таунхаус','таунхаус'];

                                                $title = '';
                                                $title = $category->seo_h1;
                                                $outTitle = str_replace($wordList,$wordListReplace,$title);


                                                ?>


                                                <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'middle'), ["alt" =>$outTitle . ', ' . $model->title]); ?>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-7 offer-large-col-right">
							<div class="offer-large-item-title">
                                <?= Html::a($model->formatString($category->product_name_mask ?: "{title}", ['категория h1' => $outTitle]), $model->url); ?>
							</div>
							<div class="row">
								<div class="col-sm-6 col-xs-12 offer-large-sub-left">
									
									<div class="offer-large-item-type">
										<i class="fa fa-home"></i> <?= $model->subtitle ?>
									</div>
									<div class="offer-large-item-description">
										<?php if ($model->house_area_from OR $model->house_area_to) { ?>
											<span class="offer-large-item-description-label">Площадь объекта недвижимости:</span> <?= $model->house_area_from ? "от " . $model->house_area_from : "" ?> <?= $model->house_area_to ? "до " . $model->house_area_to : "" ?>  м<sup>2</sup>, 
										<?php } ?>
										<?php if ($model->stead_area_from OR $model->stead_area_to) { ?>
											<span class="offer-large-item-description-label">Площадь участка:</span> <?= $model->stead_area_from ? "от " . $model->stead_area_from : "" ?> <?= $model->stead_area_to ? "до " . $model->stead_area_to : "" ?>  соток</sup>,
										<?php } ?>

										<?php foreach ($model->getFields() as $field) { ?>
											<?php if ($field['info']->show_in_preview) { ?>
												<?php $values = []; ?>
												<span class="offer-large-item-description-label"><?= $field['info']->title ?>: </span>
												<?php
												foreach ($field['values'] as $value) {
													$values[] = $value->getFormated();
												}
												?>
												<?= implode(", ", $values) ?>.
											<?php } ?>
										<?php } ?>
										<hr>
									</div>
									<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $model]); ?>
								</div>
								<div class="col-sm-6 col-xs-12 offer-large-sub-right">
									<?php if ($price = $model->priceFormated()) { ?>
										<div class="offer-large-item-price">
											<span class="offer-large-item-price-label">цена: </span>
											<span class="offer-large-item-price-value"><?= $price ?></span>
										</div>
									<?php } ?>
									<?$callOne = array("2163", "1435", "36", "1039", "907", "749", "35", "1054", "1295", "30", "8", "938");?>
									<?$callTwo = array("9", "2160", "984", "442", "2161", "1071", "710", "2162");?>
									<?if (in_array($model->id, $callOne)) { $addCallPhone = "call_phone_1"; }?>
									<?if (in_array($model->id, $callTwo)) { $addCallPhone = "call_phone_2"; }?>
									<?php if ($model->phone) { ?><div class="realt-info-contacts-phone show-hidden-data mark <?if ($addCallPhone) { echo $addCallPhone; }?>" data-mark-name="show-phone" data-hidden="<?= $model->phone ?>"><span><?= $model->phone ?></span></div><?php } ?>
<!--a href ='#' class="realt-add-to-bookmark">Добавить в избранное <i class="fa fa-heart"></i></a-->
                                    <div data-toggle="modal" data-realt-id="<?= $model->id ?>" data-target="#realt-view-request-modal" class="realt-info-signup-for-view"><a href ='<?=$model->url . '?order=show'?>'><span>Записаться на просмотр</span></a></div>

                                    <?php if ($model->id == 1254) { ?>
										<div class="offer-large-item-bestoffer">
											<img src="/img/best_offer.jpg">
										</div>
									<?php } ?>

								</div>
							</div>
						</div>
				</div>
			</div>
			<?php if ($counter == 1) { ?>
				<div class="visible-lg-block">
					<?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "category_village_center_2"]); ?>
				</div>
			<?php } ?>
		<?php } ?>

	</div>
	<div class="text-center">
		<?php
		echo LinkPager::widget([
			'pagination' => $pagination,
		]);
		?>
	</div>

	<div class="modal fade" id="realt-view-request-modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<?= $this->render("/village/_view_request_form", $view_request_form) ?>
				</div>
			</div>
		</div>
	</div>

	<script>
		$('#realt-view-request-modal').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget) // Button that triggered the modal
			var realt_id = button.data('realt-id') // Extract info from data-* attributes
			var modal = $(this)
			modal.find('#villageviewrequest-village_id').val(realt_id);
		})


        $('.drpdwn_cat').on("mouseover",function () {
            $('.subcategory_cat').css("display","block");

            $('.subcategory_cat').on("mouseover",function () {
                $('.subcategory_cat').css("display","block");
            });
            $('.subcategory_cat').on("mouseout",function () {
                $('.subcategory_cat').css("display","none");
            });
        });
        $('.drpdwn_cat').on("mouseout",function () {
            $('.subcategory_cat').css("display","none");

        });
	</script>

	<div class="block">
		<div id="map" style="height:360px; width: 100%;"></div>
		<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function () {
			ymaps.ready(init);
		});
		function init() {
			var myMap = new ymaps.Map("map", {
				center: [55.7494733, 37.6723199],
				zoom: 8,
				controls: ["zoomControl", "fullscreenControl", "geolocationControl", "routeEditor", "typeSelector"]
			}
			);
			myGeoObject = new ymaps.GeoObject();
<?php foreach ($models as $model) { ?>
				myMap.geoObjects.add(new ymaps.Placemark([<?= $model->coord_lat ?>, <?= $model->coord_lng ?>],
						{
							balloonContent: '<i class="fa fa-home"></i> <?= $model->subtitle ?><br><?= Html::a($model->title, $model->url); ?>',
						}));
<?php } ?>
		}
		</script>
	</div>

	<?php if ($category AND $current_page == 0) { ?>    
		<div id="category-description-bottom-fix"></div>
	<?php } ?>
</div>
<div class="block visible-lg-block">
	<?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "footer"]); ?>
</div>
<?php if ($category) { ?>
	<?= frontend\widgets\block_subcategories\BlockSubcategoriesWidget::widget(['category_id' => $category->id]); ?>
	<?php if ($category->id != $category->topLevel->id) { ?>
		<?= frontend\widgets\block_subcategories\BlockSubcategoriesWidget::widget(['category_id' => $category->topLevel->id]); ?>
	<?php } ?>
<?php } ?>
