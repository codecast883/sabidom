<?php

use common\helpers\Html;
use kartik\widgets\ActiveForm;
use common\models\Category;
use yii\helpers\ArrayHelper;
use common\models\CategoryFilter;
?>

<div class="filter clearfix">
    <?php
    $form = ActiveForm::begin([
                'method' => 'get',
                'id' => 'village_search_form',
                'type' => ActiveForm::TYPE_INLINE,
                'fieldConfig' => ['autoPlaceholder' => false]
    ]);
    ?>
    <div class="filter-row">
        <div class="filter-group">
            <div class="filter-group-title">
                Тип недвижимости:
            </div>
            <div class="filter-group-body">
                <div class="btn-group">
                    <?= Html::a("Дома", ['category/filter', 'type_id' => Category::TYPE_COTTAGE], ['class' => "btn btn-default"]); ?>
                    <?= Html::a("Участки", ['category/filter', 'type_id' => Category::TYPE_STEAD], ['class' => "btn btn-default"]); ?>
                    <span class='btn btn-default active'>Таунхаусы</span>

                </div>
            </div>
        </div>
        <div class="filter-group">
            <div class="filter-group-title">
                Цена, руб:
            </div>
            <div class="filter-group-body">
                <?= Html::textInput('filter[' . CategoryFilter::TYPE_CUSTOM . '][price][from]', ArrayHelper::getValue($filter[CategoryFilter::TYPE_CUSTOM]['price'], 'from'), ['class' => 'form-control size2', 'placeholder' => 'От']) ?>
                -
                <?= Html::textInput('filter[' . CategoryFilter::TYPE_CUSTOM . '][price][to]', ArrayHelper::getValue($filter[CategoryFilter::TYPE_CUSTOM]['price'], 'to'), ['class' => 'form-control size2', 'placeholder' => 'До']) ?>
            </div>
        </div>
        <div class="filter-group">
            <div class="filter-group-title">
                Дом, м<sup>2</sup>: 
            </div>
            <div class="filter-group-body">
                <?= Html::textInput('filter[' . CategoryFilter::TYPE_CUSTOM . '][house_area][from]', ArrayHelper::getValue($filter[CategoryFilter::TYPE_CUSTOM]['house_area'], 'from'), ['class' => 'form-control size2', 'placeholder' => 'От']) ?>
                - 
                <?= Html::textInput('filter[' . CategoryFilter::TYPE_CUSTOM . '][house_area][to]', ArrayHelper::getValue($filter[CategoryFilter::TYPE_CUSTOM]['house_area'], 'to'), ['class' => 'form-control size2', 'placeholder' => 'До']) ?>
            </div>
        </div>
        <div class="filter-group">
            <div class="filter-group-title">
                Участок, соток:
            </div>
            <div class="filter-group-body">
                <?= Html::textInput('filter[' . CategoryFilter::TYPE_CUSTOM . '][stead_area][from]', ArrayHelper::getValue($filter[CategoryFilter::TYPE_CUSTOM]['stead_area'], 'from'), ['class' => 'form-control size2', 'placeholder' => 'От']) ?>
                - 
                <?= Html::textInput('filter[' . CategoryFilter::TYPE_CUSTOM . '][stead_area][to]', ArrayHelper::getValue($filter[CategoryFilter::TYPE_CUSTOM]['stead_area'], 'to'), ['class' => 'form-control size2', 'placeholder' => 'До']) ?>
            </div>
        </div>
    </div>
    <div class="filter-row">
        <div class="filter-group">
            <div class="filter-group-title">
                Шоссе:
            </div>
            <div class="filter-group-body">
                <?= Html::dropDownList('filter[' . CategoryFilter::TYPE_FIELD_TAG . '][9]', ArrayHelper::getValue($filter[CategoryFilter::TYPE_FIELD_TAG], 9), $fields[9]['values'], ['prompt' => '-выбрать-', 'class' => 'form-control']) ?>
            </div>
        </div>
        <div class="filter-group">
            <div class="filter-group-title">
                Район:
            </div>
            <div class="filter-group-body">
                <?= Html::dropDownList('filter[' . CategoryFilter::TYPE_FIELD_TAG . '][7]', ArrayHelper::getValue($filter[CategoryFilter::TYPE_FIELD_TAG], 7), $fields[7]['values'], ['prompt' => '-выбрать-', 'class' => 'form-control']) ?>
            </div>
        </div>
    </div>

    <div class="filter-row">
        <div class="filter-group pull-right">
            <div class="filter-group-title">&nbsp;</div>
            <div class="filter-group-body">              
                <input type="submit" class="hidden">
                <?=
                Html::a('Подобрать ' . mb_convert_case($category->topLevel->title, MB_CASE_LOWER, "utf8"), '', [
                    'onclick' => "village_search_form.submit(); return false;",
                    'class' => 'btn btn-sm btn-green'
                ])
                ?>
            </div>
        </div>
        <div class="filter-group pull-right">
            <div class="filter-group-title">&nbsp;</div>
            <div class="filter-group-body">              
                <?= Html::resetButton("Сбросить", ['class' => 'btn btn-default']) ?>&nbsp;
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>