<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\FileInputAsset;
use yii\captcha\Captcha;
?>

<?php
if (!$model->rating) {
    $model->rating = 5;
}
?>

<?php
\yii\widgets\Pjax::begin([
    'enablePushState' => false,
    'formSelector' => "#add-review-form"
]);
?>

<div class="block-1">
    <div class="block-1-title"><i class="fa fa-commenting"></i> <?= !empty($title) ? $title : "Добавить отзыв про компанию " . $company->title ?></div>
    <div class="block-1-body">
        <?php if ($success) { ?>
            <div class="alert alert-success" role="alert"><?= $message ?></div>
            <script>$.pjax.reload('#reviews-list');</script>
        <?php } ?>
        <?php if ($error) { ?>
            <div class="alert alert-warning" role="alert"><?= $message ?></div>
        <?php } ?>

        <?php
        $form = ActiveForm::begin([
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'class' => "review-form"
                    ],
                    'id' => "add-review-form",
                    'validateOnChange' => false,
                    'validateOnBlur' => true,
                    'enableClientValidation' => false,
                    'type' => ActiveForm::TYPE_VERTICAL,
                    'formConfig' => [
                        'deviceSize' => ActiveForm::SIZE_TINY
                    ]]
        );
        ?>
        <?= Html::activeHiddenInput($model, 'company_id', ['value' => $model->company_id]); ?>
        <?= Html::activeHiddenInput($model, 'parent_id', ['value' => $model->parent_id]); ?>
        <?= $form->field($model, 'text')->textarea(['rows' => 6, 'placeholder' => "Текст отзыва"])->label("") ?>
        <?php if (!$model->parent_id) { ?>
            <div class="form-group review-form-rating row">
                <div class='col-xs-4'>
                    <div class="review-form-rating-text">
                        Что думаете о компании?
                    </div>
                </div>
                <div class='col-xs-8'>
                    <div class="review-form-rating-select">
                        <label class="radio review-form-rating-select-pos"><?= Html::radio("CompanyReview[rating]", $model->rating == 5, ['value' => 5]) ?> Рекомендую <i class="fa fa-thumbs-up"></i></label> |
                        <label class="radio review-form-rating-select-con"><?= Html::radio("CompanyReview[rating]", $model->rating == 1, ['value' => 1]) ?> Не рекомендую <i class="fa fa-thumbs-down"></i></label>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="form-group last-row">
            <div class='col-xs-6'>
                <div class="row">
                    <?=
                    $form->field($model, 'captcha')->widget(Captcha::className(), [ 'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'Введите код',
                        ], 'template' => "{image} {input}"])->label("")
                    ?>
                </div>
            </div>
            <div class='col-xs-3'>
                <div class="row">
<?= $form->field($model, 'name')->textInput(['maxlength' => 128, 'placeholder' => 'Имя'])->label("") ?>
                </div>
            </div>
            <div class='col-xs-3'>
                <div class="row">
<?= $form->field($model, 'email')->textInput(['maxlength' => 128, 'placeholder' => 'Email'])->label("") ?>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>



        <div class="form-group">
            <button type="submit" class='btn btn-orange review-form-submit'><i class="fa fa-paper-plane"></i> Отправить</button>
        </div>
<?php ActiveForm::end(); ?>
    </div>
</div>
<?php \yii\widgets\Pjax::end(); ?>