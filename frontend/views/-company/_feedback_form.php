<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\FileInputAsset;
use yii\captcha\Captcha;
?>


<?php \yii\widgets\Pjax::begin(['enablePushState' => false, 'formSelector' => "#feedback-form"]); ?>

<?php if ($success) { ?>
    <br>
    <div class="alert alert-success" role="alert"><?= $message ?></div>
<?php } ?>
<?php if ($error) { ?>
    <br>
    <div class="alert alert-warning" role="alert"><?= $message ?></div>
<?php } ?>
<?php
$form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
                'class' => "review-form"
            ],
            'id' => "feedback-form",
            'validateOnChange' => false,
            'validateOnBlur' => true,
            'enableClientValidation' => false,
            'type' => ActiveForm::TYPE_VERTICAL,
            'formConfig' => [
                'deviceSize' => ActiveForm::SIZE_TINY
            ]]
);
?>
<?= Html::activeHiddenInput($model, 'company_id'); ?>
<?= Html::activeHiddenInput($model, 'type_id'); ?>
<?= $form->field($model, 'message')->textarea(['rows' => 6, 'placeholder' => "Сообщение"])->label("") ?>
<?= $form->field($model, 'fio')->textInput(['maxlength' => 128, 'placeholder' => 'Имя'])->label("") ?>
<div class="form-group two-cols">
    <div class='col-xs-6'>
        <div class="row">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => 128, 'placeholder' => 'Телефон'])->label("") ?>
        </div>
    </div>
    <div class='col-xs-6'>
        <div class="row">
            <?= $form->field($model, 'email')->textInput(['maxlength' => 128, 'placeholder' => 'Email'])->label("") ?>
        </div>
    </div>
</div>
<?= $form->field($model, 'captcha')->widget(Captcha::className(), ['template' => "<div class='row'><div class='col-xs-2'>{image}</div><div class='col-xs-10'>{input}</div></div>"])->label("") ?>
<div class="form-group">
    <button type="submit" class='btn btn-orange review-form-submit'><i class="fa fa-paper-plane"></i> Отправить</button>
</div>
<?php ActiveForm::end(); ?>

<?php \yii\widgets\Pjax::end(); ?>
