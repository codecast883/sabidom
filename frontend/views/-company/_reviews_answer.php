<?php

use common\models\CompanyReview;
use \yii\widgets\LinkPager;
?>

<div class="block-1">
    <div class="block-1-title"><?= $title ?></div>
    <div class="block-1-body">
        <div class='reviews-list'>
            <?php \yii\widgets\Pjax::begin(['id' => 'reviews-list', 'enablePushState' => false, "linkSelector" => "none", "formSelector" => "none"]); ?>
            <?php if (count($reviews) == 0) { ?>
                <div class="reviews-list-empty-text">
                    Здесь пока никто не писал. Напишите отзыв первым!
                </div>
            <?php } else { ?>
                <?php foreach ($reviews as $review) { ?>
                    <?= $this->render("_reviews_answer_item", ['review' => $review]); ?>
                <?php } ?>
            <?php } ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
    <div class="text-center">
        <?php
        echo LinkPager::widget([
            'pagination' => $pagination,
        ]);
        ?>
    </div>
</div>


<script type="text/javascript">
    function reviewToggleStatus(review_id) {
        $.post("/company/review-toggle-status", {review_id: review_id}, function () {
            $.pjax.reload('#reviews-list');
        })
    }
</script>