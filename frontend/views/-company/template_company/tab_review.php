<div class="company-info">
    <table class="company-info-tabs">
        <tr>
            <td><a href="<?= $company->url ?>">Описание и отзывы клиентов</a></td>
            <td><a href="<?= $company->url_worker_reviews ?>">Отзывы сотрудников</a></td>
            <td><a href="<?= $company->url_company_feedback ?>">Представитель компании</a></td>
        </tr>
    </table>
    <div class="company-info-tabs-content">
        <div class="review-single">
            <div class="row">
                <div class="col-xs-2 reviews-list-item-left-col">
                    <div class='reviews-list-item-date'><?= date("Y-m-d", strtotime($review->date_post)) ?></div>
                    <div class='reviews-list-item-image'><img src="/img/generic-avatar.png"></div>
                </div>
                <div class="col-xs-10">
                    <div itemtype="http://schema.org/Person" itemscope="" itemprop="author">
                        <div class='reviews-list-item-username' itemprop="name">
                            <?= $review->name ?>
                        </div>
                    </div>
                    <?php if ($review->rating == 1) { ?>
                        <div class='reviews-list-item-rating reviews-list-item-rating-con'>Не рекомендует <i class="fa fa-thumbs-down"></i></div>
                    <?php } elseif ($review->rating == 5) { ?>
                        <div class='reviews-list-item-rating reviews-list-item-rating-pos'>Рекомендует <i class="fa fa-thumbs-up"></i></div>
                    <?php } ?>
                    <div class='reviews-list-item-comment' itemprop="reviewBody"><?= nl2br($review->text); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render("../_reviews_answer", ['reviews' => $reviews, 'pagination' => $pagination, 'title' => "<i class='fa fa-commenting'></i> Ответы на отзыв"]); ?>
<?= $this->render("../_reviews_form", array_merge($reviews_form, ["title" => "Ответить"])) ?>