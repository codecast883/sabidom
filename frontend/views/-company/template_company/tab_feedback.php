<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use \yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
?>

<div class="company-info">
    <table class="company-info-tabs">
        <tr>
            <td><a href="<?= $company->url ?>">Описание и отзывы клиентов</a></td>
            <td><a href="<?= $company->url_worker_reviews ?>">Отзывы сотрудников</a></td>
            <td class="active">Представитель компании</td>
        </tr>
    </table>
    <div class="company-info-tabs-content">
        <?= $this->render("_company_info.php", ['company' => $company]) ?>
    </div>
</div>
<div class="block-1">
    <div class="block-1-title"><i class="fa fa-envelope-o"></i> Я - представитель компании</div>
    <div class="block-1-body">
        Если Вы являетесь представителем компании <?= $company->title ?>, оставьте нам свой email и мы предоставим Вам доступ для управления страницей компании. 
        <?= $this->render("../_feedback_form", $feedback_form); ?>
    </div>
</div>
