<div class="row">
    <div class="col-xs-4">
        <div class='company-card-image'>
            <?php if ($company->image) { ?>
                <?=
                yii\helpers\Html::a(
                        yii\helpers\Html::img($company->getUploadedFileUrl('image', 'middle'), ["alt" => $company->title]), $company->getUploadedFileUrl('image'), ["class" => "fancybox logo"])
                ?>
            <?php } ?>

        </div>
    </div>
    <div class="col-xs-8">
        <table class="company-info-tabs-content-table">
            <?php if ($company->address) { ?>
                <tr>
                    <td>Адрес:</td>
                    <td><?= $company->address ?></td>
                </tr>
            <?php } ?>
            <?php if ($company->email) { ?>
                <tr>
                    <td>Email:</td>
                    <td><?= $company->email ?></td>
                </tr>
            <?php } ?>

            <?php if ($company->phones) { ?>
                <tr>
                    <td>Телефоны:</td>
                    <td><?= nl2br(strip_tags($company->phones)) ?></td>
                </tr>
            <?php } ?>
            <?php if ($company->site) { ?>
                <tr>
                    <td>Сайт:</td>
                    <td><a href="http://<?= $company->site ?>" target="_blank" rel="nofollow"><?= $company->site ?></a></td>
                </tr>
            <?php } ?>
            <?php if ($company->social_vk OR $company->social_fb OR $company->social_tw OR $company->social_in OR $company->social_gp OR $company->social_li) { ?>
                <tr>
                    <td>Соцсети:</td>
                    <td class="company-info-tabs-content-table-social">
                        <?php if ($company->social_vk) { ?>
                            <a target="_blank" rel="nofollow" href="<?= $company->social_vk ?>"><i class="fa fa-vk"></i></a>
                        <?php } ?>
                        <?php if ($company->social_fb) { ?>
                            <a target="_blank" rel="nofollow" href="<?= $company->social_fb ?>"><i class="fa fa-facebook-square"></i></a>
                        <?php } ?>
                        <?php if ($company->social_tw) { ?>
                            <a target="_blank" rel="nofollow" href="<?= $company->social_tw ?>"><i class="fa fa-twitter"></i></a>
                        <?php } ?>
                        <?php if ($company->social_in) { ?>
                            <a target="_blank" rel="nofollow" href="<?= $company->social_in ?>"><i class="fa fa-instagram"></i></a>
                        <?php } ?>    
                        <?php if ($company->social_gp) { ?>
                            <a target="_blank" rel="nofollow" href="<?= $company->social_gp ?>"><i class="fa fa-google-plus"></i></a>
                        <?php } ?>       
                        <?php if ($company->social_li) { ?>
                            <a target="_blank" rel="nofollow" href="<?= $company->social_li ?>"><i class="fa fa-linkedin"></i></a>
                        <?php } ?>       
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
<div class="company-info-extended">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-4">
            <table class="company-info-tabs-content-table">
                <?php foreach ($company->fields as $field) { ?>
                    <tr>
                        <td><?= $field->title ?>:</td>
                        <td><?= $field->value ?></td>
                    </tr>
                <?php } ?>
                <?php if ($affiliate = $company->affiliateCompanies) { ?>
                    <?php
                    $list = [];
                    foreach ($company->affiliateCompanies as $affiliate_company) {
                        $list[] = yii\helpers\Html::a($affiliate_company->title, $affiliate_company->url);
                    }
                    ?>
                    <tr>
                        <td>Аффилированные компании:</td>
                        <td><?= implode(", ", $list); ?></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="company-info-description">
                    <?= $company->description ?>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(".company-info-extended").shorten({
            moreText: 'Подробное описание компании >',
            lessText: '',
            showChars: 0,
            ellipsesText: ""
        });
    </script>
</div>