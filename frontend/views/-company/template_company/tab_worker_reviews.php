<div class="company-info">
    <table class="company-info-tabs">
        <tr>
            <td><a href="<?= $company->url ?>">Описание и отзывы клиентов</a></td>
            <td class="active">Отзывы сотрудников</td>
            <td><a href="<?= $company->url_company_feedback ?>">Представитель компании</a></td>
        </tr>
    </table>
    <div class="company-info-tabs-content">
        <?= $this->render("_company_info.php", ['company' => $company]) ?>
    </div>
</div>

<?= $this->render("../_reviews", ['reviews' => $reviews, 'pagination' => $pagination, 'title' => "<i class='fa fa-commenting'></i> Отзывы сотрудников об " . $company->title]); ?>
<?= $this->render("../_reviews_form", $reviews_form); ?>