<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use \yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
?>
<div class="container">
    <div class="row">
        <div class='col-xs-12'>
            <div class="block-1">
                <h1 class="block-1-title">
                    <i class="fa fa-bar-chart"></i> <?= $seo_h1 ?>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <?php foreach ($blocks as $block) { ?>
            <div class='col-xs-6'>
                <div class="block-1">
                    <div class="block-1-title">
                        <div class="rating-header rating-header-small">
                            <div class="row">
                                <div class="col-xs-3 text-center rating-header-image">
                                    <?php if ($block['category']->image) { ?>
                                        <?= yii\helpers\Html::img($block['category']->getUploadedFileUrl('image', 'middle'), ["alt" => $block['category']->title]); ?>
                                    <?php } ?>
                                </div>
                                <div class="col-xs-9">
                                    <div class=" rating-header-title">
                                        <a href="<?= $block['category']->url ?>"><?= $block['category']->title; ?></a><br>
                                    </div>
                                    <ul class="years-menu">
                                        <?php foreach ($block['years_menu'] as $year) { ?>
                                            <li class="<?= $year['active'] ? "active" : "" ?>"><?= Html::a($year['label'], $year['url'], ['title' => $year['title']]) ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($block['description_top']) { ?>
                        <div class="block-1-body">
                            <?= $block['description_top']; ?>
                        </div>
                    <?php } ?>
                    <div class="rating-block-body block-1-body">
                        <div id='rating-companies-container' class='rating-companies-container'>
                            <?=
                            $this->render("_list", [
                                "models" => $block['models'],
                                "start_num" => 0,
                                "category" => $block['category']
                                ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>