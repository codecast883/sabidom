<?php foreach ($models as $model) { ?>
    <table class="rating-block-body-table">
        <tr>
            <td class="rating-block-body-table-col-num"><div class="rating-block-body-table-col-num-medal"><?= $model->position ?></div></td>
            <td class="rating-block-body-table-col-logo">
                <?php if ($model->company->image) { ?>
                    <?= yii\helpers\Html::img($model->company->getUploadedFileUrl('image', 'small')); ?>
                <?php } ?>
            </td>
            <td class="rating-block-body-table-col-title">
                <a href="<?= $model->company->url ?>" title="<?= $model->company->title ?> - <?= $model->position ?> место <?= $category->title ?>">
                    <?= $model->company->title ?>
                </a>
            </td>
        </tr>
    </table>
<?php } ?>