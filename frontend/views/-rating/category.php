<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use \yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
?>
<div class="container">
    <div class="row">
        <div class='col-xs-8'>
            <div class="block-1">
                <h1 class="block-1-title">
                    <i class="fa fa-bar-chart"></i> <?= $seo_h1 ?>
                </h1>
                <div class="block-1-body">
                    <?php if ($categories_menu) { ?>
                        <table class='subcategories-list no-margin'>
                            <?php
                            $rows = [];

                            $i = 0;
                            $row_num = 0;
                            foreach ($categories_menu as $child) {
                                if ($i++ % 3 == 0) {
                                    $row_num++;
                                }

                                $rows[$row_num][] = "<td class='subcategories-list-item " . ($child['active'] ? "active" : "") . "' ><a href='" . $child['url'] . "'>" . $child['label'] . "</a></td>";
                            }
                            ?>
                            <?php foreach ($rows as $row) { ?>
                                <tr>
                                    <?= implode("", $row); ?>
                                </tr>
                            <?php } ?>
                        </table>
                    <?php } ?>
                </div>
            </div>
            <div class="block-1">
                <?php if ($category) { ?>
                    <div class="block-1-title">
                        <div class="rating-header row">
                            <div class="col-xs-2 text-center">
                                <?php if ($category->image) { ?>
                                    <?= yii\helpers\Html::img($category->getUploadedFileUrl('image', 'middle'), ["alt" => $category->title]); ?>
                                <?php } ?>
                            </div>
                            <div class="col-xs-6 rating-header-title">
                                <?= $category->title; ?>
                            </div>
                            <div class="col-xs-4 rating-header-years">
                                <ul class="years-menu">
                                    <?php foreach ($years_menu as $year) { ?>
                                        <li class="<?= $year['active'] ? "active" : "" ?>"><?= Html::a($year['label'], $year['url'], ['title' => $year['title']]) ?></li>
                                    <?php } ?>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <?php if ($description_top) { ?>
                        <div class="block-1-body">
                            <?= $description_top; ?>
                        </div>
                    <?php } ?>
                    <div class="rating-block-body block-1-body">
                        <div id='rating-companies-container' class='rating-companies-container'>
                            <?=
                            $this->render("_list", [
                                "models" => $models,
                                "start_num" => $start_num,
                                "category" => $category
                            ]);
                            ?>
                        </div>
                    </div>
                    <?php /* if ($more > 0) { ?>
                      <div class="text-center">
                      <span class='show-more-btn' id='show-more-rating-companies' data-current-page='1' data-year="<?= $current_year ?>" data-category-id="<?= $category->id ?>" >Загрузить еще <i class="fa fa-refresh"></i></span>
                      </div>
                      <?php } */ ?>
                <?php } else { ?>
                    <?php if ($description_top) { ?>
                        <div class="block-1-body">
                            <?= $description_top; ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="col-xs-4">
            <?=
            frontend\widgets\last_reviews\LastReviewsWidget::widget([
                'count' => 5,
            ]);
            ?>

        </div>
    </div>
</div>