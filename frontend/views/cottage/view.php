<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<div class="h2-badge-container"><h1 class="like-h2"><?= $cottage->title ?>.</h1> <span class="badge badge-green">ПРЕМИУМ</span></div>
<div class="row realt-info">
    <div class="col-xs-12 col-sm-8">
        <script>
            $(function () {
                $('.realt-slides').slides({
                    preload: true,
                    preloadImage: '/vendor/slides/img/loading.gif',
                    effect: 'slide, fade',
                    crossfade: true,
                    slideSpeed: 200,
                    fadeSpeed: 500,
                    generateNextPrev: false,
                    generatePagination: false
                });
            });
        </script>

        <div class="realt-slides">
            <div class="slides_container">
                <?php foreach ($cottage->images as $image) { ?>
                    <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'large'), ["alt" => $cottage->title]); ?>
                <?php } ?>
            </div>
            <ul class="pagination">
                <?php foreach ($cottage->images as $image) { ?>
                    
                    <li><a href="#"><?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'small'), ["alt" => $cottage->title]); ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="block block-white padding">
            <div class="realt-info-price"><span class="realt-info-price-label">Цена:</span><span class="realt-info-price-value">8 000 000</span><span class="realt-info-price-currency">руб.</span></div>
            <div class="realt-info-contacts">
                <div class="realt-info-contacts-type">Частное лицо</div>
                <div class="realt-info-contacts-name">А. Баранов</div>
                <div class="realt-info-contacts-phone">+7 963 711-06-74</div>
            </div>
            <div class="realt-info-contacts-row address">г. Саратов ул. Пушкина, 6</div>
            <div class="realt-info-contacts-row email">domikRus@mail.ru</div>
            <div class="realt-info-form">
                <form>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class='form-group'>
                                <input class="form-control" placeholder="Ваше имя">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class='form-group'>
                                <input class="form-control" placeholder="Ваш email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class='form-group'>
                                <textarea class="form-control" placeholder="Введите Ваше сообщение..."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class='form-group'>
                                <a href="#" class="btn btn-green btn-sm">Отправить</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row realt-info">
    <div class="col-xs-12 col-sm-8">
        <div class="block block-white padding shadow">
            <div class="like-h3">Характеристики</div>
            <div class="options">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="options-title">Дом</div>
                        <table class="options-table">
                            <tr class="options-row">
                                <td class="options-row-label"><span>Общая площадь</span></td>
                                <td class="options-row-value">282 м<sup>2</sup></td>
                            </tr>
                            <tr class="options-row">
                                <td class="options-row-label"><span>Количество этажей</span></td>
                                <td class="options-row-value">3</td>
                            </tr>
                        </table>
                        <div class="options-title">Инфраструктура</div>
                        <table class="options-table">
                            <tr class="options-row">
                                <td class="options-row-label"><span>Возможность прописки</span></td>
                                <td class="options-row-value">есть</td>
                            </tr>
                            <tr class="options-row">
                                <td class="options-row-label"><span>Отопление</span></td>
                                <td class="options-row-value">есть</td>
                            </tr>
                            <tr class="options-row">
                                <td class="options-row-label"><span>Электричество</span></td>
                                <td class="options-row-value">есть</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xs-6">
                        <div class="options-title">Участок</div>
                        <table class="options-table">
                            <tr class="options-row">
                                <td class="options-row-label"><span>Площадь участка</span></td>
                                <td class="options-row-value">9,1 соток</td>
                            </tr>
                            <tr class="options-row">
                                <td class="options-row-label"><span>Тип участка</span></td>
                                <td class="options-row-value">ижс</td>
                            </tr>
                        </table>
                        <div class="options-title">Условия сделки</div>
                        <table class="options-table">
                            <tr class="options-row">
                                <td class="options-row-label"><span>Тип продажи</span></td>
                                <td class="options-row-value">свободная продажа</td>
                            </tr>
                            <tr class="options-row">
                                <td class="options-row-label"><span>Ипотека</span></td>
                                <td class="options-row-value">возможна</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
            <hr>
            <div class="like-h3">Описание</div>
            <div class="realt-info-description">
                <?= $cottage->description ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <img style="width: 100%; height: 600px; background-color: gray" src="/img/pixel.png">
    </div>
</div>
<div class="block">
    <h2>Предложение рядом</h2>
    <div id="map" style="height:360px; width: 100%;"></div>
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript">
            ymaps.ready(init);
            function init() {
                var myMap = new ymaps.Map("map", {
                    center: [45.436554, 12.338716],
                    zoom: 18,
                    controls: ["zoomControl", "fullscreenControl", "geolocationControl", "routeEditor", "typeSelector"]
                }
                );
                myGeoObject = new ymaps.GeoObject();
                myMap.geoObjects.add(new ymaps.Placemark([45.436554, 12.338716]));
            }
    </script>
</div>

<?= frontend\widgets\block_realts\BlockRealtsWidget::widget(['title' => "Популярные", 'template' => 'template3', 'limit' => 6]); ?>
<div class="block">
    <img style="width: 100%; height: 200px; background-color: gray" src="/img/pixel.png">
</div>
<div class="block">
    <div class="like-h1">Дачи вашем регионе</div>
    <div class="row">
        <div class="col-xs-4">
            <div class="list1">
                <div class="list1-header">ГОРОДА</div>
                <ul class="list1-items">
                    <li><a href="#">Верея (6575)</a></li>
                    <li><a href="#">Высоковск (6521)</a></li>
                    <li><a href="#">Дзержинский (6521)</a></li>
                    <li><a href="#">Домодедово (6521)</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="list1">
                <div class="list1-header">ГОРОДА</div>
                <ul class="list1-items">
                    <li><a href="#">Верея (6575)</a></li>
                    <li><a href="#">Высоковск (6521)</a></li>
                    <li><a href="#">Дзержинский (6521)</a></li>
                    <li><a href="#">Домодедово (6521)</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="list1">
                <div class="list1-header">ГОРОДА</div>
                <ul class="list1-items">
                    <li><a href="#">Верея</a> (6575)</li>
                    <li><a href="#">Высоковск</a> (6521)</li>
                    <li><a href="#">Дзержинский</a> (6521)</li>
                    <li><a href="#">Домодедово</a> (6521)</li>
                </ul>
            </div>
        </div>
    </div>
</div>