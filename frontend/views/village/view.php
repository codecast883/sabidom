<?php

use common\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;;
?>

<?//= \common\owerride\yii\widgets\Breadcrumbs::widget($breadcrumbs); ?>


<!--Begin custom breadcrumbs-->
<ul class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">

    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a href="/" itemprop="name"><?= $breadcrumbs['homeLink']['label'] ?></a>
    </li>


    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
        class="drpdwn">
        <a href="<?= $breadcrumbs['links'][0]['url'] ?>" itemprop="item"><span
                    itemprop="name"><?= $breadcrumbs['links'][0]['label'] ?></a>

        <ul class="subcategory">

            <?php foreach ($top_menu as $key => $value): ?>
                <?php if ($value['label'] == 'Еще') continue; ?>
                <li><a href="<?= $value['url'] ?>"><?= $value['label'] ?></a></li>
            <?php endforeach ?>

            <?php foreach ($top_menu[7]['items'] as $values): ?>
                <li><a href="<?= $values['url'] ?>"><?= $values['label'] ?></a></li>
            <?php endforeach ?>

        </ul>
    </li>


    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="active">
        <span itemprop="name"><?= $breadcrumbs['links'][1]['label'] ?></span>
    </li>


</ul>
<!--End custom breadcrumbs-->


<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $village]); ?>
<div class="h2-badge-container"><h1 class="like-h2"><?= $h1 ?></h1> <?php if ($village->premium) { ?><span class="badge badge-green">ПРЕМИУМ</span><?php } ?></div>
<div class="row realt-info">
    <div class="col-xs-12 col-sm-8">
        <script>
            $(function () {
                $('.realt-slides').slides({
                    preload: true,
                    preloadImage: '/vendor/slides/img/loading.gif',
                    effect: 'slide, fade',
                    crossfade: true,
                    slideSpeed: 200,
                    fadeSpeed: 500,
                    generateNextPrev: false,
                    generatePagination: false
                });
            });
        </script>
        <div class="realt-slides">
            <div class="slides_container">
                <?php foreach ($village->images as $image) { ?>
                    <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'large'), ["alt" => $village->title]); ?>
                <?php } ?>
            </div>
            <ul class="pagination">
                <?php foreach ($village->images as $image) { ?>
                    <li><a href="#"><?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'small'), ["alt" => $village->title]); ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="block block-white padding">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="params">
                        <?php if ($village->house_area_from OR $village->house_area_to) { ?>
                            <div class="params-row">
                                <span class="params-row-label">Площадь объекта недвижимости:</span>
                                <span class="params-row-value">
                                    <?= $village->house_area_from ? "от " . $village->house_area_from : "" ?> <?= $village->house_area_to ? "до " . $village->house_area_to : "" ?>м<sup>2</sup>
                                </span>
                            </div>
                        <?php } ?>
                        <?php if ($village->stead_area_from OR $village->stead_area_to) { ?>
                            <div class="params-row">
                                <span class="params-row-label">Площадь участка:</span>
                                <span class="params-row-value">
                                    <?= $village->stead_area_from ? "от " . $village->stead_area_from : "" ?> <?= $village->stead_area_to ? "до " . $village->stead_area_to : "" ?> соток</sup>
                                </span>
                            </div>
                        <?php } ?>

                        <?php if ($village->village_square) { ?>
                            <div class="params-row">
                                <span class="params-row-label">Площадь посёлка:</span>
                                <span class="params-row-value">
                                    <?= $village->village_square ?>
                                </span>
                            </div>
                        <?php } ?>


                        <?php foreach ($village->getFields(NULL, 1) as $field) { ?>
                            <div class="params-row">
                                <span class="params-row-label"><?= $field['info']->title ?>:</span>
                                <span class="params-row-value">
                                    <?= $field['values_formated'] ?>
                                </span>
                            </div>
                        <?php } ?>
                        <div class="params-row">
                            <span class="params-row-label">Цена:</span>
                            <span class="params-row-value price"><?= $village->priceFormated(); ?></span>
                            <?php if ($village->price_for_hundred) { ?>
                                (за сотку: <b><?php echo $village->price_for_hundred?></b>)
                            <?php } ?>
                        </div>



                    </div>
                    <?= $this->render("_view_request_form", $view_request_form); ?>
                </div>

                <div class="col-xs-12 col-md-5 text-right">
				<?$callOne = array("2163", "1435", "36", "1039", "907", "749", "35", "1054", "1295", "30", "8","938");?>
				<?$callTwo = array("9", "2160", "984", "442", "2161", "1071", "710", "2162");?>
				<?php if (in_array($village->id, $callOne)) { $addCallPhone = "call_phone_1"; }?>
				<?php if (in_array($village->id, $callTwo)) { $addCallPhone = "call_phone_2"; }?>
                    <?php if ($village->phone) { ?><div class="realt-info-contacts-phone show-hidden-data mark <?if ($addCallPhone) { echo $addCallPhone; }?>" data-mark-name="show-phone" data-hidden="<?= $village->phone ?>"><span><?= $village->phone ?></span></div><?php } ?>
                    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="modal">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="hidden-xs">
		  <?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "village_center_bottom"]); ?>
        </div>

        <div class="block block-white padding shadow">
            <div class="like-h3">Описание</div>
            <div class="realt-info-description">
                <?= $village->description ?>
            </div>
        </div>		
        <?php if ($fields = $village->getFields(NULL, 2)) { ?>
            <div class="block block-white padding shadow">
                <div class="options">
                    <div class="options-title">Коммуникации</div>
                    <table class="options-table">
                        <colgroup>
                            <col style='width: 240px;'>
                        </colgroup>
                        <?php foreach ($fields as $field) { ?>
                            <tr class="options-row">
                                <td class="options-row-label"><div><span><?= $field['info']->title ?></span></div></td>
                                <td class="options-row-value"><?= $field['values_formated'] ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col-xs-12 col-sm-4">
        <?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "village_left"]); ?>
    </div>
</div>
<?php if ($village->coord_lat AND $village->coord_lng) { ?>
    <div class="block">
        <h2>Расположение</h2>
        <div id="map" style="height:360px; width: 100%;"></div>
        <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script type="text/javascript">
            ymaps.ready(init);
            function init() {
                var myMap = new ymaps.Map("map", {
                    center: [<?= $village->coord_lat ?>, <?= $village->coord_lng ?>],
                    zoom: 14,
                    controls: ["zoomControl", "fullscreenControl", "geolocationControl", "routeEditor", "typeSelector"]
                }
                );
                myGeoObject = new ymaps.GeoObject();
                myMap.geoObjects.add(new ymaps.Placemark([<?= $village->coord_lat ?>, <?= $village->coord_lng ?>]));
            }
        </script>
    </div>
<?php } ?>

<?php //= frontend\widgets\block_realts\BlockRealtsWidget::widget(['title' => "Объекты в поселке", 'template' => 'template3', 'limit' => 6]); ?>
<?php //= frontend\widgets\block_realts\BlockRealtsWidget::widget(['title' => "Участки в поселке", 'template' => 'template3', 'limit' => 6]); ?>

<div class="block visible-md-block visible-lg-block">
    <?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "footer"]); ?>
</div>