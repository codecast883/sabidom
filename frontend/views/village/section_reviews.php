<?php

use yii\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;

;

use yii\widgets\LinkPager;
use common\models\VillageReview;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>

<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $village, 'active' => 'reviews']); ?>

<div class="row realt-info">
    <div class="col-xs-12 col-sm-6 col-md-5">
        <div class="realt-image">
			<?php if ($village->image) { ?>
				<?= yii\helpers\Html::img($village->image->getUploadedFileUrl('image', 'large'), ["alt" => $village->title]); ?>
			<?php } ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-7">
        <h1 class="like-h2 style2"><?= $h1 ?></h1>        
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <table class="rating-total shadow">
                    <tr>
                        <td class="rating-total-col1">
                            <div class="rating-total-value">505</div>
                            <div class="rating-total-label">отзыва</div>
                        </td>
                        <td class="rating-total-col2">
                            <div class="rating-total-value">105</div>
                            <div class="rating-total-label">оценка</div>
                            <div class="rating-total-col2-arrow"></div>
                        </td>
                        <td class="rating-total-col3">
                            <div class="rating-total-value">75%</div>
                            <div class="rating-total-label">положительных</div>
                        </td>
                    </tr>
                </table>
                <b>3 года</b> на портале<br>
                У компании 1200 предложений в 300 категориях<br>
                <b>1 час</b> - время ответа компании<br>
                <b>менее суток</b> назад был на портале<br>
                Наличие документов<br>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="block block-white padding shadow">
                    Оцените качество обслуживания и оставьте ваш отзыв!<br><br>
                    <div class="text-center">
						<?= Html::a("Добавить отзыв", "#", ['class' => 'btn btn-green btn-lg', 'data-toggle' => "modal", 'data-target' => "#modal-add-review"]) ?>
                    </div>
                    <div class="modal fade modal-add-review" id="modal-add-review" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
								<?= $this->render("_reviews_form", $reviews_form) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<br>
<div class="like-h2">Оценки покупателей</div>
<div class="rating-total-extended-container">
    <table class="rating-total-extended rating-total-extended-headers hidden-xs">
        <tr>
            <td class="rating-total-extended-col1">Критерий</td>
            <td class="rating-total-extended-col2">Средняя оценка</td>
            <td class="rating-total-extended-col3">Кол-во оценок</td>
            <td class="rating-total-extended-col4"></td>
        </tr>
    </table>
    <div class="block block-white shadow padding">
        <table class="rating-total-extended rating-total-extended-body">
            <tr>
                <td class="rating-total-extended-col1">Цена/качество</td>
                <td class="rating-total-extended-col2"><span class="rating-autostars" data-rating="<?= $rating_info['rating_1']['avg'] ?>"></span></td>
                <td class="rating-total-extended-col3"><?= $rating_info['rating_1']['count'] ?></td>
                <td class="rating-total-extended-col4"><img  src="/img/raty/5/star-on.png"> Позитивные</td>
            </tr>
            <tr>
                <td class="rating-total-extended-col1">Транспортная доступность</td>
                <td class="rating-total-extended-col2"><span class="rating-autostars" data-rating="<?= $rating_info['rating_2']['avg'] ?>"></span></td>
                <td class="rating-total-extended-col3"><?= $rating_info['rating_2']['count'] ?></td>
                <td class="rating-total-extended-col4"><img  src="/img/raty/1/star-on.png"> Негативные</td>
            </tr>
            <tr>
                <td class="rating-total-extended-col1">Инфраструктура поселка</td>
                <td class="rating-total-extended-col2"><span class="rating-autostars" data-rating="<?= $rating_info['rating_3']['avg'] ?>"></span></td>
                <td class="rating-total-extended-col3"><?= $rating_info['rating_3']['count'] ?></td>
                <td class="rating-total-extended-col4"><img  src="/img/raty/3/star-on.png"> Нейтральные</td>
            </tr>
            <tr>
                <td class="rating-total-extended-col1">Инфраструктура рядом</td>
                <td class="rating-total-extended-col2"><span class="rating-autostars" data-rating="<?= $rating_info['rating_4']['avg'] ?>"></span></td>
                <td class="rating-total-extended-col3"><?= $rating_info['rating_4']['count'] ?></td>
                <td class="rating-total-extended-col4"></td>
            </tr>
            <tr>
                <td class="rating-total-extended-col1">Архитектура</td>
                <td class="rating-total-extended-col2"><span class="rating-autostars" data-rating="<?= $rating_info['rating_5']['avg'] ?>"></span></td>
                <td class="rating-total-extended-col3"><?= $rating_info['rating_5']['count'] ?></td>
                <td class="rating-total-extended-col4"></td>
            </tr>
        </table>
    </div>
</div>
<br>
<h2><?= $h2 ?></h2>
<?= Html::a("Добавить отзыв", "#", ['class' => 'btn btn-green btn-lg', 'data-toggle' => "modal", 'data-target' => "#modal-add-review"]) ?>
<br>
<br>
<div class="reviews-list-container">
    <ul class="reviews-list-tabs">
        <li class="active"><span>Все (<?= $reviews_info['total']; ?>)</span></li>
        <li><a href="#">Позитивные (<?= $reviews_info['pos']; ?>)</a></li>
        <li><a href="#">Негативные (<?= $reviews_info['con']; ?>)</a></li>
        <li><a href="#">Нейтральные (<?= $reviews_info['neytral']; ?>)</a></li>
    </ul>
    <div class="block block-white padding shadow">
		<?php \yii\widgets\Pjax::begin(['id' => 'reviews-list', 'enablePushState' => false, "linkSelector" => "none", "formSelector" => "none", 'options' => ['class' => 'ajax-fog']]); ?>
        <div class='reviews-list'>
			<?php if (count($reviews) == 0) { ?>
				<div class="reviews-list-empty-text">

				</div>
			<?php } else { ?>
				<?php foreach ($reviews as $review) { ?>
					<div class='reviews-list-item-container'>
						<div class="reviews-list-item <?= $review->status != VillageReview::STATUS_ENABLED ? "review-disabled" : "" ?>" id="div_review_<?= $review->id ?>">

							<div class="reviews-list-item-name"><?= $review->name ?></div>
							<div class="reviews-list-item-date"><?= date("d.m.Y", strtotime($review->created_at)); ?></div>
							<div class="reviews-list-item-rating"><span class="rating-autostars" data-rating="<?= $review->rating_avg ?>"></span></div>
							<div class="reviews-list-item-text">
								<?= $review->text ?>
							</div>
						</div>

						<?php /* if ($review->can("manage")) { ?>
						  <div class="reviews-list-item-cp">
						  <?php if ($review->can("toggle-status")) { ?>
						  <?php if ($review->status == 1) { ?>
						  <button class="btn btn-default btn-sm" onclick="reviewToggleStatus(<?= $review->id ?>)">Выкл</button>
						  <?php } else { ?>
						  <button class="btn btn-default btn-sm"  onclick="reviewToggleStatus(<?= $review->id ?>)">Вкл</button>
						  <?php } ?>
						  <?php } ?>
						  <?php if (\Yii::$app->user->can("admin-panel-access")) { ?>
						  <a class="btn btn-default btn-sm" href="/admin/company-review/update?id=<?= $review->id ?>" target="_blank">Ред</a>
						  <?php } ?>
						  </div>
						  <?php } */ ?>
					</div>
				<?php } ?>
			<?php } ?>
        </div>
		<?php \yii\widgets\Pjax::end(); ?>

        <script type="text/javascript">
	    function reviewToggleStatus(review_id) {
		$.post("<?= \yii\helpers\Url::to(["company/review-toggle-status"]) ?>", {review_id: review_id}, function () {
		    $.pjax.reload('#reviews-list');
		});
		return false;
	    }
        </script>

		<?php //= $this->render("view_reviews_form", $reviews_form);   ?>
    </div>
    <div class="text-center">
		<?php
		echo LinkPager::widget([
			'pagination' => $pagination,
		]);
		?>
    </div>
</div>