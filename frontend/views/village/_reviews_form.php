<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\FileInputAsset;
use yii\captcha\Captcha;
?>

<?php
\yii\widgets\Pjax::begin([
	'enablePushState' => false,
	'formSelector' => "#reviews-form", 'options' => ['class' => 'ajax-fog']
]);
?>

<?php if ($success) { ?>
	<div class="alert alert-success" role="alert"><?= $message ?></div>
<?php } ?>
<?php if ($error) { ?>
	<div class="alert alert-warning" role="alert"><?= $message ?></div>
<?php } ?>

<?php
$form = ActiveForm::begin([
			'options' => [
				'enctype' => 'multipart/form-data',
			],
			'id' => "reviews-form",
			'validateOnChange' => false,
			'validateOnBlur' => true,
			'enableClientValidation' => false,
			'type' => ActiveForm::TYPE_VERTICAL,
			'formConfig' => [
				'deviceSize' => ActiveForm::SIZE_TINY
			]]
);
?>

<div class="row">
	<div class="col-xs-12">
		<div class='form-group'>
			<?= $form->field($model, 'name')->textInput(['maxlength' => 128, 'placeholder' => "Как Вас зовут?"])->label("") ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-6">
		<table class="add-review-ratings">
			<tr>
				<td class="add-review-ratings-col1">Цена/качество</td>
				<td class="add-review-ratings-col2">
					<div class="field-companyreview-rating">
						<?= $form->field($model, 'rating_1')->hiddenInput()->label("", ['class' => 'hidden']) ?>
						<div class="review-rating-stars" data-target="#villagereview-rating_1"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="add-review-ratings-col1">Транспортная доступность</td>
				<td class="add-review-ratings-col2">
					<div class="field-companyreview-rating">
						<?= $form->field($model, 'rating_2')->hiddenInput()->label("", ['class' => 'hidden']) ?>
						<div class="review-rating-stars" data-target="#villagereview-rating_2"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="add-review-ratings-col1">Инфраструктура поселка</td>
				<td class="add-review-ratings-col2">
					<div class="field-companyreview-rating">
						<?= $form->field($model, 'rating_3')->hiddenInput()->label("", ['class' => 'hidden']) ?>
						<div class="review-rating-stars" data-target="#villagereview-rating_3"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="add-review-ratings-col1">Инфраструктура рядом</td>
				<td class="add-review-ratings-col2">
					<div class="field-companyreview-rating">
						<?= $form->field($model, 'rating_4')->hiddenInput()->label("", ['class' => 'hidden']) ?>
						<div class="review-rating-stars" data-target="#villagereview-rating_4"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="add-review-ratings-col1">Архитектура</td>
				<td class="add-review-ratings-col2">
					<div class="field-companyreview-rating">
						<?= $form->field($model, 'rating_5')->hiddenInput()->label("", ['class' => 'hidden']) ?>
						<div class="review-rating-stars" data-target="#villagereview-rating_5"></div>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class='form-group'>
			<?= $form->field($model, 'text')->textarea(['rows' => 7, 'placeholder' => "Ваш отзыв"])->label("") ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class='form-group text-center'>
			<button type="submit" class='btn btn-green btn-lg'>Добавить отзыв</button>
		</div>
	</div>
</div>

<script>
    initInputRating();
</script>
<?php ActiveForm::end(); ?>
<?php \yii\widgets\Pjax::end(); ?>