<?php

use yii\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;;
use yii\widgets\LinkPager;
use common\models\VillageReview;
?>


<?= Breadcrumbs::widget($breadcrumbs); ?>
<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $village, 'active' => 'general-layout']); ?>

<div class="h2-badge-container"><h1 class="like-h2"><?= $h1 ?></h1> <?php if ($village->premium) { ?><span class="badge badge-green">ПРЕМИУМ</span><?php } ?></div>
<div class="village-master-plan">
    <?= Html::img($village->getUploadedFileUrl('master_plan')) ?>
</div>