<?php

use yii\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;

;

use yii\widgets\LinkPager;
use common\models\VillageReview;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $village, 'active' => 'contacts']); ?>

<div class="h2-badge-container"><h1 class="like-h2"><?= $h1 ?></h1> <?php if ($village->premium) { ?><span class="badge badge-green">ПРЕМИУМ</span><?php } ?></div>

<div class="village-contacts">
    <div class="village-contacts-map-container">
        <div id="map" class="village-contacts-map"></div>
				
        <?php if ($village->route_explain_public OR $village->route_explain_auto) { ?>
            <div class="village-contacts-float">
                <?php if ($village->route_explain_auto) { ?>
                    <div class="village-contacts-route"><b>Как добраться на машине:</b> <?= $village->route_explain_auto ?></div>
                <?php } ?>
                <?php if ($village->route_explain_public) { ?>
                    <div class="village-contacts-route"><b>Как добраться на общественном транспорте:</b> <?= $village->route_explain_public ?></div>
                <?php } ?>
            </div>
        <?php } ?>
        <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script type="text/javascript">
            ymaps.ready(init);
            function init() {
                var myMap = new ymaps.Map("map", {
                    center: [<?= $village->coord_lat ?>, <?= $village->coord_lng ?>],
                    zoom: 14,
                    controls: ["zoomControl", "fullscreenControl", "geolocationControl", "routeEditor", "typeSelector"]
                }
                );
                myGeoObject = new ymaps.GeoObject();
                myMap.geoObjects.add(new ymaps.Placemark([<?= $village->coord_lat ?>, <?= $village->coord_lng ?>],
                        {
                            balloonContent: '<?= $village->address ?>',
                        }));
            }
        </script>
    </div>
</div>