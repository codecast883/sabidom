<?php

use common\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;;
use yii\widgets\LinkPager;
use common\models\VillageReview;
?>
<?= Breadcrumbs::widget($breadcrumbs); ?>
<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $village, 'active' => 'townhouse']); ?>
    <br><br>
    <div class="row">
        <div class="col-xs-6">
            <div class="block block-white padding shadow village-townhouse-image">
                <?= Html::img($village_townhouse->getUploadedFileUrl("image", 'middle')) ?>
            </div>

            <div class="block block-white padding shadow">
                <div class="like-h3">Описание</div>
                <div class="realt-info-description realt-info">
                    <p><?= $village_townhouse->description ?></p>
                </div>
            </div>



            <div class="block block-white padding shadow">
                <div class="row village-townhouse-view-buttons">
                    <div class="col-xs-6">
                        <a href="#">
                            <img src="/img/ico-3d.png">
                            Смотреть 3D-модель
                        </a>
                    </div>
                    <?php if ($village_townhouse->images) { ?>
                        <div class="col-xs-6">
                            <a href="#" id="village-townhouse-interior-view">
                                <img src="/img/ico-interior.png">
                                Интерьер
                            </a>
                            <script>
                                $("#village-townhouse-interior-view").on('click', function () {
                                    $.fancybox.open([
                                        <?php foreach ($village_townhouse->images as $image) { ?>
                                        {href: "<?= $image->getUploadedFileUrl('image') ?>"},
                                        <?php } ?>
                                    ]);
                                    return false;
                                });
                            </script>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <h1 class="like-h2"><?= $h1 ?></h1>
            <div class="village-townhouse-params">


                <div class="realt-info-price"><span
                            class="realt-info-price-label">Цена:</span><span class="price-value"><?= $village_townhouse->price_from ?></span><span class="price-currency">руб.<?php if ($village_townhouse->price_for_hundred){echo ' ('. $village_townhouse->price_for_hundred.' тыс.р./сотка)';}?></span>
                </div>

                <div class="realt-info-contacts-phone show-hidden-data mark" data-mark-name="show-phone" data-hidden="<?= $village_townhouse->phone ?>"><span><?= $village_townhouse->phone ?></span></div>

                <?php if ($village_townhouse->type) { ?>
                <div class="village-townhouse-params-row">
                    <span class="village-townhouse-params-row-label">Тип:</span>
                    <span class="village-townhouse-params-row-value"> <?= $village_townhouse->type ?></span>
                </div>
                <?php } ?>
            <div class="village-townhouse-params-row">
                <span class="village-townhouse-params-row-label">Площадь дома:</span>
                <span class="village-townhouse-params-row-value"><?php if ($village_townhouse->house_area) { ?> <?= $village_townhouse->house_area ?> м<sup>2</sup><?php } else { ?>-<?php } ?></span>
            </div>
                <?php if ($village_townhouse->number_of_floors) { ?>
            <div class="village-townhouse-params-row">
                <span class="village-townhouse-params-row-label">Количество этажей:</span>
                <span class="village-townhouse-params-row-value"> <?= $village_townhouse->number_of_floors ?></span>
            </div>
                <?php } ?>
            <div class="village-townhouse-params-row">
                <span class="village-townhouse-params-row-label">Площадь участка:</span>
                <span class="village-townhouse-params-row-value"><?php if ($village_townhouse->stead_area) { ?> <?= $village_townhouse->stead_area ?> сот.<?php } else { ?>-<?php } ?></span>
            </div>

        </div>

        <div class="block village-townhouse-tabs">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Коммуникации</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Технические параметры</a></li>
                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Инфраструктура</a></li>
                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Услуги</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <div class="village-townhouse-params-ext row">
                        <?php foreach ($village_townhouse->getFields(NULL, 3) as $field) { ?>
                            <div class="col-xs-6">
                                <div class="item"><?= $field['values_formated'] ?></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <table class="village-townhouse-params-ext2">
                        <?php foreach ($village_townhouse->getFields(NULL, 4) as $field) { ?>
                            <tr class="village-townhouse-params-ext2-row">
                                <td class="village-townhouse-params-ext2-row-label"><?= $field['info']->title ?>:</td>
                                <td class="village-townhouse-params-ext2-row-value"><?= $field['values_formated'] ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="messages">
                    <div class="village-townhouse-params-ext row">
                        <?php foreach ($village_townhouse->getFields(NULL, 6) as $field) { ?>
                            <div class="col-xs-6">
                                <div class="item"><?= $field['values_formated'] ?></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="settings">
                    <div class="village-townhouse-params-ext row">
                        <?php foreach ($village_townhouse->getFields(NULL, 5) as $field) { ?>
                            <div class="col-xs-6">
                                <div class="item"><?= $field['values_formated'] ?></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>

    <div class="block text-center">
        <a href="#" class="btn btn-lg btn-green">Записаться на просмотр</a>
    </div>

<?php if ($village_townhouse->layout) { ?>
    <div class="block block-white padding shadow village-townhouse-layout">
        <div class="like-h2">
            <div class="text-center">
                Планировка
            </div>
        </div>
        <div class="text-center">
            <?= Html::img($village_townhouse->getUploadedFileUrl("layout")); ?>
        </div>
    </div>
<?php } ?>

<?php if ($other_projects) { ?>
    <h1 class="like-h2">Типовые проекты домов в поселке <?= $village->title ?></h1>
    <div class="offer-large grid">
        <?php foreach (Html::explodeToRows($other_projects, 3) as $row) { ?>
            <div class="row">
                <?php foreach ($row as $model) { ?>
                    <div class="col-xs-12 col-sm-4">
                        <div class="offer-large-item">
                            <div class="offer-large-item-image block block-white padding shadow">
                                <?php if ($model->image) { ?>
                                    <?= yii\helpers\Html::img($model->getUploadedFileUrl('image', 'small')); ?>
                                <?php } ?>
                            </div>
                            <a href ='#' class="realt-add-to-bookmark">Добавить в избранное <i class="fa fa-heart"></i></a>
                            <div class="offer-large-item-title">
                                <?= Html::a($model->title, $model->url); ?>
                            </div>

                            <div class="options-simple2">
                                <?php if ($model->type) { ?>
                                    <div class="options-simple2-row">
                                        <span class="options-simple2-row-label">Тип: </span>
                                        <span class="options-simple2-row-value"> <?= $model->type ?></span>
                                    </div>
                                <?php }?>
                                <div class="options-simple2-row">
                                    <span class="options-simple2-row-label">Площадь дома: </span>
                                    <span class="options-simple2-row-value"><?php if ($model->house_area) { ?> <?= $model->house_area ?> м<sup>2</sup><?php } else { ?>-<?php } ?></span>
                                </div>


                                <?php if ($model->number_of_floors) { ?>
                                    <div class="options-simple2-row">
                                        <span class="options-simple2-row-label">Этажей: </span>
                                        <span class="options-simple2-row-value"> <?= $model->number_of_floors ?></span>
                                    </div>
                                <?php }  ?>

                                <div class="options-simple2-row">
                                    <span class="options-simple2-row-label">Площадь участка: </span>
                                    <span class="options-simple2-row-value"><?php if ($model->stead_area) { ?> <?= $model->stead_area ?> сот.<?php } else { ?>-<?php } ?></span>
                                </div>
                                <div class="options-row-simple2">
                                    <span class="options-simple2-row-label">Цена: </span>
                                    <span class="options-simple2-row-value"><?= $model->priceFormated(); ?></span>
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg btn-green">Записаться на просмотр</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>