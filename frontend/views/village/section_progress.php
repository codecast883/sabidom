<?php

use yii\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;;
use yii\widgets\LinkPager;
use common\models\VillageReview;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $village, 'active' => 'progress']); ?>
<div class="h2-badge-container"><h1 class="like-h2"><?= $h1 ?></h1> <?php if ($village->premium) { ?><span class="badge badge-green">ПРЕМИУМ</span><?php } ?></div>

<?php if ($village->progress_images) { ?>
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <div id="carousel-example-generic" class="carousel slide village-progress-carousel" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    $i = 0;
                    foreach ($village->progress_images as $image) {
                        ?>
                        <div class="item <?= $i++ == 0 ? "active" : "" ?> ">
                            <?= Html::img($image->getUploadedFileUrl('image', 'large')); ?>
                        </div>
                    <?php } ?>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
    </div>
<?php } ?>
<br><br>