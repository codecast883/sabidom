<?php

use yii\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;;
use yii\widgets\LinkPager;
use common\models\VillageReview;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>

<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $village, 'active' => 'documentation']); ?>

<div class="h2-badge-container"><h1 class="like-h2"><?= $h1 ?></h1> <?php if ($village->premium) { ?><span class="badge badge-green">ПРЕМИУМ</span><?php } ?></div>
<div class="block block-white padding">
    <div class="village-documents">
        <?php foreach ($village->documents as $document) { ?>
            <div class='village-documents-item'><i class="fa fa-file-text-o" aria-hidden="true"></i> <?= Html::a($document->title ? $document->title : $document->file, $document->getUploadedFileUrl('file')) ?></div>
        <?php } ?>
    </div>
</div>
