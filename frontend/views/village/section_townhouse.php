<?php

use common\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;
use common\models\VillageReview;
?>

<div class="clearfix">
    <div class="pull-left"><?= Breadcrumbs::widget($breadcrumbs); ?></div>
    <div class="print pull-right"><a href="#"><i class="fa fa-print" aria-hidden="true"></i> Распечатать прайслист</a></div>
</div>
<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $village, 'active' => 'townhouse']); ?>
<div class="fix-height-container">
	<div class="category-title-row row">
		<div class="col-xs-12">
			<h1 class="like-h2 pull-left"><?= $h1 ?></h1>
			<div class="category-controls  pull-right">
<!--				<div class="category-view-type-selector-block">-->
<!--					Параметры отображения:-->
<!--					<a class="btn btn-default category-view-type-selector category-view-type-selector-list" data-type="list"></a>-->
<!--					<a class="btn btn-default category-view-type-selector category-view-type-selector-grid" data-type="grid"></a>&nbsp;&nbsp;-->
<!--				</div>-->
				<div class="category-order-selector">
					<select class="form-control" onchange="location.assign($(this).find('option:selected').data('location'))">
						<option disabled <?= \Yii::$app->request->get('sort') == "" ? "selected" : "" ?>>Сортировать</option>
						<option <?= \Yii::$app->request->get('sort') == "p.asc" ? "selected" : "" ?> data-location="<?= $category->url . "?sort=p.asc"; ?>">Сначала дешевле</option>
						<option <?= \Yii::$app->request->get('sort') == "p.desc" ? "selected" : "" ?> data-location="<?= $category->url . "?sort=p.desc"; ?>">Сначала дороже</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="fix-height" data-fix-container="#village-description-price-bottom-fix">
		<?= $village->description_price ?>
	</div>

	<div id="village-ads"></div>
	<script>
	function loadAds(type) {
	    $("#village-ads").load(
		    "<?= yii\helpers\Url::to(['village/ajax-ads', 'id' => $village->id]) ?>?list-type=" + type,
		    function () {
			seoFixHeight();
		    }
	    );

	    $(".category-view-type-selector").removeClass("active");
	    $(".category-view-type-selector-" + type).addClass("active");
	    $.totalStorage('display-ads', type);
	}


	$(".category-view-type-selector").on('click', function () {
	    loadAds($(this).data('type'));
	    return false;
	});

	view = $.totalStorage('display-ads');
	if (view) {
	    loadAds(view);
	} else {
	    loadAds('grid');
	}
	</script>

	<?php if ($projects) { ?>
		<h1 class="like-h2">Типовые проекты домов в поселке <?= $village->title ?></h1>
		<div class="offer-large grid">
			<?php foreach (Html::explodeToRows($projects, 3) as $row) { ?>
				<div class="row">
					<?php foreach ($row as $model) { ?>
						<div class="col-xs-12 col-sm-4">
							<div class="offer-large-item">
								<div class="offer-large-item-image block block-white padding shadow">
									<?php if ($model->image) { ?>
										<?= yii\helpers\Html::img($model->getUploadedFileUrl('image', 'small')); ?>
									<?php } ?>
								</div>
								<div class="row">
									<div class="col-xs-8">
										<a href ='#' class="realt-add-to-bookmark">Добавить в избранное <i class="fa fa-heart"></i></a>
									</div>
									<div class="col-xs-4 text-right">
										<a href="#" class="add-to-compare">Сравнить</a>
									</div>
								</div>
								<div class="offer-large-item-title">
									<?= Html::a($model->title, $model->url); ?>
								</div>

								<div class="options-simple2">
                                    <?php if ($model->type) { ?>
                                    <div class="options-simple2-row">
                                        <span class="options-simple2-row-label">Тип: </span>
                                        <span class="options-simple2-row-value"> <?= $model->type ?></span>
                                    </div>
                                    <?php }?>
									<div class="options-simple2-row">
										<span class="options-simple2-row-label">Площадь дома: </span>
										<span class="options-simple2-row-value"><?php if ($model->house_area) { ?> <?= $model->house_area ?> м<sup>2</sup><?php } else { ?>-<?php } ?></span>
									</div>


                                    <?php if ($model->number_of_floors) { ?>
									<div class="options-simple2-row">
										<span class="options-simple2-row-label">Этажей: </span>
										<span class="options-simple2-row-value"> <?= $model->number_of_floors ?></span>
									</div>
                                    <?php }  ?>

									<div class="options-simple2-row">
										<span class="options-simple2-row-label">Площадь участка: </span>
										<span class="options-simple2-row-value"><?php if ($model->stead_area) { ?> <?= $model->stead_area ?> сот.<?php } else { ?>-<?php } ?></span>
									</div>
									<div class="options-row-simple2">
										<span class="options-simple2-row-label">Цена: </span>
										<span class="options-simple2-row-value"><?= $model->priceFormated(); ?></span>
									</div>
								</div>
								<a href="#" class="btn btn-lg btn-green">Записаться на просмотр</a>
							</div>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	<?php } ?>

	<div id="village-description-price-bottom-fix"></div>
</div>

<div class="modal fade modal-view-request" id="modal-view-request" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
			<?= $this->render("_view_request_form", $view_request_form); ?>
        </div>
    </div>
</div>