<?php

use yii\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;

;

use yii\widgets\LinkPager;
use common\models\VillageReview;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<?= frontend\widgets\village_subpages_menu\VillageSubpagesMenuWidget::widget(['village' => $village, 'active' => 'photo']); ?>
<div class="h2-badge-container"><h1 class="like-h2"><?= $h1 ?></h1> <?php if ($village->premium) { ?><span class="badge badge-green">ПРЕМИУМ</span><?php } ?></div>

<?php if ($village->images) { ?>
	<div class="row village-images">
		<?php
		$i = 0;
		foreach ($village->images as $image) {
			?>
			<div class="col-xs-6 col-sm-4 village-images-item">
				<?= Html::a('<i class="hover"></i>' . Html::img($image->getUploadedFileUrl('image', 'middle')), $image->getUploadedFileUrl('image', 'large'), ['class' => 'fancybox', 'rel' => 'gallery']); ?>
			</div>
		<?php } ?>
	</div>
<?php } ?>
<div>
	<?= $village->description_photo ?>
</div>
<br><br>