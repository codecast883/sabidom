<?php

use common\owerride\yii\widgets\Breadcrumbs;;
use common\helpers\Html;
?>
<?= Breadcrumbs::widget($breadcrumbs); ?>
<h1 ><?= $h1 ?></h1>

<?php if ($villages) { ?>
    <div class="like-h2">Поселки</div>
    <div class="offer-large grid">
        <?php foreach (Html::explodeToRows($villages, 3) as $row) { ?>
            <div class="row">
                <?php foreach ($row as $model) { ?>
                    <div class="col-xs-12 col-sm-4">
                        <div class="offer-large-item">
                            <div class="offer-large-item-image block block-white padding shadow">
                                <?php if ($model->premium) { ?><div class="badge badge-green">ПРЕМИУМ</div><?php } ?>
                                <div class="images-count"><i class="fa fa-picture-o"></i> <?= count($model->images) ?></div>
                                <div class="carousel-3">
                                    <div class="slick items-1">
                                        <?php foreach ($model->images as $image) { ?>
                                            <div class="item">
                                                <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'middle'), ["alt" => $model->title]); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="offer-large-item-title">
                                <?= Html::a($model->title, $model->url); ?>
                            </div>

                            <div class="options-simple2">
                                <?php if ($fields = $model->getFields([9, 26])) { ?>
                                    <?php foreach ($fields as $field) { ?>
                                        <div class="options-simple2-row">
                                            <span class="options-simple2-label"><?= $field['info']->title ?>: </span>
                                            <span class="options-simpl2e-value"><?= $field['values_formated'] ?></span>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <div class="options-row-simple2">
                                    <span class="options-simple2-row-label">Цена: </span>
                                    <span class="options-simple2-row-value"><?= $model->priceFormated() ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>
<?php if ($realts) { ?>
    <div class="like-h2">Частные объявления</div>
    <div class="offer-large grid">
        <?php foreach (Html::explodeToRows($realts, 3) as $row) { ?>
            <div class="row">
                <?php foreach ($row as $model) { ?>
                    <div class="col-xs-12 col-sm-4">
                        <div class="offer-large-item">
                            <div class="offer-large-item-image block block-white padding shadow">
                                <?php if ($model->premium) { ?><div class="badge badge-green">ПРЕМИУМ</div><?php } ?>
                                <div class="images-count"><i class="fa fa-picture-o"></i> <?= count($model->images) ?></div>
                                <div class="carousel-3">
                                    <div class="slick items-1">
                                        <?php foreach ($model->images as $image) { ?>
                                            <div class="item">
                                                <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'middle'), ["alt" => $model->title]); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="offer-large-item-title">
                                <?= Html::a($model->title, $model->url); ?>
                            </div>

                            <div class="options-simple2">
                                <?php if ($fields = $model->getFields([9, 26])) { ?>
                                    <?php foreach ($fields as $field) { ?>
                                        <div class="options-simple2-row">
                                            <span class="options-simple2-label"><?= $field['info']->title ?>: </span>
                                            <span class="options-simpl2e-value"><?= $field['values_formated'] ?></span>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($model->house_area) { ?>
                                    <div class="options-simple2-row">
                                        <span class="options-simple2-label">Площадь объекта недвижимости: </span>
                                        <span class="options-simple2-value"><?= $model->house_area ?> м<sup>2</sup></span>
                                    </div>
                                <?php } ?>

                                <?php if ($model->stead_area) { ?>
                                    <div class="options-simple2-row">
                                        <span class="options-simple2-label">Площадь участка: </span>
                                        <span class="options-simpl2e-value"><?= $model->stead_area ?> сот.</span>
                                    </div>
                                <?php } ?>
                                <div class="options-row-simple2">
                                    <span class="options-simple2-row-label">Цена: </span>
                                    <span class="options-simple2-row-value"><?= $model->priceFormated() ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>

<?php if (!empty($description_bottom)) { ?>
    <div class="block">
        <?= $description_bottom; ?>
    </div>
<?php } ?>
<div class="block visible-md-block visible-lg-block">
    <?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "footer"]); ?>
</div>