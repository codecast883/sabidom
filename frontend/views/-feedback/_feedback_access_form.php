<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php \yii\widgets\Pjax::begin(['enablePushState' => false]); ?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">Я представитель компании</h4>
</div>
<div class="modal-body">
	<p>Если Вы являетесь представителем компании <?= $company->title ?>, оставьте нам свой email и мы предоставим Вам доступ для управления страницей компании. </p>

	<?php $form = ActiveForm::begin(['options' => ['data-pjax' => 1], 'validateOnChange' => false, 'validateOnBlur' => false, 'action' => ['/feedback/insert']]); ?>

	<?= Html::activeHiddenInput($model, 'type_id'); ?>
	<?= Html::activeHiddenInput($model, 'company_id'); ?>
	<?= $form->field($model, 'fio')->textInput(['placeholder' => "Ваше имя"]) ?>
	<?= $form->field($model, 'email')->textInput(['placeholder' => "Ваш email"]) ?>
	<?= $form->field($model, 'phone')->textInput(['placeholder' => "Ваш телефон"]) ?>
	<?= $form->field($model, 'message')->textarea(['rows' => 6, 'placeholder' => "Ваше сообщение"]) ?>
</div>

<div class="modal-footer">
	<?= Html::button('Отмена', ['class' => 'btn btn-default', "data-dismiss" => "modal"]) ?>
	<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php \yii\widgets\Pjax::end(); ?>