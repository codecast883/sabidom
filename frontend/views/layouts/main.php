<?php

use frontend\assets\AppAsset;
use common\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use kartik\widgets\AlertBlock;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="verify-admitad" content="625c852ab0" />
    <?= Html::csrfMetaTags() ?>
    <link rel="canonical" href="<?= Url::canonical() ?>" />
    <link rel="shortcut icon" href="/img/favicon.ico" >
    <title><?= $page_title ?></title>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php $this->head() ?>

</head>
    <body>
		<?php $this->beginBody() ?>
        <div id='page'>
			<?php if (!empty($admin_menu)) { ?>
				<?php
				echo yii\widgets\Menu::widget([
					'options' => ['id' => 'admin-menu'],
					'items' => $admin_menu
				]);
				?>
			<?php } ?>

            <div class="header-collapse-menu-line">
                <div class="container">
                    <div id="header-collapse-menu" aria-multiselectable="true" role="tablist" class="header-collapse-menu">
                        <div class="panel">
                            <div role="tabpanel" class="collapse header-dropdown-menu-block" id="collapse-menu-directions" aria-expanded="false">
                                <div class="panel-body">
                                    <div class="row">
										<?php foreach (Html::explodeToColumns($top_menu_highway, 4) as $col) { ?>
											<div class="col-xs-3">
												<ul class="list1-items">
													<?php foreach ($col as $item) { ?>
														<li><?= Html::a($item['label'], $item['url']) ?></li>
													<?php } ?>
												</ul>
											</div>
										<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div role="tabpanel" class="collapse header-dropdown-menu-block" id="collapse-menu-areas" aria-expanded="false">
                                <div class="panel-body">
                                    <div class="row">
										<?php foreach (Html::explodeToColumns($top_menu_area, 4) as $col) { ?>
											<div class="col-xs-3">
												<ul class="list1-items">
													<?php foreach ($col as $item) { ?>
														<li><?= Html::a($item['label'], $item['url']) ?></li>
													<?php } ?>
												</ul>
											</div>
										<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-first-line">
                <div class="container">
                    <div class="header-first-line-menu">
                        <div class="header-first-line-menu-item header-first-line-menu-item-logo pull-left">
							<?= Html::a(Html::img("/img/logo.png"), Yii::$app->homeUrl); ?>
                        </div>
						<div class="header-first-line-menu-adress">
							<div>+7 (926) 861-58-80</div>
							<div>+7 (495) 664-56-15</div>
							<div  class="header-first-line-menu-adress-info hidden-xs">Метро Тропарево<br> Ленинский проспект, д.137, к.1, 2 эт.</div>
						</div>
                        <div class="header-first-line-menu-item header-first-line-menu-item_btn1 hidden-xs">
                            <a class="header-first-line-menu-item-link" aria-controls="collapse-menu-directions" data-parent="#header-collapse-menu" data-target="#collapse-menu-directions" data-toggle="collapse" aria-expanded="false" role="button" href="#">Направления</a>
                        </div>
                        <div class="header-first-line-menu-item header-first-line-menu-item_btn2 hidden-xs">
                            <a class="header-first-line-menu-item-link" aria-controls="collapse-menu-areas" data-parent="#header-collapse-menu" data-target="#collapse-menu-areas" data-toggle="collapse" aria-expanded="false" role="button" href="#">Районы</a>
                        </div>
                        <div class="header-first-line-menu-item header-first-line-menu-item_search">
                            <div class="header-search"><form action="<?= Url::to(["search/index"]); ?>"><input name="q" type="text" autocomplete="off" placeholder="Поиск по сайту" class="header-search-input"> <button  class="header-search-button"><i class="fa fa-search"></i></button></form></div>
                        </div>
                        <div class="header-first-line-menu-item header-first-line-menu-item_add">
                            <a href="<?= Yii::$app->homeUrl ?>/novoe-obiavlenie" class='btn btn-lg btn-green'><i class='ico-plus'>+</i><span class="hidden-sm hidden-xs">Дать объявление</span></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="header-second-line visible-md-block visible-lg-block">
				<?php /* <div class="container">
				  <div class="header-slogan">Загородная недвижимость в 2016 году</div>
				  <div class="header-slogan-show-more"><a class = "btn btn-lg btn-green" href="#">Подробнее</a></div>
				  </div> */ ?>
            </div>

            <div class="header-third-line">
                <div class="container">
                    <button type="button" class="navbar-toggle collapsed col-md-block" data-toggle="collapse" data-target="#header_menu" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    <div class="collapse navbar-collapse" id="header_menu">
                        <div class="header-menu">
    						<?php foreach ($top_menu as $item) { ?>
    							<div class="<?= yii\helpers\ArrayHelper::getValue($item, 'container-class'); ?>">
    								<?= Html::a($item['label'], $item['url']) ?>
    								<?php if (!empty($item['items'])) { ?>
    									<div class="dropdown">
    										<ul>
    											<?php foreach ($item['items'] as $subitem) { ?>
    												<li><?= Html::a($subitem['label'], $subitem['url']) ?></li>
    											<?php } ?>
    										</ul>
    									</div>
    								<?php } ?>
    							</div>
    						<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
				<?= $content; ?>
                <div class="footer-buffer">
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="footer-first-line">
                    <div class="footer-phones visible-xs-block">
                        <a href="tel:+79268615880">+7 (926) 861-58-80</a>
                        <a href="tel:+74956645615">+7 (495) 664-56-15</a>
                    </div>
                    <div class="footer-menu">
                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                <ul>
									<?php foreach ($top_menu as $item) { ?>
										<?php if ($item['url']) { ?>
											<li>
												<?= Html::a($item['label'], $item['url']) ?>
											</li>
										<?php } ?>
									<?php } ?>
                                </ul>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                <ul>
                                    <li><?= Html::a("Вопросы", Url::to(['information/view', 'id' => 7], TRUE)) ?></li>
                                    <li><?= Html::a("Новости", Url::to(['article/news'], TRUE)) ?></li>
                                    <li><?= Html::a("Статьи", Url::to(['article/articles'], TRUE)) ?></li>
                                    <li><?= Html::a("Акции", Url::to(['action/index'], TRUE)) ?></li>
                                    <li><?= Html::a("О компании", Url::to(['information/view', 'id' => 5], TRUE)) ?></li>
                                    <li><?= Html::a("Контакты", Url::to(['information/view', 'id' => 6], TRUE)) ?></li>
                                    <li><?= Html::a("Карта сайта", Url::to(['sitemap/index'], TRUE)) ?></li>
                                    <li><?= Html::a("Реклама на сайте", Url::to(['feedback/commercial'], TRUE)) ?></li>
                                </ul>
                            </div>
                            <div class="col-xs-3 hidden-md">
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-3 text-right">
								<?= Html::a(Html::img("/img/logo2.png"), Yii::$app->homeUrl); ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer-social">
                        <a href='#' class="social-ico social-ico-vk"></a>
                        <a href='#' class="social-ico social-ico-fb"></a>
                        <a href='#' class="social-ico social-ico-gp"></a>
                        <a href='#' class="social-ico social-ico-tw"></a>
                        <a href='#' class="social-ico social-ico-ok"></a>
                    </div>
                </div>
                <div class="footer-second-line">
                    <div class="container">
                        <p style='text-align: center'>
							<?= common\models\Settings::getByAlias('footer') ?>
                            ©  Загородная недвижимость <?= date("Y")?>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
		<?= common\models\Settings::getByAlias('counters') ?>

		<?= $seo_schemas ?>

		<?php $this->endBody() ?>
		<!-- calltouch code -->
		<script type="text/javascript">
		(function (w, d, nv, ls, yac){
			var lwait = function (w, on, trf, dly, ma, orf, osf) { var pfx = "ct_await_", sfx = "_completed";  if(!w[pfx + on + sfx]) { var ci = clearInterval, si = setInterval, st = setTimeout , cmld = function () { if (!w[pfx + on + sfx]) {  w[pfx + on + sfx] = true; if ((w[pfx + on] && (w[pfx + on].timer))) { ci(w[pfx + on].timer);  w[pfx + on] = null;   }  orf(w[on]);  } };if (!w[on] || !osf) { if (trf(w[on])) { cmld();  } else { if (!w[pfx + on]) { w[pfx + on] = {  timer: si(function () { if (trf(w[on]) || ma < ++w[pfx + on].attempt) { cmld(); } }, dly), attempt: 0 }; } } }   else { if (trf(w[on])) { cmld();  } else { osf(cmld); st(function () { lwait(w, on, trf, dly, ma, orf); }, 0); } }}};
			var ct = function (w, d, e, c, n){ var a = 'all', b = 'tou', src = b + 'c' + 'h';  src = 'm' + 'o' + 'd.c' + a + src;  var jsHost = "https://" + src, p = d.getElementsByTagName(e)[0], s = d.createElement(e); var jsf = function (w, d, p, s, h, c, n, yc) { if (yc !== null) { lwait(w, 'yaCounter'+yc, function(obj) { return (obj && obj.getClientID ? true : false); }, 50, 100, function(yaCounter) { s.async = 1; s.src = jsHost + "." + "r" + "u/d_client.js?param;" + (yaCounter  && yaCounter.getClientID ? "ya_client_id" + yaCounter.getClientID() + ";" : "") + (c ? "client_id" + c + ";" : "") + "ref" + escape(d.referrer) + ";url" + escape(d.URL) + ";cook" + escape(d.cookie) + ";attrs" + escape("{\"attrh\":" + n + ",\"ver\":170523}") + ";";p.parentNode.insertBefore(s, p); }, function (f) { if(w.jQuery) {  w.jQuery(d).on('yacounter' + yc + 'inited', f ); }}); } else { s.async = 1; s.src = jsHost + "." + "r" + "u/d_client.js?param;" + (c ? "client_id" + c + ";" : "") + "ref" + escape(d.referrer) + ";url" + escape(d.URL) + ";cook" + escape(d.cookie) + ";attrs" + escape("{\"attrh\":" + n + ",\"ver\":170523}") + ";"; p.parentNode.insertBefore(s, p);}}; if (!w.jQuery) { var jq = d.createElement(e); jq.src = jsHost + "." + "r" + 'u/js/jquery-1.7.min.js'; jq.onload = function () { lwait(w, 'jQuery', function(obj) { return (obj ? true : false); }, 30, 100, function () { jsf(w, d, d.getElementsByTagName(e)[0], s, jsHost, c, n, yac);  }); }; p.parentNode.insertBefore(jq, p);  } else { jsf(w, d,  p, s, jsHost, c, n, yac); }};
			var gaid = function (w, d, o, ct, n) { if (!!o) { lwait(w, o, function (obj) {  return (obj && obj.getAll ? true : false); }, 200, (nv.userAgent.match(/Opera|OPR\//) ? 10 : 20), function (gaCounter) { var clId = null; try {  var cnt = gaCounter && gaCounter.getAll ? gaCounter.getAll() : null; clId = cnt && cnt.length > 0 && !!cnt[0] && cnt[0].get ? cnt[0].get('clientId') : null; } catch (e) { console.warn("Unable to get clientId, Error: " + e.message); } ct(w, d, 'script', clId, n); }, function (f) { w[o](function () {  f(w[o]); })});} else{ ct(w, d, 'script', null, n); }};
			var cid  = function () { try { var m1 = d.cookie.match('(?:^|;)\\s*_ga=([^;]*)');if (!(m1 && m1.length > 1)) return null; var m2 = decodeURIComponent(m1[1]).match(/(\d+\.\d+)$/); if (!(m2 && m2.length > 1)) return null; return m2[1]} catch (err) {}}();
			if (cid === null && !!w.GoogleAnalyticsObject){
				if (w.GoogleAnalyticsObject=='ga_ckpr') w.ct_ga='ga'; else w.ct_ga = w.GoogleAnalyticsObject;
				if (typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1){new Promise(function (resolve) {var db, on = function () {  resolve(true)  }, off = function () {  resolve(false)}, tryls = function tryls() { try { ls && ls.length ? off() : (ls.x = 1, ls.removeItem("x"), off());} catch (e) { nv.cookieEnabled ? on() : off(); }};w.webkitRequestFileSystem ? webkitRequestFileSystem(0, 0, off, on) : "MozAppearance" in d.documentElement.style ? (db = indexedDB.open("test"), db.onerror = on, db.onsuccess = off) : /constructor/i.test(w.HTMLElement) ? tryls() : !w.indexedDB && (w.PointerEvent || w.MSPointerEvent) ? on() : off();}).then(function (pm){
					if (pm){gaid(w, d, w.ct_ga, ct, 2);}else{gaid(w, d, w.ct_ga, ct, 3);}})}else{gaid(w, d, w.ct_ga, ct, 4);}
			}else{ct(w, d, 'script', cid, 1);}})
		(window, document, navigator, localStorage, "34788730");
		</script>
		<!-- /calltouch code -->

    </body>
</html>
<?php $this->endPage() ?>
