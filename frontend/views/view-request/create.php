<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ViewRequest */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'View Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="view-request-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
