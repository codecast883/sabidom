<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ViewRequest */

$this->title = 'Изменить: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'View Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="view-request-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
