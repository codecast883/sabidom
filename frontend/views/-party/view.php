<?php

use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use common\models\Person;
?>
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?= Breadcrumbs::widget($breadcrumbs); ?>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-9">
            <div class="row">
                <div class="col-xs-4">
                    <div class="person-image">
                        <?= Html::img($party->getUploadedFileUrl('image', 'middle'), ['class' => '']) ?> 
                    </div>
                </div>
                <div class="col-xs-8">
                    <h1 class="person-title"><?= $party->title ?></h1>
                    <div class="">
                        <?= $party->description_short ?>
                    </div>
                </div>
            </div>
            <div class="block1">
                <div class="block1-title"><span>История</span></div>
                <div class="block1-body">
                    <div class="text-block">
                        <?= $party->description_short ?>
                        <?= $party->description ?>
                    </div>
                </div>
            </div>
            <div class="block-share">
                <div class="block-share-title">Поделиться:</div>
                <div class="block-share-buttons"><script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,gplus,moimir" data-yashareTheme="counter"></div></div>
            </div>
            <div class="block1">
                <div class="block1-title"><span>Комментарии</span></div>
                <div class="block1-body">
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="block1">
                <div class="block1-title"><span>Фото</span></div>
                <div class="block1-body">
                    <?php
                    foreach ($party->images as $image) {
                        ?>

                        <?= Html::img($image->getUploadedFileUrl('image')) ?> 
                        <?= Html::hiddenInput("images_list[]", $image->id); ?>

                    <?php } ?>
                </div>
                <div class="block1-view-more">
                    <a href="#">Все фото <i class="zmdi zmdi-long-arrow-right"></i></a>
                </div>
            </div>
            <div class="block1">
                <div class="block1-title"><span>Новости</span></div>
                <div class="block1-body">
                </div>
                <div class="block1-view-more">
                    <a href="#">Все новости <i class="zmdi zmdi-long-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>