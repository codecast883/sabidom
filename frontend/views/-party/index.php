<?php

use yii\helpers\Html;
use \yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
?>

<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?= Breadcrumbs::widget($breadcrumbs); ?>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-9">
            <h1 class="category-title"><?= $seo_h1 ?></h1>
            <div class='company-list'>
                <?php
                foreach ($partys as $party) {
                    ?>
                    <div class='company-list-item'>
                        <div class="row">
                            <div class="col-xs-2 company-list-item-left-col">
                                <div class='company-list-item-image'>
                                    <a href="<?= $party->url; ?>">
                                        <?php if ($party['image']) { ?>
                                            <?= yii\helpers\Html::img($party->getUploadedFileUrl('image', 'small'), ["alt" => $party->title]); ?>
                                        <?php } else { ?>
                                            <img src='/img/pixel.png'>
                                        <?php } ?>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-10">
                                <div class='company-list-item-title'><a href="<?= $party->url; ?>"><?= $party->title; ?></a></div>
                                <div class='company-list-item-description'><?= $party->description_short; ?></div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="text-center">
                <?php
                echo LinkPager::widget([
                    'pagination' => $paginaton,
                    'prevPageLabel' => "<span class='pager-nav-ico'></span> Предыдущая",
                    'nextPageLabel' => "Следующая <span class='pager-nav-ico'></span>",
                        //'pageSize' => 7
                ]);
                ?>
            </div>
        </div>
        <div class="col-xs-3">
        </div>

    </div>
</div>