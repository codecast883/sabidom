<?php

use common\helpers\Html;
use \yii\widgets\LinkPager;
use common\owerride\yii\widgets\Breadcrumbs;;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<h1 class="like-h2"><?= $h1 ?></h1>



<div class="actions-list">    
    <?php foreach (Html::explodeToRows($models, 4) as $row) { ?>
        <div class="row">
            <?php foreach ($row as $model) { ?>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="block block-white  shadow">
                        <div class="actions-list-item">
                            <div class="actions-list-item-image">
                                <?= Html::img($model->getUploadedFileUrl('image', 'small')) ?> 
                            </div>

                            <div class="actions-list-item-title">
                                <?= Html::a($model->title, $model->url) ?>
                            </div>
                            <div class="actions-list-item-preview">
                                <?= $model->description_short ? $model->description_short : \common\helpers\YText::wordLimiter(strip_tags($model->description), 50) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>