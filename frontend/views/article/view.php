<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use \yii\widgets\LinkPager;
use common\owerride\yii\widgets\Breadcrumbs;;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<h1 class=""><?= $h1 ?></h1>



<div class="block block-white padding shadow">
    <?= $model->description ?>
</div>

<div class="block article-prevnext">
    <div class="row ">
        <div class="col-xs-6 article-prevnext-col">
            <?php if ($model->prev) { ?>
                <table>
                    <tr>
                        <td class="article-prevnext-col-img">
                            <?= Html::img($model->prev->getUploadedFileUrl('image', 'small')) ?> 
                        </td>
                        <td>
                            <div class="article-prevnext-col-label">
                                Предыдущая новость
                            </div>
                            <div class="article-prevnext-col-title">
                                <?= Html::a($model->prev->title, $model->prev->url) ?>
                            </div>
                        </td>
                    </tr>
                </table>
            <?php } ?>
        </div>
        <div class="col-xs-6 article-prevnext-col">
            <?php if ($model->next) { ?>
                <table>
                    <tr>
                        <td class="article-prevnext-col-img">
                            <?= Html::img($model->next->getUploadedFileUrl('image', 'small')) ?> 
                        </td>
                        <td>
                            <div class="article-prevnext-col-label">
                                Следующая новость
                            </div>
                            <div class="article-prevnext-col-title">
                                <?= Html::a($model->next->title, $model->next->url) ?>
                            </div>
                        </td>
                    </tr>
                </table>
            <?php } ?>
        </div>
    </div>
</div>