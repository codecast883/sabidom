<?php

use yii\helpers\Html;
use \yii\widgets\LinkPager;
use common\owerride\yii\widgets\Breadcrumbs;;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<h1 class="like-h2"><?= $h1 ?></h1>


<div class="row">
    <div class="col-xs-12 col-sm-9">
        <div class="news-list">
            <?php foreach ($models as $model) { ?>
                <div class="block block-white padding shadow">
                    <div class="news-list-item">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="news-list-item-image">
                                    <?= Html::img($model->getUploadedFileUrl('image', 'small')) ?> 
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="news-list-item-title">
                                    <?= Html::a($model->title, $model->url) ?><br>
                                </div>
                                <div class="news-list-item-preview">
                                    <?= $model->description_short ? $model->description_short : \common\helpers\YText::wordLimiter(strip_tags($model->description), 50) ?>
                                </div>
                                <div class="news-list-item-date"><i class="zmdi zmdi-calendar"></i> <?= date("d.m.Y", strtotime($model->created_at)) ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3">
        <div class="calendar">
            <ul class="navigation">
                <?php foreach ($calendar_menu as $menu_item) { ?>
                    <?php if (empty($menu_item['active'])) { ?>
                        <li><?= Html::a($menu_item['label'], $menu_item['href']) ?></li>
                    <?php } else { ?>
                        <li class="active">
                            <b><?= $menu_item['label'] ?></b>
                            <?php if ($menu_item['items']) { ?>
                                <ul class="navigation">
                                    <?php foreach ($menu_item['items'] as $subitem) { ?>
                                        <?php if (empty($subitem['active'])) { ?>
                                            <li><?= Html::a($subitem['label'], $subitem['href']) ?></li>
                                        <?php } else { ?>
                                            <li class="active"><b><?= $subitem['label'] ?></b></li>
                                                <?php } ?>
                                            <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>