<?php

use yii\helpers\Html;
?>
<div class='container'>
	<div class='row'>
		<div class='col-xs-12'>
			<h1><?= Yii::$app->user->identity->username ?>: список компаний</h1>
		</div>
	</div>
</div>
<div class='container'>
	<div class='row'>
		<div class='col-xs-2'>
			<?= $this->render("_sidebar", ['menu' => $menu]); ?>
		</div>
		<div class='col-xs-10'>
			<table class="table table-condensed table-striped">
				<colgroup>
					<col class="">
					<col class="col-xs-2">
					<col class="col-xs-2">
				</colgroup>
				<thead>
					<tr>
						<th>Название</th>
						<th>Отзывов</th>
						<th>Отзывов сотрудников</th>
					</tr>
					<?php foreach ($companies as $company) { ?>
						<tr>
							<td><?= Html::a($company->title, $company->getUrl()) ?></td>
							<td><?= count($company->reviews); ?></td>
							<td><?= count($company->workerReviews); ?></td>
						</tr>
					<?php } ?>
			</table>
		</div>
	</div>
</div>