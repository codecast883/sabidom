<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;
use common\models\Company;
?>
<div class='container'>
	<div class='row'>
		<div class='col-xs-12'>
			<h1><?= Yii::$app->user->identity->username ?>: профиль</h1>
		</div>
	</div>
</div>
<div class='container'>
	<div class='row'>
		<div class='col-xs-2'>
			<?= $this->render("_sidebar", ['menu' => $menu]); ?>
		</div>
		<div class='col-xs-10'>
			<?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data'], 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_TINY]]); ?>
			<div class="form-group field-user-username">
				<label for="user-username" class="col-xs-3 control-label">Username</label>
				<div class="col-xs-9"><p class="form-control-static"><?= $user_model->username ?></p></div>
			</div>
			<div class="form-group field-user-username">
				<label for="user-username" class="col-xs-3 control-label">Премиум аккаунт до</label>
				<div class="col-xs-9"><p class="form-control-static"><?= $user_model->paid_expired ?></p></div>
			</div>
			<?= $form->field($user_model, 'email')->textInput(['maxlength' => 1024]) ?>
			
			<?php /* = $form->field($user_model, 'password_new')->textInput(['maxlength' => 1024]) ?>
			  <?= $form->field($user_model, 'password_new_repeat')->textInput(['maxlength' => 1024]) ?>
			  <?= $form->field($user_model, 'password_old')->textInput(['maxlength' => 1024]) */ ?>

			<div class="form-group">
				<div class="col-xs-9 col-xs-offset-3">
					<?= Html::submitButton('Сохранить', ['class' => 'btn btn-default', 'name' => 'submit-type']) ?>
				</div>
			</div>

			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>