<div class='container'>
	<div class='row'>
		<div class='col-xs-12'>
			<h1><?= Yii::$app->user->identity->username ?></h1>
		</div>
	</div>
</div>
<div class='container'>
	<div class='row'>
		<div class='col-xs-2'>
			<?= $this->render("_sidebar", ['menu' => $menu]); ?>
		</div>
	</div>
</div>