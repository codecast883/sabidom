<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use \yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
use yii\helpers\StringHelper;
use yii\i18n\Formatter;
?>
<div class="container">
    <div class="row">
        <div class="col-xs-8">
            <div class="block-1">
                <div class="block-1-title">
                    <?= $seo_h1 ?>
                </div>
                <div class="block-1-body">
                    <div class='company-list'>
                        <?php
                        foreach ($companies as $company) {
                            ?>
                            <a href="<?= $company->url; ?>" class='company-list-item'>
                                <div class="row">
                                    <div class="col-xs-2 company-list-item-left-col">
                                        <table class="">
                                            <tr>
                                                <td colspan="2" class='company-list-item-image'>
                                                    <?php if ($company['image']) { ?>
                                                        <?= yii\helpers\Html::img($company->getUploadedFileUrl('image', 'middle'), ["alt" => $company->title]); ?>
                                                    <?php } else { ?>
                                                        <img src='/img/pixel.png'>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <tr class="company-list-item-rating">
                                                <td class="company-list-item-neg"><div class="company-vote" data-company_id = "<?= $company->id ?>" data-rating="-1"><i class="fa fa-minus-circle"></i><div class="company-vote-count con"><?= $company->rating_con ?></div></div></td>
                                                <td class="company-list-item-pos"><div class="company-vote" data-company_id = "<?= $company->id ?>" data-rating="1"><i class="fa fa-plus-circle"></i><div class="company-vote-count pos"><?= $company->rating_pos ?></div></div></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-xs-10">
                                        <div class='company-list-item-title'><?= $company->title; ?></div>
                                        <div class='company-list-item-description'><?= $company->description_short; ?></div>
                                    </div>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="pagination-block text-center">
                        <?php
                        echo LinkPager::widget([
                            'pagination' => $paginaton,
                            'prevPageLabel' => "<i class='fa fa-caret-left'></i>",
                            'nextPageLabel' => "<i class='fa fa-caret-right'></i>",
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>