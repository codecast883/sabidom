<a class="news-item" href="<?= $model->url ?>">
    <div class="news-item-image">
        <?php if ($model['image']) { ?>
            <?= yii\helpers\Html::img($model->getUploadedFileUrl('image', 'small'), ["alt" => $model->title]); ?>
        <?php } else { ?>
            <img src='/img/pixel.png'>
        <?php } ?>
    </div>
    <div class="news-item-date"><i class="fa fa-calendar"></i> <?= $model->date_add ?></div>
    <div class="news-item-title"><?= $model->title ?></div>
</a>
<div class="news-delimiter"></div>