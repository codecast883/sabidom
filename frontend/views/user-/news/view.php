<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use \yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
?>
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?= Breadcrumbs::widget($breadcrumbs); ?>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-9">
            <div class="block-1">
                <div class="category-title"> <?= $model->generateH1() ?></div>
                <div class="block-1-body">
                    <div class="news-text">
                        <?= $model->description ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="block1">
                <div class="block1-title"><span>Связанные политики</span></div>
                <div class="block1-body">
                    <div class="column-persons-block">
                        <div class="column-persons-block-list">
                            <?php foreach ($model->getPersons() as $person) { ?>
                                <div class="column-persons-block-list-item">
                                    <a class="column-persons-block-list-item-link" href="<?= $person->url; ?>">
                                        <div class="column-persons-block-list-item-image">
                                            <?php if ($person['image']) { ?>
                                                <?= yii\helpers\Html::img($person->getUploadedFileUrl('image', 'middle2'), ["alt" => $person->title]); ?>
                                            <?php } else { ?>
                                                <img src='/img/pixel.png'>
                                            <?php } ?>
                                        </div>
                                        <div class="column-persons-block-list-item-name">
                                            <?= $person->title ?>
                                        </div>
                                    </a>                       
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>