<?php

use yii\helpers\Html;
use \yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
?>

<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?= Breadcrumbs::widget($breadcrumbs); ?>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-9">
            <h1 class="category-title"><?= $seo_h1 ?></h1>
            <div class="news-list">
                <?php foreach ($news as $new) { ?>
                    <div class="news-list-item">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="news-list-item-image">
                                    <?= Html::img($new->getUploadedFileUrl('image', 'middle')) ?> 

                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="news-list-item-date"><i class="zmdi zmdi-calendar"></i> <?= date("d.m.Y H:i", strtotime($new->date_add)) ?></div>
                                <div class="news-list-item-title">
                                    <a href="<?= $new->url ?>">
                                        <?= $new->title ?>
                                    </a>
                                </div>
                                <div class="news-list-item-preview">
                                    <?= \common\helpers\YText::wordLimiter(strip_tags($new->description), 70) ?>
                                </div>
                                <div class="news-list-item-readmore">
                                    <?= Html::a("Читать полностью", $new->url) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="text-center">
                <?php
                echo LinkPager::widget([
                    'pagination' => $paginaton,
                    'prevPageLabel' => "<span class='pager-nav-ico'></span> Предыдущая",
                    'nextPageLabel' => "Следующая <span class='pager-nav-ico'></span>",
                        //'pageSize' => 7
                ]);
                ?>
            </div>
        </div>
        <div class="col-xs-3">
            <?= frontend\widgets\block_persons\BlockPersonsWidget::widget(["on_top" => \common\models\Person::ON_TOP_YES, 'template' => 'column']); ?>
        </div>
    </div>
</div>