<?php
$rows = [];

$i = 0;
$row_num = 0;
foreach ($models as $model) {
    if ($i++ % 2 == 0) {
        $row_num++;
    }

    $rows[$row_num][] = "<td>" . $this->render('_list_with_image_item', ['model' => $model]) . "</td>";
}
?>


<table class='news'>
    <?php foreach ($rows as $row) { ?>
        <tr>
            <?= implode("", $row); ?>
        </tr>
    <?php } ?>
</table>