<?php

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\widgets\AlertBlock;
use yii\helpers\Url;

/* @var $content string */

AppAsset::register($this);
if (isset($settings['seo_keywords'])) {
    $this->registerMetaTag(['name' => 'keywords', 'content' => $settings['seo_keywords']]);
}
if (isset($settings['seo_description'])) {
    $this->registerMetaTag(['name' => 'description', 'content' => $settings['seo_description']]);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <?= Html::csrfMetaTags() ?>
        <?php /*<link rel="canonical" href="<?= Url::canonical() ?>" /> */ ?>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <title><?= $pageTitle ?></title>
        <?php $this->head() ?>
    </head>
    <body id="top">
        <!--[if lt IE 7]>
                <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <?php $this->beginBody() ?>
        <?php if (!empty($admin_menu)) { ?>
            <?php
            echo yii\widgets\Menu::widget([
                'options' => ['id' => 'admin-menu'],
                'items' => $admin_menu
            ]);
            ?>
        <?php } ?>
        <!--==============================header=================================-->
        <div class="page-wrapper">
            <header>
                <div class="header-first-line">
                    <div class='container'>
                        <div class='row'>
                            <div class="col-xs-6">
                                <a class="logo" href="<?= Yii::$app->homeUrl ?>">
                                    <?= Html::img("/img/logo.png") ?>
                                    <div class="logo-slogan">ПОЛИТИКИ <span>РОССИИ</span></div>
                                </a>
                            </div>
                            <div class="col-xs-6">
                                <div class="search header-search">
                                    <form action="<?= Url::to(["/search/index"]); ?>"><input name="q" type="text" autocomplete="off" placeholder="Поиск по сайту" class="search-input"> <button  class="search-button">ИСКАТЬ <i class="zmdi zmdi-search"></i></button></form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-second-line">
                    <div class="container">
                        <table class="header-menu">
                            <tr>
                                <?php foreach ($main_menu as $item) { ?>
                                    <td>
                                        <?= Html::a($item['label'], $item['url']) ?>
                                    </td>
                                <?php } ?>
                            </tr>
                        </table>
                    </div>
                </div>
            </header>
            <?php echo $content; ?>
        </div>
        <div class="page-footer">
            <footer>
                <div class="footer-first-line">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                <a class="logo" href="<?= Yii::$app->homeUrl ?>">
                                    <?= Html::img("/img/logo2.png") ?>
                                    <div class="logo-slogan">ПОЛИТИКИ <span>РОССИИ</span></div>
                                </a>
                            </div>
                            <div class="col-xs-6">
                                <div class="search footer-search">
                                    <form action="<?= Url::to(["/search/index"]); ?>"><input name="q" type="text" autocomplete="off" placeholder="Поиск по сайту" class="search-input"> <button  class="search-button">ИСКАТЬ <i class="zmdi zmdi-search"></i></button></form>                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-second-line">
                    <div class="container">
                        <div class='row'>
                            <div class="col-xs-12">
                                <div class="footer-copy">
                                    <?= common\models\Settings::getByAlias('footer_copy') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>  
        <?= common\models\Settings::getByAlias('counters') ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>