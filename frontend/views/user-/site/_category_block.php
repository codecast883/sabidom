<div class="category-block">
    <div class="category-block-title"><a href="<?= $category->url ?>" title="Читать отзывы в разделе <?= $category->title ?>"><?= $category->title ?></a></div>
    <div class="category-block-subcategories">
        <div class="row">
            <?php foreach ($category->childrens as $child) { ?>
                <div class="col-xs-4">
                    - <a href="<?= $child->url ?>" title="<?= $child->title ?>"><?= $child->title ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>