<?php

use common\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;
use common\models\Field;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>

<table class="table">
	<tr>
		<th>Название</th>
		<th>Тип</th>
		<th>Возможные значения</th>
	</tr>
	<?php foreach ($fields as $field) { ?>
		<tr>
			<td><?= $field->title ?></td>
			<td>
				<?php if ($field->type_id == Field::TYPE_TEXT) { ?>
					Текст
				<?php } elseif ($field->type_id == Field::TYPE_LIST) { ?>
					Список
				<?php } ?>
			</td>
			<td>
				<?php if ($field->type_id == Field::TYPE_LIST) { ?>
					<?= implode("; ", \yii\helpers\ArrayHelper::getColumn($field->getFieldTags()->orderBy(['value' => SORT_ASC])->all(), 'value')); ?>
				<?php } ?>
			</td>
		</tr>
	<?php } ?>

</table>