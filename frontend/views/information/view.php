<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use common\owerride\yii\widgets\Breadcrumbs;

?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<h1><?= $h1 ?></h1>
<?= $model->text ?>

<script type="text/javascript" charset="utf-8" async
        src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Af655b2efd77a539807dd0c277eab92c81a84d466d835a3a37804641c479ffc72&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
<br><br>

<?php


?>

<?php
\yii\widgets\Pjax::begin([
    'enablePushState' => false,
    'formSelector' => "#view-request-form", 'options' => ['class' => 'ajax-fog']
]);
?>

<?php if ($success) { ?>
    <div class="alert alert-success" role="alert"><?= $message ?></div>
<?php } ?>
<?php if ($error) { ?>
    <div class="alert alert-warning" role="alert"><?= $message ?></div>
<?php } ?>

<?php
$form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
        'id' => "view-request-form",
        'validateOnChange' => true,
        'validateOnBlur' => true,
        'enableClientValidation' => true,
        'type' => ActiveForm::TYPE_VERTICAL,
        'formConfig' => [
            'deviceSize' => ActiveForm::SIZE_TINY
        ]]
);
?>


    <div class="row">
        <div class="col-xs-12">
            <div class='form-group'>
                <?= $form->field($model, 'name')->textInput(['maxlength' => 128, 'placeholder' => "Ваше имя"])->label("") ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class='form-group'>
                <?= $form->field($model, 'phone')->textInput(['maxlength' => 128, 'placeholder' => "Ваш телефон", 'class' => 'masked-phone form-control'])->label("") ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class='form-group'>
                <?= $form->field($model, 'text')->textarea(['rows' => 3, 'placeholder' => "Введите Ваше сообщение..."])->label("") ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="privacy-text">Отправляя данную форму, Вы подтверждаете свое согласие на обработку персональных данных, в соответствии с Федеральным законом №152-ФЗ. Обработка персональных данных осуществляется в соответствии с <a href="http://sabidom.ru/privacy">Политикой конфиденциальности</a></p>
            <div class='form-group'>
                <button type="submit" class='btn btn-green btn-sm'>Отправить</button>
            </div>
        </div>
    </div>


<?php /*
  $form->field($model, 'captcha')->widget(Captcha::className(), [ 'options' => [
  'class' => 'form-control',
  ], 'template' => "{image} {input}"])->label("Введите код") */
?>
    <script>
        if (!isMobile.any()) {
            $(".masked-phone").mask("+7(ddd)ddd-dd-dd");
        }
    </script>
<?php ActiveForm::end(); ?>
<?php \yii\widgets\Pjax::end(); ?>

    <div class="fog"
         style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: url(&quot;/img/loading.gif&quot;) center center no-repeat rgba(0,0,0,0.05); background-size: 100px 100px;"></div>
</div>



