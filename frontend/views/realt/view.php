<?php

use common\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;

?>

<? //= Breadcrumbs::widget($breadcrumbs); ?>

<?php $fieldTable = [];

$types = [
  1 => 'Поселок',
  2 => 'Коттедж',
  3 => 'Земля',
  4 => 'Таунхаус',
];

$type_id = $realt['type_id'];

if ($fields = $realt->getFields(NULL, 1)) {
    foreach ($fields as $field) {

            switch ($field['info']->title) {
                case 'Район':
                    $fieldTable['район'] = $field['values'][0]->value;
                    break;
                case 'Шоссе':
                    $fieldTable['шоссе'] = $field['values'][0]->value;
                    break;
                case 'Городской округ':
                    $fieldTable['городской округ'] = $field['values'][0]->value;
                    break;
                default:  $fieldTable['default'] = 'в Подмосковье';
            }

    }

    $title = $types[$type_id];
    $house_area = '';
    if($realt['house_area'] != 0){
        $house_area = ' ' . $realt['house_area']. 'м2';
    }
    foreach ($fieldTable as $key => $value){
        if ($key == 'район'){
            $title .= ' в районе ' . $value . ', ' . $realt['title'] . $house_area;
            break;
        }elseif ($key == 'шоссе'){
            $title .= ' на ' . $value . ' шоссе'. ', ' . $realt['title'] . $house_area;
            break;
        }elseif ($key == 'городской округ'){
            $title .= ', городской округ ' . $value. ', ' . $realt['title'] . $house_area;
            break;
        }else{
            $title .= ' в Подмосковье, '.  $realt['title'] . $house_area;
            break;
        }
    }



}
?>


<!--Begin custom breadcrumbs-->
<ul class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">

    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a href="/" itemprop="name"><?= $breadcrumbs['homeLink']['label'] ?></a>
    </li>


    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
        class="drpdwn">
        <a href="<?= $breadcrumbs['links'][0]['url'] ?>" itemprop="item"><span
                    itemprop="name"><?= $breadcrumbs['links'][0]['label'] ?></a>

        <ul class="subcategory">

            <?php foreach ($top_menu as $key => $value): ?>
                <?php if ($value['label'] == 'Еще') continue; ?>
                <li><a href="<?= $value['url'] ?>"><?= $value['label'] ?></a></li>
            <?php endforeach ?>

            <?php foreach ($top_menu[7]['items'] as $values): ?>
                <li><a href="<?= $values['url'] ?>"><?= $values['label'] ?></a></li>
            <?php endforeach ?>

        </ul>
    </li>


    <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="active">
        <span itemprop="name"><?= $breadcrumbs['links'][1]['label'] ?></span>
    </li>


</ul>
<!--End custom breadcrumbs-->


<div class="h2-badge-container"><h1 class="like-h2"><?= $realt['seo_h1']?$realt['seo_h1']:$title?>.</h1><?php if ($realt->premium) { ?> <span
            class="badge badge-green">ПРЕМИУМ</span><?php } ?></div>
<div class="row realt-info">
    <div class="col-xs-8">
        <?php if ($realt->images) { ?>
            <script>
                $(function () {
                    $('.realt-slides').slides({
                        preload: true,
                        preloadImage: '/vendor/slides/img/loading.gif',
                        effect: 'slide, fade',
                        crossfade: true,
                        slideSpeed: 200,
                        fadeSpeed: 500,
                        generateNextPrev: false,
                        generatePagination: false
                    });
                });
            </script>

            <div class="realt-slides">
                <div class="slides_container">
                    <?php foreach ($realt->images as $image) { ?>
                        <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'large'), ["alt" => $realt['seo_h1']?$realt['seo_h1']:$title ]); ?>
                    <?php } ?>
                </div>
                <ul class="pagination">
                    <?php foreach ($realt->images as $image) { ?>

                        <li>
                            <a href="#"><?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'small'), ["alt" => $realt->title]); ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
        <div class="block block-white padding shadow">
            <?php if (trim(strip_tags($realt->description_bottom)) OR trim(strip_tags($realt->description))) { ?>
                <div class="like-h3">Описание</div>
                <?php if ($realt->description) { ?>
                    <div class="realt-info-description">
                        <?= $realt->description ?>
                    </div>
                <?php } ?>
                <?php if ($realt->description_bottom) { ?>
                    <div class="realt-info-description">
                        <?= $realt->description_bottom ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php if ($fields = $realt->getFields(NULL, 2)) { ?>
            <div class="block block-white padding shadow">
                <div class="options">
                    <div class="options-title">Коммуникации</div>
                    <table class="options-table">
                        <colgroup>
                            <col style='width: 240px;'>
                        </colgroup>
                        <?php if ($realt->price_service) { ?>
                        <tr class="options-row">
                            <td class="options-row-label">
                                <div><span>Стоимость обслуживания</span></div>
                            </td>
                            <td class="options-row-value"><?= $realt->price_service; ?></td>
                        </tr>
                        <?php } ?>
                        <?php foreach ($fields as $field) { ?>
                            <tr class="options-row">
                                <td class="options-row-label">
                                    <div><span><?= $field['info']->title ?></span></div>
                                </td>
                                <td class="options-row-value"><?= $field['values_formated'] ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col-xs-4">
        <div class="block block-white padding">
            <div data-toggle="modal" data-realt-id="<?= $model->id ?>"
                 data-target="#realt-view-request-modal" class="realt-info-signup-for-view">
                <a href ='<?=$model->url . '?order=show'?>'><span>Записаться на просмотр</span></a></div>
            <div class="realt-info-price"><span

                        class="realt-info-price-label">Цена:</span><?= $realt->priceFormated() ?></div>
            <div class="realt-info-contacts">
                <div class="realt-info-contacts-type">Частное лицо</div>
                <div class="realt-info-contacts-name"><?= $realt->agent_name ?></div>
                <?php if ($realt->phone) { ?>
                    <div class="realt-info-contacts-phone show-hidden-data mark" data-mark-name="show-phone"
                         data-hidden="<?= $realt->phone ?>"><span><?= $realt->phone ?></span></div><?php } ?>
            </div>
            <?php /* <div class="realt-info-contacts-row address">г. Саратов ул. Пушкина, 6</div> */ ?>
            <?php /* <div class="realt-info-contacts-row email">domikRus@mail.ru</div> */ ?>

            <div class="options">

                <table class="options-table">
                    <colgroup>
                        <col style='width: 145px;'>
                    </colgroup>
                    <?php if ($realt->house_area) { ?>
                        <tr class="options-row">
                            <td class="options-row-label">
                                <div><span>Площадь дома</span></div>
                            </td>
                            <td class="options-row-value"><?= $realt->house_area ?> м<sup>2</sup></td>
                        </tr>
                    <?php } ?>

                    <?php if ($realt->stead_area) { ?>
                        <tr class="options-row">
                            <td class="options-row-label">
                                <div><span>Площадь участка</span></div>
                            </td>
                            <td class="options-row-value"><?= $realt->stead_area ?> сот.</td>
                        </tr>
                    <?php } ?>
                    <?php if ($fields = $realt->getFields(NULL, 1)) { ?>
                        <?php foreach ($fields as $field) { ?>
                            <tr class="options-row">
                                <td class="options-row-label">
                                    <div><span><?= $field['info']->title ?></span></div>
                                </td>
                                <td class="options-row-value"><?= $field['values_formated'] ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </table>

            </div>
            <div class="realt-info-form">
                <?= $this->render("_view_request_form", $view_request_form); ?>
            </div>
        </div>
        <?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "realt_left"]); ?>
    </div>
</div>
<?php if ($realt->coord_lat AND $realt->coord_lng) { ?>
    <div class="block">
        <h2>Расположение</h2>
        <div id="map" style="height:360px; width: 100%;"></div>
        <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script type="text/javascript">
            ymaps.ready(init);

            function init() {
                var myMap = new ymaps.Map("map", {
                        center: [<?= $realt->coord_lat ?>, <?= $realt->coord_lng ?>],
                        zoom: 14,
                        controls: ["zoomControl", "fullscreenControl", "geolocationControl", "routeEditor", "typeSelector"]
                    }
                );
                myGeoObject = new ymaps.GeoObject();
                myMap.geoObjects.add(new ymaps.Placemark([<?= $realt->coord_lat ?>, <?= $realt->coord_lng ?>]));
            }
        </script>
    </div>
<?php } ?>
<?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "realt_center_bottom"]); ?>
<?= frontend\widgets\block_realts\BlockRealtsWidget::widget(['title' => "Популярные", 'template' => 'template3', 'limit' => 6]); ?>
<div class="block">
    <?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "footer"]); ?>
</div>

<div class="modal fade" id="realt-view-request-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <?= $this->render("/realt/_view_request_form", $view_request_form) ?>
            </div>
        </div>
    </div>
</div>
