<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\FileInputAsset;
use yii\captcha\Captcha;
?>

<?php
\yii\widgets\Pjax::begin([
    'enablePushState' => false,
    'formSelector' => "#view-request-form", 'options' => ['class' => 'ajax-fog']
]);
?>

<?php if ($success) { ?>
    <div class="alert alert-success" role="alert"><?= $message ?></div>
<?php } ?>
<?php if ($error) { ?>
    <div class="alert alert-warning" role="alert"><?= $message ?></div>
<?php } ?>

<?php
$form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
            'id' => "view-request-form",
            'validateOnChange' => false,
            'validateOnBlur' => true,
            'enableClientValidation' => false,
            'type' => ActiveForm::TYPE_VERTICAL,
            'formConfig' => [
                'deviceSize' => ActiveForm::SIZE_TINY
            ]]
);
?>

<?= $form->field($model, 'realt_id')->hiddenInput() ?>
<div class="row">
    <div class="col-xs-12">
        <div class='form-group'>
            <?= $form->field($model, 'name')->textInput(['maxlength' => 128, 'placeholder' => "Ваше имя"])->label("") ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class='form-group'>
            <?= $form->field($model, 'phone')->textInput(['maxlength' => 128, 'placeholder' => "Ваш телефон", 'class' => 'masked-phone form-control'])->label("") ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class='form-group'>
            <?= $form->field($model, 'text')->textarea(['rows' => 3, 'placeholder' => "Введите Ваше сообщение..."])->label("") ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
	<p class="privacy-text">Отправляя данную форму, Вы подтверждаете свое согласие на обработку персональных данных, в соответствии с Федеральным законом №152-ФЗ. Обработка персональных данных осуществляется в соответствии с <a href="http://sabidom.ru/privacy">Политикой конфиденциальности</a></p>
        <div class='form-group'>

            <a class='btn btn-green' href ='<?=$_SERVER['REQUEST_URI'] . '?order=show'?>'><span>Отправить</span></a>
        </div>
    </div>

</div>


<?php /*
  $form->field($model, 'captcha')->widget(Captcha::className(), [ 'options' => [
  'class' => 'form-control',
  ], 'template' => "{image} {input}"])->label("Введите код") */
?>
<script>
    if (!isMobile.any()) {
        $(".masked-phone").mask("+7(ddd)ddd-dd-dd");
    }
</script>
<?php ActiveForm::end(); ?>
<?php \yii\widgets\Pjax::end(); ?>