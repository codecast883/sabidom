<?php

use yii\widgets\Breadcrumbs;
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?= Breadcrumbs::widget(["homeLink" => ['label' => "Главная", "url" => "/"], 'links' => $breadcrumbs]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?= frontend\widgets\block_tags\BlockTagsWidget::widget(['group_id' => $group->id]); ?>
        </div>
    </div>
</div>