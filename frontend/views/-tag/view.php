<?php

use yii\helpers\Html;
use \yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
use yii\grid\GridView;
use yii\widgets\ListView;
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?= Breadcrumbs::widget(["homeLink" => ['label' => "Главная", "url" => "/"], 'links' => $breadcrumbs]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?= frontend\widgets\block_tags\BlockTagsWidget::widget(['group_id' => $group->id]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model->title), $model->url);
        },
            ])
            ?>



        </div>
    </div>
</div>