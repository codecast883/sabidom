<?php

use common\helpers\Html;
use common\owerride\yii\widgets\Breadcrumbs;;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<h1 class ="like-h1"><?= $h1 ?></h1>
<div class="">
    <?php
    echo yii\widgets\Menu::widget([
        'items' => $sitemap
    ]);
    ?>
</div>