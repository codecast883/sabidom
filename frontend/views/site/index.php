<?php

use common\helpers\Html;
?>

<div class="visible-md-block visible-lg-block"><?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "home_top"]); ?></div>

<?=
frontend\widgets\block_villages\BlockVillagesWidget::widget(
		[
			'template' => 'slider1',
			'limit' => 40,
			'on_home' => 1,
			'title' => 'Коттеджные поселки',
			'showmore' => ['label' => 'Все коттеджные поселки', 'url' => ['category/view', 'id' => 12]]
]);
?>

<div class ="like-h1">Новые объявления</div>
<div class="offers-big">
    <div class="row">
        <div class="col-md-9 col-xs-12">
			<?//= frontend\widgets\block_realts\BlockRealtsWidget::widget(['template' => 'template1', 'limit' => 6]); ?>
			<?=
			frontend\widgets\block_villages\BlockVillagesWidget::widget(
					[
						'template' => 'new-villages',
						'limit' => 15,
						'ids' => array(2164,2165,2168,2167,2169,2170,2171,2172,2173,2174,2175,2176,2177,2178),
						'showmore' => ['label' => '', 'url' => ['category/view', 'id' => 12]]
			]);
			?>
        </div>

        <div class="col-md-3 hidden-sm hidden-xs">
					<?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "home_left"]); ?>
        </div>
    </div>

	<?= frontend\widgets\block_realts\BlockRealtsWidget::widget(['template' => 'template2', 'limit' => 4]); ?>

</div>
<div class="block">
    <h1><?= $h1 ?></h1>
	<?= $description_top ?>
	<?= $description_bottom ?>
</div>


<?php foreach ($categories_for_blocks as $category) { ?>
	<?= frontend\widgets\block_subcategories\BlockSubcategoriesWidget::widget(['category_id' => $category->id, 'title' => Html::a($category->title, $category->url), 'visible_rows' => 5]); ?>
<?php } ?>
<div class="block visible-md-block visible-lg-block">
	<?= frontend\widgets\banners\BannersWidget::widget(['position_id' => "footer"]); ?>
</div>