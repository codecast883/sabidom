<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>


<div class='container contacts-page'>
    <div class='row'>
        <div class='col-xs-12 main-column'>
            <div class="site-error">
               <h1>Страница не найдена</h1>

<!--                <div class="alert alert-danger">-->
<!--                    --><?//= nl2br(Html::encode($message)) ?>
<!--                </div>-->

                <p>
                    Страница, которую вы запрашиваете не существует или была удалена. Воспользуйтесь пожалуйста поиском в шапке сайта, чтобы найти подходящее вам предложение по недвижимости.

                </p>


            </div>


        </div>
    </div>
</div>






