<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\captcha\Captcha;
use common\owerride\yii\widgets\Breadcrumbs;;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<h1 class="like-h2"><?= $h1 ?></h1>

<?php \yii\widgets\Pjax::begin(['enablePushState' => false, 'formSelector' => "#add-feedback-form", "id" => "feedback-form", 'options' => ['class' => 'ajax-fog']]); ?>

<?= $description_top ?>
<div class="row">
    <div class="col-xs-4 col-xs-offset-4">
        <?php if ($success) { ?>
            <div class="alert alert-success" role="alert"><?= $message ?></div>
        <?php } ?>
        <?php if ($error) { ?>
            <div class="alert alert-warning" role="alert"><?= $message ?></div>
        <?php } ?>

        <?php
        $form = ActiveForm::begin([
                    'options' =>
                    ['enctype' => 'multipart/form-data'],
                    'id' => "add-feedback-form",
                    'validateOnChange' => false,
                    'validateOnBlur' => true,
                    'formConfig' => ['deviceSize' => ActiveForm::SIZE_TINY]
        ]);
        ?>


        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'phone')->textInput() ?>
        <?= $form->field($model, 'message')->textarea(['rows' => 3]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'captcha')->widget(Captcha::className(), ['template' => "<div class='row'><div class='col-xs-6'>{image}</div><div class='col-xs-6'>{input}</div></div>"])->label("") ?>
            </div>
            <div class="col-xs-4 text-right">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-green']) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?php \yii\widgets\Pjax::end(); ?>
<?= $description_bottom ?>