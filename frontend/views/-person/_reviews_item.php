<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
?>

<div id="div_review_<?= $review->id ?>" class='reviews-list-item <?= $review->status != common\models\Review::STATUS_ENABLED ? "review-disabled" : "" ?>'  itemtype="http://schema.org/Review" itemscope="" itemprop="reviews">
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-9">
                    <div itemtype="http://schema.org/Person" itemscope="" itemprop="author">
                        <div class='reviews-list-item-username' itemprop="name">
                            <?= $review->name ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 text-right">
                    <div class='reviews-list-item-date'>
                        <?= date("d-m-Y H:i", strtotime($review->date_post)) ?>
                        <i class="zmdi zmdi-calendar"></i>
                    </div>
                </div>
            </div>
            <div class='reviews-list-item-comment' itemprop="reviewBody"><?= nl2br($review->text); ?></div>
        </div>
    </div>
    <?php if ($review->can("manage")) { ?>
        <!--div class="reviews-list-item-cp">
            <?php if ($review->can("toggle-status")) { ?>
                <?php if ($review->status == 1) { ?>
                    <button class="btn btn-default btn-sm" onclick="reviewToggleStatus(<?= $review->id ?>)">Выкл</button>
                <?php } else { ?>
                    <button class="btn btn-default btn-sm"  onclick="reviewToggleStatus(<?= $review->id ?>)">Вкл</button>
                <?php } ?>
            <?php } ?>
            <?php if (\Yii::$app->user->can("admin-panel-access")) { ?>
                <a class="btn btn-default btn-sm" href="/admin/company-review/update?id=<?= $review->id ?>" target="_blank">Ред</a>
            <?php } ?>
        </div-->
    <?php } ?>
</div>