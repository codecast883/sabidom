<?php

use yii\helpers\Html;
?>

<div class="news-list">
    <?php foreach ($news as $new) { ?>
        <div class="news-list-item">
            <div class="row">
                <div class="col-xs-3">
                    <div class="news-list-item-image">
                        <?= Html::img($new->getUploadedFileUrl('image', 'middle')) ?> 

                    </div>
                </div>
                <div class="col-xs-9">
                    <div class="news-list-item-date"><i class="zmdi zmdi-calendar"></i> <?= date("d.m.Y H:i", strtotime($new->date_add)) ?></div>
                    <div class="news-list-item-title">
                        <a href="<?= $new->url ?>">
                            <?= $new->title ?>
                        </a>
                    </div>
                    <div class="news-list-item-preview">
                        <?= \common\helpers\YText::wordLimiter(strip_tags($new->description), 70) ?>
                    </div>
                    <div class="news-list-item-readmore">
                        <?= Html::a("Читать полностью", $new->url) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>