<?php

use common\models\CompanyReview;
use \yii\widgets\LinkPager;
?>

<div class="block1">
    <div class="block1-title"><span><?= $title ?></span></div>
    <div class="block1-body">


        <div class='reviews-list'>
            <?php \yii\widgets\Pjax::begin(['id' => 'reviews-list', 'enablePushState' => false, "linkSelector" => "none", "formSelector" => "none"]); ?>
            <?php if (count($reviews) == 0) { ?>
                <div class="reviews-list-empty-text">
                    
                </div>
            <?php } else { ?>
                <?php foreach ($reviews as $review) { ?>
                    <?= $this->render("_reviews_item", ['review' => $review]); ?>
                <?php } ?>
            <?php } ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>

        <div class="text-center">
            <?php
            echo LinkPager::widget([
                'pagination' => $pagination,
            ]);
            ?>
        </div>
    </div>        
</div>