<div class="block1">
    <div class="block1-title"><span>Биография</span></div>
    <div class="block1-body">
        <div class="text-block">
            <?= $person->description_short ?>
            <?= $person->description ?>
        </div>
    </div>
</div>
<div class="block-share">
    <div class="block-share-title">Поделиться:</div>
    <div class="block-share-buttons"><script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,gplus,moimir" data-yashareTheme="counter"></div></div>
</div>

<?= $this->render("_reviews", ['reviews' => $reviews, 'pagination' => $pagination, 'title' => "Комментарии"]); ?>
<?= $this->render("_reviews_form", $reviews_form); ?>