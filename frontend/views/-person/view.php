<?php

use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use common\models\Person;
?>
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?= Breadcrumbs::widget($breadcrumbs); ?>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-9">
            <div class="row">
                <div class="col-xs-4">
                    <div class="person-image">
                        <?= Html::img($person->getUploadedFileUrl('image', 'middle'), ['class' => '']) ?> 
                    </div>
                </div>
                <div class="col-xs-8">
                    <h1 class="person-title"><?= $person->title ?></h1>
                    <?php if ($person->subtitle) { ?>
                        <div class="person-subtitle"><?= $person->subtitle ?></div>
                    <?php } ?>
                    <div class="person-options">
                        <?php if ($person->date_of_birth) { ?>
                            <div class="person-options-row">
                                <div class="person-options-row-title">Дата рождения:</div>
                                <div class="person-options-row-value"><?= date("d.m.Y", strtotime($person->date_of_birth)) ?></div>
                            </div>
                        <?php } ?>
                        <?php if ($person->sex) { ?>
                            <div class="person-options-row">
                                <div class="person-options-row-title">Пол:</div>
                                <div class="person-options-row-value"><?= Person::itemAlias("sex", $person->sex) ?></div>
                            </div>
                        <?php } ?>
                        <?php if ($person->place_birth) { ?>
                            <div class="person-options-row">
                                <div class="person-options-row-title">Место рождения:</div>
                                <div class="person-options-row-value"><?= $person->place_birth ?></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?= $this->render($tab, $data) ?>
        </div>
        <div class="col-xs-3">
            <?php if ($person->images) { ?>
                <div class="block1">
                    <div class="block1-title"><span>Фото</span></div>
                    <div class="block1-body">
                        <div class="person-column-images">
                            <?php for ($i = 0; $i < count($person->images) AND $i < 2; $i++) { ?>
                                <div class="person-column-images-item">
                                    <?= Html::img($person->images[$i]->getUploadedFileUrl('image', 'small')) ?> 
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="block1-view-more">
                        <a href="<?= $person->getUrl("foto"); ?>">Все фото <i class="zmdi zmdi-long-arrow-right"></i></a>
                    </div>
                </div>
            <?php } ?>
            <?php if ($news = $person->getNews(3)) { ?>
                <div class="block1">
                    <div class="block1-title"><span>Новости</span></div>
                    <div class="block1-body">
                        <div class="person-column-news">
                            <?php foreach ($news as $new) { ?>
                                <div class="person-column-news-item">
                                    <div class="person-column-news-item-image">
                                        <a href="<?= $new->url ?>">
                                            <?= Html::img($new->getUploadedFileUrl('image', 'middle')) ?> 
                                        </a>
                                    </div>
                                    <div class="person-column-news-item-title">
                                        <a href="<?= $new->url ?>">
                                            <?= $new->title ?>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="block1-view-more">
                        <a href="<?= $person->getUrl("news"); ?>">Все новости <i class="zmdi zmdi-long-arrow-right"></i></a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>