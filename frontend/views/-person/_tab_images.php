<?php

use yii\helpers\Html;
?>
<div class="person-images-list">
    <?php foreach ($person->images as $image) { ?>
        <div class="person-images-list-item">
            <?= Html::a(Html::img($image->getUploadedFileUrl('image', 'small')), $image->getUploadedFileUrl('image'), ['class' => 'fancybox', 'rel' => 'gal']) ?> 
        </div>
    <?php } ?>
</div>