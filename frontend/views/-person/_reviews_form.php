<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\FileInputAsset;
use yii\captcha\Captcha;
?>


<?php
\yii\widgets\Pjax::begin([
    'enablePushState' => false,
    'formSelector' => "#add-review-form"
]);
?>

<div class="review-form">
    <?php if ($success) { ?>
        <div class="alert alert-success" role="alert"><?= $message ?></div>
        <script>$.pjax.reload('#reviews-list');</script>
    <?php } ?>
    <?php if ($error) { ?>
        <div class="alert alert-warning" role="alert"><?= $message ?></div>
    <?php } ?>

    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'class' => "review-form"
                ],
                'id' => "add-review-form",
                'validateOnChange' => false,
                'validateOnBlur' => true,
                'enableClientValidation' => false,
                'type' => ActiveForm::TYPE_VERTICAL,
                'formConfig' => [
                    'deviceSize' => ActiveForm::SIZE_TINY
                ]]
    );
    ?>
    <?= $form->field($model, 'text')->textarea(['rows' => 6, 'placeholder' => "Написать комментарий"])->label("") ?>


    <div class="form-group row last-row">
        <div class='col-xs-6'>
            <?= $form->field($model, 'name')->textInput(['maxlength' => 128, 'placeholder' => 'Имя'])->label("") ?>
        </div>
        <div class='col-xs-4'>
            <?=
            $form->field($model, 'captcha')->widget(Captcha::className(), [ 'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'Введите код',
                ], 'template' => "{image} {input}"])->label("")
            ?>

        </div>
        <div class='col-xs-2 text-right'>
            <button type="submit" class='btn btn-default review-form-submit'>Отправить</button>
        </div>
    </div>

</div>
<?php ActiveForm::end(); ?>

<?php \yii\widgets\Pjax::end(); ?>