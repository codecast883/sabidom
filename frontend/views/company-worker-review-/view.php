<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use \yii\widgets\LinkPager;
?>


<div class="container">
    <div class="row">
        <div class='col-xs-8'>
            <div class="company-card">
                <h1 class='company-card-title'><?= $company->generateH1() ?></h1>
                <div class='company-card-description'><?= $company->description_short ?></div>
                <?php if ($company_rating_positions) { ?>
                    <div class="row company-card-awards">
                        <div class="col-xs-2">
                            <div class="company-card-awards-label">
                                Награды:
                            </div>
                        </div>
                        <div class="col-xs-10">
                            <?php foreach ($company_rating_positions as $position) { ?>
                                <div class="company-rating-position"><span class="company-rating-position-position"><?= $position->position ?></span> <span class="company-rating-position-label"><?php print_r($position->rating->title_short) ?></span></div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="company-info">
                <table class="company-info-tabs">
                    <tr>
                        <td><a href="<?= $company->url ?>">Описание и отзывы клиентов</a></td>
                        <td class="active">Отзывы сотрудников</td>
                        <td><a href="<?= $company->url ?>/company-feedback">Представитель компании</a></td>
                    </tr>
                </table>
            </div>
            <?= $this->render("_reviews", ['reviews' => $reviews, 'pagination' => $pagination, 'title' => "<i class='fa fa-commenting'></i> Отзывы сотрудников о " . $company->title]); ?>
            <?= $this->render("_reviews_form", ['company' => $company, 'model' => new frontend\models\CompanyWorkerReview(['scenario' => 'create_from_front'])]); ?>
        </div>
        <div class="col-xs-4">
            <?php foreach ($ratings_id as $rating_id) { ?>
                <?=
                frontend\widgets\rating_companies\RatingCompaniesWidget::widget([
                    'rating_id' => $rating_id
                ]);
                ?>
            <?php } ?>
            <?php if ($categories_crosslink) { ?>
                <div class="block-1">
                    <div class="block-1-title">
                        <i class="fa fa-cogs"></i> Каталог компаний
                    </div>
                    <div class="block-1-body">
                        <div class="categories-list-1">
                            <?php foreach ($categories_crosslink as $item) { ?>
                                <?php if ($item['companies']) { ?>
                                    <div class="categories-list-1-item">
                                        <div class="categories-list-1-item-image">
                                            <?php if ($item['category']->image) { ?>
                                                <?= yii\helpers\Html::img($item['category']->getUploadedFileUrl('image', 'middle'), ["alt" => $item['category']->title]); ?>
                                            <?php } else { ?>
                                                <img src="/img/pixel.png">
                                            <?php } ?>
                                        </div>
                                        <div class="categories-list-1-item-title"><a href="<?= $item['category']->url ?>"><?= $item['category']->title ?></a></div>
                                        <?php foreach ($item['companies'] as $item_company) { ?>
                                            <div class="categories-list-1-item-company"><a href="<?= $item_company->url ?>"><?= $item_company->title ?></a></div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="block-1">
                <div class="block-1-title">
                    <i class="fa fa-files-o"></i> Похожие компании
                </div>
                <div class="block-1-body">
                    <div class="companies-list-1">
                        <?php foreach ($top_companies as $model) { ?>
                            <a href="<?= $model->url ?>" class="companies-list-1-item">
                                <div class="companies-list-1-item-image"><?= yii\helpers\Html::img($model->getUploadedFileUrl('image', 'middle'), ["alt" => $model->title]); ?></div>
                                <div class="companies-list-1-item-title"><?= $model->title ?></div>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>