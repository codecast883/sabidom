<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
?>

<div id="div_review_<?= $review->id ?>" class='reviews-list-item <?= $review->status != common\models\CompanyReview::STATUS_ENABLED ? "review-disabled" : "" ?>'  itemtype="http://schema.org/Review" itemscope="" itemprop="reviews">
    <div class="row">
        <div class="col-xs-2 reviews-list-item-left-col">
            <div class='reviews-list-item-image'><img src="/img/generic-avatar.png"></div>
            <div class='reviews-list-item-date'><?= date("Y-m-d", strtotime($review->date_post)) ?></div>
        </div>
        <div class="col-xs-10">
            <div itemtype="http://schema.org/Person" itemscope="" itemprop="author">
                <div class='reviews-list-item-username' itemprop="name">
                    <?= $review->name ?>
                </div>
            </div>
            <?php if ($review->rating == 1) { ?>
                <div class='reviews-list-item-rating reviews-list-item-rating-con'>Не рекомендует <i class="fa fa-thumbs-down"></i></div>
            <?php } elseif ($review->rating == 5) { ?>
                <div class='reviews-list-item-rating reviews-list-item-rating-pos'>Рекомендует <i class="fa fa-thumbs-up"></i></div>
            <?php } ?>
            <div class='reviews-list-item-comment' itemprop="reviewBody"><?= nl2br($review->text); ?></div>
            <!--div class='reviews-list-item-more'><a href='#'>Читать отзыв полностью</a></div-->
        </div>
    </div>
    <?php if ($review->can("manage")) { ?>
        <div class="reviews-list-item-cp">
            <?php if ($review->can("toggle-status")) { ?>
                <?php if ($review->status == 1) { ?>
                    <button class="btn btn-default btn-sm" onclick="reviewToggleStatus(<?= $review->id ?>)">Выкл</button>
                <?php } else { ?>
                    <button class="btn btn-default btn-sm"  onclick="reviewToggleStatus(<?= $review->id ?>)">Вкл</button>
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>
</div>