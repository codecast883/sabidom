<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
?>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<div class="h2-badge-container"><h1 class="like-h2"><?= $h1 ?></h1> <?php if ($village->premium) { ?><span class="badge badge-green">ПРЕМИУМ</span><?php } ?></div>
<div class="row realt-info">
    <div class="col-xs-12 col-sm-8">
        <script>
            $(function () {
                $('.realt-slides').slides({
                    preload: true,
                    preloadImage: '/vendor/slides/img/loading.gif',
                    effect: 'slide, fade',
                    crossfade: true,
                    slideSpeed: 200,
                    fadeSpeed: 500,
                    generateNextPrev: false,
                    generatePagination: false
                });
            });
        </script>

        <div class="realt-slides">
            <div class="slides_container">
                <?php foreach ($village->images as $image) { ?>
                    <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'large'), ["alt" => $village->title]); ?>
                <?php } ?>
            </div>
            <ul class="pagination">
                <?php foreach ($village->images as $image) { ?>

                    <li><a href="#"><?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'small'), ["alt" => $village->title]); ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="block block-white padding">
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <div class="params">
                        <div class="params-row">
                            <span class="params-row-label">Регион:</span>
                            <span class="params-row-value">Московская обл.</span>
                        </div>
                        <div class="params-row">
                            <span class="params-row-label">Район:</span>
                            <span class="params-row-value">Истринский</span>
                        </div>
                        <div class="params-row">
                            <span class="params-row-label">Шоссе:</span>
                            <span class="params-row-value">Новорижское</span>
                        </div>
                        <div class="params-row">
                            <span class="params-row-label">Расстояние от МКАД:</span>
                            <span class="params-row-value">23 км</span>
                        </div>
                        <div class="params-row">
                            <span class="params-row-label">Всего объектов в поселке:</span>
                            <span class="params-row-value">846</span>
                        </div>
                        <div class="params-row">
                            <span class="params-row-label">Цена:</span>
                            <span class="params-row-value price"><?= $village->priceFormated(); ?></span>
                        </div>
                    </div>
                    <div class="">
                        <form>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class='form-group'>
                                        <input class="form-control" placeholder="Ваше имя">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class='form-group'>
                                        <input class="form-control" placeholder="Ваш email">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class='form-group'>
                                        <textarea class="form-control" placeholder="Введите Ваше сообщение..."></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class='form-group'>
                                        <a href="#" class="btn btn-green btn-lg">Заявка на просмотр</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5 text-right">
                    <?php if ($village->phone) { ?><div class="realt-info-contacts-phone"><?= $village->phone ?></div><?php } ?>
                </div>
            </div>
        </div>
        <div class="block block-white padding shadow">
            <div class="options">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="options-title">Дома</div>
                        <table class="options-table">
                            <tr class="options-row">
                                <td class="options-row-label"><span>Общая площадь</span></td>
                                <td class="options-row-value">282 м<sup>2</sup></td>
                            </tr>
                            <tr class="options-row">
                                <td class="options-row-label"><span>Количество этажей</span></td>
                                <td class="options-row-value">3</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xs-6">
                        <div class="options-title">Участки</div>
                        <table class="options-table">
                            <tr class="options-row">
                                <td class="options-row-label"><span>Площадь участка</span></td>
                                <td class="options-row-value">9,1 соток</td>
                            </tr>
                            <tr class="options-row">
                                <td class="options-row-label"><span>Тип участка</span></td>
                                <td class="options-row-value">ижс</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <div class="block block-white padding shadow">
            <div class="like-h3">Описание</div>
            <div class="realt-info-description">
                <?= $village->description ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <img style="width: 100%; height: 600px; background-color: gray" src="/img/pixel.png">
        <br>
        <br>
        <img style="width: 100%; height: 600px; background-color: gray" src="/img/pixel.png">
        <br>
        <br>
        <img style="width: 100%; height: 600px; background-color: gray" src="/img/pixel.png">
        <br>
    </div>
</div>
<?php if ($village->coord_lat AND $village->coord_lng) { ?>
    <div class="block">
        <h2>Расположение</h2>
        <div id="map" style="height:360px; width: 100%;"></div>
        <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script type="text/javascript">
            ymaps.ready(init);
            function init() {
                var myMap = new ymaps.Map("map", {
                    center: [<?= $village->coord_lat ?>, <?= $village->coord_lng ?>],
                    zoom: 14,
                    controls: ["zoomControl", "fullscreenControl", "geolocationControl", "routeEditor", "typeSelector"]
                }
                );
                myGeoObject = new ymaps.GeoObject();
                myMap.geoObjects.add(new ymaps.Placemark([<?= $village->coord_lat ?>, <?= $village->coord_lng ?>]));
            }
        </script>
    </div>
<?php } ?>

<?= frontend\widgets\block_realts\BlockRealtsWidget::widget(['title' => "Объекты в поселке", 'template' => 'template3', 'limit' => 6]); ?>
<?= frontend\widgets\block_realts\BlockRealtsWidget::widget(['title' => "Участки в поселке", 'template' => 'template3', 'limit' => 6]); ?>
<div class="block">
    <img style="width: 100%; height: 200px; background-color: gray" src="/img/pixel.png">
</div>
<div class="block">
    <div class="like-h1">Дачи вашем регионе</div>
    <div class="row">
        <div class="col-xs-4">
            <div class="list1">
                <div class="list1-header">ГОРОДА</div>
                <ul class="list1-items">
                    <li><a href="#">Верея (6575)</a></li>
                    <li><a href="#">Высоковск (6521)</a></li>
                    <li><a href="#">Дзержинский (6521)</a></li>
                    <li><a href="#">Домодедово (6521)</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="list1">
                <div class="list1-header">ГОРОДА</div>
                <ul class="list1-items">
                    <li><a href="#">Верея (6575)</a></li>
                    <li><a href="#">Высоковск (6521)</a></li>
                    <li><a href="#">Дзержинский (6521)</a></li>
                    <li><a href="#">Домодедово (6521)</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="list1">
                <div class="list1-header">ГОРОДА</div>
                <ul class="list1-items">
                    <li><a href="#">Верея</a> (6575)</li>
                    <li><a href="#">Высоковск</a> (6521)</li>
                    <li><a href="#">Дзержинский</a> (6521)</li>
                    <li><a href="#">Домодедово</a> (6521)</li>
                </ul>
            </div>
        </div>
    </div>
</div>