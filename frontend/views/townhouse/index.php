<?php

use yii\helpers\Html;
use \yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
?>

<div class="block block-white padding shadow">
    <div class="filter">
        <form class="form-inline" id="form1">
            <div class="filter-row">
                <div class="filter-group">
                    <div class="filter-group-title">
                        Тип недвижимости:
                    </div>
                    <div class="filter-group-body">
                        <div class="btn-group"><a class="btn btn-default" href="#">Дома</a><a class="btn btn-default" href="#">Участки</a><a class="btn btn-default" href="#">Таунхаусы</a></div>
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        Цена, руб:
                    </div>
                    <div class="filter-group-body">
                        <input type="text" class="form-control size2" placeholder="От"> - <input type="text" class="form-control size2" placeholder="До">
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        От города:
                    </div>
                    <div class="filter-group-body">
                        <input type="text" class="form-control size1" placeholder="От"> - <input type="text" class="form-control size1" placeholder="До">
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        Жилая, м<sup>2</sup>: 
                    </div>
                    <div class="filter-group-body">
                        <input type="text" class="form-control size1" placeholder="От"> - <input type="text" class="form-control size1" placeholder="До">
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        Участок, м<sup>2</sup>: 
                    </div>
                    <div class="filter-group-body">
                        <input type="text" class="form-control size1" placeholder="От"> - <input type="text" class="form-control size1" placeholder="До">
                    </div>
                </div>
            </div>
            <div class="filter-row">
                <div class="filter-group">
                    <div class="filter-group-title">
                        Тип участка:
                    </div>
                    <div class="filter-group-body">
                        <label class="checkbox-inline"><input type="checkbox"> ИЖС</label>
                        <label class="checkbox-inline"><input type="checkbox"> Дачное строительство</label>
                    </div>
                </div>
            </div>
            <div class="filter-row">
                <div class="filter-group">
                    <div class="filter-group-title">
                        Газ:
                    </div>
                    <div class="filter-group-body">
                        <label class="checkbox-inline"><input type="checkbox"> Магистральный</label>
                        <label class="checkbox-inline"><input type="checkbox"> Баллон</label>
                        <label class="checkbox-inline"><input type="checkbox"> Газгольдер</label>
                    </div>
                </div>
                <div class="filter-group">
                    <div class="filter-group-title">
                        Спален:
                    </div>
                    <div class="filter-group-body">
                        <label class="checkbox-inline"><input type="checkbox"> 1</label>
                        <label class="checkbox-inline"><input type="checkbox"> 2</label>
                        <label class="checkbox-inline"><input type="checkbox"> 3</label>
                        <label class="checkbox-inline"><input type="checkbox"> 4+</label>
                    </div>
                </div>
                <div class="filter-group pull-right">
                    <div class="filter-group-title">&nbsp;</div>
                    <div class="filter-group-body">
                        <?=
                        Html::a('Подобрать', '', [
                            'onclick' => "form1.submit(); return false;",
                            'class' => 'btn btn-sm btn-green'
                        ])
                        ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?= Breadcrumbs::widget($breadcrumbs); ?>
<h1 class="like-h2"><?= $h1 ?></h1>
<div class="offer-large">
    <?php foreach ($models as $model) { ?>
        <div class="offer-large-item block block-white padding shadow">
            <table>
                <tr>
                    <td class="offer-large-item-image">
                        <?php if ($model->premium) { ?><div class="badge badge-green">ПРЕМИУМ</div><?php } ?>
                        <div class="images-count"><i class="fa fa-picture-o"></i> <?= count($model->images) ?></div>
                        <div class="carousel-2">
                            <div class="slick items-1">
                                <?php foreach ($model->images as $image) { ?>
                                    <div class="item">
                                        <?= yii\helpers\Html::img($image->getUploadedFileUrl('image', 'middle'), ["alt" => $model->title]); ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </td>
                    <td class="offer-large-col-right">
                        <table>
                            <tr>
                                <td class="offer-large-sub-left">
                                    
                                    <div class="offer-large-item-title">
                                        <?= Html::a($model->title, $model->url); ?>
                                    </div>

                                    <div class="options-row-simple">
                                        <span class="options-row-simple-value">Леинградское ш. (80 КМ от МКАД)</span>
                                    </div>
                                    <div class="options-row-simple">
                                        <span class="options-row-simple-value">Бутырки д. <span class="gray">(Клинский р-н.)</span> <i class="fa fa-map-marker gray"></i> <a href="#" class="dotted">показать на карте</a></span>
                                    </div>
                                    <div class="options-row-simple">
                                        <span class="options-row-simple-label">Площадь объекта недвижимости: </span>
                                        <span class="options-row-simple-value">133 м<sup>2</sup></span>
                                    </div>

                                    <div class="options-row-simple">
                                        <span class="options-row-simple-label">Площадь участка: </span>
                                        <span class="options-row-simple-value">282 м<sup>2</sup></span>
                                    </div>
                                </td>
                                <td class="offer-large-sub-right">
                                    <div class="offer-large-item-price">
                                        <span class="offer-large-item-price-label">цена</span>
                                        <span class="offer-large-item-price-value"><?= $model->priceFormated() ?></span>
                                    </div>
                                    <?php if ($model->phone) { ?><div class="realt-info-contacts-phone"><?= $model->phone ?></div><?php } ?>
                                    <a href ='#' class="realt-add-to-bookmark">Добавить в избранное <i class="fa fa-heart"></i></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    <?php } ?>
</div>
<div class="text-center">
    <?php
    echo LinkPager::widget([
        'pagination' => $pagination,
    ]);
    ?>
</div>

<div class="block">
    <h2>Коттеджные поселки в Подмосковье</h2>
    <div id="map" style="height:360px; width: 100%;"></div>
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(init);
        function init() {
            var myMap = new ymaps.Map("map", {
                center: [45.436554, 12.338716],
                zoom: 18,
                controls: ["zoomControl", "fullscreenControl", "geolocationControl", "routeEditor", "typeSelector"]
            }
            );
            myGeoObject = new ymaps.GeoObject();
            myMap.geoObjects.add(new ymaps.Placemark([45.436554, 12.338716]));
        }
    </script>
</div>

<?php if (!empty($description_bottom)) { ?>
    <div class="block">
        <?= $description_bottom; ?>
    </div>
<?php } ?>
<div class="block">
    <img style="width: 100%; height: 200px; background-color: gray" src="/img/pixel.png">
</div>
<div class="block">
    <div class="like-h1">Дачи вашем регионе</div>
    <div class="row">
        <div class="col-xs-4">
            <div class="list1">
                <div class="list1-header">ГОРОДА</div>
                <ul class="list1-items">
                    <li><a href="#">Верея (6575)</a></li>
                    <li><a href="#">Высоковск (6521)</a></li>
                    <li><a href="#">Дзержинский (6521)</a></li>
                    <li><a href="#">Домодедово (6521)</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="list1">
                <div class="list1-header">ГОРОДА</div>
                <ul class="list1-items">
                    <li><a href="#">Верея (6575)</a></li>
                    <li><a href="#">Высоковск (6521)</a></li>
                    <li><a href="#">Дзержинский (6521)</a></li>
                    <li><a href="#">Домодедово (6521)</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="list1">
                <div class="list1-header">ГОРОДА</div>
                <ul class="list1-items">
                    <li><a href="#">Верея</a> (6575)</li>
                    <li><a href="#">Высоковск</a> (6521)</li>
                    <li><a href="#">Дзержинский</a> (6521)</li>
                    <li><a href="#">Домодедово</a> (6521)</li>
                </ul>
            </div>
        </div>
    </div>
</div>