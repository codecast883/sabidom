<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [		
		'//netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
		/*
		'vendor/slick.css',
		'vendor/jquery.rateyo.min.css',
		'vendor/raty/lib/jquery.raty.css',
		'vendor/fancybox/jquery.fancybox.css',
		'vendor/jquery-ui/1.11.4/jquery-ui.min.css',
		'vendor/jquery-ui/1.11.4/jquery-ui.theme.min.css',
		'css/styles.css?v=0.01',
		*/
		'/frontend/web/css/style.min.css?v=0.01'
	];
	public $js = [
		/*
		'vendor/slick.min.js',
		'vendor/jquery.rateyo.min.js',
		'vendor/raty/lib/jquery.raty.js',
		'vendor/jquery.total-storage.min.js',
		'vendor/jquery.maskedinput.min.js',
		'vendor/modernizr.custom.js',
		'vendor/scroll_to.js',
		'vendor/jquery-ui/1.11.4/jquery-ui.min.js',
		'vendor/slides/js/slides.min.jquery.js',
		'vendor/fancybox/jquery.fancybox.js',
		'js/php_functions.js',
		'js/main.js'*/
		'/frontend/web/js/scripts.min.js'
	];
	public $depends = [
		'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
		'yii\web\YiiAsset',
	];
	
	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
