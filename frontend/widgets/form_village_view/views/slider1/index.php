<?php

use common\helpers\Html;
?>

<?php if (!empty($title)) { ?>
	<div class="like-h1"><?= $title ?></div>
<?php } ?>
<div class="block">
    <div class="carousel-l block-1">
        <div class="block-1-body">
            <div class="slick items-3">
				<?php foreach ($models as $model) { ?>
					<div class="item">
						<a href="<?= $model->url ?>">
							<?php if ($model->image) { ?>
								<?= yii\helpers\Html::img($model->image->getUploadedFileUrl('image', 'middle'), ["alt" => $realt->title]); ?>
							<?php } else { ?>
								<img src='img/demo2.jpg'>
							<?php } ?>
						</a>
						<div class="carousel-l-item-description">
							<a href="<?= $model->url ?>" class='carousel-l-item-description-title'><?= $model->title ?></a>
							<?php if ($fields = $model->getFields([9])) { ?>
								<?php foreach ($fields as $field) { ?>
									<?= $field['info']->title ?>: <?= $field['values_formated'] ?><br>
								<?php } ?>
							<?php } ?>
							<?php if ($fields = $model->getFields([1, 2])) { ?>
								<?php foreach ($fields as $field) { ?>
									<?= $field['info']->title ?>: <?= $field['values_formated'] ?> 
								<?php } ?>
							<?php } ?>

							<span class='carousel-l-item-description-price'>Цена: <?= $model->priceFormated() ?></span>
						</div>
					</div>
				<?php } ?>
            </div>
        </div>
    </div>
	<?php if (!empty($showmore)) { ?>
		<div class="block-showmore">
			<?= Html::a($showmore['label'], $showmore['url']); ?>
		</div>
	<?php } ?>
</div>