<?php

namespace frontend\widgets\block_subcategories;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Category;

class BlockSubcategoriesWidget extends Widget {

    public $template = "default";
    public $title = "";
    public $category_id = "";
    public $visible_rows = 10;

    public function init() {
        parent::init();
    }

    public function run() {
        $data = [];
        $data['title'] = $this->title;
        $data['childrens_menu'] = $this->getData();
        $data['visible_rows'] = $this->visible_rows;

        if (!empty($data['childrens_menu']['menu'])) {
            return $this->render($this->template . "/index", $data);
        } else {
            return "";
        }
    }

    public function getData() {
        $cache_key = "category_childrens_menu_" . $this->category_id;
        $data = \Yii::$app->cache->get($cache_key);
        if ($data === false) {
            if ($category = Category::findOne($this->category_id)) {
                $data = [
                    'menu' => [],
                    'need_expand' => FALSE,
                ];

                foreach ($category->getChildrens()->where(['status' => 1])->all() as $child) {
                    if (!isset($data['menu'][$child->group_title])) {
                        $data['menu'][$child->group_title]['group'] = ['title' => $child->group_title];
                        $data['menu'][$child->group_title]['models'] = [];
                    }

                    $data['menu'][$child->group_title]['models'][] = [
                        'label' => $child->title,
                        'url' => $child->url
                    ];
                }

                if (count($data['menu']) > 3) {
                    $data['need_expand'] = TRUE;
                } else {
                    foreach ($data['menu'] as $group) {
                        if (count($group['models']) > $this->visible_rows) {
                            $data['need_expand'] = TRUE;
                            break;
                        }
                    }
                }

                ksort($data['menu']);

                $dependency = new \yii\caching\DbDependency();
                $dependency->sql = 'SELECT MAX(updated_at) FROM category';
                \Yii::$app->cache->set($cache_key, $data, 0, $dependency);
            }
        }

        return $data;
    }

}

?>
