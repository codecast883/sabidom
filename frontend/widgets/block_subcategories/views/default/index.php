<?php

use common\helpers\Html;
?>
<?php if ($childrens_menu) { ?>
    <div class="block">
        <?php if ($title) { ?>
            <div class="subcategories-list-title"><?= $title ?></div>
        <?php } ?>
        <?php if ($childrens_menu['need_expand']) { ?>
            <div class="subcategories-list-container">
                <div class="subcategories-list visible-<?= $visible_rows?>">
                <?php } ?>
                <?php foreach (Html::explodeToRows($childrens_menu['menu'], 3) as $row) { ?>
                    <div class="row">
                        <?php foreach ($row as $col) { ?>
                            <div class="col-xs-6 col-sm-4">
                                <div class="list1-header"><?= $col['group']['title']; ?></div>
                                <div class="list1">
                                    <ul class="list1-items">
                                        <?php foreach ($col['models'] as $model) { ?>
                                            <li><?= Html::a($model['label'], $model['url']) ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($childrens_menu['need_expand']) { ?>
                </div>
                <span data-text-off="Показать все" data-text-on="Свернуть" class="subcategories-list-control">Показать все</span>
            </div>
        <?php } ?>
    </div>
<?php } ?>