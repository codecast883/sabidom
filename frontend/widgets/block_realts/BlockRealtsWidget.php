<?php

namespace frontend\widgets\block_realts;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Realt;

class BlockRealtsWidget extends Widget {

    public $order_by = "title";
    public $limit = 4;
    public $on_top = NULL;
    public $on_home = NULL;
    public $template = "default";
    public $title = "";

    public function init() {
        parent::init();
    }

    public function run() {
        $data = [];
        $query = Realt::find();

//        if ($this->on_top !== NULL) {
//            $query->andWhere(["on_top" => $this->on_top]);
//        }
//
//        if ($this->on_home !== NULL) {
//            $query->andWhere(["on_home" => $this->on_home]);
//        }

        $query->andWhere(['type_id' => [2,3,4]]);
        if ($this->limit) {
            $query->limit($this->limit);
        }
		
		// if ($this->order_by) {
             // $query->orderBy($this->order_by);
        // }
       

        $data['models'] = $query->all();
        $data['title'] = $this->title;

        if (count($data['models']) > 0) {
            return $this->render($this->template . "/index", $data);
        } else {
            return "";
        }
    }

}

?>
