<?php

use common\helpers\Html;
?>
<div class="block">
    <?php if ($title) { ?>
        <div class="like-h2"><?= $title ?></div>
    <?php } ?>
    <div class="offer-middle">
        <?php foreach (Html::explodeToRows($models, 3) as $row) { ?>
            <div class="row">
                <?php foreach ($row as $model) { ?>
                    <div class="col-xs-12 col-sm-4">
                        <div class="offer-middle-item">
                            <table>
                                <tr>
                                    <td class="offer-middle-item-title">
                                        <a href="<?= $model->url ?>"><?= $model->title ?></a>
                                    </td>
                                    <td class="offer-middle-item-price">
                                        Цена: <?= $model->priceFormated() ?>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td class="offer-middle-item-image">
                                        <a href="<?= $model->url ?>">
                                            <?php if ($model->image) { ?>
                                                <?= yii\helpers\Html::img($model->image->getUploadedFileUrl('image', 'middle'), ["alt" => $realt->title]); ?>
                                            <?php } else { ?>
                                                
                                            <?php } ?>
                                        </a>
                                    </td>
                                    <td class="offer-middle-item-descr">
                                        <?php if ($fields = $model->getFields([9])) { ?>
                            <?php foreach ($fields as $field) { ?>
                                <?= $field['values_formated'] ?> шоссе<br>
                            <?php } ?>
                        <?php } ?>
                        <?php $params = []; ?>
                        <?php
                        if ($model->stead_area) {
                            $params[] = $model->stead_area . " соток";
                        }
                        ?>
                        <?php
                        if ($model->house_area) {
                            $params[] = $model->house_area . " м2";
                        }
                        ?>
                        <?= implode(", ", $params) ?>
                                        <!--noindex-->
                                        <?= Html::a("Подробнее", $model->url, ['class' => 'btn btn-green btn-sm']) ?>
                                        <!--/noindex-->
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>