<?php

use common\helpers\Html;
?>

<div class="block">
    <div class="carousel-l block-1">
        <div class="block-1-body">
            <div class="slick items-3">
                <?php foreach ($models as $model) { ?>
                    <div class="item">
                        <a href="<?= $model->url ?>">
                            <?php if ($model->image) { ?>
                                <?= yii\helpers\Html::img($model->image->getUploadedFileUrl('image', 'middle'), ["alt" => $realt->title]); ?>
                            <?php } else { ?>
                                <img src='img/demo2.jpg'>
                            <?php } ?>
                        </a>
                        <div class="carousel-l-item-description">
                            <a href="<?= $model->url ?>" class='carousel-l-item-description-title'><?= $model->title ?></a>
                            <?php if ($fields = $model->getFields([9])) { ?>
                                <?php foreach ($fields as $field) { ?>
                                    <?= $field['values_formated'] ?> шоссе<br>
                                <?php } ?>
                            <?php } ?>
                            <?php $params = []; ?>
                            <?php
                            if ($model->stead_area) {
                                $params[] = $model->stead_area . " соток";
                            }
                            ?>
                            <?php
                            if ($model->house_area) {
                                $params[] = $model->house_area . " м2";
                            }
                            ?>
                            <?= implode(", ", $params) ?>

                            <span class='carousel-l-item-description-price'>Цена: <?= $model->priceFormated() ?></span>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>