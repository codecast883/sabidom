<?php

use common\helpers\Html;
?>

<?php foreach (Html::explodeToRows($models, 4) as $row) { ?>
    <div class='row'>
        <?php foreach ($row as $model) { ?>
            <div class="col-xs-12 col-sm-3">
                <div class="offer-big-item">
                    <div class="offer-big-item-image">
                        <a href="<?= $model->url ?>">
                            <?php if ($model->image) { ?>
                                <?= yii\helpers\Html::img($model->image->getUploadedFileUrl('image', 'middle'), ["alt" => $realt->title]); ?>
                            <?php } else { ?>
                                <img src='img/demo2.jpg'>
                            <?php } ?>
                        </a>
                    </div>
                    <div class="offer-big-item-title">
                        <?= Html::a($model->title, $model->url) ?>
                    </div>
                    <div class="offer-big-item-price">
                        цена:  <?= $model->priceFormated() ?>
                    </div>
                    <div class="offer-big-item-descr">
                        <?php if ($fields = $model->getFields([9])) { ?>
                            <?php foreach ($fields as $field) { ?>
                                <?= $field['values_formated'] ?> шоссе<br>
                            <?php } ?>
                        <?php } ?>
                        <?php $params = []; ?>
                        <?php
                        if ($model->stead_area) {
                            $params[] = $model->stead_area . " соток";
                        }
                        ?>
                        <?php
                        if ($model->house_area) {
                            $params[] = $model->house_area . " м2";
                        }
                        ?>
                        <?= implode(", ", $params) ?>
                    </div>
                    <div class="offer-big-item-footer">
                        <div class="offer-big-item-like">
                            <i class="fa fa-thumbs-up"></i> <span class="quantity">1100</span>
                        </div>
                        <div class="offer-big-item-views">
                            <i class="fa fa-eye"></i> <span class="quantity"><?= $model->views ?></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>