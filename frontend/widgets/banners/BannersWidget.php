<?php

namespace frontend\widgets\banners;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Banner;

class BannersWidget extends Widget {

    public $template = "default";
    public $position_id = NULL;
    public $limit = 0;

    public function init() {
        parent::init();
    }

    public function run() {
        if (!$this->position_id) {
            return "";
        }

        $data['banners'] = [];

        $query = Banner::find()->where(['position_id' => $this->position_id, 'status' => Banner::STATUS_ENABLED]);

        if ($this->limit) {
            $query->limit($this->limit);
        }

        foreach ($query->orderBy(['sort_order' => SORT_ASC])->asArray()->all() as $item) {
            $banner = [
                'position' => $item['position_id'],
                'html' => $item['code'] ? $item['code'] : $item['html'],
            ];

            if ($banner['html']) {
                $data['banners'][] = $banner;
            }
        }

        if ($data['banners']) {
            return $this->render($this->template, $data);
        }
        else{
            return "";
        }
    }

}

?>
