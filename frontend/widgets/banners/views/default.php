<?php

use common\helpers\Html;
?>

<?php foreach ($banners as $banner) { ?>
    <div class="banner banner-pos-<?= $banner['position'] ?>">
        <?= $banner['html'] ?>
    </div>
<?php } ?>

