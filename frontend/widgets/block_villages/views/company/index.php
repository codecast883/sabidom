<?php

use common\helpers\Html;
?>
<div class="company-list company-list-small">
    <?php if ($title) { ?>
        <h1 class="company-list-title ash2 text-center"><?= $title ?></h1>
    <?php } ?>
    <div class="company-list-body">
        <?php foreach (Html::explodeToRows($models, 6) as $row) { ?>
            <div class="company-list-row row">
                <?php foreach ($row as $model) { ?>
                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <?= $this->render("/company/part/list_anonce_small", ['model' => $model]) ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>