<?php

namespace frontend\widgets\block_villages;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Village;

class BlockVillagesWidget extends Widget {

    public $order_by = "title";
    public $limit = 4;
    public $on_top = NULL;
    public $on_home = NULL;
    public $template = "default";
    public $title = "";
    public $showmore = NULL;
    public $ids = NULL;

    public function init() {
        parent::init();
    }

    public function run() {
        $data = [];
        $query = Village::find();

        if ($this->on_top !== NULL) {
            $query->andWhere(["on_top" => $this->on_top]);
        }
        if ($this->on_home !== NULL) {
            $query->andWhere(["on_home" => $this->on_home]);
        }

        if ($this->limit) {
            $query->limit($this->limit);
        }
			
		if ($this->ids) {
            $query->andWhere(["id" => $this->ids]);
        }
	
        $query->orderBy(['sort_order' => SORT_ASC]);

        $data['models'] = $query->all();
        $data['title'] = $this->title;
        $data['showmore'] = $this->showmore;

        if (count($data['models']) > 0) {
            return $this->render($this->template . "/index", $data);
        } else {
            return "";
        }
    }

}

?>
