<?php

namespace frontend\widgets\village_subpages_menu;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Village;

class VillageSubpagesMenuWidget extends Widget {

    public $village = NULL;
    public $template = "default";
    public $active = "";

    public function init() {
        parent::init();
    }

    public function run() {
        if (!$this->village) {
            return "";
        }


        $data = [];
        $data['model'] = $this->village;
        $data['active'] = $this->active;

        return $this->render($this->template, $data);
    }

}

?>
