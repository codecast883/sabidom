<?php

use common\helpers\Html;
?>
<ul class="offer-large-links">
    <?php if ($model->address OR ( $model->coord_lat AND $model->coord_lng)) { ?>
        <li><?php if ($active == "contacts") { ?>Как добраться<?php } else { ?><?= Html::a("Как добраться", $model->getUrl("contacts")) ?><?php } ?></li>
    <?php } ?>
    <?php if ($model->getDocuments()->count()) { ?>
        <li><?php if ($active == "documentation") { ?>Документация<?php } else { ?><?= Html::a("Документация", $model->getUrl("documentation")) ?><?php } ?></li>
    <?php } ?>
    <?php if ($model->master_plan) { ?>
        <li><?php if ($active == "general-layout") { ?>Генплан<?php } else { ?><?= Html::a("Генплан", $model->getUrl("general-layout")) ?><?php } ?></li>
    <?php } ?>

    <?php if ($model->getProgress_images()->count()) { ?>
        <li><?php if ($active == "progress") { ?>Ход строительства<?php } else { ?><?= Html::a("Ход строительства", $model->getUrl("progress")) ?><?php } ?></li>
    <?php } ?>
		

    <li><?php if ($active == "reviews") { ?>Отзывы<?php } else { ?><?= Html::a("Отзывы", $model->getUrl("reviews")) ?><?php } ?></li>
    <li><?php if ($active == "townhouse") { ?>Цены<?php } else { ?><?= Html::a("Цены", $model->getUrl("townhouse")) ?><?php } ?></li>
	
    <?php if ($model->getImages()->count()) { ?>
        <li><?php if ($active == "photo") { ?>Фото<?php } else { ?><?= Html::a("Фото", $model->getUrl("photo")) ?><?php } ?></li>
    <?php } ?>
</ul>