<?php

namespace common\behaviors;

use common\models\Seo;
use common\helpers\YText;
use yii\helpers\ArrayHelper;

class SeoPlus extends \yii\base\Behavior {

    private $_templates = [];

    /**
     * Массив автозамен для сео маски
     *   [
     *       '{название}' => 'title',
     *       'маска замены' => поле объекта
     *   ];
     *
     * также можно определить функцию, в которой описать сложные замены
     * public function seoPlusCustomReplaces() {
     *     return [
     *         'h1 или название' => $this->h1 ? $this->h1 :$this->title
     *     ];
     * }
     * 
     */
    private $_replace_map = [
        'название' => 'title',
    ];

    public function setReplace_map($replace) {
        $this->_replace_map = $replace;
    }

    /**
     * будет заполнено реальными текстовыми значениями для замены на основе массива replaces
     */
    private $_replace;

    public function attach($owner) {
        parent::attach($owner);

        //$route = NULL;
        //if ($route === NULL) {
        $route = \Yii::$app->requestedRoute ? \Yii::$app->requestedRoute : \Yii::$app->defaultRoute;
        //}

        $this->_templates = Seo::getByRoute($route);
    }

    public function events() {
        return [
            \yii\db\ActiveRecord::EVENT_AFTER_FIND => 'fillReplaceMap'
        ];
    }

    public function fillReplaceMap() {
        foreach ($this->_replace_map as $key => $value) {
            $this->_replace[$key] = $this->owner->{$value};
        }

        if (method_exists($this->owner, "seoPlusCustomReplaces")) {
            $this->_replace = array_merge($this->_replace, $this->owner->seoPlusCustomReplaces());
        }
    }

    public function seoPlusH1() {
        $return = "";

        if (!empty($this->owner->seo_h1)) {
            $return = $this->owner->seo_h1;
        } elseif ($return = ArrayHelper::getValue($this->_templates, "h1")) {
            $return = YText::textGenerator($return, $this->_replace);
        }

        return $return;
    }

    public function seoPlusTitle() {
        $return = "";

        if (!empty($this->owner->seo_title)) {
            $return = $this->owner->seo_title;
        } elseif ($return = ArrayHelper::getValue($this->_templates, "title")) {
            $return = YText::textGenerator($return, $this->_replace);
        }

        return $return;
    }

    public function seoPlusKeywords() {
        $return = "";

        if (!empty($this->owner->seo_keywords)) {
            $return = $this->owner->seo_keywords;
        } elseif ($return = ArrayHelper::getValue($this->_templates, "keywords")) {
            $return = YText::textGenerator($return, $this->_replace);
        }

        return $return;
    }

    public function seoPlusDescriptionTop() {
        $return = "";

        if (!empty($this->owner->description_top)) {
            $return = $this->owner->description_top;
        } elseif ($return = ArrayHelper::getValue($this->_templates, "description_top")) {
            $return = YText::textGenerator($return, $this->_replace);
        }

        return $return;
    }

    public function seoPlusDescriptionBottom() {
        $return = "";

        if (!empty($this->owner->description_bottom)) {
            $return = $this->owner->description_bottom;
        } elseif ($return = ArrayHelper::getValue($this->_templates, "description_bottom")) {
            $return = YText::textGenerator($return, $this->_replace);
        }

        return $return;
    }

    public function seoPlusBreadcrumb() {
        $return = "";

        if (!empty($this->owner->seo_breadcrumb)) {
            $return = $this->owner->seo_breadcrumb;
        } elseif ($return = ArrayHelper::getValue($this->_templates, "breadcrumb")) {
            $return = YText::textGenerator($return, $this->_replace);
        }

        return $return;
    }

    public function seoPlusBreadcrumbs($url = NULL) {
        $breadcrumbs = [];
        if (method_exists($this->owner, "seoPlusBreadcrumbs")) {
            return $this->owner->seoPlusBreadcrumbs();
        }

        $label = $this->seoPlusBreadcrumb();

        if ($label) {
            $breadcrumbs[] = ['label' => $label, 'url' => $url];
        }

        return $breadcrumbs;
    }

    public function seoPlusDescription() {

        $return = "";

        if (!empty($this->owner->seo_description)) {
            $return = $this->owner->seo_description;
        } elseif ($return = ArrayHelper::getValue($this->_templates, "description")) {
            $return = YText::textGenerator($return, $this->_replace);
        }

        return $return;
    }

    public function getSeo() {
        return [
            'title' => $this->seoPlusTitle(),
            'h1' => $this->seoPlusH1(),
            'description_top' => $this->seoPlusDescriptionTop(),
            'description_bottom' => $this->seoPlusDescriptionBottom(),
            'keywords' => $this->seoPlusKeywords(),
            'description' => $this->seoPlusDescription(),
            'breadcrumb' => $this->seoPlusBreadcrumb(),
            'breadcrumbs' => $this->seoPlusBreadcrumbs(),
        ];
    }

}
