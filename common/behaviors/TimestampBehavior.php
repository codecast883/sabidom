<?php

namespace common\behaviors;

class TimestampBehavior extends \yii\behaviors\TimestampBehavior {

    protected function getValue($event) {
        if ($this->value === null) {
            return date("Y-m-d H:i:s");
        }
        return parent::getValue($event);
    }
}
