<?php

namespace common\behaviors;

class SlugGeneratorBehavior extends \yii\base\Behavior {

    public $dependent_fields = []; // зависимые поля для проверки на уникальность 
    public $slug_param = "slug"; // поле для генерации slug
    public $slug_generate_param = "<title>"; // маска генерации slug

    public function events() {
        return [
            \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'createSlug',
            \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'createSlug'
        ];
    }

    public function init() {
        
    }

    public function createSlug() {
        if (empty($this->owner->{$this->slug_param})) {
            $title = $this->slug_generate_param;
            if (preg_match_all('/<(.*?)>/sim', $this->slug_generate_param, $regs)) {
                if (isset($regs[1])) {
                    foreach ($regs[1] as $reg) {
                        $title = preg_replace("/<$reg>/usi", $this->owner->{$reg}, $title);
                    }
                }
            } else {
                throw new NotFoundHttpException('Не установлена маска генерации slug');
            }


            $this->owner->{$this->slug_param} = $this->generateSlug($title);
        } else {
            $this->owner->{$this->slug_param} = $this->generateSlug($this->owner->{$this->slug_param});
        }
    }

    private function generateSlug($slug) {
        $slug = html_entity_decode(trim($slug));
        $slug = \common\helpers\YText::translitForSlug($slug);
        if ($this->checkUniqueSlug($slug)) {
            return $slug;
        } else {
            for ($suffix = 2; !$this->checkUniqueSlug($new_slug = $slug . '-' . $suffix); $suffix++)
                ;
            return $new_slug;
        }
    }

    private function checkUniqueSlug($slug) {
        $owner_classname = $this->owner->className();
        $query = $owner_classname::find();

        $params = [];
        $params[$this->slug_param] = $slug;
        foreach ($this->dependent_fields as $field) {
            $params[$field] = $this->owner->{$field};
        }

        $query->where($params);

        if (!$this->owner->isNewRecord) {
            $pk = $this->owner->primaryKey();
            $pk = $pk[0];

            $query->andWhere(["!=", $pk, $this->owner->{$pk}]);
        }

        return !$query->one();
    }

}
