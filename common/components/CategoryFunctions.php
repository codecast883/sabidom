<?php

namespace common\components;

class CategoryFunctions {

	private $data = array();
	private $categories = array();

	public function __construct($categories = array()) {
		$this->categories = $categories;
	}

	public function createDropDown($null_first = false) {

		// ArrayHelper::map(Account::find()->where('parent IS NULL')->asArray()->all(), 'id', 'name')
		global $data;
		$data = array();
		
		if($null_first){
			$data[0] = "***корневая категория***";
		}
		
		foreach ($this->categories as $parent) {
			$data[$parent->id] = $parent->title;
			if (isset($parent->childrens)) {
				$this->subDropDown($parent->childrens, $parent->title." | ");
			}
		}
		return $data;
	}

	/**
	 * возвращает список id дочерних категорий, включая родительскую
	 */
	public static function getChildIdList($category) {
		$data = array();
		$data[] = $category->id;
		if (isset($category->childrens)) {
			foreach ($category->childrens as $childrens) {
				$data = array_merge($data, self::getChildIdList($childrens));
			}
		}

		return $data;
	}

	public function subDropDown($childrens, $space = '- ') {
		global $data;
		foreach ($childrens as $child) {
			$data[$child->id] = $space . $child->title;
			$this->subDropDown($child->childrens, $space.$child->title . ' | ');
		}
	}

	public function createMenu($get_submenu = TRUE) {
		global $data;
		$data = array();
		foreach ($this->categories as $parent) {
			$menu_item = array(
				'label' => $parent->title,
				'url' => $parent->getLink(),
			);

			if ($get_submenu AND isset($parent->childrens)) {
				$menu_item['items'] = $this->subMenu($parent->childrens);
			}

			$data[] = $menu_item;
		}
		return $data;
	}

	public function subMenu($childrens) {
		$local_menu = array();
		foreach ($childrens as $child) {
			$menu_item = array(
				'label' => $child->title,
				'url' => $child->getLink(),
			);

			if (isset($child->childrens)) {
				$menu_item['items'] = $this->subMenu($child->childrens);
			}
			$local_menu[] = $menu_item;
		}
		return $local_menu;
	}
}
