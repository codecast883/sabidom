<?php

namespace common\components\base;

use common\models\Seo;
use common\helpers\YText;
use yii\helpers\ArrayHelper;

class BaseSeoPlus extends \yii\base\Component {

	private $_templates = [];
	private $_replace;
	private $_model;
	private $_canonical_url = "";
	private $breadcrumbs = [];
	private $route = "";
	private $breadcrumbs_homelink = [
		'label' => "Главная",
		'url' => "/"
	];
	private $_h1;
	private $_schemas = [];

	//Html::encode(
	//$this->data['breadcrumbs'] = ["homeLink" => ['label' => "Главная", "url" => "/"], 'links' => $this->seo->breadcrumbs];

	public function init($route = NULL) {
		if ($route === NULL) {
			$route = \Yii::$app->requestedRoute ? \Yii::$app->requestedRoute : \Yii::$app->defaultRoute;
		}

		$this->route = $route;

		$this->_templates = Seo::getByRoute($this->route);
	}

	/**
	 * подключение дополнительной модели с подстановками
	 */
	public function fillReplacesFromModel($model) {
		if (isset($model->seo_plus_autoreplaces)) {
			foreach (ArrayHelper::getValue($model, 'seo_plus_autoreplaces', []) as $key => $model_field) {
				$this->_replace["{" . $key . "}"] = $model_field;
			}
		}
	}

	/**
	 * установка главной модели страницы, с нее будут браться переопределения значений и подстановки
	 */
	public function attachModel($model, $fill_autorelaces = TRUE) {
		$this->_model = $model;

		if ($fill_autorelaces) {
			$this->fillReplacesFromModel($model);
		}

		if (isset($model->seo_plus_schema)) {
			$this->addSchema($model->seo_plus_schema);
		}
	}

	public function setReplaces($data) {
		foreach ($data as $key => $value) {
			$this->_replace["{" . $key . "}"] = $value;
		}
	}

	public function getH1() {
		if ($this->_h1) {
			$return = $this->_h1;
		} elseif (!$return = $this->getModelField("seo_h1")) {
			$return = ArrayHelper::getValue($this->_templates, "h1");
			$return = YText::textGenerator($return, $this->_replace);
		}

		return $return;
	}

	public function setH1($h1) {
		$this->_h1 = $h1;
	}

	public function getH2() {
		if (!$return = $this->getModelField("seo_h2")) {
			$return = ArrayHelper::getValue($this->_templates, "h2");
			$return = YText::textGenerator($return, $this->_replace);
		}

		return $return;
	}

	public function getTitle() {
		if (!$return = $this->getModelField("seo_title")) {
			$return = ArrayHelper::getValue($this->_templates, "title");
			$return = YText::textGenerator($return, $this->_replace);
		}

		return $return;
	}

	public function renderSchemas() {
		$script_blocks = [];
		foreach ($this->getSchemas() as $schema) {
			$script_blocks[] = \common\helpers\Html::script(json_encode($schema), ['type' => "application/ld+json"]);
		}
		
		return implode("\n", $script_blocks);
	}

	public function getSchemas() {
		return $this->_schemas;
	}

	public function getKeywords() {
		if (!$return = $this->getModelField("seo_keywords")) {
			$return = ArrayHelper::getValue($this->_templates, "keywords");
			$return = YText::textGenerator($return, $this->_replace);
		}

		return $return;
	}

	public function getDescription() {
		if (!$return = $this->getModelField("seo_description")) {
			$return = ArrayHelper::getValue($this->_templates, "description");
			$return = YText::textGenerator($return, $this->_replace);
		}

		return $return;
	}

	public function getBreadcrumb() {
		if (!$return = $this->getModelField("seo_breadcrumb")) {
			$return = ArrayHelper::getValue($this->_templates, "breadcrumb");
			$return = YText::textGenerator($return, $this->_replace);
		}

		return $return;
	}

	public function getDescription_top() {
		if (!$return = $this->getModelField("description_top")) {
			$return = ArrayHelper::getValue($this->_templates, "description_top");
			$return = YText::textGenerator($return, $this->_replace);
		}

		return $return;
	}

	public function getDescription_bottom() {
		if (!$return = $this->getModelField("description_bottom")) {
			$return = ArrayHelper::getValue($this->_templates, "description_bottom");
			$return = YText::textGenerator($return, $this->_replace);
		}

		return $return;
	}

	public function addBreadcrumb($breadcrumb) {
		if (is_array($breadcrumb)) {
			$breadcrumb['label'] = strip_tags(html_entity_decode($breadcrumb['label']));
		} else {
			$breadcrumb = strip_tags(html_entity_decode($breadcrumb));
		}
		$this->breadcrumbs[] = $breadcrumb;
	}

	public function addSchema($data) {
		$this->_schemas[] = $data;
	}

	public function setBreadcrumbs($breadcrumbs) {
		$this->breadcrumbs = $breadcrumbs;
	}

	public function setBreadcrumbsHomelink($url, $label) {
		$this->breadcrumbs_homelink = [
			'url' => $url,
			'label' => $label
		];
	}

	public function getBreadcrumbs() {
		if ($this->breadcrumbs) {
			return ["homeLink" => $this->breadcrumbs_homelink, 'links' => $this->breadcrumbs];
		} else {
			return NULL;
		}
	}

//    public function getBreadcrumb() {
//        $return = "";
//
////        if (!empty($this->owner->seo_breadcrumb)) {
////            $return = $this->owner->seo_breadcrumb;
////        } elseif ($return = ArrayHelper::getValue($this->_templates, "breadcrumb")) {
////            $return = YText::textGenerator($return, $this->_replace);
////        }
//
//        return $return;
//    }
//
//    public function getBreadcrumbs($url = NULL) {
//        $breadcrumbs = [];
////        if (method_exists($this->owner, "seoPlusBreadcrumbs")) {
////            return $this->owner->seoPlusBreadcrumbs();
////        }
////
////        $label = $this->seoPlusBreadcrumb();
////
////        if ($label) {
////            $breadcrumbs[] = ['label' => $label, 'url' => $url];
////        }
//
//        return $breadcrumbs;
//    }
//    public function getSeo() {
//        return [
//            'title' => $this->seoPlusTitle(),
//            'h1' => $this->seoPlusH1(),
//            'h2' => $this->seoPlusH2(),
//            'description_top' => $this->seoPlusDescriptionTop(),
//            'description_bottom' => $this->seoPlusDescriptionBottom(),
//            'keywords' => $this->seoPlusKeywords(),
//            'description' => $this->seoPlusDescription(),
//            'breadcrumb' => $this->seoPlusBreadcrumb(),
//            'breadcrumbs' => $this->seoPlusBreadcrumbs(),
//        ];
//    }

	private function getModelField($field) {
		if ($this->_model AND ! empty($this->_model->{$field})) {
			return $this->_model->{$field};
		}
		return NULL;
	}

	public function getRoute() {
		return $this->route;
	}

	public function setCanonical_url($url) {
		$this->_canonical_url = $url;
	}

	public function getCanonical_url() {
		return $this->_canonical_url;
	}

}
