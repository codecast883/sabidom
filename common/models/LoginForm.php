<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace common\models;

use yii\base\Model;
use common\helpers\Password;

/**
 * LoginForm get user's login and password, validates them and logs the user in. If user has been blocked, it adds
 * an error to login form.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class LoginForm extends Model {

	/** @var string User's email or username */
	public $login;

	/** @var string User's plain password */
	public $password;

	/** @var string Whether to remember the user */
	public $rememberMe = false;

	/** @var \dektrium\user\models\User */
	protected $user;
	public $rememberFor = 1209600; // two weeks

	/** @inheritdoc */

	public function attributeLabels() {
		return [
			'login' => 'Логин',
			'password' => 'Пароль',
			'rememberMe' => 'Запомнить меня',
		];
	}

	/** @inheritdoc */
	public function rules() {
		return [
			[['login', 'password'], 'required'],
			['login', 'trim'],
			['password', function ($attribute) {
					if ($this->user === null || !Password::validate($this->password, $this->user->password_hash)) {
						$this->addError($attribute, 'Invalid login or password');
					}
				}],
			['login', function ($attribute) {
					if ($this->user !== null) {
						if ($this->user->getIsBlocked()) {
							$this->addError($attribute, 'Your account has been blocked');
						}
					}
				}],
			['rememberMe', 'boolean'],
		];
	}

	/**
	 * Validates form and logs the user in.
	 *
	 * @return boolean whether the user is logged in successfully
	 */
	public function login() {
		if ($this->validate()) {
			return \Yii::$app->getUser()->login($this->user, $this->rememberMe ? $this->rememberFor : 0);
		} else {
			return false;
		}
	}

	/** @inheritdoc */
	public function formName() {
		return 'login-form';
	}

	/** @inheritdoc */
	public function beforeValidate() {
		if (parent::beforeValidate()) {
			$manager = new \common\components\UserManager();
			$this->user = $manager->findUserByUsernameOrEmail($this->login);
			return true;
		} else {
			return false;
		}
	}

}
