<?php

namespace common\models;

use yii\helpers\Url;
use Yii;
use himiklab\sitemap\behaviors\SitemapBehavior;

/**
 * This is the model class for table "realt".
 *
 * @property integer $id
 * @property integer $mark
 * @property string $created_at
 * @property string $updated_at
 * @property string $title
 * @property string $address
 * @property string $price
 * @property integer $date
 * @property integer $type_id
 * @property string $address_pos
 * @property string $description
 * @property string $phone
 * @property string $subtitle
 * @property integer $local_id
 * @property string $seller
 * @property string $src_url
 * @property string $src_html
 * @property string $stead_area
 * @property string $house_area
 * @property string $agent_name
 * @property string $seo_h1
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 */
class Realt extends \yii\db\ActiveRecord
{

	const TYPE_COTTAGE = 2;
	const TYPE_STEAD = 3;
	const TYPE_TOWNHOUSE = 4;
    private $_field_values_for_save;
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'realt';
	}

	public function __construct($config = array())
	{
		$this->on(\yii\db\ActiveRecord::EVENT_AFTER_DELETE, [$this, 'afterDeleteHandler']);
		$this->on(\yii\db\ActiveRecord::EVENT_BEFORE_VALIDATE, [$this, 'beforeValidateHandler']);
        $this->on(\yii\db\ActiveRecord::EVENT_AFTER_UPDATE, [$this, 'afterUpdateHandler']);
        $this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, [$this, 'afterUpdateHandler']);


        parent::__construct($config);
	}

	public function beforeValidateHandler()
	{
		$this->house_area = preg_replace("|,|isu", ".", $this->house_area);
		$this->house_area = preg_replace("|\s|isu", "", $this->house_area);
		$this->stead_area = preg_replace("|,|isu", ".", $this->stead_area);
		$this->stead_area = preg_replace("|\s|isu", "", $this->stead_area);
	}

	public static function itemAlias($type, $code = NULL)
	{
		$_items = [
			'type_id' => [
				self::TYPE_COTTAGE => "Коттедж",
				self::TYPE_TOWNHOUSE => "Таунхаус",
				self::TYPE_STEAD => "Земля",
			],
		];

		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	public function getSeo_plus_autoreplaces()
	{
		return [
			'title' => $this->title,
			'название' => $this->title,
			'id' => $this->id,
			'цена формат' => strip_tags($this->priceFormated())
		];
	}

	public function afterDeleteHandler()
	{
		foreach ($this->images as $image) {
			$image->delete();
		}

		RealtViewRequest::deleteAll(['realt_id' => $this->id]);
		RealtFieldValue::deleteAll(['realt_id' => $this->id]);
	}

	public function getFieldValues()
	{
		return $this->hasMany(RealtFieldValue::className(), ['realt_id' => 'id']);
	}

	public function behaviors()
	{
		return [
			\common\behaviors\TimestampBehavior::className(),
			#\common\behaviors\SeoPlus::className(),
			'sitemap' => [
				'class' => SitemapBehavior::className(),
				'scope' => function ($model) {
					$model->select(['id', 'updated_at']);
//                    $model->andWhere(['status' => 1]);
				},
				'dataClosure' => function ($model) {
					return [
						'loc' => $model->url,
						'lastmod' => strtotime($model->updated_at),
						'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
						'priority' => 0.8
					];
				}
			],
		];
	}

	public function getFields($field_id = NULL, $field_group_id = NULL)
	{
		$cache_key = md5($this->id . serialize($field_id) . serialize($field_group_id));

		if (!$result = \Yii::$app->cache->get($cache_key)) {
			$query = Field::find();
			if ($field_id !== NULL) {
				$query->where(['id' => $field_id]);
			}

			if ($field_group_id !== NULL) {
				$query->where(['field_group_id' => $field_group_id]);
			}

			$fields = $query->orderBy("sort_order")->all();

			$result = [];

			foreach ($fields as $field) {
				if ($values = $this->fieldValues($field->id)) {
					$values_formated = [];
					foreach ($values as $value) {
						$values_formated[] = $value->getFormated();
					}

					$result[] = [
						'info' => $field,
						'values' => $values,
						'values_formated' => implode(", ", $values_formated)
					];
				}
			}

			$dependency = new \yii\caching\DbDependency();
			$dependency->sql = 'SELECT MAX(updated_at) FROM realt';
			\Yii::$app->cache->set($cache_key, $result, 0, $dependency);
		}

		return $result;
	}

	public function fieldValues($field_id)
	{
		return RealtFieldValue::find()->where(['field_id' => $field_id, 'realt_id' => $this->id])->all();
	}

	public function incrementViews()
	{
		//$this->updateCounters(['views' => 1]);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['mark', 'date', 'type_id', 'local_id', 'village_id', 'views'], 'integer'],
			[['seo_h1', 'seo_title', 'seo_description', 'seo_keywords'], 'string', 'max' => 1024],
			[['stead_area', 'house_area', 'price'], 'number'],
			[['created_at', 'updated_at'], 'safe'],
			[['title', 'address', 'description', 'phone', 'subtitle', 'seller', 'src_html', 'agent_name', 'description_bottom','price_service'], 'string'],
			[['src_url'], 'string', 'max' => 512]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'price_service' => 'Cтоимость обслуживания',
			'title' => 'Заголовок',
			'address' => 'Адрес',
			'price' => 'Цена',
			'type_id' => 'Тип объявления',
			'description' => 'Описание',
			'description_bottom' => 'Описание (низ)',
			'phone' => 'Телефон',
			'village_id' => 'Поселок',
			'stead_area' => 'Площадь участка',
			'house_area' => 'Площадь здания',
			'agent_name' => 'Продавец',
		];
	}

    public function afterUpdateHandler()
    {

        $this->unlinkAll('fieldValues', TRUE);
//        echo "<script type='text/javascript'>console.log('sdf');</script>";
//print_r();
        $this->_field_values_for_save = Yii::$app->request->post('Realt')['field_values'];
        foreach ($this->_field_values_for_save as $field_id => $field_values) {
            foreach ($field_values as $field_value) {

                $field_value = trim($field_value);
                if (!$field_value)
                    continue;

                $field = Field::findOne($field_id);
                if ($field->type_id == Field::TYPE_TEXT) {
                    $value = new RealtFieldValue;
                    $value->field_id = $field->id;
                    $value->realt_id = $this->id;
                    $value->value = $field_value;
                    if (!$value->save()) {
                        throw new \Exception(print_r($value->getErrors(), 1));
                    }
                } elseif ($field->type_id == Field::TYPE_LIST) {
                    $field_tag = FieldTag::findOne($field_value);
                    $value = new RealtFieldValue;
                    $value->field_id = $field->id;
                    $value->realt_id = $this->id;
                    $value->value = $field_tag->value;
                    $value->field_tag_id = $field_tag->id;
                    if (!$value->save()) {
                        throw new \Exception(print_r($value->getErrors(), 1));
                    }
                }

            }
        }

    }

    public function setField_values($values)
    {
        $this->_field_values_for_save = $values;
    }

	public function getImage()
	{
		return $this->hasOne(RealtImage::className(), ['realt_id' => 'id'])->orderBy(['sort_order' => SORT_ASC]);
	}

	public function getImages()
	{
		return $this->hasMany(RealtImage::className(), ['realt_id' => 'id'])->orderBy(['sort_order' => SORT_ASC]);
	}

	public function getUrl()
	{
		return Url::to(['/realt/view', 'id' => $this->id], TRUE);
	}

	public function priceFormated()
	{
		return "<span class='price-value'>" . trim(number_format($this->price, 0, '.', ' ') . "</span> <span class='price-currency'>руб.</span>");
	}

	public function formatString($template = "", $replaces = [])
	{
		$replaces = array_merge($this->getSeo_plus_autoreplaces(), $replaces);
		return \common\helpers\YText::textGenerator($template, $replaces, ['escape_keys' => TRUE]);
	}

}
