<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_template".
 *
 * @property integer $id
 * @property string $alias
 * @property string $description
 * @property string $value
 * @property string $type
 * @property integer $sort_order
 * @property string $help
 */
class EmailTemplate extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'email_template';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['replaces', 'body', 'theme', 'help', 'send_from'], 'string'],
            [['sort_order', 'send_from'], 'required'],
            [['sort_order'], 'integer'],
            [['alias'], 'string', 'max' => 128],
            [['title'], 'string', 'max' => 1024],
            [['alias'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'send_from' => 'Отправитель',
            'theme' => 'Тема',
            'body' => 'Содержимое',
            'title' => 'Название',
            'description' => 'Описание',
            'sort_order' => 'Sort Order',
            'help' => 'Help',
        ];
    }

}
