<?php

namespace common\models;

use yii\helpers\Url;
use Yii;

/**
 * This is the model class for table "stead".
 *
 * @property integer $id
 * @property integer $premium
 * @property string $title
 * @property string $created_at
 * @property string $updated_at
 * @property string $description
 * @property string $src_url
 * @property string $src_html
 * @property integer $views
 * @property string $coord_lat
 * @property string $coord_lng
 * @property string $image
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_breadcrumb
 * @property string $price_from
 * @property string $price_to
 * @property string $phone
 */
class Stead extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'stead';
    }

    public function __construct($config = array()) {
        $this->on(\yii\db\ActiveRecord::EVENT_AFTER_DELETE, [$this, 'afterDeleteHandler']);

        parent::__construct($config);
    }

    public function behaviors() {
        return [
            \common\behaviors\TimestampBehavior::className(),
            #\common\behaviors\SeoPlus::className(),
            \common\behaviors\SlugGeneratorBehavior::className(),
        ];
    }

    public function afterDeleteHandler() {
        foreach ($this->images as $image) {
            $image->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title', 'description', 'subtitle', 'src_url', 'src_html'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['views', 'premium', 'mark'], 'integer'],
            [['price_from', 'price_to'], 'number'],
            [['slug'], 'string', 'max' => 256],
            [['phone'], 'string', 'max' => 256],
            [['coord_lat', 'coord_lng'], 'number'],
            [['seo_title', 'seo_h1', 'seo_description', 'seo_keywords', 'seo_breadcrumb'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'description' => 'Description',
            'src_url' => 'Src Url',
            'src_html' => 'Src Html',
            'views' => 'Views',
            'coord_lat' => 'Coord Lat',
            'coord_lng' => 'Coord Lng',
            'seo_title' => 'Seo Title',
            'seo_h1' => 'Seo H1',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'seo_breadcrumb' => 'Seo Breadcrumb',
        ];
    }

    public function getImage() {
        return $this->hasOne(SteadImage::className(), ['stead_id' => 'id']);
    }

    public function getImages() {
        return $this->hasMany(SteadImage::className(), ['stead_id' => 'id']);
    }

    public function getUrl() {
        return Url::to(['stead/view', 'id' => $this->id], TRUE);
    }

    public function priceFormated() {
        $price = "";
        if ($this->price_from) {
            $price.= " от <span class='price-value price-value-from'>" . round($this->price_from, 1) . "</span>";
        }

        if ($this->price_to) {
            $price.= " до <span class='price-value price-value-to'>" . round($this->price_to, 1) . "</span>";
        }

        return trim($price . " млн. руб.");
    }

    public function seoPlusBreadcrumbs() {
        $return = [];
        if ($seo_params = Seo::getByRoute('stead/index')) {
            $return[] = ['label' => $seo_params['breadcrumb'], 'url' => ['stead/index']];
        }

        $return[] = ['label' => $this->seoPlusBreadcrumb()];
        return $return;
    }

}
