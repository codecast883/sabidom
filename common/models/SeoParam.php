<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "seo_param".
 *
 * @property integer $id
 * @property string $field
 * @property string $template
 * @property string $field_type
 * @property integer $sort_order
 * @property integer $seo_id
 * @property string $created_at
 * @property string $updated_at
 */
class SeoParam extends \yii\db\ActiveRecord {

    public function behaviors() {
        return [
            \common\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'seo_param';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['field', 'seo_id'], 'required'],
            [['template', 'alias'], 'string'],
            [['sort_order', 'seo_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['field', 'field_type'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'field' => 'Field',
            'template' => 'Template',
            'field_type' => 'Field Type',
            'sort_order' => 'Sort Order',
            'seo_id' => 'Seo ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

}
