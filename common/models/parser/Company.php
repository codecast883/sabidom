<?php

namespace common\models\parser;

use Yii;

class Person {

    private $src_url;
    private $dom;
    private $debug = FALSE;

    public static function loadList($src_url) {
        $cache = new \yii\caching\FileCache;
        $cache_key = "parsed_firms_new_" . md5($src_url);

        if (!$firms = $cache->get($cache_key)) {
            $firms = [];
            $pages_list = [];

            $dom = \Sunra\PhpSimple\HtmlDomParser::file_get_html($src_url);
            if ($objs = $dom->find("div.mt15 > big a")) {
                $last_page_url = "";
                foreach ($objs as $obj) {
                    if ($obj->class != "hl") {
                        $last_page_url = $obj->href;
                    }
                }

                $pages_count = 0;
                if (preg_match("/\?p=(\d+)/isu", $last_page_url, $matches)) {
                    $pages_count = $matches[1];
                }

                for ($i = 1; $i <= $pages_count; $i++) {
                    $pages_list[] = $src_url . "?p=" . $i;
                }
            } else {
                $pages_list[] = $src_url . "?p=1";
            }


            foreach ($pages_list as $page) {

                $page_firms = [];
                $dom = \Sunra\PhpSimple\HtmlDomParser::file_get_html($page);
                $obj_table = $dom->find(".main > table", 11);
                $obj_rows = $obj_table->find("tr");

                $obj_rows = array_slice($obj_rows, 3);
                foreach ($obj_rows as $obj_row) {
                    if ($obj_row->find("td")) {
                        $obj_descr = $obj_row->find("td", 1);
                        $obj_a = $obj_descr->find("a", 0);
                        $page_firms[] = [
                            'title' => mb_convert_encoding($obj_a->innertext, "utf8", "cp1251"),
                            'url' => mb_convert_encoding($obj_a->href, "utf8", "cp1251"),
                        ];
                    }
                }

                $firms = array_merge($firms, $page_firms);
            }


            $cache->set($cache_key, $firms, 3600);
        }

        return $firms;
    }

    public function __construct($src_url = "") {
        if ($src_url) {
            $this->src_url = $src_url;
            $this->dom = \Sunra\PhpSimple\HtmlDomParser::file_get_html($this->src_url);
        }
    }

    public function getLogo() {
        $result = "";
        if ($description_table_obj = $this->dom->find("table table", 10)) {
            if ($obj = $description_table_obj->find("img", 0)) {
                if (preg_match("|/img/org/logo/.*|isu", $obj->src)) {
                    $result = "http://www.kleos.ru" . $obj->src;
                }
            }
        }

        return $result;
    }

    public function getTitle() {
        $result = "";
        if ($description_table_obj = $this->dom->find("table table", 10)) {
            if ($obj = $description_table_obj->find("h1", 0)) {
                $result = trim(mb_convert_encoding($obj->innertext, "utf8", "cp1251"));
            }
        }
        return $result;
    }

    public function getSubtitle() {
        $result = "";
        if ($description_table_obj = $this->dom->find("table table", 10)) {
            if ($obj = $description_table_obj->find(".pale", 0)) {
                $result = trim(mb_convert_encoding($obj->innertext, "utf8", "cp1251"));
            }
        }

        return $result;
    }

    public function getMetro() {
        $result = "";
        if ($description_table_obj = $this->dom->find("table table", 10)) {
            if ($objs = $description_table_obj->find("p.mt12 b")) {
                $temp_res = [];
                foreach ($objs as $obj) {
                    $temp_res[] = trim(mb_convert_encoding($obj->innertext, "utf8", "cp1251"));
                }

                $result = implode(", ", $temp_res);
            }
        }

        return $result;
    }

    public function getAddress() {
        $result = "";
        if ($description_table_obj = $this->dom->find("table table", 10)) {
            if ($obj = $description_table_obj->find("div span", 0)) {
                $result = trim(mb_convert_encoding($obj->innertext, "utf8", "cp1251"));
            }
        }

        return $result;
    }

    public function getPhones() {
        $result = "";
        if ($objs = $this->dom->find("#hide_phones p")) {
            $temp_res = [];
            foreach ($objs as $obj) {
                $temp_res[] = trim(mb_convert_encoding($obj->innertext, "utf8", "cp1251"));
            }

            $result = implode("\n", $temp_res);
        }
        return $result;
    }

    public function getReviewsUrl() {
        $result = "";
        if ($reviews_link = $this->dom->find(".reviews .all a", 0)) {
            $result = "http://www.medkrug.ru" . $reviews_link->href;
        }

        return $result;
    }

    public function getDescription() {
        $result = "";
        if ($obj = $this->dom->find(".instruction", 0)) {
            $temp = $obj->find("h2", 0);
            $temp->outertext = "";

            $temp = $obj->find(".add_text", 0);
            $temp->outertext = "";

            $temp = $obj->find("img", 0);
            $temp->outertext = "";

            $result = $obj->innertext;
        }
        return $result;
    }

    public function getDescriptionImages() {
        $result = [];

        if ($objs = $this->dom->find("td.main div")) {
            foreach ($objs as $obj) {
                if ($obj->style == "clear: both; margin-top: 2em; font-family: Arial,sans-serif; font-size: 115%; overflow:hidden") {
                    if ($images_obj = $obj->find("img")) {
                        foreach ($images_obj as $img_obj) {
                            $result[] = "http://www.kleos.ru" . $img_obj->src;
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function getSite() {
        $result = "";
        if ($description_table_obj = $this->dom->find("table table", 11)) {
            if ($objs = $description_table_obj->find("a")) {
                foreach ($objs as $obj) {
                    if ($obj->target = '_blank' AND preg_match("%http://(.*)%isu", $obj->href, $matches)) {
                        $result = $matches[1];
                    }
                }
            }
        }
        return $result;
    }

    public function getPeoples() {
        $result = [];

        if ($description_table_obj = $this->dom->find("table.bgpale", 0)) {
            if ($objs = $description_table_obj->find("a")) {
                foreach ($objs as $obj) {
                    if ($image = $obj->find("img", 0)) {
                        $result[$obj->href]['url'] = "http://www.kleos.ru" . $obj->href;
                        $result[$obj->href]['subtitle'] = trim($image->alt);
                    } else {
                        $result[$obj->href]['url'] = "http://www.kleos.ru" . $obj->href;
                        $result[$obj->href]['title'] = trim(mb_convert_encoding($obj->innertext, "utf8", "cp1251"));
                    }
                }
            }
        }

        foreach ($result as &$res) {
            if (isset($res['subtitle'])) {
                $res['subtitle'] = preg_replace("%" . preg_quote($res['title'], "%") . "%isu", "", $res['subtitle']);
                $res['subtitle'] = trim($res['subtitle'], " -");
            }
        }

        return $result;
    }

    private function _getReviewsFromJson($url) {
        $months_trans = [
            "января" => 1,
            "февраля" => 2,
            "марта" => 3,
            "апреля" => 4,
            "мая" => 5,
            "июня" => 6,
            "июля" => 7,
            "августа" => 8,
            "сентября" => 9,
            "октября" => 10,
            "ноября" => 11,
            "декабря" => 12,
        ];

        $result = [
            'reviews' => [],
            'next_page' => FALSE
        ];

        $json = json_decode(file_get_contents($url));

        $result['next_page'] = $json->next_page;

        if ($this->debug)
            print $url . "<br>";

        foreach ($json->results as $review) {
            $date = "";

            if (preg_match('/(\d+) (\w+)( \d+)?/isu', $review->date, $regs)) {
                $day = $regs[1];
                $month = isset($months_trans[$regs[2]]) ? $months_trans[$regs[2]] : "";

                if ($month <= 8) {
                    $years = [2015, 2014, 2013];
                } else {
                    $years = [2014, 2013];
                }

                $year = $years[array_rand($years)];

                $date = $year . "-" . $month . "-" . $day;


                if ($this->debug)
                    print $review->date . " - " . $date . "<br>";

                if (!$day OR ! $month OR ! $year) {
                    print "ошибка разбора даты";
                    print_r($review);
                    exit;
                }
            } else {
                print $review->date;
                exit;
            }

            $review = [
                'author' => trim($review->owner_name),
                'rating' => $review->star,
                'text' => trim($review->comment),
                'datePublished' => $date,
            ];

            if ($review['text'] AND $review['author']) {
                $result['reviews'][] = $review;
            }
        }

        return $result;
    }

    public function getReviews($reviews_url) {
        $result = [];

        $page_id = NULL;
        if (preg_match('/\d+$/sim', $reviews_url, $regs)) {
            $page_id = $regs[0];
        }

        $page = 1;

        $answer['next_page'] = TRUE;
        for ($page = 1; $page < 100 AND $answer['next_page'] == TRUE; $page++) {


            $url = "http://www.medkrug.ru/medicament/get_json_opinion?page=" . $page . "&id=" . $page_id . "&community_id=0";

            $answer = $this->_getReviewsFromJson($url, $page);
            $result+= $answer['reviews'];
        }

        return $result;
    }

}
