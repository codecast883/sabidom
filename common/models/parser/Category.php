<?php

namespace common\models\parser;

use Yii;

class ategory {

    private $src_url;
    private $dom;

    public static function loadList($src_url) {
        $products = [];
        $dom = \Sunra\PhpSimple\HtmlDomParser::file_get_html($src_url);
        if ($objs = $dom->find(".product-forms tr")) {
            foreach ($objs as $obj) {
                $url_obj = $obj->find("h4 a", 0);

                $product = [];
                $product['url'] = "http://www.medkrug.ru" . $url_obj->href;
                $product['title'] = $url_obj->innertext;
                $product['img'] = "";
                if ($img_obj = $obj->find(".product-photo img", 0)) {
                    $product['img'] = $img_obj->src;

                    if (preg_match("|medic_no_photo_big|isu", $product['img'])) {
                        $product['img'] = "";
                    }else{
                        $product['img'] = preg_replace("|94_94_2_|isu", "128_128_2_", $product['img']);
                    }
                }

                $products[] = $product;
            }
        }

        return $products;
    }

    public function __construct($src_url) {
        $this->src_url = $src_url;
        $this->dom = \Sunra\PhpSimple\HtmlDomParser::file_get_html($this->src_url);
    }

    public function getLogo() {
        $result = "";
        if ($obj = $this->dom->find(".brand-icon img", 0)) {
            $result = $obj->src;
        }

        return $result;
    }

    public function getDescription() {
        $result = "";
        if ($obj = $this->dom->find(".b-about__preambule", 0)) {
            $result = $obj->innertext;
            $result = strip_tags($result, "<p><br>");
        }
        return $result;
    }

}
