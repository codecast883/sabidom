<?php

namespace common\models\parser;

use Yii;

class Modification {

    private $src_url;
    private $dom;

    public static function loadLinks($body_url) {
        $result = [];
        
        if ($html = @file_get_contents($body_url)) {
            $dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);
            if($links = $dom->find(".au-mod-body__title a, .au-mod-equip__table a")){
                foreach ($links as $link){
                    $result[$link->href] = $link->href;
                }
            }
        } else {
            return false;
        }
        
        return $result;
    }

    public function __construct($src_url) {
        $this->src_url = $src_url;
    }

    public function load() {
        if ($html = @file_get_contents($this->src_url)) {
            $this->dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);
            return true;
        } else {
            return false;
        }
    }

    public function getFeatures() {
        $result = [];

        if ($blocks = $this->dom->find(".au-tabs-tab .au-technical-data .au-technical-data__block")) {
            foreach ($blocks as $block) {
                $block_label = $block->find("b", 0);

                if ($params = $block->find(".au-technical-data__row")) {
                    foreach ($params as $param) {
                        $result[$block_label->innertext][] = [
                            'title' => $param->find(".au-technical-data__cell", 0)->innertext,
                            'value' => $param->find(".au-technical-data__cell", 1)->innertext,
                        ];
                    }
                }
            }
        }

        return $result;
    }

    public function getTitle() {
        $result = "";

        if ($obj = $this->dom->find(".breadcrumbs span[data-object='modification']", 0)) {
            $result = $obj->innertext;
        }
        
        return $result;
    }
    
    public function getSubtitle() {
        $result = "";

        if ($obj = $this->dom->find(".breadcrumbs span[data-object='equipment']", 0)) {
            $result = $obj->innertext;
        }

        return $result;
    }

    public function getOptions() {
        $result = [];

        if ($blocks = $this->dom->find(".au-tabs-tab .au-options .au-options__block")) {
            foreach ($blocks as $block) {
                $block_label = $block->find("b", 0);

                if ($params = $block->find(".au-options__list .au-options__item")) {
                    foreach ($params as $param) {

                        if ($param->find("i.au-sprite-options-installed", 0)) {
                            $result[$block_label->innertext][] = [
                                'value' => "installed",
                                'title' => $param->find(".au-options__text", 0)->innertext,
                            ];
                        } elseif ($param->find("i.au-sprite-options-additional", 0)) {
                            $result[$block_label->innertext][] = [
                                'value' => "additional",
                                'title' => $param->find(".au-options__text", 0)->innertext,
                            ];
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function getSizeImages() {
        $result = [];
        if ($links = $this->dom->find(".au-card-dimensions__block img")) {
            foreach ($links as $link) {
                $result[] = $link->getAttribute("data-original");
            }
        }

        return $result;
    }

}
