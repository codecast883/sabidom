<?php

namespace common\models\parser;

use Yii;

class Model {
    
    private $src_url;
    private $dom;
    
    public static function loadList($brand_id) {
        $src_url = "http://catalog.am.ru/int/controls/get-crumb-sub-menu/?object=model&category=catalog&brand=" . $brand_id . "&all=true";
        
        $src_json = file_get_contents($src_url);
        $src = json_decode($src_json);
        
        $result = [];
        if (isset($src->data) AND is_array($src->data)) {
            foreach ($src->data as $brand) {
                $result[] = [
                    'id' => $brand->id,
                    'title' => $brand->value,
                    'url' => $brand->link,
                ];
            }
        }
        
        return $result;
    }
    
    public function __construct($src_url) {
        $this->src_url = $src_url;
        $this->dom = \Sunra\PhpSimple\HtmlDomParser::file_get_html($this->src_url);
    }
    
    public function getDescription() {
        $result = "";
        if ($obj = $this->dom->find(".b-about__preambule", 0)) {
            $result = $obj->innertext;
            $result = strip_tags($result, "<p><br>");
        }
        return $result;
    }

}
