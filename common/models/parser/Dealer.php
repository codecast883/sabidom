<?php

namespace common\models\parser;

use Yii;

class Dealer {

    private $src_url;
    private $dom;

    public static function loadDealers() {
        $src_url = "http://msk.am.ru/int/controls/get-dealers-ajax/?brand=&area=30.522693004050154%2C11.759364356065422%2C81.13504120032603%2C165.04061435606545";
        $src_json = file_get_contents($src_url);
        $src_dealers = json_decode($src_json);

        $result = [];
        if (!empty($src_dealers) AND is_array($src_dealers)) {
            foreach ($src_dealers as $src_dealer) {
                $dealer = [];
                $dealer['title'] = $src_dealer->title;
                $dealer['id'] = $src_dealer->id;
                $dealer['url'] = $src_dealer->link;
                $dealer['image'] = "";

                if ($src_dealer->person) {
                    $dealer['dealer_network'] = $src_dealer->person->title;
                } else {
                    $dealer['dealer_network'] = "";
                }

                $dealer['official'] = preg_match("/официальный/isu", $src_dealer->official);

                $dealer['city'] = $src_dealer->city;
                $dealer['address'] = $src_dealer->address;
                if ($src_dealer->coords) {
                    $dealer['coordinates'] = $src_dealer->coords[0] . "," . $src_dealer->coords[1];
                }

                $dealer['phones'] = [];
                foreach ($src_dealer->phones as $phone) {
                    $dealer['phones'][] = [
                        'phone' => $phone->phone,
                        'title' => $phone->title,
                    ];
                }

                $dealer['work_hours'] = [];
                foreach ($src_dealer->workHours as $key => $value) {
                    $dealer['work_hours'][] = $key . ": " . $value;
                }
                $dealer['work_hours'] = implode("\n", $dealer['work_hours']);

                $dealer['brands_src_id'] = [];
                foreach ($src_dealer->brandIds as $brand) {
                    $dealer['brands_src_id'][] = $brand;
                }

                $dealer['has_new'] = (bool) $src_dealer->hasNew;
                $dealer['has_used'] = (bool) $src_dealer->hasUsed;

                $dealer['site'] = "";
                $dealer['description'] = "";

                $result[] = $dealer;
            }
        }

        return $result;
    }

    public function __construct($src_url) {
        $this->src_url = $src_url;
    }

    public function load() {
        if ($html = @file_get_contents($this->src_url)) {
            $this->dom = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);

            return true;
        } else {
            return false;
        }
    }

    public function getDescription() {
        $result = "";
        if ($obj = $this->dom->find("#au-tab-info .main-wrapper", 0)) {
            if ($obj->innertext) {
                $result = $obj->innertext;
                $result = strip_tags($result, "<p><br>");
            }
        }

        return $result;
    }

    public function getLogo() {
        $result = "";
        if ($obj = $this->dom->find("#js-salon-card .b-new-card-image", 0)) {
            if ($obj->src) {
                $result = $obj->src;
            }
        }

        return $result;
    }

    public function getSite() {
        $result = "";

        if ($obj = $this->dom->find("#js-salon-card .au-link", 0)) {
            if ($obj->href) {
                $result = $obj->href;
                $result = trim($result, "/");
                if (preg_match('%(http://.*?)/%sim', $result, $matches)) {
                    $result = $matches[1];
                }
            }
        }



        return $result;
    }

//
//    public function getBodyThumbs() {
//        $result = [];
//        if ($links = $this->dom->find(".au-preview-box .au-preview-list__link")) {
//            foreach ($links as $link) {
//                if ($image = $link->find(".au-preview-list__img", 0)) {
//                    $result[] = [
//                        'href' => $link->href,
//                        'img' => $image->src,
//                    ];
//                }
//            }
//        }
//
//        return $result;
//    }
}
