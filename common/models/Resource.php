<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "resource".
 *
 * @property string $uuid
 * @property string $file_name
 * @property string $xml_name
 * @property string $description
 * @property integer $sort_order
 */
class Resource extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resource';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid', 'file_name'], 'required'],
            [['description'], 'string'],
            [['sort_order'], 'integer'],
            [['uuid'], 'string', 'max' => 36],
            [['file_name', 'xml_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uuid' => 'Uuid',
            'file_name' => 'File Name',
            'xml_name' => 'Xml Name',
            'description' => 'Description',
            'sort_order' => 'Sort Order',
        ];
    }
}
