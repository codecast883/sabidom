<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "village_image".
 *
 * @property integer $id
 * @property integer $village_id
 * @property string $image
 * @property integer $sort_order
 */
class VillageTownhouseImage extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'village_townhouse_image';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['village_townhouse_id', 'image'], 'required'],
            [['village_townhouse_id', 'sort_order'], 'integer'],
            [['image'], 'file', 'extensions' => ['jpg', 'gif', 'png']],
        ];
    }

    public function getVillage_id() {
        return $this->villageTownhouse->id;
    }

    public function getVillageTownhouse() {
        return $this->hasOne(VillageTownhouse::className(), ['id' => 'village_townhouse_id']);
    }

    public function behaviors() {
        return [
            'file-upload' => [
                'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
                'replacePairs' => [
                    '<modelId>' => 'id',
                    '<village_townhouse_id>' => 'village_townhouse_id',
                    '<village_id>' => 'village_id'
                ],
                'attributes' => [
                    'image' => [
                        'group_by_attr' => 'village_id',
                        'group_folder_count' => 1000,
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/village/<group_folder>/<village_id>/townhouse-<village_townhouse_id>/gallery/<modelId>.<fileExtension>',
                        'url' => '/img/village/<group_folder>/<village_id>/townhouse-<village_townhouse_id>/gallery/<modelId>.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
                        'saveOptions' => ['quality' => 90],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'village_id' => 'Realt ID',
            'image' => 'Image',
            'sort_order' => 'Sort Order',
        ];
    }

}
