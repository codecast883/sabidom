<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "field_tag".
 *
 * @property integer $id
 * @property integer $field_id
 * @property string $title
 */
class FieldTag extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'field_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['field_id', 'value'], 'required'],
            [['field_id'], 'integer'],
            [['value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'field_id' => 'Field ID',
        ];
    }

    public static function autoGet($field_id, $value) {
        $value = trim($value);

        if (!$model = self::find()->where(['field_id' => $field_id])->andWhere(["value" => $value])->one()) {
            $model = new self;
            $model->value = $value;
            $model->field_id = $field_id;
            if (!$model->save()) {
                print_r($model->getErrors());
                print_r($model);
                exit;
            }
        }

        return $model;
    }

    public static function itemAlias($type, $code = NULL) {
        $_items = array(
                //'status' => array(
                //       self::STATUS_DISABLED => 'выкл',
                //       self::STATUS_ENABLED => 'вкл',
                //),
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function getField() {
        return $this->hasOne(Field::className(), ['id' => 'field_id']);
    }

}
