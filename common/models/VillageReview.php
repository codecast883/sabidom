<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "company_review".
 *
 * @property integer $id
 * @property integer $village_id
 * @property string $name
 * @property string $text
 * @property integer $rating_1
 * @property integer $rating_2
 * @property integer $rating_3
 * @property integer $rating_4
 * @property integer $rating_5
 * @property integer $rating_avg
 * @property integer $profit_yes
 * @property integer $profit_no
 * @property integer $status
 */
class VillageReview extends \yii\db\ActiveRecord {

	const STATUS_DISABLED = 0;
	const STATUS_ENABLED = 1;
	const STATUS_WAIT = 2;
	const IS_NEW_YES = 1;
	const IS_NEW_NO = 0;

	public $captcha;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'village_review';
	}

	public function behaviors() {
		return [
			\common\behaviors\TimestampBehavior::className(),
		];
	}

	public function __construct($config = array()) {
		$this->on(\yii\db\ActiveRecord::EVENT_BEFORE_INSERT, [$this, 'beforeInsertHandler']);

		$this->on(\yii\db\ActiveRecord::EVENT_BEFORE_INSERT, [$this, 'beforeUpdateHandler']);
		$this->on(\yii\db\ActiveRecord::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdateHandler']);
		$this->on(\yii\db\ActiveRecord::EVENT_AFTER_UPDATE, [$this, 'afterUpdateHandler']);
		$this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, [$this, 'afterUpdateHandler']);
		$this->on(\yii\db\ActiveRecord::EVENT_BEFORE_VALIDATE, [$this, 'beforeValidateHandler']);
		parent::__construct($config);
	}

	protected function beforeValidateHandler() {
		$this->name = strip_tags($this->name);
		$this->text = strip_tags($this->text);
	}

	protected function fillSeoVariables() {
		$this->_seo_replaces = [
			'компания: название' => $this->company->title,
			'компания: подзаголовок' => $this->company->subtitle,
			'id' => $this->id,
		];
	}

	public function beforeInsertHandler() {
		$this->ip = Yii::$app->getRequest()->getUserIP();
		$this->user_agent = Yii::$app->getRequest()->userAgent;

		if ($this->status === NULL) {
			$this->status = self::STATUS_ENABLED;
		}

		if ($this->is_new === NULL) {
			$this->is_new = self::IS_NEW_YES;
		}
	}

	public function beforeUpdateHandler() {
		$this->rating_avg = 0;
		$count = 0;
		if ($this->rating_1 > 0) {
			$count++;
			$this->rating_avg += $this->rating_1;
		}
		if ($this->rating_2 > 0) {
			$count++;
			$this->rating_avg += $this->rating_2;
		}
		if ($this->rating_3 > 0) {
			$count++;
			$this->rating_avg += $this->rating_3;
		}
		if ($this->rating_4 > 0) {
			$count++;
			$this->rating_avg += $this->rating_4;
		}
		if ($this->rating_5 > 0) {
			$count++;
			$this->rating_avg += $this->rating_5;
		}

		if ($count > 0) {
			$this->rating_avg = round($this->rating_avg / $count, 2);
		}
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['village_id', 'name', 'text'], 'required'],
			[['village_id', 'rating_1', 'rating_2', 'rating_3', 'rating_4', 'rating_5', 'rating_avg', 'profit_yes', 'profit_no', 'status', 'user_id'], 'integer'],
			[['text'], 'string'],
			[['name'], 'string', 'max' => 128],
			[['created_at', 'updated_at'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'parent_id' => 'Ответ на отзыв',
			'company_id' => 'Компания',
			'name' => 'Имя',
			'text' => 'Сообщение',
			'rating' => 'Рейтинг',
			'profit_yes' => 'Полезный: +',
			'profit_no' => 'Полезный: -',
			'status' => 'Статус',
			'is_new' => 'Новый',
			'captcha' => 'Проверочный код',
		];
	}

	public static function itemAlias($type, $code = NULL) {
		$_items = array(
			'status' => array(
				self::STATUS_DISABLED => 'выкл',
				self::STATUS_ENABLED => 'вкл',
				self::STATUS_WAIT => 'ожидает',
			),
			'is_new' => array(
				0 => 'нет',
				1 => 'да'
			)
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}

	public function getVillage() {
		return $this->hasOne(Village::className(), ['id' => 'village_id']);
	}

	public function can($action) {
		if ($action == "manage") {
			if (\Yii::$app->user->can("front-company-manager")) {
				return true;
			}

			if (\Yii::$app->user->can("front-company-owner")) {
				if ($this->company->user_id == \Yii::$app->user->identity->getId()) {
					return true;
				}
			}
		} elseif ($action == "answer") {
			if (\Yii::$app->user->can("front-company-manager")) {
				return true;
			}

			if (\Yii::$app->user->can("front-company-owner")) {
				if ($this->company->user_id == \Yii::$app->user->identity->getId()) {
					return true;
				}
			}
		} elseif ($action == "toggle-status") {
			if (\Yii::$app->user->can("front-company-manager")) {
				return true;
			}

			if (\Yii::$app->user->can("front-company-owner")) {
				if ($this->company->user_id == \Yii::$app->user->identity->getId()) {
					if (\Yii::$app->user->identity->paidExpiredDays() <= 0) {
						return false;
					} else {
						return TRUE;
					}
				}
			}
		}

		return false;
	}

	public function afterUpdateHandler() {
//        $this->company->trigger(Company::TRIGGER_REVIEWS_CHANGED);
	}

}
