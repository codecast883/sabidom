<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "realt_image".
 *
 * @property integer $id
 * @property integer $realt_id
 * @property string $image
 * @property integer $sort_order
 */
class RealtImage extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'realt_image';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['realt_id', 'image'], 'required'],
            [['realt_id', 'sort_order'], 'integer'],
            [['image'], 'file', 'extensions' => ['jpg', 'gif', 'png']],
        ];
    }

    public function behaviors() {
        return [
            'file-upload' => [
                'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
                'replacePairs' => [
                    '<modelId>' => 'id',
                    '<realt_id>' => 'realt_id',
                ],
                'attributes' => [
                    'image' => [
                        'group_by_attr' => 'realt_id',
                        'group_folder_count' => 1000,
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/realt-image/<group_folder>/<realt_id>/gallery/<modelId>.<fileExtension>',
                        'url' => '/img/realt-image/<group_folder>/<realt_id>/gallery/<modelId>.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
                        'saveOptions' => ['quality' => 90],
                        'thumbs' => [
                            'small' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/realt-image/<group_folder>/<realt_id>/gallery/<modelId>-small.<fileExtension>',
                                'url' => '/img/cache/realt-image/<group_folder>/<realt_id>/gallery/<modelId>-small.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 100, 100, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 80,
                                ],
                            ],
                            'middle' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/realt-image/<group_folder>/<realt_id>/gallery/<modelId>-middle.<fileExtension>',
                                'url' => '/img/cache/realt-image/<group_folder>/<realt_id>/gallery/<modelId>-middle.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 400, 300, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 70,
                                ],
                            ],
                            'large' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/realt-image/<group_folder>/<realt_id>/gallery/<modelId>-large.<fileExtension>',
                                'url' => '/img/cache/realt-image/<group_folder>/<realt_id>/gallery/<modelId>-large.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 770, 516, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 80,
                                ],
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'realt_id' => 'Realt ID',
            'image' => 'Image',
            'sort_order' => 'Sort Order',
        ];
    }

}
