<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "parse_pages".
 *
 * @property integer $id
 * @property string $url
 * @property integer $mark
 * @property integer $type_id
 */
class ParsePages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parse_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url'], 'string'],
            [['mark', 'type_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'mark' => 'Mark',
            'type_id' => 'Type ID',
        ];
    }
    
    public static function itemAlias($type, $code = NULL) {
            $_items = array(
                //'status' => array(
                //       self::STATUS_DISABLED => 'выкл',
                //       self::STATUS_ENABLED => 'вкл',
                //),
            );

            if (isset($code))
                    return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
            else
                    return isset($_items[$type]) ? $_items[$type] : false;
    }
    
}
