<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use himiklab\sitemap\behaviors\SitemapBehavior;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $title
 * @property string $seo_h1
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_title
 * @property string $seo_breadcrumb
 * @property string $description
 * @property string $description_short
 * @property string $created_at
 * @property string $updated_at
 * @property integer $on_top
 * @property integer $on_home
 * @property string $image
 * @property integer $status
 */
class Action extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'action';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['seo_breadcrumb', 'description', 'description_short', 'image'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['on_top', 'on_home', 'status'], 'integer'],
            [['title', 'seo_h1', 'seo_description', 'seo_keywords', 'seo_title'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'seo_h1' => 'Seo H1',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'seo_title' => 'Seo Title',
            'seo_breadcrumb' => 'Seo Breadcrumb',
            'description' => 'Description',
            'description_short' => 'Description Short',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'on_top' => 'On Top',
            'on_home' => 'On Home',
            'image' => 'Image',
            'status' => 'Status',
        ];
    }

    public function getUrl() {
        return Url::to(['/action/view', 'id' => $this->id], TRUE);
    }

    public function behaviors() {
        return [
            \common\behaviors\TimestampBehavior::className(),
            'file-upload' => [
                'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/action/<modelId>.<fileExtension>',
                        'url' => '/img/action/<modelId>.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
                        'saveOptions' => ['quality' => 90],
                        'thumbs' => [
                            'small' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/action/<modelId>-small.<fileExtension>',
                                'url' => '/img/cache/action/<modelId>-small.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 280, 157, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 80,
                                ],
                            ],
                        ]
                    ]
                ],
            ],
                                        'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    $model->select(['id', 'updated_at']);
                    $model->andWhere(['status' => 1]);
                },
                        'dataClosure' => function ($model) {
                    return [
                        'loc' => $model->url,
                        'lastmod' => strtotime($model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
                    ],
        ];
    }

}
