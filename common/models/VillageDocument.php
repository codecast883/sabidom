<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "village_progress".
 *
 * @property integer $id
 * @property integer $village_id
 * @property string $image
 * @property integer $sort_order
 */
class VillageDocument extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'village_document';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['village_id', 'file'], 'required'],
            [['village_id', 'sort_order'], 'integer'],
            [['title'], 'string'],
            [['file'], 'file'],
        ];
    }

    public function behaviors() {
        return [
            'file-upload' => [
                'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
                'replacePairs' => [
                    '<modelId>' => 'id',
                    '<village_id>' => 'village_id',
                ],
                'attributes' => [
                    'file' => [
                        'group_by_attr' => 'village_id',
                        'group_folder_count' => 1000,
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/village/<group_folder>/<village_id>/document/<modelId>.<fileExtension>',
                        'url' => '/img/village/<group_folder>/<village_id>/document/<modelId>.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_FILE,
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'village_id' => 'Поселок',
            'file' => 'Файл',
            'sort_order' => 'Сортировка',
        ];
    }

}
