<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Realt;

/**
 * RealtSearch represents the model behind the search form about `common\models\Realt`.
 */
class RealtSearch extends Realt
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mark', 'price', 'date', 'type_id', 'local_id', 'views', 'premium', 'village_id'], 'integer'],
            [['created_at', 'updated_at', 'title', 'address', 'description', 'phone', 'subtitle', 'seller', 'src_url', 'src_html', 'coord_lat', 'coord_lng', 'agent_name', 'description_bottom', 'seo_title', 'seo_h1', 'seo_description', 'seo_keywords'], 'safe'],
            [['stead_area', 'house_area'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Realt::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'mark' => $this->mark,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'price' => $this->price,
            'date' => $this->date,
            'type_id' => $this->type_id,
            'local_id' => $this->local_id,
            'views' => $this->views,
            'premium' => $this->premium,
            'stead_area' => $this->stead_area,
            'house_area' => $this->house_area,
            'village_id' => $this->village_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'subtitle', $this->subtitle])
            ->andFilterWhere(['like', 'seller', $this->seller])
            ->andFilterWhere(['like', 'src_url', $this->src_url])
            ->andFilterWhere(['like', 'src_html', $this->src_html])
            ->andFilterWhere(['like', 'coord_lat', $this->coord_lat])
            ->andFilterWhere(['like', 'coord_lng', $this->coord_lng])
            ->andFilterWhere(['like', 'agent_name', $this->agent_name])
            ->andFilterWhere(['like', 'description_bottom', $this->description_bottom])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_h1', $this->seo_h1])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords]);

        return $dataProvider;
    }
}
