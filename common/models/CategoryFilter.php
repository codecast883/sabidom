<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category_filter".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $field_id
 * @property string $field_operand
 * @property integer $field_tag_id
 * @property string $field_value
 */
class CategoryFilter extends \yii\db\ActiveRecord {

    const TYPE_FIELD_TEXT = 1;
    const TYPE_FIELD_TAG = 2;
    const TYPE_FIELD_FROM_TO = 3;
    const TYPE_CUSTOM = 10;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'category_filter';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['category_id', 'type_id'], 'required'],
            [['category_id'], 'integer'],
            [['operand', 'field'], 'string', 'max' => 10],
            [['value'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'field_id' => 'Field ID',
            'field_operand' => 'Field Operand',
            'field_tag_id' => 'Field Tag ID',
            'field_value' => 'Field Value',
        ];
    }

    public function getField() {
        return $this->hasOne(Field::className(), ['id' => 'field_id']);
    }

    public function getField_tag() {
        return $this->hasOne(FieldTag::className(), ['id' => 'field_tag_id']);
    }

    public static function getFilter($category_id) {
        $filter = [
            self::TYPE_CUSTOM => [],
            self::TYPE_FIELD_TEXT => [],
            self::TYPE_FIELD_FROM_TO => [],
            self::TYPE_FIELD_TAG => [],
        ];

        foreach (self::find()->where(['category_id' => $category_id])->all() as $rule) {
            if ($rule->type_id == self::TYPE_CUSTOM) {
                if ($rule->field == "price") {
                    $value = explode("-", $rule->value);
                    $filter[self::TYPE_CUSTOM]['price'] = [
                        'from' => ArrayHelper::getValue($value, 0),
                        'to' => ArrayHelper::getValue($value, 1),
                    ];
                } elseif ($rule->field == "house_area") {
                    $value = explode("-", $rule->value);
                    $filter[self::TYPE_CUSTOM]['house_area'] = [
                        'from' => ArrayHelper::getValue($value, 0),
                        'to' => ArrayHelper::getValue($value, 1),
                    ];
                } elseif ($rule->field == "stead_area") {
                    $value = explode("-", $rule->value);
                    $filter[self::TYPE_CUSTOM]['stead_area'] = [
                        'from' => ArrayHelper::getValue($value, 0),
                        'to' => ArrayHelper::getValue($value, 1),
                    ];
                }
            } elseif ($rule->type_id == self::TYPE_FIELD_TEXT) {
                $filter[self::TYPE_FIELD_TEXT][$rule->field] = $rule->value;
            } elseif ($rule->type_id == self::TYPE_FIELD_TAG) {
                $filter[self::TYPE_FIELD_TAG][$rule->field][] = $rule->value;
            } elseif ($rule->type_id == self::TYPE_FIELD_FROM_TO) {
                $value = explode("-", $rule->value);
                $filter[self::TYPE_FIELD_FROM_TO][$rule->field] = [
                    'from' => ArrayHelper::getValue($value, 0),
                    'to' => ArrayHelper::getValue($value, 1),
                ];
            }
        }

        return $filter;
    }

}
