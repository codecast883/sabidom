<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Category;

/**
 * CategorySearch represents the model behind the search form about `common\models\Category`.
 */
class CategorySearch extends Category {

    public $parent_title;
    public $filter_exist;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'parent_id'], 'integer'],
            [['title', 'slug'], 'safe'],
            [['parent_title', 'filters_count'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Category::find();
        //$query->joinWith(['parent'], TRUE, "RIGHT JOIN");
        $query->joinWith(['parent']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'parent_title' => [
                    'asc' => [
                        'parent.title' => SORT_ASC
                    ],
                    'desc' => [
                        'parent.title' => SORT_DESC
                    ],
                    'label' => 'Parent Name',
                    'default' => SORT_ASC
                ],
                'title'
            ]
        ]);

        
        $query->andFilterWhere([
            'category.id' => $this->id,
            'category.filters_count' => $this->filters_count,
            'category.parent_id' => $this->parent_id,
        ]);

        $query->andFilterWhere(['like', 'category.title', $this->title])
                ->andFilterWhere(['like', 'category.slug', $this->slug]);


        if ($this->parent_title) {
            $query->joinWith(['parent' => function ($q) {
                    $q->where('parent.title LIKE "%' . $this->parent_title . '%" ');
                }]);
        }

        return $dataProvider;
    }

}
