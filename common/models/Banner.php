<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $code
 * @property string $html
 * @property string $created_at
 * @property string $updated_at
 * @property string $position_id
 * @property integer $status
 */
class Banner extends \yii\db\ActiveRecord {

    static $positions = [
        'home_top' => 'главная - верх',
        'home_left' => 'главная - колонка',
        'realt_left' => 'объявление - колонка',
        'realt_center_bottom' => 'объявление - центр, низ',
        'village_left' => 'поселок - колонка',
        'village_center_bottom' => 'поселок - центр, низ',
        'category_village_center_2' => 'категория(частные) - центр 2',
        'category_realt_center_2' => 'категория(поселки) - центр 2',
        'footer' => 'футер',
    ];

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['position_id'], 'required'],
            [['code', 'html'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['position_id'], 'string', 'max' => 48],
            ['status', 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'html' => 'Html',
            'position_id' => 'position_id',
        ];
    }

    public static function itemAlias($type, $code = NULL) {
        $_items = [
            'position_id' => self::$positions,
            'status' => [
                self::STATUS_DISABLED => "выкл",
                self::STATUS_ENABLED => "вкл"
            ],
        ];


        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function behaviors() {
        return [
            \common\behaviors\TimestampBehavior::className(),
        ];
    }
}
