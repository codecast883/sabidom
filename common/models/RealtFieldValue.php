<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "village_field_value".
 *
 * @property integer $id
 * @property integer $realt_id
 * @property integer $model_type_id
 * @property integer $field_id
 * @property integer $field_tag_id
 * @property string $template
 * @property double $value_from
 * @property double $value_to
 */
class RealtFieldValue extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'realt_field_value';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['realt_id', 'field_id'], 'required'],
            [['realt_id', 'field_id', 'field_tag_id'], 'integer'],
            [['value_from', 'value_to', 'distance'], 'number'],
            [['value'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'field_id' => 'Field ID',
            'field_tag_id' => 'Field Tag ID',
            'value' => 'Value',
            'value_from' => 'Value From',
            'value_to' => 'Value To',
        ];
    }

    public function beforeValidateHandler() {
        if ($this->field_tag_id AND $tag = FieldTag::findOne($this->field_tag_id)) {
            $this->value = $tag->value;
        }
    }

    public function getField() {
        return $this->hasOne(Field::className(), ['id' => 'field_id']);
    }

    public static function itemAlias($type, $code = NULL) {
        $_items = array(
                //'status' => array(
                //       self::STATUS_DISABLED => 'выкл',
                //       self::STATUS_ENABLED => 'вкл',
                //),
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function __construct($config = array()) {
        $this->on(\yii\db\ActiveRecord::EVENT_BEFORE_VALIDATE, [$this, 'beforeValidateHandler']);
    }

    public function getFormated() {
        $string = "";
        $templates = explode("|", $this->field->template ? $this->field->template : "%s");
        if ($this->field->type_id == Field::TYPE_FROM_TO) {
            if (!empty($this->value_from) AND ! empty($this->value_to)) {
                $string = sprintf(ArrayHelper::getValue($templates, 2), $this->value_from, $this->value_to);
            } elseif (!empty($this->value_from)) {
                $string = sprintf(ArrayHelper::getValue($templates, 0), $this->value_from);
            } elseif (!empty($this->value_to)) {
                $string = sprintf(ArrayHelper::getValue($templates, 1), $this->value_to);
            }
        } else {
            $string = sprintf(ArrayHelper::getValue($templates, 0), $this->value);
        }

        return $string;
    }

}
