<?php

namespace common\models;

use Yii;
use common\helpers\YText;

/**
 * This is the model class for table "view_request".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $text
 * @property string $item_type
 * @property string $item_id
 */
class VillageViewRequest extends \yii\db\ActiveRecord {

	public $gorod;

	public function behaviors() {
		return [
			\common\behaviors\TimestampBehavior::className(),
		];
	}

	public function __construct($config = array()) {
		$this->on(\yii\db\ActiveRecord::EVENT_BEFORE_VALIDATE, [$this, 'cleanValues']);
		$this->on(\yii\db\ActiveRecord::EVENT_BEFORE_INSERT, [$this, 'fillInfo']);

		$this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, [$this, 'sendAdminNotification']);
		$this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, [$this, 'sendOwnerNotification']);
		parent::__construct($config);
	}

	public function save($runValidation = true, $attributeNames = null) {
		if (!empty($this->gorod)) {
			if ($runValidation AND ! $this->validate())
				return FALSE;
			return TRUE;
		}

		return parent::save($runValidation, $attributeNames);
	}

	public function fillInfo() {
		$this->ip = Yii::$app->getRequest()->getUserIP();
		$this->user_agent = Yii::$app->getRequest()->userAgent;
	}

	public function cleanValues() {
		$this->name = trim(strip_tags($this->name));
		$this->text = trim(strip_tags($this->text));
		$this->email = trim(strip_tags($this->email));
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'village_view_request';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['created_at', 'updated_at'], 'safe'],
			[['village_id'], 'integer'],
			[['name', 'phone', 'village_id'], 'required'],
			[['text', 'name'], 'string'],
			[['phone', 'email'], 'string', 'max' => 256],
			['gorod', 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'created_at' => 'Дата создания',
			'updated_at' => 'Updated At',
			'name' => 'Имя',
			'phone' => 'Телефон',
			'email' => 'Email',
			'text' => 'Сообщение',
		];
	}

	public function getVillage() {
		return $this->hasOne(Village::className(), ['id' => 'village_id']);
	}

	public function sendAdminNotification() {
		$replaces = [
			"{дата}" => $this->created_at,
			"{имя}" => $this->name,
			"{телефон}" => $this->phone,
			"{email}" => $this->email,
			"{сообщение}" => $this->text,
			"{объявление_название}" => $this->village->title,
			"{объявление_ссылка}" => \yii\helpers\Html::a($this->village->title, $this->village->url),
		];

		if ($email_template = \common\models\EmailTemplate::find()->where(['alias' => "village_view_request_admin"])->one()) {
			$mail = Yii::$app->mail->compose('simple', ['message' => YText::textGenerator($email_template->body, $replaces)]);
			$mail->setSubject(YText::textGenerator($email_template->theme, $replaces));
			$mail->setFrom($email_template->send_from);
			$mail->setTo(explode(",", Settings::getByAlias('email_for_order')));
			//$mail->setTo("chuzzlik@gmail.com");
			$mail->send();
		}
	}

	public function sendOwnerNotification() {
		if ($this->village->email_for_notifications_notify AND trim($this->village->email_for_notifications)) {
			$replaces = [
				"{дата}" => $this->created_at,
				"{имя}" => $this->name,
				"{телефон}" => $this->phone,
				"{email}" => $this->email,
				"{сообщение}" => $this->text,
				"{объявление_название}" => $this->village->title,
				"{объявление_ссылка}" => \yii\helpers\Html::a($this->village->title, $this->village->url),
			];

			if ($email_template = \common\models\EmailTemplate::find()->where(['alias' => "village_view_request_owner"])->one()) {
				$mail = Yii::$app->mail->compose('simple', ['message' => YText::textGenerator($email_template->body, $replaces)]);
				$mail->setSubject(YText::textGenerator($email_template->theme, $replaces));
				$mail->setFrom($email_template->send_from);
				$mail->setTo(explode(",", $this->village->email_for_notifications));
				//$mail->setTo("chuzzlik@gmail.com");
				$mail->send();
			}
		}
	}

}
