<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "village_image".
 *
 * @property integer $id
 * @property integer $village_id
 * @property string $image
 * @property integer $sort_order
 */
class VillageImage extends \yii\db\ActiveRecord {

	public $image_load_url = NULL; // для одновременного вывода поля загрузки и через url и через файл

	public static function tableName() {
		return 'village_image';
	}

	public function __construct($config = array()) {
		$this->on(self::EVENT_BEFORE_VALIDATE, [$this, 'beforeValidateHandler'], null, FALSE);
		parent::__construct($config);
	}

	public function beforeValidateHandler() {
		if (!empty($this->image_load_url)) {
			$this->image = $this->image_load_url;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['village_id', 'image'], 'required'],
			[['village_id', 'sort_order'], 'integer'],
			[['image'], 'file', 'extensions' => ['jpg', 'gif', 'png']],
			['image_load_url', 'string']
		];
	}

	public function behaviors() {
		return [
			'file-upload' => [
				'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
				'replacePairs' => [
					'<modelId>' => 'id',
					'<village_id>' => 'village_id',
				],
				'attributes' => [
					'image' => [
						'group_by_attr' => 'village_id',
						'group_folder_count' => 1000,
						'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/village/<group_folder>/<village_id>/gallery/<modelId>.<fileExtension>',
						'url' => '/img/village/<group_folder>/<village_id>/gallery/<modelId>.<fileExtension>',
						'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
						'saveOptions' => [],
						'thumbs' => [
							'small' => [
								'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/village/<group_folder>/<village_id>/gallery/<modelId>-small.<fileExtension>',
								'url' => '/img/cache/village/<group_folder>/<village_id>/gallery/<modelId>-small.<fileExtension>',
								'imagine' => function($filename) {
									return \mihaildev\imagine\Image::thumbnail($filename, 100, 100, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
								},
								'saveOptions' => [
									'quality' => 60,
								],
							],
							'middle' => [
								'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/village/<group_folder>/<village_id>/gallery/<modelId>-middle.<fileExtension>',
								'url' => '/img/cache/village/<group_folder>/<village_id>/gallery/<modelId>-middle.<fileExtension>',
								'imagine' => function($filename) {
									return \mihaildev\imagine\Image::thumbnail($filename, 400, 300, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
								},
								'saveOptions' => [
									'quality' => 60,
								],
							],
							'large' => [
								'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/village/<group_folder>/<village_id>/gallery/<modelId>-large.<fileExtension>',
								'url' => '/img/cache/village/<group_folder>/<village_id>/gallery/<modelId>-large.<fileExtension>',
								'imagine' => function($filename) {
									return \mihaildev\imagine\Image::thumbnail($filename, 770, 516, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
								},
								'saveOptions' => [
									'quality' => 80,
								],
							],
						]
					]
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'village_id' => 'Realt ID',
			'image' => 'Image',
			'sort_order' => 'Sort Order',
		];
	}

}
