<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use himiklab\sitemap\behaviors\SitemapBehavior;

/**
 * This is the model class for table "village".
 *
 * @property integer $id
 * @property integer $premium
 * @property string $title
 * @property string $created_at
 * @property string $updated_at
 * @property string $description
 * @property string $description_price
 * @property string $description_photo
 * @property string $src_url
 * @property string $src_html
 * @property integer $views
 * @property integer $sort_order
 * @property string $coord_lat
 * @property string $coord_lng
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_breadcrumb
 * @property string $price_from
 * @property string $price_to
 * @property string $phone
 * @property string $on_top
 * @property string $on_home
 * @property string $house_area_from
 * @property string $house_area_to
 * @property string $stead_area_from
 * @property string $stead_area_to
 * @property string $master_plan
 * @property string $route_explain_auto
 * @property string $route_explain_public
 * @property string $address
 */
class Village extends \yii\db\ActiveRecord
{

	public $model_type_id = 1;
	private $_field_values_for_save = NULL;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'village';
	}

	public function getSeo_plus_autoreplaces()
	{
		$values = [
			'title' => $this->title,
			'название' => $this->title,
			'шоссе' => '',
			'поселок' => $this->title
		];

//		if ($fields = $this->getFields([9])) {
//			foreach ($fields as $field) {
//				$values['шоссе'] = $field['values_formated'] . " шоссе";
//			}
//		}

		return $values;
	}

	public function getSeo_plus_schema()
	{
		return [
			"@context" => "http://schema.org/",
			"@type" => "Product",
			"name" => $this->title,
			"image" => $this->image ? Url::to($this->image->getUploadedFileUrl('image', 'large'), TRUE) : "",
			"description" => \common\helpers\YText::wordLimiter(strip_tags($this->description), 100),
//                "aggregateRating" => [
//                    "@type" => "AggregateRating",
//                    "ratingValue" => "4.4",
//                    "reviewCount" => "89"
//                ],
//			"offers" => [
//				"@type" => "Offer",
//				"priceCurrency" => "RUB",
//				"price" => strip_tags($this->priceFormated()),
////                    "seller" => [
////                        "@type" => "Organization",
////                        "name" => "Executive Objects"
////                    ]
//			]
		];
	}

	public function __construct($config = array())
	{
		$this->on(\yii\db\ActiveRecord::EVENT_AFTER_DELETE, [$this, 'afterDeleteHandler']);

		$this->on(\yii\db\ActiveRecord::EVENT_AFTER_UPDATE, [$this, 'afterUpdateHandler']);
		$this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, [$this, 'afterUpdateHandler']);

		parent::__construct($config);
	}

	public function afterUpdateHandler()
	{
		if ($this->_field_values_for_save !== NULL) {
			$this->unlinkAll('fieldValues', TRUE);

			foreach ($this->_field_values_for_save as $field_id => $field_values) {
				foreach ($field_values as $field_value) {
					$field_value = trim($field_value);
					if (!$field_value)
						continue;

					$field = Field::findOne($field_id);
					if ($field->type_id == Field::TYPE_TEXT) {
						$value = new VillageFieldValue;
						$value->field_id = $field->id;
						$value->village_id = $this->id;
						$value->value = $field_value;
						if (!$value->save()) {
							throw new \Exception(print_r($value->getErrors(), 1));
						}
					} elseif ($field->type_id == Field::TYPE_LIST) {
						$field_tag = FieldTag::findOne($field_value);
						$value = new VillageFieldValue;
						$value->field_id = $field->id;
						$value->village_id = $this->id;
						$value->value = $field_tag->value;
						$value->field_tag_id = $field_tag->id;
						if (!$value->save()) {
							throw new \Exception(print_r($value->getErrors(), 1));
						}
					}
				}
			}
		}
	}

	public function setField_values($values)
	{
		$this->_field_values_for_save = $values;
	}

	public function getField_values()
	{
//		print "get field values";
//		exit;
//		$this->field_values = [];
//
//		foreach ($this->fieldValues as $row) {
//			if (!isset($this->field_values[$row->field_id])) {
//				$this->field_values[$row->field_id] = [];
//			}
//
//			if ($row->field->type_id == Field::TYPE_TEXT) {
//				$this->field_values[$row->field_id][] = $row->value;
//			} else {
//				$this->field_values[$row->field_id][] = $row->field_tag_id;
//			}
//		}
	}

	public function afterDeleteHandler()
	{
		foreach ($this->images as $image) {
			$image->delete();
		}

		VillageFieldValue::deleteAll(['village_id' => $this->id]);
// отзывы $this->unlinkAll('fieldValues', TRUE);
	}

	/**
	 * @inheritdoc
	 */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'description', 'description_price', 'description_photo', 'subtitle', 'src_url', 'src_html', 'route_explain_auto', 'route_explain_public', 'address', 'price_for_hundred','village_square'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['views', 'premium', 'mark', 'sort_order'], 'integer'],
            [['price_from', 'price_to'], 'number'],
            [['slug'], 'string', 'max' => 256],
            [['phone'], 'string', 'max' => 256],
            [['email_for_notifications'], 'string'],
            [['email_for_notifications_notify'], 'integer'],
            [['coord_lat', 'coord_lng'], 'number'],
            [['house_area_from', 'house_area_to', 'stead_area_from', 'stead_area_to'], 'number'],
            [['on_top', 'on_home'], 'number'],
            [['seo_title', 'seo_h1', 'seo_description', 'seo_keywords', 'seo_breadcrumb'], 'string', 'max' => 1024],
            [['master_plan'], 'file', 'extensions' => ['jpg', 'gif', 'png']],
            [['field_values'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'subtitle' => 'Подзаголовок',
            'phone' => 'Телефон',
            'price_from' => 'Цены от',
            'price_to' => 'Цены до',
            'house_area_from' => 'Площадь объектов недвижимости от',
            'house_area_to' => 'Площадь объектов недвижимости до',
            'stead_area_from' => 'Площадь участков от',
            'stead_area_to' => 'Площадь участков до',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'description' => 'Описание',
            'description_price' => 'Описание на странице цен',
            'description_photo' => 'Описание на странице фотографий',
            'src_url' => 'Src Url',
            'src_html' => 'Src Html',
            'views' => 'Views',
            'sort_order' => 'Сортировка',
            'coord_lat' => 'Coord Lat',
            'coord_lng' => 'Coord Lng',
            'seo_title' => 'Seo Title',
            'seo_h1' => 'Seo H1',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'seo_breadcrumb' => 'Seo Breadcrumb',
            'email_for_notifications' => 'Email для отправки сообщений',
            'email_for_notifications_notify' => 'Отправлять сообщения с заказами',
            'address' => 'Адрес',
            'route_explain_auto' => 'Как добраться на машине',
            'route_explain_public' => 'Как добраться на общественном транспорте',
            'master_plan' => 'Генплан',
            'price_for_hundred' => 'Цена за сотку',
            'village_square' => 'Площадь поселка',
        ];
    }

	public function incrementViews()
	{
		//$this->updateCounters(['views' => 1]);
	}

	public function getImage()
	{
		return $this->hasOne(VillageImage::className(), ['village_id' => 'id']);
	}

	public function getImages()
	{
		return $this->hasMany(VillageImage::className(), ['village_id' => 'id'])->orderBy('sort_order');
	}

	public function getProgress_images()
	{
		return $this->hasMany(VillageProgress::className(), ['village_id' => 'id'])->orderBy('sort_order');
	}

	public function getDocuments()
	{
		return $this->hasMany(VillageDocument::className(), ['village_id' => 'id'])->orderBy('sort_order');
	}

	public function getFields($field_id = NULL, $field_group_id = NULL)
	{
//		$cache_key = md5($this->id . serialize($field_id) . serialize($field_group_id));
//		if (!$result = \Yii::$app->cache->get($cache_key)) {
		$query = Field::find();

		if ($field_id !== NULL) {
			$query->where(['id' => $field_id]);
		}

		if ($field_group_id !== NULL) {
			$query->where(['field_group_id' => $field_group_id]);
		}

		$fields = $query->orderBy("sort_order")->all();

		$result = [];

		foreach ($fields as $field) {
			if ($values = $this->fieldValues($field->id)) {

				$values_formated = [];
				foreach ($values as $value) {
					$values_formated[] = $value->getFormated();
				}

				$result[] = [
					'info' => $field,
					'values' => $values,
					'values_formated' => implode(", ", $values_formated)
				];
			}
		}


//
//			$dependency = new \yii\caching\DbDependency();
//			$dependency->sql = 'SELECT MAX(updated_at) FROM village';
//			\Yii::$app->cache->set($cache_key, $result, 0, $dependency);
//		}

		return $result;
	}

	public function fieldValues($field_id)
	{
		return VillageFieldValue::find()->where(['field_id' => $field_id, 'village_id' => $this->id])->all();
	}

	public function getFieldValues()
	{
		return $this->hasMany(VillageFieldValue::className(), ['village_id' => 'id']);
	}

	public function getUrl($section = "view")
	{
		return \Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/village/' . $section, 'id' => $this->id]);
	}

	public function priceFormated()
	{
		if ($this->price_from) {
			$price .= " от <span class='price-value price-value-from'>" . round($this->price_from / 1000000, 1) . "</span>";
		}

		if ($this->price_to) {
			$price .= " до <span class='price-value price-value-to'>" . round($this->price_to / 1000000, 1) . "</span>";
		}

		if ($price) {
			return trim($price . " млн. руб.");
		}
	}

	public function seoPlusBreadcrumbs()
	{
		$return = [];
		if ($seo_params = Seo::getByRoute('village/index')) {
			$return[] = ['label' => $seo_params['breadcrumb'], 'url' => ['village/index']];
		}

		$return[] = ['label' => $this->seoPlusBreadcrumb()];
		return $return;
	}

	public function getReviewsInfo()
	{
		return [
//			'total' => (int) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->count(),
//			'pos' => (int) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>=', 'rating_avg', 4])->count(),
//			'con' => (int) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['<=', 'rating_avg', 2])->count(),
//			'neytral' => (int) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['<', 'rating_avg', 4])->andWhere(['>', 'rating_avg', 2])->count(),
			'total' => 0,
			'pos' => 0,
			'con' => 0,
			'neytral' => 0,
		];
	}

	public function getRatingInfo()
	{
		return [
			'rating_1' => [
				'avg' => (float) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_1', 0])->average('rating_1'),
				'count' => (int) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_1', 0])->count(),
			],
			'rating_2' => [
				'avg' => (float) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_2', 0])->average('rating_2'),
				'count' => (int) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_2', 0])->count(),
			],
			'rating_3' => [
				'avg' => (float) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_3', 0])->average('rating_3'),
				'count' => (int) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_3', 0])->count(),
			],
			'rating_4' => [
				'avg' => (float) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_4', 0])->average('rating_4'),
				'count' => (int) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_4', 0])->count(),
			],
			'rating_5' => [
				'avg' => (float) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_5', 0])->average('rating_5'),
				'count' => (int) VillageReview::find()->where(['village_id' => $this->id, 'status' => VillageReview::STATUS_ENABLED])->andWhere(['>', 'rating_5', 0])->count(),
			],
		];
	}

	public function behaviors()
	{
		return [
			\common\behaviors\TimestampBehavior::className(),
			\common\behaviors\SlugGeneratorBehavior::className(),
			'file-upload' => [
				'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
				'replacePairs' => [
					'<modelId>' => 'id',
				],
				'attributes' => [
					'master_plan' => [
						'group_by_attr' => 'id',
						'group_folder_count' => 1000,
						'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/village/<group_folder>/<modelId>/master_plan.<fileExtension>',
						'url' => '/img/village/<group_folder>/<modelId>/master_plan.<fileExtension>',
						'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
						'saveOptions' => ['quality' => 90],
					]
				],
			],
			'sitemap' => [
				'class' => SitemapBehavior::className(),
				'scope' => function ($model) {
					$model->select(['id', 'updated_at']);
				},
				'dataClosure' => function ($model) {
					/** @var self $model */
					return [
						[
							'loc' => $model->getUrl(),
							'lastmod' => strtotime($model->updated_at),
							'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
							'priority' => 0.8
						]
					];
				}
			],
		];
	}

	public function formatString($template = "", $replaces = [])
	{
		$replaces = array_merge($this->getSeo_plus_autoreplaces(), $replaces);
		return \common\helpers\YText::textGenerator($template, $replaces, ['escape_keys' => TRUE]);
	}

}
