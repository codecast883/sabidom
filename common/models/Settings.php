<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $alias
 * @property string $description
 * @property string $value
 * @property string $type
 */

class Settings extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'settings';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['value'], 'string'],
			[['alias'], 'string', 'max' => 128],
			[['description'], 'string', 'max' => 1024],
			[['type'], 'string', 'max' => 20],
			[['alias'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'alias' => 'alias',
			'description' => 'Описание',
			'value' => 'Значение',
		];
	}
	
	public static function getByAlias($alias){
		$row = Settings::find()->where(['alias' => $alias])->one();
		if($row)
			return $row->value;
		
		return NULL;
	}
}
