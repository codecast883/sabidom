<?php

namespace common\models\parser;

use Yii;

class Generation {

    private $src_url;
    private $dom;

    public static function loadGenerations($brand_id, $model_id) {
        $src_url = "http://catalog.am.ru/int/controls/get-crumb-sub-menu/?object=generation&category=catalog&brand=" . $brand_id . "&model=" . $model_id . "&all=true";
        $src_json = file_get_contents($src_url);
        $src = json_decode($src_json);

        $result = [];
        if (isset($src->data) AND is_array($src->data)) {
            foreach ($src->data as $brand) {
                $result[] = [
                    'id' => $brand->id,
                    'title' => $brand->value,
                    'url' => $brand->link,
                ];
            }
        }

        return $result;
    }

    public function __construct($src_url) {
        $this->src_url = $src_url;
        $this->dom = \Sunra\PhpSimple\HtmlDomParser::file_get_html($this->src_url);
    }

    public function getYears() {
        $result = ['from' => 0, 'to' => 0];
        if ($obj = $this->dom->find(".breadcrumbs li span", 2)) {
            if (preg_match('/(\d\d\d\d{0,1}) - (\d\d\d\d{0,1})/sim', $obj->innertext, $regs)) {
                $result['from'] = $regs[1];
                $result['to'] = $regs[2];
            }
        }

        return $result;
    }

    public function getBodyThumbs() {
        $result = [];
        if ($links = $this->dom->find(".au-preview-box .au-preview-list__link")) {
            foreach ($links as $link){
                if($image = $link->find(".au-preview-list__img", 0)){
                    $result[] = [
                        'href' => $link->href,
                        'img' => $image->src,
                    ];
                }
            }
        }
        
        return $result;
    }
}
