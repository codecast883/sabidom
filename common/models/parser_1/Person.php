<?php

namespace common\models\parser;

use Yii;

class Person {

    private $src_url;
    private $dom;

    public function __construct($src_url) {
        $this->src_url = $src_url;
        $this->dom = \Sunra\PhpSimple\HtmlDomParser::file_get_html($this->src_url);
    }

    public function getLogo() {
        $result = "";
        if ($description_table_obj = $this->dom->find("table table", 10)) {
            if ($obj = $description_table_obj->find("a", 0)) {
                if (preg_match("|/img/spec/.*|isu", $obj->href)) {
                    $result = "http://www.kleos.ru" . $obj->href;
                }
            }
        }

        return $result;
    }

    public function getDescription() {
        $result = "";
        if ($objs = $this->dom->find("td.main div")) {
            foreach ($objs as $obj) {
                if ($obj->style == "clear: both; margin-top: 2em") {
                    $result = $obj->innertext;
                }
            }
        }
        return $result;
    }

    public function getDescriptionImages() {
        $result = [];

        if ($objs = $this->dom->find("td.main div")) {
            foreach ($objs as $obj) {
                if ($obj->style == "clear: both; margin-top: 2em") {
                    if ($images_obj = $obj->find("img")) {
                        foreach ($images_obj as $img_obj) {
                            $result[] = "http://www.kleos.ru" . $img_obj->src;
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function getReviews() {
        $reviews = [];

        if ($divs = $this->dom->find("div")) {
            foreach ($divs as $div) {
                if ($div->style == "width: 90%; margin-top: 2.5em; border-bottom: #fff solid 1px") {
                    preg_match_all('%<b>(.*?)</b>(.*?)<br>.*?<span style="font-fa.*?>(.*?)</span>.*?<p align="right">.*?<b.*?>(.*?)</b>%sim', $div->innertext, $result, PREG_SET_ORDER);
                    for ($matchi = 0; $matchi < count($result); $matchi++) {
                        $review = [
                            'author' => $result[$matchi][1],
                            'datePublished' => trim(mb_convert_encoding($result[$matchi][2], "utf8", "cp1251")),
                            'text' => $result[$matchi][3],
                            'rating' => $result[$matchi][4],
                        ];

                        if(preg_match("%.*(\d\d\.\d\d\.\d\d\d\d).*%isu", $review['datePublished'], $matches)){
                            $review['datePublished'] = $matches[1];
                        }

                        if (!preg_match("%Телефоны организации:%isu", $review['author'])) {
                            $reviews[] = $review;
                        }
                    }
                }
            }
        }

        return $reviews;
    }

}
