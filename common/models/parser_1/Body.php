<?php

namespace common\models\parser;

use Yii;

class Body {

    private $src_url;
    private $dom;

    public static function loadBodys($brand_id, $model_id, $generation_id) {
        $src_url = "http://catalog.am.ru/int/controls/get-crumb-sub-menu/?object=body&category=catalog&brand=" . $brand_id . "&model=" . $model_id . "&generation=" . $generation_id . "&all=true";
        $src_json = file_get_contents($src_url);
        $src = json_decode($src_json);

        $result = [];
        if (isset($src->data) AND is_array($src->data)) {
            foreach ($src->data as $item) {
                $result[] = [
                    'id' => $item->id,
                    'title' => $item->value,
                    'url' => $item->link,
                ];
            }
        }

        return $result;
    }

    public function __construct($src_url) {
        $this->src_url = $src_url;
        $this->dom = \Sunra\PhpSimple\HtmlDomParser::file_get_html($this->src_url);
        if(!$this->dom){
            print $html;
            exit;
        }
    }

    public function getImages() {
        $result = [];
        if ($links = $this->dom->find(".au-rama-thumbs__item .au-rama-thumbs__link")) {
            foreach ($links as $link) {
                if ($image = $link->getAttribute('data-original')) {
                    $result[] = $image;
                }
            }
        }

        return $result;
    }
    
    public function getVideos() {
        $result = [];
        if ($links = $this->dom->find(".au-rama-thumbs__item .js-youtube-btn")) {
            foreach ($links as $link) {
                if ($video = $link->getAttribute('data-iframe')) {
                    $result[] = $video;
                }
            }
        }

        return $result;
    }

}
