<?php

namespace common\models;

use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "information".
 *
 * @property integer $id
 * @property string $title
 * @property string $seo_h1
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $text
 * @property string $slug
 * @property integer $status
 */
class Information extends \yii\db\ActiveRecord {

    const NO = 0;
    const YES = 1;
    public $name;
    public $phone;
    public $text;
    const STATUS_ENABLED = 1;
    const STATUS_DISSABLEd = 0;

    public function __construct($config = array()) {
        parent::__construct($config);
    }

    public function behaviors() {
        return [
            'slug' => [
                'class' => \common\behaviors\SlugGeneratorBehavior::className(),
            ],
            \common\behaviors\TimestampBehavior::className(),
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    $model->select(['id', 'updated_at']);
                    //$model->andWhere(['status' => 1]);
                },
                        'dataClosure' => function ($model) {
                    return [
                        'loc' => $model->url,
                        'lastmod' => strtotime($model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
                    ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'information';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['text'], 'string'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'phone', 'text'], 'required'],
            [['text', 'name'], 'string'],
            [['phone'], 'string', 'max' => 256],
            [['title', 'seo_h1', 'seo_title', 'seo_description', 'seo_keywords', 'slug'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'seo_h1' => 'H1',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'text' => 'Текст',
            'slug' => 'Slug',
            'status' => 'Статус',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',

        ];
    }

    public static function itemAlias($type, $code = NULL) {
        $_items = array(
            'status' => array(
                0 => 'выкл',
                1 => 'вкл',
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function getUrl() {
        return Url::to(["/information/view", 'id' => $this->id]);
    }

}
