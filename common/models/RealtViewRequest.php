<?php

namespace common\models;

use Yii;
use common\helpers\YText;

/**
 * This is the model class for table "view_request".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $text
 * @property string $realt_id
 */
class RealtViewRequest extends \yii\db\ActiveRecord {

    public function behaviors() {
        return [
            \common\behaviors\TimestampBehavior::className(),
        ];
    }

    public function __construct($config = array()) {
        $this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, [$this, 'afterInsertHandler']);
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'realt_view_request';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['realt_id'], 'integer'],
            [['name', 'phone', 'realt_id'], 'required'],
            [['text', 'name'], 'string'],
            [['phone', 'email'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Updated At',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'text' => 'Сообщение',
        ];
    }

    public function afterInsertHandler() {
        $this->sendAdminNotify();
    }

    public function getRealt() {
        return $this->hasOne(Realt::className(), ['id' => 'realt_id']);
    }

    public function sendAdminNotify() {
        $replaces = [
            "{дата}" => $this->created_at,
            "{имя}" => $this->name,
            "{телефон}" => $this->phone,
            "{email}" => $this->email,
            "{text}" => $this->text,
            "{объявление_название}" => $this->realt->title,
            "{объявление_ссылка}" => \yii\helpers\Html::a($this->realt->title, $this->realt->url),
        ];

        if ($email_template = \common\models\EmailTemplate::find()->where(['alias' => "realt_view_request_admin"])->one()) {
            $mail = Yii::$app->mail->compose('simple', ['message' => YText::textGenerator($email_template->body, $replaces)]);
            $mail->setSubject(YText::textGenerator($email_template->theme, $replaces));
            $mail->setFrom($email_template->send_from);
            $mail->setTo(explode(",", Settings::getByAlias('email_for_order')));
            $mail->send();
        }
    }
}
