<?php

namespace common\models;

use Yii;
use common\helpers\YText;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property string $fio
 * @property string $email
 * @property string $message
 */
class Feedback extends \yii\db\ActiveRecord {

    public $captcha;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'feedback';
    }

    public function __construct($config = array()) {
        $this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, [$this, 'afterInsertHandler']);
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'message'], 'required', 'message' => 'Необходимо заполнить поле'],
            [['message', 'email', 'phone'], 'string'],
            [['email'], 'email', 'message' => 'Необходимо ввести валидный Email'],
            [['name', 'email'], 'string', 'max' => 256],
            ['captcha', 'required', 'on' => ['create_from_front']],
            ['captcha', 'captcha', 'on' => ['create_from_front']],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => 'Имя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
            'captcha' => 'Проверочный код',
            'created_at' => 'Дата создания',
        ];
    }

    public function scenarios() {
        return [
            'create_from_front' => ['fio', 'created_at', 'updated_at', 'is_new', 'email', 'name', 'message', 'phone', 'type_id', 'captcha'],
            'default' => ['fio', 'created_at', 'updated_at', 'is_new', 'email', 'name', 'message', 'phone', 'type_id'],
        ];
    }

    public function behaviors() {
        return [
            \common\behaviors\TimestampBehavior::className(),
        ];
    }

    public function afterInsertHandler() {
        $this->sendAdminNotify();
    }

    public function sendAdminNotify() {
        $replaces = [
            "{дата}" => $this->created_at,
            "{имя}" => $this->name,
            "{телефон}" => $this->phone,
            "{email}" => $this->email,
            "{сообщение}" => $this->message,
        ];

        if ($email_template = \common\models\EmailTemplate::find()->where(['alias' => "feedback_admin"])->one()) {
            $mail = Yii::$app->mail->compose('simple', ['message' => YText::textGenerator($email_template->body, $replaces)]);
            $mail->setSubject(YText::textGenerator($email_template->theme, $replaces));
            $mail->setFrom($email_template->send_from);
            $mail->setTo(explode(",", Settings::getByAlias('email_for_feedback')));
            //$mail->setTo("chuzzlik@gmail.com");
            $mail->send();
        }
    }

}
