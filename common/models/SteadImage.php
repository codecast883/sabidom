<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stead_image".
 *
 * @property integer $id
 * @property integer $stead_id
 * @property string $image
 * @property integer $sort_order
 */
class SteadImage extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'stead_image';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['stead_id', 'image'], 'required'],
            [['stead_id', 'sort_order'], 'integer'],
            [['image'], 'file', 'extensions' => ['jpg', 'gif', 'png']],
        ];
    }

    public function behaviors() {
        return [
            'file-upload' => [
                'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
                'replacePairs' => [
                    '<modelId>' => 'id',
                    '<stead_id>' => 'stead_id',
                ],
                'attributes' => [
                    'image' => [
                        'group_by_attr' => 'stead_id',
                        'group_folder_count' => 1000,
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/stead-image/<group_folder>/<stead_id>/gallery/<modelId>.<fileExtension>',
                        'url' => '/img/stead-image/<group_folder>/<stead_id>/gallery/<modelId>.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
                        'saveOptions' => ['quality' => 90],
                        'thumbs' => [
                            'small' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/stead-image/<group_folder>/<stead_id>/gallery/<modelId>-small.<fileExtension>',
                                'url' => '/img/cache/stead-image/<group_folder>/<stead_id>/gallery/<modelId>-small.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 100, 100, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 80,
                                ],
                            ],
                            'middle' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/stead-image/<group_folder>/<stead_id>/gallery/<modelId>-middle.<fileExtension>',
                                'url' => '/img/cache/stead-image/<group_folder>/<stead_id>/gallery/<modelId>-middle.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 400, 300, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 70,
                                ],
                            ],
                            'large' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/stead-image/<group_folder>/<stead_id>/gallery/<modelId>-large.<fileExtension>',
                                'url' => '/img/cache/stead-image/<group_folder>/<stead_id>/gallery/<modelId>-large.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 770, 516, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 80,
                                ],
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'stead_id' => 'Realt ID',
            'image' => 'Image',
            'sort_order' => 'Sort Order',
        ];
    }

}
