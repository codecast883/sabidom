<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VillageTownhouse;

/**
 * VillageTownhouseSearch represents the model behind the search form about `common\models\VillageTownhouse`.
 */
class VillageTownhouseSearch extends VillageTownhouse
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price_from', 'number_of_floors'], 'integer'],
            [['title', 'created_at', 'updated_at', 'description', 'seo_title', 'seo_h1', 'seo_description', 'seo_keywords', 'seo_breadcrumb', 'slug'], 'safe'],
            [['house_area', 'stead_area'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VillageTownhouse::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'price_from' => $this->price_from,
            'house_area' => $this->house_area,
            'stead_area' => $this->stead_area,
            'number_of_floors' => $this->number_of_floors,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_h1', $this->seo_h1])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', 'seo_breadcrumb', $this->seo_breadcrumb])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
