<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "field_group".
 *
 * @property integer $id
 * @property string $title
 * @property string $created_at
 * @property string $updated_at
 */
class FieldGroup extends \yii\db\ActiveRecord {

	public function behaviors() {
		return [
			\common\behaviors\TimestampBehavior::className(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'field_group';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['title'], 'required'],
			[['created_at', 'updated_at'], 'safe'],
			[['title'], 'string', 'max' => 256]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'title' => 'Title',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		];
	}

	public function getFields() {
		return $this->hasMany(Field::className(), array('field_group_id' => 'id'));
	}

}
