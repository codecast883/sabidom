<?php

namespace common\models;
use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;

/**
 * This is the model class for table "village_townhouse".
 *
 * @property integer $id
 * @property string $title
 * @property string $created_at
 * @property string $updated_at
 * @property string $description
 * @property string $seo_title
 * @property string $seo_h1
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_breadcrumb
 * @property string $slug
 * @property integer $price_from
 * @property double $house_area
 * @property double $stead_area
 * @property integer $number_of_floors
 */
class VillageTownhouse extends \yii\db\ActiveRecord {

    public $model_type_id = 4;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'village_townhouse';
    }

    public function __construct($config = array()) {
        $this->on(\yii\db\ActiveRecord::EVENT_AFTER_DELETE, [$this, 'afterDeleteHandler']);

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'village_id'], 'required'],
            [['title', 'description', 'type','phone'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['price_from', 'number_of_floors', 'village_id','price_for_hundred'], 'integer'],
            [['house_area', 'stead_area'], 'number'],
            [['seo_title', 'seo_h1', 'seo_description', 'seo_keywords', 'seo_breadcrumb'], 'string', 'max' => 1024],
            [['slug'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'description' => 'Description',
            'seo_title' => 'Seo Title',
            'seo_h1' => 'Seo H1',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'seo_breadcrumb' => 'Seo Breadcrumb',
            'slug' => 'Slug',
            'price_from' => 'Price From',
            'house_area' => 'House Area',
            'stead_area' => 'Stead Area',
            'number_of_floors' => 'Number Of Floors',
            'type' => 'Тип',
            'phone' => 'Телефон',
            'price_for_hundred' => 'Цена за сотку(тыс.р.)',
        ];
    }

    public function getUrl() {
        return \yii\helpers\Url::to(['/village/townhouse-view', 'id' => $this->id]);
    }

    public function behaviors() {
        return [
            \common\behaviors\TimestampBehavior::className(),
            \common\behaviors\SlugGeneratorBehavior::className(),
            'file-upload' => [
                'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
                'replacePairs' => [
                    '<modelId>' => 'id',
                    '<village_id>' => 'village_id'
                ],
                'attributes' => [
                    'image' => [
                        'group_by_attr' => 'village_id',
                        'group_folder_count' => 1000,
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/village/<group_folder>/<village_id>/townhouse-<modelId>/image.<fileExtension>',
                        'url' => '/img/village/<group_folder>/<village_id>/townhouse-<modelId>/image.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
                        'saveOptions' => ['quality' => 90],
                        'thumbs' => [
                            'small' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/village/<group_folder>/<village_id>/townhouse-<modelId>/image-small.<fileExtension>',
                                'url' => '/img/cache/village/<group_folder>/<village_id>/townhouse-<modelId>/image-small.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 400, 300, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 80,
                                ],
                            ],
                            'middle' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/village/<group_folder>/<village_id>/townhouse-<modelId>/image-middle.<fileExtension>',
                                'url' => '/img/cache/village/<group_folder>/<village_id>/townhouse-<modelId>/image-middle.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 510, 400, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 80,
                                ],
                            ],
                        ],
                    ],
                    '3d' => [
                        'group_by_attr' => 'village_id',
                        'group_folder_count' => 1000,
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/village/<group_folder>/<village_id>/townhouse-<modelId>/3d.<fileExtension>',
                        'url' => '/img/village/<group_folder>/<village_id>/townhouse-<modelId>/3d.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_FILE,
                    ],
                    'layout' => [
                        'group_by_attr' => 'village_id',
                        'group_folder_count' => 1000,
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/village/<group_folder>/<village_id>/townhouse-<modelId>/layout.<fileExtension>',
                        'url' => '/img/village/<group_folder>/<village_id>/townhouse-<modelId>/layout.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
                        'saveOptions' => ['quality' => 90],
                    ],
                ],
            ],
                                        'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    $model->select(['id', 'updated_at']);
                    ///$model->andWhere(['status' => 1]);
                },
                        'dataClosure' => function ($model) {
                    return [
                        'loc' => $model->url,
                        'lastmod' => strtotime($model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
                    ],
        ];
    }

    public function priceFormated() {
        $price = "";
        if ($this->price_from) {
            $price.= " от <span class='price-value price-value-from'>" . round($this->price_from / 1000000, 1) . "</span>";
        }
        if ($price) {
            return trim($price . " млн. руб.");
        }
    }

    public function getVillage() {
        return $this->hasOne(Village::className(), ['id' => 'village_id']);
    }

    public function getSeo_plus_autoreplaces() {
        return ['название проекта' => $this->title];
    }

    public function getImages() {
        return $this->hasMany(VillageTownhouseImage::className(), ['village_townhouse_id' => 'id'])->orderBy('sort_order');
    }

    public function afterDeleteHandler() {
        foreach ($this->images as $image) {
            $image->delete();
        }

        VillageTownhouseFieldValue::deleteAll(['model_id' => $this->id]);
    }

    public function getFields($field_id = NULL, $field_group_id = NULL) {
        $query = Field::find();

        if ($field_id !== NULL) {
            $query->where(['id' => $field_id]);
        }

        if ($field_group_id !== NULL) {
            $query->where(['field_group_id' => $field_group_id]);
        }

        $fields = $query->orderBy("sort_order")->all();

        $result = [];

        foreach ($fields as $field) {
            if ($values = $this->fieldValues($field->id)) {

                $values_formated = [];
                foreach ($values as $value) {
                    $values_formated[] = $value->getFormated();
                }

                $result[] = [
                    'info' => $field,
                    'values' => $values,
                    'values_formated' => implode(", ", $values_formated)
                ];
            }
        }

        return $result;
    }

    public function fieldValues($field_id) {
        return VillageTownhouseFieldValue::find()->where(['field_id' => $field_id, 'model_id' => $this->id])->all();
    }

    public function getFieldValues() {
        return $this->hasMany(VillageTownhouseFieldValue::className(), ['model_id' => 'id']);
    }

}
