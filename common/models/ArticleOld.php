<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property integer $object_id
 * @property integer $complex_id
 * @property integer $lang_id
 * @property integer $is_published
 * @property string $dt_removed
 * @property string $dt
 * @property string $dt_published
 * @property string $title
 * @property string $author
 * @property string $source
 * @property string $summary
 * @property string $content
 * @property string $resource_uuid
 * @property integer $is_sent
 * @property string $dt_sent
 * @property string $meta_keywords
 * @property string $meta_title
 * @property string $meta_description
 */
class ArticleOld extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_old';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'complex_id', 'lang_id', 'is_published', 'is_sent'], 'integer'],
            [['dt_removed', 'dt', 'dt_published', 'dt_sent'], 'safe'],
            [['summary', 'content', 'meta_keywords', 'meta_title', 'meta_description'], 'string'],
            [['title', 'author', 'source'], 'string', 'max' => 255],
            [['resource_uuid'], 'string', 'max' => 36]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'Object ID',
            'complex_id' => 'Complex ID',
            'lang_id' => 'Lang ID',
            'is_published' => 'Is Published',
            'dt_removed' => 'Dt Removed',
            'dt' => 'Dt',
            'dt_published' => 'Dt Published',
            'title' => 'Title',
            'author' => 'Author',
            'source' => 'Source',
            'summary' => 'Summary',
            'content' => 'Content',
            'resource_uuid' => 'Resource Uuid',
            'is_sent' => 'Is Sent',
            'dt_sent' => 'Dt Sent',
            'meta_keywords' => 'Meta Keywords',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
        ];
    }
}
