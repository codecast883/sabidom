<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cottage_image".
 *
 * @property integer $id
 * @property integer $cottage_id
 * @property string $image
 * @property integer $sort_order
 */
class CottageImage extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cottage_image';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['cottage_id', 'image'], 'required'],
            [['cottage_id', 'sort_order'], 'integer'],
            [['image'], 'file', 'extensions' => ['jpg', 'gif', 'png']],
        ];
    }

    public function behaviors() {
        return [
            'file-upload' => [
                'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
                'replacePairs' => [
                    '<modelId>' => 'id',
                    '<cottage_id>' => 'cottage_id',
                ],
                'attributes' => [
                    'image' => [
                        'group_by_attr' => 'cottage_id',
                        'group_folder_count' => 1000,
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cottage-image/<group_folder>/<cottage_id>/gallery/<modelId>.<fileExtension>',
                        'url' => '/img/cottage-image/<group_folder>/<cottage_id>/gallery/<modelId>.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
                        'saveOptions' => ['quality' => 90],
                        'thumbs' => [
                            'small' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/cottage-image/<group_folder>/<cottage_id>/gallery/<modelId>-small.<fileExtension>',
                                'url' => '/img/cache/cottage-image/<group_folder>/<cottage_id>/gallery/<modelId>-small.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 100, 100, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 80,
                                ],
                            ],
                            'middle' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/cottage-image/<group_folder>/<cottage_id>/gallery/<modelId>-middle.<fileExtension>',
                                'url' => '/img/cache/cottage-image/<group_folder>/<cottage_id>/gallery/<modelId>-middle.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 400, 300, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 70,
                                ],
                            ],
                            'large' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/cottage-image/<group_folder>/<cottage_id>/gallery/<modelId>-large.<fileExtension>',
                                'url' => '/img/cache/cottage-image/<group_folder>/<cottage_id>/gallery/<modelId>-large.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 770, 516, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
                                },
                                'saveOptions' => [
                                    'quality' => 80,
                                ],
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'cottage_id' => 'Realt ID',
            'image' => 'Image',
            'sort_order' => 'Sort Order',
        ];
    }

}
