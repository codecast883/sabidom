<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "field".
 *
 * @property integer $id
 * @property string $title
 * @property integer $type_id
 * @property integer $sort_order
 * @property integer $field_group_id
 */
class Field extends \yii\db\ActiveRecord {

    const TYPE_TEXT = 1;
    const TYPE_LIST = 2;
    const TYPE_FROM_TO = 3;
    const TYPE_YES_NO = 4;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'field';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'type_id'], 'required'],
            [['title'], 'string'],
            [['type_id', 'sort_order', 'show_in_preview', 'field_group_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'type_id' => 'Type ID',
            'sort_order' => 'Sort Order',
        ];
    }

    public static function autoGet($title, $type_id = self::TYPE_TEXT, $field_group_id = 1) {
        $title = trim($title);

        if (!$model = self::find()->where(["title" => $title, 'field_group_id' => $field_group_id])->one()) {
            $model = new self;
            $model->title = $title;
            $model->type_id = $type_id;
            $model->field_group_id = $field_group_id;
            if (!$model->save()) {
                print_r($model->getErrors());
                print_r($model);
                exit;
            }
        }

        return $model;
    }

    public static function itemAlias($type, $code = NULL) {
        $_items = array(
                //'status' => array(
                //       self::STATUS_DISABLED => 'выкл',
                //       self::STATUS_ENABLED => 'вкл',
                //),
        );

        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function getFieldTags() {
        return $this->hasMany(FieldTag::className(), ['field_id' => 'id']);
    }

    public static function getDropdown($type_id = NULL, $empty = TRUE) {
        $rows = [];
        if ($empty) {
            $rows[0] = "-выбрать-";
        }
        $query = self::find()->orderBy('title');
        if ($type_id !== NULL) {
            $query->andWhere(['type_id' => $type_id]);
        }

        foreach ($query->asArray()->all() as $model) {
            $rows[$model['id']] = $model['title'];
        }

        return $rows;
    }

}
