<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use himiklab\sitemap\behaviors\SitemapBehavior;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $description_top
 * @property string $description_bottom
 * @property string $product_name_mask
 * @property string $seo_h1
 * @property string $seo_title
 * @property string $seo_breadcrumb
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $image
 * @property string $slug
 * @property string $path_cache
 * @property string $level
 * @property string $kw1
 * @property string $kw2
 * @property string $path_id_cache
 * @property string $filters_count
 */
class Category extends \yii\db\ActiveRecord {

    /**
     * on_header
     * 1 - основное меню
     * 2 - пункт меню "еще"
     */
    const TRIGGER_PARENT_CHANGED = "parent_changed";
    const TYPE_INHERIT = 0;
    const TYPE_VILLAGE = 1;
    const TYPE_COTTAGE = 2;
    const TYPE_STEAD = 3;
    const TYPE_TOWNHOUSE = 4;
    const CACHE_KEY_CATEGORIES_DROPDOWN = "categories_dropdown";

    protected $path_changed = FALSE;
    static $default_content_type_id = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'category';
    }

    public function getSeo_plus_autoreplaces() {
        return ['категория' => $this->title];
    }

    public function behaviors() {
        return [
            \common\behaviors\TimestampBehavior::className(),
            [
                'class' => \common\behaviors\SlugGeneratorBehavior::className(),
                'dependent_fields' => ['parent_id'],
            ],
            #\common\behaviors\SeoPlus::className(),
            'file-upload' => [
                'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/category/<modelId>.<fileExtension>',
                        'url' => '/img/category/<modelId>.<fileExtension>',
                        'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
                        'saveOptions' => ['quality' => 90],
                        'thumbs' => [
                            'middle' => [
                                'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/category/middle/<modelId>.<fileExtension>',
                                'url' => '/img/cache/category/middle/<modelId>.<fileExtension>',
                                'imagine' => function($filename) {
                                    return \mihaildev\imagine\Image::thumbnail($filename, 90, 80, \Imagine\Image\ManipulatorInterface::THUMBNAIL_INSET);
                                    //return \mihaildev\imagine\Image::aligning($filename, 90, 80);
                                },
                                'saveOptions' => ['quality' => 90],
                            ],
                        ]
                    ]
                ]
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    $model->select(['id', 'updated_at']);
                    $model->andWhere(['status' => 1]);
                },
                        'dataClosure' => function ($model) {
                    return [
                        'loc' => $model->url,
                        'lastmod' => strtotime($model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                        'priority' => 0.8
                    ];
                }
                    ],
                ];
            }

            public function __construct($config = array()) {
                $this->on(\yii\db\ActiveRecord::EVENT_BEFORE_INSERT, [$this, 'beforeUpdateHandler']);
                $this->on(\yii\db\ActiveRecord::EVENT_BEFORE_UPDATE, [$this, 'beforeUpdateHandler']);

                $this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, [$this, 'afterUpdateHandler']);
                $this->on(\yii\db\ActiveRecord::EVENT_AFTER_UPDATE, [$this, 'afterUpdateHandler']);

                $this->on(\yii\db\ActiveRecord::EVENT_BEFORE_DELETE, [$this, 'beforeDeleteHandler']);
                $this->on(\yii\db\ActiveRecord::EVENT_AFTER_DELETE, [$this, 'afterDeleteHandler']);

                $this->on(self::TRIGGER_PARENT_CHANGED, [$this, 'parentChangedTrigger']);

                $this->on(\yii\db\ActiveRecord::EVENT_AFTER_FIND, [$this, 'afterFindHandler']);

                parent::__construct($config);
            }

            public function afterFindHandler() {
                
            }

            public function parentChangedTrigger() {
                $this->rebuildPathCache();
            }

            private function rebuildPathCache() {
                $this->updateValue_path_cache();
                $this->updateValue_path_id_cache();

                if ($this->path_changed) {
                    foreach ($this->childrens as $child) {
                        $child->trigger(self::TRIGGER_PARENT_CHANGED);
                    }
                }
            }

            public function afterUpdateHandler() {
                $this->rebuildPathCache();
            }

            public function updateValue_path_cache() {
                $this->path_cache = "";
                if ($this->parent) {
                    $this->path_cache = $this->parent->path_cache;
                }
                $this->path_cache.="/" . $this->slug;

                if ($this->getOldAttribute('path_cache') != $this->path_cache) {
                    $this->path_changed = TRUE;
                    $this->updateAttributes(['path_cache' => $this->path_cache]);
                }
            }

            public function updateValue_path_id_cache() {
                $this->path_id_cache = "";
                if ($this->parent) {
                    $this->path_id_cache = $this->parent->path_id_cache;
                } else {
                    $this->path_id_cache .= "_";
                }

                $this->path_id_cache .=$this->id . "_";

                if ($this->getOldAttribute('path_id_cache') != $this->path_id_cache) {
                    $this->path_changed = TRUE;
                    $this->updateAttributes(['path_id_cache' => $this->path_id_cache]);
                }
            }

            public function beforeUpdateHandler() {
                
            }

            public function beforeDeleteHandler() {
                foreach ($this->childrens as $child) {
                    $child->delete();
                }
            }

            public function afterDeleteHandler() {
                CategoryFilter::deleteAll(['category_id' => $this->id]);
            }

            /**
             * @inheritdoc
             */
            public function rules() {
                return [
                    [['parent_id', 'on_header'], 'integer'],
                    [['title'], 'required'],
                    [['level', 'content_type_id', 'sort_order', 'filters_count'], 'integer'],
                    [['description_top', 'description_bottom', 'kw1', 'kw2', 'product_name_mask'], 'string'],
                    [['title', 'seo_h1', 'seo_title', 'seo_description', 'seo_keywords', 'seo_breadcrumb', 'slug'], 'string', 'max' => 1024],
                    [['path_cache', 'path_id_cache', 'group_title'], 'string'],
                    ['image', 'file', 'extensions' => ['jpg', 'gif', 'png']],
                    [['created_at', 'updated_at'], 'safe'],
                ];
            }

            /**
             * @inheritdoc
             */
            public function attributeLabels() {
                return [
                    'id' => 'ID',
                    'parent_id' => 'ID родительской категоии',
                    'parent_title' => 'Родительская категория',
                    'title' => 'Название',
                    'ratings_id' => 'Отображать рейтинги на странице категории',
                    'description_top' => 'Описание (верх)',
                    'description_bottom' => 'Описание (низ)',
                    'seo_h1' => 'Seo H1',
                    'seo_title' => 'Seo Title',
                    'class_icon' => 'Класс иконки',
                    'seo_description' => 'Seo Description',
                    'seo_keywords' => 'Seo Keywords',
                    'image' => 'Изображение',
                    'slug' => 'Slug',
                    'content_type_id' => 'Тип объявлений',
					'product_name_mask' => 'Маска имени товаров'
                ];
            }

            public function getUrl() {
                return Url::to(["/category/view", "id" => $this->id], TRUE);
            }

            public function getPath() {
                return $this->path_cache;
//        $path = "";
//
//        $parent = $this;
//        while ($parent = $parent->parent) {
//            $path = "/" . $parent->slug . $path;
//        }
//
//        return $path . "/" . $this->slug;
            }

//    public function getTitleWithPath($glue = " ::: ") {
//        $path = [];
//
//        $parent = $this;
//        $path[] = $parent->title;
//        while ($parent = $parent->parent) {
//            $path[] = $parent->title;
//        }
//        $path = array_reverse($path);
//
//        return implode($glue, $path);
//    }

            public function getParent() {
                return $this->hasOne(self::className(), ['id' => 'parent_id'])->from(self::tableName() . ' AS parent');
            }

            public function getChildrens() {
                return $this->hasMany(self::className(), array('parent_id' => 'id'))->orderBy("title");
            }

            public function getFilters() {
                return $this->hasMany(CategoryFilter::className(), array('category_id' => 'id'));
            }

            public function getFullFilterArray() {
                $filter = [];

                if ($this->parent) {
                    $filter = $this->parent->getFullFilterArray();
                }

                foreach ($this->getFilterArray() as $type_id => $field) {
                    foreach ($field as $field_id => $value) {
                        $filter[$type_id][$field_id] = $value;
                    }
                }


                return $filter;
            }

            public function getFilterArray() {
                return CategoryFilter::getFilter($this->id);
            }

            static function findByPath($path) {
                $path = trim($path, "/");
                return Category::find()->where(['path_cache' => "/" . $path])->one();
            }

//    public static function getDropdown() {
//        return yii\helpers\ArrayHelper::map(self::find()->orderBy('title')->all(), 'id', 'title');
//    }
//    
            public function titlePath($delimiter = " ::: ") {
                $path = "";
                if ($this->parent) {
                    $path = $this->parent->titlePath() . $delimiter . $this->title;
                } else {
                    $path = $this->title;
                }

                return $path;
            }

            public static function clearDropdownCache() {
                \Yii::$app->cache->delete(self::CACHE_KEY_CATEGORIES_DROPDOWN);
            }

            public static function categoriesDropdown() {
                if (!$dropdown = \Yii::$app->cache->get(self::CACHE_KEY_CATEGORIES_DROPDOWN)) {
                    $unformated_categories = self::find()->where("parent_id = 0 OR parent_id IS NULL")->orderBy("title")->all();
                    $categories = new \common\components\CategoryFunctions($unformated_categories);
                    $dropdown = $categories->createDropDown(true);

                    \Yii::$app->cache->set(self::CACHE_KEY_CATEGORIES_DROPDOWN, $dropdown, 0);
                }

                return $dropdown;
            }

            public function getContentTypeId() {
                $content_type_id = $this->content_type_id;
                $category = $this;
                while ($content_type_id == self::TYPE_INHERIT AND $category->parent_id > 0) {
                    $category = $category->parent;
                    $content_type_id = $category->content_type_id;
                }

                return $content_type_id ? $content_type_id : self::$default_content_type_id;
            }

            public function getTopLevel() {
                $category = $this;
                while ($category->parent_id > 0) {
                    $category = $category->parent;
                }

                return $category;
            }

            public static function itemAlias($type, $code = NULL) {
                $_items = [
                    'content_type_id' => [
                        self::TYPE_INHERIT => "Наследовать",
                        self::TYPE_VILLAGE => "Поселок",
                        self::TYPE_COTTAGE => "Коттедж",
                        self::TYPE_TOWNHOUSE => "Таунхаус",
                        self::TYPE_STEAD => "Земля",
                    ],
                ];


                if (isset($code))
                    return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
                else
                    return isset($_items[$type]) ? $_items[$type] : false;
            }

            public function seoPlusBreadcrumbs() {

                $breadcrumbs = [];

//        if ($seo_params = Seo::getByRoute(['category/index'], Seo::TYPE_SECTION)) {
//            $breadcrumbs[] = ['label' => $seo_params['breadcrumb'], "url" => ['category/index']];
//        }

                $t_cat = $this->parent;
                $t_breadcrumbs = [];
                while ($t_cat) {
                    $t_breadcrumbs[] = ['label' => $t_cat->seoPlusBreadcrumb(), 'url' => $t_cat->url];
                    $t_cat = $t_cat->parent;
                }

                $breadcrumbs = array_merge($breadcrumbs, array_reverse($t_breadcrumbs));
                $breadcrumbs = array_merge($breadcrumbs, ['label' => $this->seoPlusBreadcrumb()]);

                return $breadcrumbs;
            }

            public function getFrontUrl() {
                return file_get_contents("http://sabidom.ru/get-url?" . http_build_query(['url' => ["category/view", 'id' => $this->id]]));
            }

        }
        