<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "seo".
 *
 * @property integer $id
 * @property string $route
 * @property string $title
 * @property string $help
 * @property integer $sort_order
 * @property integer $type_id
 */
class Seo extends \yii\db\ActiveRecord {

    const TYPE_SECTION = 1;
    const TYPE_PAGE = 2;

    public function behaviors() {
        return [
            \common\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'seo';
    }

    public function getParams() {
        return $this->hasMany(SeoParam::className(), ['seo_id' => 'id'])->orderBy(['sort_order' => SORT_ASC]);
    }

    public function __construct($config = array()) {
        $this->on(\yii\db\ActiveRecord::EVENT_AFTER_DELETE, [$this, 'afterDeleteHandler']);

        parent::__construct($config);
    }

    public function afterDeleteHandler() {
        $this->unlinkAll('params', TRUE);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['route', 'title'], 'required'],
            [['title', 'help'], 'string'],
            [['sort_order', 'type_id'], 'integer'],
            [['route'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'route' => 'Route',
            'title' => 'Title',
            'help' => 'Help',
            'sort_order' => 'Sort Order',
        ];
    }

    public static function getByRoute($route, $type_id = NULL) {
        $cache_key = "seo_route_" . md5($route . $type_id);

        if (!$params = \Yii::$app->cache->get($cache_key)) {
            $query = self::find()->where(['route' => $route]);

            if ($type_id !== NULL) {
                $query->andWhere(['type_id' => $type_id]);
            }

            $params = [];
            if ($model = $query->one()) {
                foreach ($model->params as $param) {
                    $params[$param->field] = $param->template;
                }
            }
            $dependency = new \yii\caching\DbDependency(['sql' => 'SELECT MAX(updated_at) FROM seo_param']);
            \Yii::$app->cache->set($cache_key, $params, 0, $dependency);
        }

        return $params;
    }

    public static function itemAlias($type, $code = NULL) {
        $_items = [
            'type_id' => [
                self::TYPE_SECTION => 'Раздел',
                self::TYPE_PAGE => 'Страница модели',
            ],
        ];


        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

}
