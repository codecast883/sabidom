<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "village_progress".
 *
 * @property integer $id
 * @property integer $village_id
 * @property string $image
 * @property integer $sort_order
 */
class VillageProgress extends \yii\db\ActiveRecord {

	public $image_load_url = NULL; // для одновременного вывода поля загрузки и через url и через файл
	
	public function __construct($config = array()) {
		$this->on(self::EVENT_BEFORE_VALIDATE, [$this, 'beforeValidateHandler'], null, FALSE);
		parent::__construct($config);
	}

	public function beforeValidateHandler() {
		if (!empty($this->image_load_url)) {
			$this->image = $this->image_load_url;
		}
	}

	public static function tableName() {
		return 'village_progress';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['village_id', 'image'], 'required'],
			[['village_id', 'sort_order'], 'integer'],
			[['image'], 'file', 'extensions' => ['jpg', 'gif', 'png']],
			['image_load_url', 'string']
		];
	}

	public function behaviors() {
		return [
			'file-upload' => [
				'class' => \mihaildev\fileupload\FileUploadBehavior::className(),
				'replacePairs' => [
					'<modelId>' => 'id',
					'<village_id>' => 'village_id',
				],
				'attributes' => [
					'image' => [
						'group_by_attr' => 'village_id',
						'group_folder_count' => 1000,
						'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/village/<group_folder>/<village_id>/progress/<modelId>.<fileExtension>',
						'url' => '/img/village/<group_folder>/<village_id>/progress/<modelId>.<fileExtension>',
						'handler' => \mihaildev\fileupload\FileUploadBehavior::HANDLER_IMAGE,
						'saveOptions' => ['quality' => 90],
						'thumbs' => [
							'large' => [
								'path' => $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/img/cache/village/<group_folder>/<village_id>/progress/<modelId>-large.<fileExtension>',
								'url' => '/img/cache/village/<group_folder>/<village_id>/progress/<modelId>-large.<fileExtension>',
								'imagine' => function($filename) {
									return \mihaildev\imagine\Image::thumbnail($filename, 711, 400, \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND);
								},
								'saveOptions' => [
									'quality' => 80,
								],
							],
						],
					],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'village_id' => 'Realt ID',
			'image' => 'Image',
			'sort_order' => 'Sort Order',
		];
	}

}
