<?php

return [
    'timeZone' => "Europe/Moscow",
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii2mod\rbac\components\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'baseUrl' => '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
//			'transport' => [
//				'class' => 'Swift_SmtpTransport',
//				'host' => 'localhost', // e.g. smtp.mandrillapp.com or smtp.gmail.com
//				'username' => '',
//				'password' => '',
//				'port' => '587', // Port 25 is a very common port too
//				'encryption' => 'tls', // It is often used, check your provider or mail server specs
//			],
            'useFileTransport' => false,
        ],
//		'i18n' => [
//			'translations' => [
//				'app*' => [
//					'class' => 'yii\i18n\PhpMessageSource',
//					//'basePath' => '@app/messages',
//					'sourceLanguage' => 'ru-RU',
//					'fileMap' => [
//						'app' => 'app.php',
//						'app/error' => 'error.php',
//					],
//				],
//			],
//		],
    ],
];


