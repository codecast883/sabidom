<?php

namespace common\owerride\yii\grid;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class ActionColumn extends \yii\grid\ActionColumn {

    public $template = '{update} {delete}';
#    public $options = ['class' => 'col-control col-control-2'];
    public $contentOptions = ['class' => 'col-control col-control-2'];

    public function init() {
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = [
                    'class' => 'btn btn-xs btn-primary',
                    'title' => Yii::t('yii', 'Update'),
                ];

                return Html::a('<i class="fa fa-pencil"></i>', $url, $options);
            };
        }

        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $url = Url::toRoute(['delete', 'id' => $model->id, 'return' => array_merge([Yii::$app->controller->action->id], Yii::$app->request->get())]);

                $options = array_merge([
                    'title' => Yii::t('yii', 'Delete'),
                    'class' => 'btn btn-xs btn-default',
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }

        parent::init();
    }

}
