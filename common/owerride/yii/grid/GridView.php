<?php

namespace common\owerride\yii\grid;

class GridView extends \yii\grid\GridView {

    public $tableOptions = ['class' => 'table table-bordered table-hover'];
    public $layout = "{before}\n{summary}\n{items}\n{pager}\n{after}";
    public $checkboxColumn = FALSE;

    public function renderSection($name) {
        switch ($name) {
            case "{errors}":
                return $this->renderErrors();
            case "{before}":
                return $this->renderBefore();
            case "{after}":
                return $this->renderAfter();
            default:
                return parent::renderSection($name);
        }
    }

    public function init() {
        if ($this->checkboxColumn) {
            $checkbox_column_config = [
                'class' => 'yii\grid\CheckboxColumn',
                'options' => ['class' => 'col-checkbox'],
            ];
            array_unshift($this->columns, $checkbox_column_config);
        }

        parent::init();
    }

    public function renderBefore() {
        $html = "";
        if ($this->checkboxColumn) {
            $html = "<form method='post' id ='list-form'>";
        }

        return $html;
    }

    public function renderAfter() {
        $html = "";
        if ($this->checkboxColumn) {
            $html = "</form >";
        }

        return $html;
    }

}
