<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\helpers;

/**
 * File system helper
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alex Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
class FileHelper extends \yii\helpers\FileHelper {

    public static function dirSize($dir) {
        $totalsize = 0;
        if ($dirstream = @opendir($dir)) {
            while (false !== ($filename = readdir($dirstream))) {
                if ($filename != "." && $filename != "..") {
                    if (is_file($dir . "/" . $filename))
                        $totalsize+=filesize($dir . "/" . $filename);

                    if (is_dir($dir . "/" . $filename))
                        $totalsize+=self::dirSize($dir . "/" . $filename);
                }
            }
        }
        closedir($dirstream);
        return $totalsize;
    }
}
