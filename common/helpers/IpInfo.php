<?php

namespace common\helpers;

/**
 * Password helper.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class IpInfo {

    private $ip;
    private $info = [];

    public function __construct($ip) {
        $url = "http://api.sypexgeo.net/json/" . $ip;
        
        if ($answer_json = file_get_contents($url)) {
            if($answer = @json_decode($answer_json) AND $answer->country){
                $this->info['country']['iso'] = $answer->country->iso;
            }
        }
    }
    
    public function getCountryInfo() {
        return isset($this->info['country']) ? $this->info['country'] : NULL;
    }

}
