<?php

namespace common\helpers;

class Html extends \yii\helpers\Html {

    /**
     * разбивает массив на строки с указанным количеством столбцов в строке
     */
    public static function explodeToRows($models, $cols_in_row, $fill_void_cells = FALSE) {
        $result = [];

        $i = 0;
        $line = 0;

        foreach ($models as $model) {
            if (($i % $cols_in_row) == 0) {
                $i = 0;
                $line++;
            }

            $result[$line][] = $model;
            $i++;
        }

        if ($fill_void_cells AND count($result) > 0) {
            while (count($result[$line]) < $cols_in_row) {
                $result[$line][] = NULL;
            }
        }

        return $result;
    }

    /**
     * разбиает массив на указанное количество частей
     */
    public static function explodeToColumns($models, $cols, $fill_void_cells = FALSE) {
        $result = [];

        $i = 0;
        $col = 0;

        $models_in_col = ceil(count($models) / $cols);
        
        foreach ($models as $model) {
            if (($i % $models_in_col) == 0) {
                $i = 0;
                $col++;
            }

            $result[$col][] = $model;
            $i++;
        }

        if ($fill_void_cells AND count($result) > 0) {
            while (count($result[$col]) < $models_in_col) {
                $result[$col][] = NULL;
            }
        }

        return $result;
    }

}
